<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Utilities
{

    function now_date_time()
    {
        $this->CI = & get_instance();
        $this->CI->load->helper('date');
        $now = time();
        $human = unix_to_human($now, TRUE, 'EU');
        return $human;
    }

    function now_date()
    {
        $this->CI = & get_instance();
        $this->CI->load->helper('date');
        $now = time();
        $human = substr(unix_to_human($now, TRUE, 'EU'), 0, 10);
        return $human;
    }
	
	function now_year()
    {
        $this->CI = & get_instance();
        $this->CI->load->helper('date');
        $now = time();
        $human = substr(unix_to_human($now, TRUE, 'EU'), 0, 4);
        return $human;
    }

    function validate_access($modules_assigned = array(), $module_id)
    {
        if ($modules_assigned)
        {
            if (in_array($module_id, $modules_assigned))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
    }

    //Reformat mySQL date format (YYYY-MM-DD HH:MM:SS) to the British Standard format DD/MM/YYYY
    function date_format($datetime=false)
    {
        if ($datetime)
        {
            $dt = date_create($datetime); // shift SQL date into PHP format
            return date_format($dt, 'd/m/Y'); // format and return
        }
        else
        {
            return "never";
        }
    }

    //Reformat mySQL date format (YYYY-MM-DD HH:MM:SS) to the British Standard format DD/MM/YYYY
    function date_time_format($datetime=false)
    {
        if ($datetime)
        {
            $dt = date_create($datetime); // shift SQL date into PHP format
            return date_format($dt, 'd/m/Y H:m:s'); // format and return
        }
        else
        {
            return "never";
        }
    }

    function date_Add($interval, $number, $date)
    {

        $date_time_array = getdate($date);
        $hours = $date_time_array['hours'];
        $minutes = $date_time_array['minutes'];
        $seconds = $date_time_array['seconds'];
        $month = $date_time_array['mon'];
        $day = $date_time_array['mday'];
        $year = $date_time_array['year'];

        switch ($interval)
        {

            case 'yyyy':
                $year+=$number;
                break;
            case 'q':
                $year+= ( $number * 3);
                break;
            case 'm':
                $month+=$number;
                break;
            case 'y':
            case 'd':
            case 'w':
                $day+=$number;
                break;
            case 'ww':
                $day+= ( $number * 7);
                break;
            case 'h':
                $hours+=$number;
                break;
            case 'n':
                $minutes+=$number;
                break;
            case 's':
                $seconds+=$number;
                break;
        }

        $timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
        return $timestamp;
    }

    function elapsed_time($from_stamp = FALSE, $now_stamp = FALSE)
    {
        if ($from_stamp && $now_stamp)
        {
            $from_date_time = $from_stamp;
            $to_date_time = $now_stamp;
            $dateDiff = strtotime($to_date_time) - strtotime($from_date_time);

            $fullHours = floor($dateDiff / 3600);
            $fullMinutes = floor(($dateDiff - $fullHours * 3600) / 60);
            $fullSeconds = floor($dateDiff - $fullHours * 3600 - $fullMinutes * 60);

            //return date("$fullHours:$fullMinutes:$fullSeconds"); 
            return date("$fullHours.$fullMinutes");
        }
    }

    function return_page($store_refered = FALSE)
    {
        $this->CI = & get_instance();
        $this->CI->load->library('user_agent');

        if ($store_refered)
        {
            if ($this->CI->session->userdata('refered_from') == FALSE)
            {
                if ($this->CI->agent->is_referral())
                {
                    $this->CI->session->set_userdata('refered_from', $this->CI->agent->referrer());
                }
            }
            return TRUE;
        }
        else
        {
            if ($this->CI->session->userdata('refered_from') == TRUE)
            {
                $refered_path = $this->CI->session->userdata('refered_from');
                $this->CI->session->unset_userdata('refered_from');
                redirect($refered_path);
            }
        }
    }

    //Converts a given integer (in range [0..1T-1], inclusive) into alphabetical format ("one", "two", etc.)
    function convert_number($number)
    {
        if (($number < 0) || ($number > 999999999))
        {
            throw new Exception("Number is out of range");
        }

        $Gn = floor($number / 1000000);  /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);     /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);      /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);       /* Tens (deca) */
        $n = $number % 10;               /* Ones */

        $res = "";

        if ($Gn)
        {
            $res .= $this->convert_number($Gn) . " Million";
        }

        if ($kn)
        {
            $res .= ( empty($res) ? "" : " ") .
                    $this->convert_number($kn) . " Thousand";
        }

        if ($Hn)
        {
            $res .= ( empty($res) ? "" : " ") .
                    $this->convert_number($Hn) . " Hundred";
        }

        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
            "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
            "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
            "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
            "Seventy", "Eigthy", "Ninety");

        if ($Dn || $n)
        {
            if (!empty($res))
            {
                $res .= " and ";
            }

            if ($Dn < 2)
            {
                $res .= $ones[$Dn * 10 + $n];
            }
            else
            {
                $res .= $tens[$Dn];

                if ($n)
                {
                    $res .= "-" . $ones[$n];
                }
            }
        }

        if (empty($res))
        {
            $res = "zero";
        }

        return $res;
    }

    function reverse_htmlentities($string)
    {
        $string = str_replace('&amp;', '&', $string);
        $string = str_replace('&#039;', '\'', $string);
        $string = str_replace('&quot;', '\"', $string);
        $string = str_replace('&lt;', '<', $string);
        $string = str_replace('&gt;', '>', $string);
        $string = str_replace('&AMP;', '&', $string);
        $string = str_replace('&#039;', '\'', $string);
        $string = str_replace('&QUOT;', '\"', $string);
        $string = str_replace('&LT;', '<', $string);
        $string = str_replace('&GT;', '>', $string);

        return $string;
    }

    function print_r_html($arr)
    {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }
	
	/* Extract array element and assign to variabel (add ro list out come)*/
	function extract_array($array, $index_count)
	{
		$a = $array[0];
		
		for($i = 1; $i < $index_count; $i++){
		
			$a = $a . "*" . $array[$i];
		}
		return $a;
	}
	
	/* Extract array result */
	function extract_part_array($result_array, $db_field)
	{
		foreach($result_array as $pc_data){
		
			return $result = explode("*", $pc_data[$db_field]);
		}
		//return $result;
	}
	
	function get_participant_infor($pc_participant_data_set)
	{	
		foreach ($pc_participant_data_set->result_array() as $arr_participant_data){	
							
				return $arr_participant_data;
		}		
	}
	
	/* Trigger Email */

	function send_email_alert($reciever, $subject, $msg, $from_email, $from_name)
	{
		//$this->$CI = & get_instance();
        $this->$CI->load->library('email');
        $this->$CI->email->set_newline("\r\n");

        $this->$CI->email->from($from_email,$from_name);
        $this->$CI->email->to($reciever);
        $this->$CI->email->cc('');
        $this->$CI->email->bcc('');

        $this->$CI->email->subject($subject);
        $this->$CI->email->message($msg);

        $this->$CI->email->send();

        echo $this->email->print_debugger();
	}
	
	function get_participant_data_utiliti($policy_no, $ren_count, $mpv_data)
	{	
		foreach ($mpv_data->result_array() as $arr_mpv_data){
			if((trim($arr_mpv_data['certNo']) == trim($policy_no)) && (trim($arr_mpv_data['certRenew']) == trim($ren_count))){
				return $arr_mpv_data;
			}
		}		
	}
}

?>
