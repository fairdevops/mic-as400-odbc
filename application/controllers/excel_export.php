<?php

class Excel_export extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url'); /////http://localhost/mic_db/index.php/excel_export/reports_transcation_summery
        $this->load->helper('date');
        $this->load->library('session');
    }

    /* function reports_transcation_summery() {


      ini_set('display_errors', 1);
      ini_set('log_errors', 1);
      error_reporting(E_ALL);
      $this->load->library('pxl');

      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()->setTitle("title")
      ->setDescription("description");

      // Assign cell values
      $objPHPExcel->setActiveSheetIndex(0);

      $Y = 1;

      $this->load->model('excel_model');


      $data=$this->excel_model->print_user_information(16);


      $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Feedback Type'.$data['users_name']);
      $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Opening Balance');
      $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Received');
      $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Closed');
      $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Carried Forward');





      $objPHPExcel->createSheet();
      $sheet = $objPHPExcel->getActiveSheet();


      ///$sheet->getStyle('A1:F1')->getFont()->getColor()->setRGB('0000FF');

      //$sheet->getStyle('B'.($Y + 3))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE)->getColor()->setRGB('000000');
      //$sheet->getStyle('C'.($Y + 3))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE)->getColor()->setRGB('000000');





      $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");
      $report_name = 'Transcation Summery';

      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-M-y') . '.xls"');
      header('Cache-Control: max-age=0');
      $objWriter->save('php://output');
      }

     */

    ////	POLICY NUMBER	PERIOD OF COVER	CUSTOMER NAME	ADDRESS LINE 1	ADDRESS LINE 2	DEBTOR CODE	MARKETING CODE	REMARK
    ///localhost/mic_db/index.php/excel_export/export_to_by_hand_list/002159

    function export_to_by_hand_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 1;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'by_hand_list_mic';
        } else {
            $card_type = 'N';
            $report_name = 'by_hand_list_nexus';
        }



        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];


        $no_records = $this->excel_model->export_excel_by_hand_count($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, 'B', 'Y', $card_type);



        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS LINE 2');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'DEBTOR CODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'MARKETING CODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'REMARK');

        if ($no_records > 0) {


            $rs_data = $this->excel_model->export_excel_by_hand_display($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, 'B', 'Y', $card_type);

            foreach ($rs_data as $row) {

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD2']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICSPNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICMKCD']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICNAME2']);
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        $sheet->getStyle('A1:I1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_list_printing_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $mdate = date("m/d/Y");

        $Y = 3;
        $k = 0;
        $h = 0;
        $non_colombo_count = 0;
        $b = "";


        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'list_printing_list_mic';
        } else {
            $card_type = 'N';
            $report_name = 'list_printing_list_nexus';
        }



        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

		 if ($card_type_p == 2) {
              $colombo_count = $this->excel_model->export_excel_list_print_coolombo($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'Y', $card_type);

              $none_colombo_count = $this->excel_model->export_excel_list_print_coolombo($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'N', $card_type);
          
		  }
		 
		 else
		    {
			
              $colombo_count = $this->excel_model->export_excel_list_print_colombo_speration_branch($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'Y', $card_type);
			 
			  $none_colombo_count = $this->excel_model->export_excel_list_print_colombo_for_head_office($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'N', $card_type);
          
			
			}  

        ///$no_records=1;//$this->excel_model->export_excel_by_hand_count($my_branch_state, $branch_code, $region_code, $zone_code,$user_epf,'B','Y') ;


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', '');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'COLOMBO');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'UNION ASSURANCE PLC');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', $mdate);
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', '');

        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2', 'REGISTER NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2', 'NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('D2', 'POSTAL TOWN');
        $objPHPExcel->getActiveSheet()->SetCellValue('E2', 'AMOUNT');









        ////export_excel_list_print_coolombo_display($branch_state, $branch_code, $region_code, $zone_code,$epf,$bstate,$print_state,$city) 

        if ($colombo_count > 0) {

			 if ($card_type_p == 2) {
                  $rs_data = $this->excel_model->export_excel_list_print_coolombo_display($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'Y', $card_type);
              }
			  else
			  {
			     $rs_data = $this->excel_model->export_excel_list_print_colombo_display_ho_use($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'Y', $card_type);
			  }
            foreach ($rs_data as $row) {

                $Y++;
                $k++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $k);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, str_replace('-', '', $row['PR_MICTOWN']));
            }
        }






        $non_colombo_count = $non_colombo_count + $k + 8;
        $m = $k + 6;
        $p = $k + 7;

        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $m, '');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $m, 'COLOMBO');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $m, 'UNION ASSURANCE PLC');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $m, $mdate);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $m, '');

        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $p, 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $p, 'REGISTER NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $p, 'NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $p, 'POSTAL TOWN');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $p, 'AMOUNT');




        $b = $non_colombo_count;
        
		 if ($card_type_p == 2) {
            
			 $rs_data_nc = $this->excel_model->export_excel_list_print_coolombo_display($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'N', $card_type);
         }
		 
		 else
		 {
		 
		    $rs_data_nc = $this->excel_model->export_excel_list_print_colombo_display_ho_use($my_branch_state, $branch_code, $region_code, $zone_code, $user_epf, '', 'Y', 'N', $card_type);
		 }
		 
        if ($none_colombo_count > 0) {


            foreach ($rs_data_nc as $row_t) {

                $b++;
                $h++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $b, $h);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $b, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $b, $row_t['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $b, str_replace('-', '', $row_t['PR_MICTOWN']));
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }



        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);



        $sheet->getStyle('A1:E1')->getFont()->setBold(true)->setName('Verdana')->setSize(11)->getColor()->setRGB('6F6F6F');
        $sheet->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A2:E2')->getFont()->setBold(true)->setName('Verdana')->setSize(10)->getColor()->setRGB('6F6F6F');
        $sheet->getStyle('A2:E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('A' . $m . ':E' . $m)->getFont()->setBold(true)->setName('Verdana')->setSize(11)->getColor()->setRGB('6F6F6F');
        $sheet->getStyle('A' . ($m + 1) . ':E' . ($m + 1))->getFont()->setBold(true)->setName('Verdana')->setSize(10)->getColor()->setRGB('6F6F6F');


        $sheet->getStyle('A' . $m . ':E' . $m)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A' . ($m + 1) . ':E' . ($m + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_letter_printing_list() {


        ini_set('display_errors', 1);
        $my_title = "";
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 1;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'letter_printing_list_with_out_by_hand_mic';
        } else {
            $card_type = 'N';
            $report_name = 'letter_printing_list_nexus';
        }



        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

         if ($card_type_p == 2) {
            
			 $no_records = $this->excel_model->export_excel_by_all_letters($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
		
		}
		else
		{
		
		    $no_records = $this->excel_model->export_excel_by_all_letters_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);

         }

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'TITLE');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADDRESS LINE 2');

        if ($no_records > 0) {
         
		     if ($card_type_p == 2) {

            $rs_data = $this->excel_model->export_excel_by_all_letters_display($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			
			}
			else
			{
			
			  $rs_data = $this->excel_model->export_excel_by_all_letters_display_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			
			}


            foreach ($rs_data as $row) {
                $title = $row['PR_MICTITL'];
                if (($title == 'M/S') || ($title == '')) {
                    $my_title = "The Manager,";
                } else {
                    $my_title = "";//$title;
                }

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $my_title);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD2']);
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        $sheet->getStyle('A1:E1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
	
	
	function export_letter_printing_list_all_nexus() {


        ini_set('display_errors', 1);
        $my_title = "";
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'letter_printing_list_with_out_by_hand_mic';
        } else {
            $card_type = 'N';
            $report_name = 'letter_printing_list_nexus';
        }



        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->excel_model->export_excel_by_all_letters($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'TITLE');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADDRESS LINE 2');

        if ($no_records > 0) {


            $rs_data = $this->excel_model->export_excel_by_all_letters_for_nexus_only($my_branch_state, $branch_code, $region_code, $zone_code,'N');


            foreach ($rs_data as $row) {
                $title = $row['PR_MICTITL'];
                if (($title == 'M/S') || ($title == '')) {
                    $my_title = "The Manager,";
                } else {
                    $my_title = "";//$title;
                }

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $my_title);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD2']);
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        $sheet->getStyle('A1:E1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_serail_number_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 1;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'serail_number_list_mic';
        } else {
            $card_type = 'N';
            $report_name = 'serail_number_list_nexus';
        }



        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];
		
		  if ($card_type_p == 2) {

        $no_records = $this->excel_model->export_excel_by_all_cards_count($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
	   
	   }
	   else
	   {
	   
	    if ($branch_code == 'A' ){
	        $no_records = $this->excel_model->export_excel_by_all_cards_count_for_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			}
		else
		{	
	     
		 $no_records = $this->excel_model->export_excel_by_all_cards_count_for_branch($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
		 
		 
		 }
	   }



        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SERIAL NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'PERIOD OF COVER');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS LINE 2');

        if ($no_records > 0) {

             if ($card_type_p == 2) {
            $rs_data = $this->excel_model->export_excel_by_all_serail_data($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			
			}
			else
			{ 
			    if ($branch_code == 'A' ){
			  
			        $rs_data = $this->excel_model->export_excel_by_all_serail_data_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			 
			    }
				else
				{
				     $rs_data = $this->excel_model->export_excel_by_all_serail_data_branch($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
				
				}
			}


            foreach ($rs_data as $row) {

                $Y++;
                
				if ( $card_type == 'M' ){

				     $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y,$row['PR_MICREFNO']);
				}
				else
				{
				     $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, "dd".$row['PR_MICREFNO']);
				}
				
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICADD2']);
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);


        $sheet->getStyle('A1:G1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    ////card printing list


    function export_card_printing_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y =1;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'card_printing_list_mic';
        } else {
            $card_type = 'N';
            $report_name = 'card_printing_list_nexus';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];


          if ($card_type_p == 2) {
           
		     $no_records = $this->excel_model->export_excel_by_all_cards_count($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
	   }
	   else
	   {
	        if ($branch_code == 'A' ){
			    $no_records = $this->excel_model->export_excel_by_all_cards_count_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			}
			else
			{
			  $no_records = $this->excel_model->export_excel_by_all_cards_count_branch($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			}	
	   }


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS LINE 2');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'MARKETING CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'TELEPHONE NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'TITLE');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'BASIC PREMIUM');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'REMARK');
			


        if ($no_records > 0) {

               if ($card_type_p == 2) {
			
            $rs_data = $this->excel_model->export_excel_by_all_cards_display_data($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			
			}
			else
			{
			    if ($branch_code == 'A' ){
			   
			    $rs_data = $this->excel_model->export_excel_by_all_cards_display_data_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
				
				}
				else
				{
				
				 $rs_data = $this->excel_model->export_excel_by_all_cards_display_data_branch_view($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
				
				}
			
			
			}


            foreach ($rs_data as $row) {

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['PR_MICTELN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['PR_MICTITL']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $row['PR_MICPRDU']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, $row['PR_MICNAME2']);
				
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_fleet_policy_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);

        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->excel_model->export_excel_all_fleets($my_branch_state, $branch_code, $region_code, $zone_code);



        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER'); //
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS LINE 2');

        if ($no_records > 0) {


            $rs_data = $this->excel_model->export_excel_all_fleets_display($my_branch_state, $branch_code, $region_code, $zone_code);


            foreach ($rs_data as $row) {

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD2']);
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A1:F1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");

        $report_name = 'fleet_policy_list';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_card_printing_points_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'mic_card_printing_points';
        } else {
            $card_type = 'N';
            $report_name = 'nexus_card_printing_points';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->excel_model->export_nexus_points_list($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NERGNO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NEPOLN');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'NEPCOV');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Nename');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADD1');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADD2');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'POSTAL TOWN');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'DEBTOR CODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'UL CODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'NEPRDU');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', '1% AMOUNT');

        if ($no_records > 0) {


            $rs_data = $this->excel_model->export_nexus_points_list_display($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


            foreach ($rs_data as $row) {

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD2']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICTOWN']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICSPNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICMKCD']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['PR_MICPRDU']);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, ($row['PR_MICPRDU'] / 100));
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);


        $sheet->getStyle('A1:K1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function ntb_upload_list() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'mic_ntb_upload_list';
        } else {
            $card_type = 'N';
            $report_name = 'nexus_ntb_upload_list';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->excel_model->export_excel_by_all_cards_count($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);//export_excel_by_all_letters($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NXAPTP');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'NXCRTP');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'NXRPNO');

        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'NXTITE');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'NXFNAM');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'NXLNAM');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'NXGNDR');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'NXMADD');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'NXMCTY');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'NXDADD');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'NXDCTY');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'NXMPNO');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'NXHPNO');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'NXOPNO');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'NXNICN');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'NXDOBT');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'NXEMAL');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'NXAPDA');
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'NXSPNM');
        $objPHPExcel->getActiveSheet()->SetCellValue('T1', 'NXULNX');
        $objPHPExcel->getActiveSheet()->SetCellValue('U1', 'NXCNAM');
        $objPHPExcel->getActiveSheet()->SetCellValue('V1', 'NXVLDT');
        $objPHPExcel->getActiveSheet()->SetCellValue('W1', 'NXEDTE');
        $objPHPExcel->getActiveSheet()->SetCellValue('X1', 'NXPCOV');
        $objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'NXFUR1');
        $objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'NXFUR2');
        $objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'NXFUR3');
        //$objPHPExcel->getActiveSheet()->getStyle('U1:U'.($no_records+6))->getNumberFormat()->setFormatCode('000000000');
        //










		


		
		if ($no_records > 0) {


            $rs_data = $this->excel_model->export_ntb_report_data($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


            foreach ($rs_data as $row) {

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, "Update");
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, "Instant");
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICTITL']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICGNDR']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICADD1'] . "" . $row['PR_MICADD2']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICTOWN']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $row['PR_MICMPNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('N' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('O' . $Y, $row['PR_MICNICN']);
                $objPHPExcel->getActiveSheet()->SetCellValue('P' . $Y, 0);
                $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('R' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('S' . $Y, $row['PR_MICRML3']);
                $objPHPExcel->getActiveSheet()->SetCellValue('T' . $Y, 0);
                $objPHPExcel->getActiveSheet()->SetCellValue('U' . $Y, $row['PR_MICREFNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('V' . $Y, "1Year");
                $objPHPExcel->getActiveSheet()->SetCellValue('W' . $Y, 0);
                $objPHPExcel->getActiveSheet()->SetCellValue('X' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $Y, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $Y, 0);
                $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $Y, "");
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);
        $sheet->getColumnDimension('R')->setAutoSize(true);
        $sheet->getColumnDimension('S')->setAutoSize(true);
        $sheet->getColumnDimension('T')->setAutoSize(true);
        $sheet->getColumnDimension('U')->setAutoSize(true);
        $sheet->getColumnDimension('V')->setAutoSize(true);
        $sheet->getColumnDimension('W')->setAutoSize(true);
        $sheet->getColumnDimension('X')->setAutoSize(true);
        $sheet->getColumnDimension('Y')->setAutoSize(true);
        $sheet->getColumnDimension('Z')->setAutoSize(true);
        $sheet->getColumnDimension('AA')->setAutoSize(true);





        $sheet->getStyle('A1:AA1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
	
    function export_mic_detail_report_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 1;



        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'export_mic_detail_report';
        } else {
            $card_type = 'N';
            $report_name = 'export_nexus_detail_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];
		
		 if ($card_type_p == 2) {

             $no_records = $this->excel_model->export_excel_by_all_cards_count($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
	   
	   }
	   else
	   {
	         if ($branch_code == "A") {
	             $no_records = $this->excel_model->export_excel_by_all_cards_count_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			}
			else
			{
			  $no_records = $this->excel_model->export_excel_by_all_cards_count_branch($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			
			}	 
	   
	   }


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');
		$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'SENT ON RETURN DATE');
		$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'REASON');
			


        if ($no_records > 0) {

            if ($card_type_p == 2) {
			 
            $rs_data = $this->excel_model->export_mic_detail_report_data_display($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			
			}
			else
			{
			   if ($branch_code == "A") {
			   
			       $rs_data = $this->excel_model->export_mic_detail_report_data_display_ho_level($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
				   
				}
				else
				{
				  $rs_data = $this->excel_model->export_mic_detail_report_data_display_branch_split_level($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
				
				}   
			
			}


            foreach ($rs_data as $row) {
			 
			    if ($row['PR_MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['PR_MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['PR_MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['PR_MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
	
	
	
	
	
    function export_mic_daily_detail_report_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 1;

        $card_type_p=1;

        $this->load->model('excel_model');

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'export_mic_daily_all_printed_card_detail_report';
        } else {
            $card_type = 'N';
            $report_name = 'none';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];
		
	   
	     $no_records = $this->excel_model->export_excel_by_all_cards_count_ho($my_branch_state, $branch_code, $region_code, $zone_code, $card_type); ///adust
	   


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');
		$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'SENT ON RETURN DATE');
		$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'REASON');
			


        if ($no_records > 0) {

			
			$rs_data = $this->excel_model->export_mic_daily_detail_report_all_cards($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);
			

            foreach ($rs_data as $row) {
			 
			    if ($row['PR_MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['PR_MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['PR_MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['PR_MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

	function export_mic_mannual_report_data(){
	   
	    ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 1;

	    $this->load->model('excel_model');

        $user_epf =  $this->uri->segment(3);
	 
	    $no_records= $this->excel_model-> export_excel_by_all_cards_count_branch_for_mannaul_print($user_epf, 'M');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'CUSTOMER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ADDRESS LINE 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS LINE 2');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'DEBTOR CODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'MARKETING CODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'REMARK');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SERIAL');

        if ($no_records > 0) {


            $rs_data = $this->excel_model->export_excel_by_all_cards_count_branch_for_mannaul_print_details($user_epf, 'M');

            foreach ($rs_data as $row) {

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['PR_MICVENO']); 
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['PR_MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['PR_MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['PR_MICNAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['PR_MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['PR_MICADD2']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['PR_MICSPNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['PR_MICMKCD']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['PR_MICNAME2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['PR_MICREFNO']);
				
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A1:J1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . "Mannual Cards" . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

		
		
	
	}

}

?>