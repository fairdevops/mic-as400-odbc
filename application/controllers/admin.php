<?php

class Admin extends CI_Controller {

    public function __construct() {
        parent :: __construct();

        $this->load->helper(array('url', 'form', 'security', 'date'));
        $this->load->library('session');
        $data['base_url'] = base_url();
    }

    public function index() {

        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            redirect('admin/user_creation');
        } else {

            if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'U')) {


                redirect('user/daily_mic_list');
            } else {


                redirect('member/login');
                $data['message'] = "";
                $data['usr_login'] = 'N';
            }
        }
    }

    public function login() {

        $data['message'] = "";
        $data['usr_login'] = '';
        $access_epf_no = $this->input->post('epf');
        $access_level = $this->input->post('auth');

        $userdata = array(
            'loguser_username' => $access_epf_no,
            'loguser_login' => 'A',
            'loguser_level' => 'A',
            'loguser_first_name' => $access_epf_no,
            'loguser_last_name' => $access_epf_no,
            'loguser_tbl_id' => $access_epf_no
        );

        $this->session->set_userdata($userdata);


        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            //redirect('email_shedule_configure/assign_notifications');
            redirect('admin/user_creation');
        } else {


            redirect('http://link/');
        }
    }

    public function check_logged_in() {
        $access_level = $this->session->userdata('loguser_level');

        if (($this->session->userdata('loguser_username')) && ($access_level != '')) {
            $state = true;
            return $state;
        } else {

            $state = false;
            redirect('http://link/');
            exit();
        }
    }

    public function logout() {

        $user_data = array(
            'loguser_username' => '',
            'loguser_login' => '',
            'loguser_level' => '',
            'loguser_first_name' => '',
            'loguser_first_name' => '',
            'loguser_tbl_id' => ''
        );

        $this->session->unset_userdata($user_data);
        ?>
        <a href="http://localhost/mic_db/login.html">RE LOGIN<a>
                <?php
                ///redirect('member/login');
            }



            function user_creation() {

                $data[''] = "assign_notifications";
                $data['search_type'] = 0;
                $data['seek_data'] = "";
                $this->check_logged_in();

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);

                $total_rows = 0;
                $this->load->model('user_model');
                $this->load->library('pagination');

                $config['base_url'] = base_url() . "index.php/admin/user_creation";
                $config['per_page'] = 5;
                $no_perpage = $config['per_page'];

                $config['full_tag_open'] = "<div class='pagination_new'>";
                $config['full_tag_close'] = "</div>";

                $data['rs_email_type'] = $this->user_model->get_all_active_users($no_perpage);

                $total_rows = $this->user_model->count_users_count();
                $data['email_type_count'] = $this->user_model->count_users_count();

                $config['total_rows'] = $total_rows;
                $data['total_rows'] = $total_rows;

                $this->pagination->initialize($config);

                $this->load->view('main_page/header_view', $data);
                $this->load->view('admin/user_creation_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function view_assign_debtor_name() {

                $data['search_type'] = 0;
                $data['seek_data'] = "";
                $this->check_logged_in();

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);

                $total_rows = 0;
                $this->load->model('admin_model');
                $this->load->library('pagination');

                $config['base_url'] = base_url() . "index.php/admin/view_assign_debtor_name";
                $config['per_page'] = 5;
                $no_perpage = $config['per_page'];

                $config['full_tag_open'] = "<div class='pagination_new'>";
                $config['full_tag_close'] = "</div>";

                $data['rs_email_type'] = $this->admin_model->get_all_active_debtor_codes($no_perpage);

                $total_rows = $this->admin_model->count_active_debtor_rec_count();
                $data['rec_count'] = $this->admin_model->count_active_debtor_rec_count();

                $config['total_rows'] = $total_rows;
                $data['total_rows'] = $total_rows;

                $this->pagination->initialize($config);

                $this->load->view('main_page/header_view', $data);
                $this->load->view('admin/view_assign_debtor_name_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function debtor_name_search() {


                $data['seek_data'] = "";
                $data['search_type'] = 0;
                $this->check_logged_in();

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);

                if ($this->input->post('search_type')) {

                    $search_type = $this->input->post('search_type');
                    $data['search_type'] = $this->input->post('search_type');
                    $seek_data = $this->input->post('seek_data');

                    $userdata_p = array(
                        'my_search_type' => $search_type,
                        'my_seek_data' => $seek_data
                    );

                    $this->session->set_userdata($userdata_p);
                } else {

                    $data['search_type'] = $this->session->userdata('my_search_type');
                    $data['seek_data'] = $this->session->userdata('my_seek_data');
                    $search_type = $this->session->userdata('my_search_type');
                }

                if ($this->input->post('seek_data')) {

                    $seek_data = $this->input->post('seek_data');
                    $data['seek_data'] = $this->input->post('seek_data');
                } else {

                    $seek_data = $this->session->userdata('my_seek_data');
                }
                $total_rows = 0;
                $this->load->model('admin_model');
                $this->load->library('pagination');

                $config['base_url'] = base_url() . "index.php/admin/debtor_name_search";
                $config['per_page'] = 5;
                $no_perpage = $config['per_page'];

                $config['full_tag_open'] = "<div class='pagination_new'>";
                $config['full_tag_close'] = "</div>";

                $data['rs_email_type'] = $this->admin_model->get_all_search_count_debtor_codes($no_perpage, $search_type, $seek_data);

                $total_rows = $this->admin_model->search_count_debtor_codes($search_type, $seek_data);
                $data['rec_count'] = $total_rows;

                $config['total_rows'] = $total_rows;
                $data['total_rows'] = $total_rows;

                $this->pagination->initialize($config);

                $this->load->view('main_page/header_view', $data);
                $this->load->view('admin/view_assign_debtor_name_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function add_new_user() {

                $data[''] = "add_new_notification_types";
                $data['branch'] = "";
                $data['region'] = "";
                $data['zone'] = "";
                $data['rad_check'] = "";
                $data['vonly'] = "A";

                $this->check_logged_in();

                $this->load->model('branch_model');
                $this->load->model('region_model');
                $this->load->model('zone_model');

                $data['rs_branch'] = $this->branch_model->get_branch_codes();
                $data['rs_region'] = $this->region_model->get_region_codes();
                $data['rs_zone'] = $this->zone_model->get_zone_codes();

                $this->load->view('admin/add_new_user', $data);
            }

            function form_p1_q1_send_user_registration_data() {


                $this->load->model('user_model'); ////extract($_POST)

                $this->load->library('form_validation');
                $this->form_validation->set_rules('epf_no', 'EPF Number', 'trim|min_length[6]|required|callback_user_epf_not_exists');
                $this->form_validation->set_rules('user_name', 'Actual Name', 'trim|required|xss_clean');
                $this->form_validation->set_rules('group_name', 'Card Printing Level', 'trim|required|xss_clean');
                $this->form_validation->set_rules('vonly', 'Define roles View Only', 'trim|required|xss_clean');

                if ($this->input->post('group_name') == 'B') {

                    $this->form_validation->set_rules('branch', 'Branch', 'trim|required|xss_clean');
                }

                if ($this->input->post('group_name') == 'R') {

                    $this->form_validation->set_rules('region', 'Region', 'trim|required|xss_clean');
                }

                if ($this->input->post('group_name') == 'Z') {

                    $this->form_validation->set_rules('zone', 'Zone', 'trim|required|xss_clean');
                }


                if ($this->form_validation->run() == FALSE) {

                    $jason_output['my_msg']['error_epf_no'] = form_error('epf_no');
                    $jason_output['my_msg']['error_user_name'] = form_error('user_name');
                    $jason_output['my_msg']['error_group_name'] = form_error('group_name');
                    $jason_output['my_msg']['error_vonly'] = form_error('vonly');


                    if ($this->input->post('group_name') == 'B') {

                        $jason_output['my_msg']['error_branch'] = form_error('branch');
                    }


                    if ($this->input->post('group_name') == 'R') {

                        $jason_output['my_msg']['error_region'] = form_error('region');
                    }

                    if ($this->input->post('group_name') == 'Z') {

                        $jason_output['my_msg']['error_zone'] = form_error('zone');
                    }


                    echo json_encode($jason_output);
                } else {


                    $act_epf_no = $this->input->post('epf_no');
                    $epf_no         = str_replace("T","0",$act_epf_no);
                    $epf_no         = str_replace("F","0",$act_epf_no);

                    $user_name = $this->input->post('user_name');
                    $group_name = $this->input->post('group_name');


                    if ($this->input->post('group_name') == 'B') {

                        $branch_code = $this->input->post('branch');
                    } else {
                        $branch_code = 0;
                    }


                    if ($this->input->post('group_name') == 'R') {

                        $region_code = $this->input->post('region');
                    } else {
                        $region_code = 0;
                    }


                    if ($this->input->post('group_name') == 'Z') {

                        $zone_code = $this->input->post('zone');
                    } else {
                        $zone_code = 0;
                    }



                    $user_level = $this->input->post('user_level');
                    $view_only = $this->input->post('vonly');
                    $ponly = $this->input->post('ponly');
                    $all_reports = $this->input->post('all_reports');
                    $up_fleet_policy = $this->input->post('up_fleet_policy');
                    $search_allow = $this->input->post('ponly');
                    $search_allow = $this->input->post('search_allow');
                    $upload_allow_nexus = $this->input->post('all_nexus');
                    $add_user_epf = $this->session->userdata('loguser_username');
                    $user_level = $this->input->post('user_level');


                    $dbResult = $this->user_model->add_new_user($epf_no, $act_epf_no, $user_name, $group_name, $branch_code, $region_code, $zone_code, $view_only, $ponly, $all_reports, $search_allow, $up_fleet_policy, $upload_allow_nexus, $add_user_epf, $user_level);



                    if ($dbResult > 0) {

                        $jason_output['result'] = $dbResult;
                    } else {

                        $jason_output['result'] = 0;
                    }

                    $jason_output['result'] = $dbResult;


                    echo json_encode($jason_output);
                }
            }

            function user_epf_not_exists($epf_no) {

                $this->form_validation->set_message('user_epf_not_exists', 'Thats %s Exists');
                $this->load->model('user_model');

                if ($this->user_model->user_epf_not_exists($epf_no) == TRUE) {
                    return false;
                } else {
                    return true;
                }
            }

            function user_creation_search() {



                $data[''] = "assign_notifications";
                $data['search_type'] = 0;
                $this->check_logged_in();

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);







                if ($this->input->post('search_type')) {


                    $search_type = $this->input->post('search_type');
                    $data['search_type'] = $this->input->post('search_type');
                    $seek_data = $this->input->post('seek_data');


                    $userdata_us = array(
                        'us_search_type' => $search_type,
                        'us_seek_data' => $seek_data
                    );

                    $this->session->set_userdata($userdata_us);
                } else {

                    $search_type = $this->session->userdata('us_search_type');
                    $data['search_type'] = $this->session->userdata('us_search_type');
                }








                if ($this->input->post('seek_data')) {

                    $seek_data = $this->input->post('seek_data');
                    $data['seek_data'] = $this->input->post('seek_data');
                } else {
                    $seek_data = $this->session->userdata('us_seek_data');
                    $data['seek_data'] = $this->session->userdata('us_seek_data');
                }

                $total_rows = 0;
                $this->load->model('admin_model');
                $this->load->library('pagination');

                $config['base_url'] = base_url() . "index.php/admin/user_creation";
                $config['per_page'] = 5;
                $no_perpage = $config['per_page'];

                $config['full_tag_open'] = "<div class='pagination_new'>";
                $config['full_tag_close'] = "</div>";

                $data['rs_email_type'] = $this->admin_model->get_all_search_count_users($no_perpage, $search_type, $seek_data);

                $total_rows = $this->admin_model->search_count_users($search_type, $seek_data);
                $data['email_type_count'] = $total_rows;

                $config['total_rows'] = $total_rows;
                $data['total_rows'] = $total_rows;

                $this->pagination->initialize($config);

                $this->load->view('main_page/header_view', $data);
                $this->load->view('admin/user_creation_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function edit_user() {

                $this->check_logged_in();
                $id = $this->uri->segment(3);
                $data['update_id'] = $this->uri->segment(3);

                $this->load->model('user_model');
                $this->load->model('branch_model');
                $this->load->model('region_model');
                $this->load->model('zone_model');

                $data['rs_branch'] = $this->branch_model->get_branch_codes();
                $data['rs_region'] = $this->region_model->get_region_codes();
                $data['rs_zone'] = $this->zone_model->get_zone_codes();


                $user_infor = $this->user_model->get_user_information($this->uri->segment(3));

                $data['epf_no'] = $user_infor['epf_no'];
                $data['users_name'] = $user_infor['users_name'];
                $data['brz_state'] = $user_infor['brz_state'];
                $data['db_branch'] = $user_infor['branch'];
                $data['db_region'] = $user_infor['region'];
                $data['db_zone'] = $user_infor['zone'];
                $data['view_only'] = $user_infor['view_only'];
                $data['print_only'] = $user_infor['print_only'];
                $data['reports_allow'] = $user_infor['reports_allow'];
                $data['search_allow'] = $user_infor['search_allow'];
                $data['upload_allow_fleet'] = $user_infor['upload_allow_fleet'];
                $data['upload_allow_nexus'] = $user_infor['upload_allow_nexus'];
                $data['user_level'] = $user_infor['user_level'];


                $data[''] = "add_new_notification_types";





                $this->load->view('admin/edit_user', $data);
            }

            function delete_user($id) {
                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);

                $id = $this->uri->segment(3);
                $this->load->model('user_model');

                $DeleteData = $this->user_model->delete_user($id);

                if ($DeleteData) {

                    redirect('admin/user_creation', 'refresh');
                }
            }

            function assign_debtor_name() {

                $data[''] = "add_new_notification_types";
                $this->check_logged_in();

                $this->load->model('debtor_model');

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div class="validation fielderror" style="width:285px">', '</class>');
                $this->form_validation->set_rules('debtor_name', 'Debtor Name', 'trim|required|callback_debtor_name_not_exists');

                if ($this->form_validation->run() === FALSE) {

                    $data['debtor_name'] = strtoupper($this->input->post('debtor_name'));



                    $this->load->view('admin/add_new_debtor_name_view', $data);
                } else {

                    if ($this->input->post('BtnSubmit') != NULL) {

                        $debtor_name = strtoupper($this->input->post('debtor_name'));


                        $add_debtor_name = $this->debtor_model->add_new_debtor_name($debtor_name, $this->session->userdata('loguser_username'));

                        if ($add_debtor_name) {

                            echo "dddd";
                            //$this->load->view('main_page/header_view', $data);
                            $this->load->view('admin/add_new_debtor_name_view', $data);
                            //$this->load->view('main_page/footer_view');
                        } else {

                            $this->load->view('main_page/header_view', $data);
                            $this->load->view('admin/add_new_debtor_name_view', $data);
                            $this->load->view('main_page/footer_view');
                        }
                    }
                }
            }

            function debtor_name_not_exists($debtor_name) {

                $this->form_validation->set_message('debtor_name_not_exists', 'Thats %s Exists');
                $this->load->model('debtor_model');

                if ($this->debtor_model->debtor_name_not_exists($debtor_name) == TRUE) {
                    return false;
                } else {
                    return true;
                }
            }

            function form_p2_q2_edit_user_registration_data() {


                $this->load->model('user_model'); ////extract($_POST)
                $this->load->model('admin_model');


                $this->load->library('form_validation');
                $this->form_validation->set_rules('epf_no', 'EPF Number', 'trim|min_length[6]|integer|required');
                $this->form_validation->set_rules('user_name', 'Actual Name', 'trim|required|xss_clean');
                $this->form_validation->set_rules('group_name', 'Card Printing Level', 'trim|required|xss_clean');
                $this->form_validation->set_rules('vonly', 'Define roles View Only', 'trim|required|xss_clean');

                if ($this->input->post('epf_no')) {

                    if (($this->admin_model->check_user_epf_no_to_edit($this->input->post('epf_no'), $this->input->post('hdd_update_id'))) == 'N') {

                        $this->form_validation->set_rules('epf_no', 'EPF Number', 'trim|required|integer|xss_clean|callback_user_epf_not_exists');
                    }
                }


                if ($this->input->post('group_name') == 'B') {

                    $this->form_validation->set_rules('branch', 'Branch', 'trim|required|xss_clean');
                }

                if ($this->input->post('group_name') == 'R') {

                    $this->form_validation->set_rules('region', 'Region', 'trim|required|xss_clean');
                }

                if ($this->input->post('group_name') == 'Z') {

                    $this->form_validation->set_rules('zone', 'Zone', 'trim|required|xss_clean');
                }


                if ($this->form_validation->run() == FALSE) {

                    $jason_output['my_msg']['error_epf_no'] = form_error('epf_no');
                    $jason_output['my_msg']['error_user_name'] = form_error('user_name');
                    $jason_output['my_msg']['error_group_name'] = form_error('group_name');
                    $jason_output['my_msg']['error_vonly'] = form_error('vonly');


                    if ($this->input->post('group_name') == 'B') {

                        $jason_output['my_msg']['error_branch'] = form_error('branch');
                    }


                    if ($this->input->post('group_name') == 'R') {

                        $jason_output['my_msg']['error_region'] = form_error('region');
                    }

                    if ($this->input->post('group_name') == 'Z') {

                        $jason_output['my_msg']['error_zone'] = form_error('zone');
                    }


                    echo json_encode($jason_output);
                } else {


                    $epf_no = $this->input->post('epf_no');
                    $user_name = $this->input->post('user_name');
                    $group_name = $this->input->post('group_name');


                    if ($this->input->post('group_name') == 'B') {

                        $branch_code = $this->input->post('branch');
                    } else {
                        $branch_code = 0;
                    }


                    if ($this->input->post('group_name') == 'R') {

                        $region_code = $this->input->post('region');
                    } else {
                        $region_code = 0;
                    }


                    if ($this->input->post('group_name') == 'Z') {

                        $zone_code = $this->input->post('zone');
                    } else {
                        $zone_code = 0;
                    }



                    $user_level = $this->input->post('user_level');
                    $view_only = $this->input->post('vonly');
                    $ponly = $this->input->post('ponly');
                    $all_reports = $this->input->post('all_reports');
                    $up_fleet_policy = $this->input->post('up_fleet_policy');
                    $search_allow = $this->input->post('ponly');
                    $search_allow = $this->input->post('search_allow');
                    $upload_allow_nexus = $this->input->post('all_nexus');
                    $add_user_epf = $this->session->userdata('loguser_username');
                    $user_level = $this->input->post('user_level');


                    $dbResult = $this->admin_model->update_user_creation_data($epf_no, $user_name, $group_name, $branch_code, $region_code, $zone_code, $view_only, $ponly, $all_reports, $search_allow, $up_fleet_policy, $upload_allow_nexus, $add_user_epf, $user_level, $this->input->post('hdd_update_id'));



                    if ($dbResult > 0) {

                        $jason_output['result'] = $dbResult;
                    } else {

                        $jason_output['result'] = 0;
                    }

                    $jason_output['result'] = $dbResult;


                    echo json_encode($jason_output);
                }
            }

        }
        ?>