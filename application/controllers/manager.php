<?php

class Manager extends CI_Controller {

    public function __construct() {
        parent :: __construct();

        $this->load->helper(array('url', 'form', 'security', 'date'));
        $this->load->library('session');
        $data['base_url'] = base_url();
    }

    public function index() {

        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            redirect('admin/user_creation');
        } else {

            if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'M')) {


                redirect('manager/manager_dashboard');
				
				
            } else {


                redirect('member/login');
                $data['message'] = "";
                $data['usr_login'] = 'N';
            }
        }
    }

    public function login() {

        $data['message'] = "";
        $data['usr_login'] = '';
        $access_epf_no = $this->input->post('epf');
        $access_level = $this->input->post('auth');

        $userdata = array(
            'loguser_username' => $access_epf_no,
            'loguser_login' => 'A',
            'loguser_level' => 'A',
            'loguser_first_name' => $access_epf_no,
            'loguser_last_name' => $access_epf_no,
            'loguser_tbl_id' => $access_epf_no
        );

        $this->session->set_userdata($userdata);


        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            //redirect('email_shedule_configure/assign_notifications'); 
            redirect('admin/user_creation');
        } else {


            redirect('http://link/');
        }
    }

    public function check_logged_in() {

        $access_level = $this->session->userdata('loguser_level');



        if (($this->session->userdata('loguser_username')) && ($access_level == 'M')) {

            $state = true;
            return $state;
        } else {

            $state = false;
            redirect('http://link/');
            exit();
        }
    }

    public function logout() {

        $user_data = array(
            'loguser_username' => '',
            'loguser_login' => '',
            'loguser_level' => '',
            'loguser_first_name' => '',
            'loguser_first_name' => '',
            'loguser_tbl_id' => ''
        );

        $this->session->unset_userdata($user_data);
        ?>
        <a href="http://link/">RE LOGIN<a>
        <?php
        ///redirect('member/login');
    }

    function manager_dashboard() {

        $data[''] = "manager_dashboard";
        $data['search_type'] = 0;
        $data['seek_data'] = "";
        $this->check_logged_in();

        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);

        $total_rows = 0;
        $this->load->model('user_model');
        $this->load->model('excel_model');


        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
        $data['access_epf'] = $access_epf;

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];

        $no_fleet = $this->excel_model->export_excel_all_fleets($branch_state, $branch_code, $region_code, $zone_code);
        $data['no_fleet'] = $no_fleet;

        $no_mic_card = $this->excel_model->export_excel_by_all_letters($branch_state, $branch_code, $region_code, $zone_code, 'M');
        $data['no_mic_card'] = $no_mic_card;

        $no_nexus_card = $this->excel_model->export_excel_by_all_letters($branch_state, $branch_code, $region_code, $zone_code, 'N');
        $data['no_nexus_card'] = $no_nexus_card;

        $no_manual_card = $this->excel_model->export_excel_by_all_letters_for_manual_print($branch_state, $branch_code, $region_code, $zone_code, 'N', 'M');
        $data['no_manual_card'] = $no_manual_card;
		$data['branch_code' ]=$branch_code;





        $this->load->view('main_page/header_view', $data);
        $this->load->view('manager/my_dashboard_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function printer_shedule() {

        $this->check_logged_in();

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $data['access_epf'] = $access_epf;

        $this->load->model('fleet_model');
        $this->load->model('user_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];

        $reports_allow = $access_user_infor['reports_allow'];
        $data['reports_allow'] = $reports_allow;


        $data['shedule_fleet_count'] = $this->fleet_model->get_fleet_print_count($branch_state, $branch_code, $region_code, $zone_code, 'N');


        $data['rec_count'] = "0";
        $data['card_print_state'] = "A";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('manager/printer_shedule_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function authorize_print() {


        $this->check_logged_in();
        $this->load->model('user_model');
        $this->load->model('man_authorize_model');


        $access_epf = $this->session->userdata('loguser_tbl_id');
        $data['access_epf'] = $access_epf;


        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];

        $reports_allow = $access_user_infor['reports_allow'];
        $data['reports_allow'] = $reports_allow;
		
        $auth_count = $this->man_authorize_model->count_authorize_pending_record_count($branch_state, $branch_code, $region_code, $zone_code, 'P');
        $data['auth_count'] = $auth_count;
	//echo $auth_count;
        if ($auth_count > 0) {

            $data['rs_auth_data'] = $this->man_authorize_model->display_authorize_pending_records($branch_state, $branch_code, $region_code, $zone_code, 'P');
			
        }
		

        $this->load->view('main_page/header_view', $data);
        $this->load->view('manager/authorize_print_view', $data);
        $this->load->view('main_page/footer_view');
    }

        function new_endosment_policy() { //daily_mic_list_generate(){
                
				$this->check_logged_in(); /// ABMPC/00490
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('manager/user_endosment_view', $data);
                $this->load->view('main_page/footer_view');
            }


         
	function ajax_new_endosment() {
			
			    
				$this->load->model('endose_model');
				
				$pol_no =trim($this->input->post('txt_search'));
				$search_count=$this->endose_model->get_endosement_data_count($pol_no);
				
				$data['search_count'] = $search_count;
				
				if ($search_count >0 ) {
				        $data['search_listing']= $this->endose_model->get_endosement_data($pol_no);
				}
				
								
			     $this->load->view('manager/ajax_new_endosment_view',$data);
			
			}
			
			
	 function delete_old_data() { //daily_mic_list_generate(){
                               				$this->check_logged_in();
				$this->load->model('user_model');
				$this->load->model('endose_model');
				
				 $itemExt_infor = array();
				 $itemExt_infor2 = array();
				 $itemExt_infor3 = array();
				 
				 $itemExt_infor = explode("-", $_SERVER['REQUEST_URI']);
				 $itemExt_infor_count = count($itemExt_infor);
						
					
						for ($m = 0; $m < ($itemExt_infor_count-1); $m++) {  $itemExt_infor[$m];  }
									
									
						 $itemExt_infor2 = explode("/delete_old_data/", $_SERVER['REQUEST_URI']);
						 $itemExt_infor_count2 = count($itemExt_infor2);
						 
						 
						 for ($b = 0; $b < ($itemExt_infor_count-1); $b++) {  $itemExt_infor2[$b];  } 
				
				  $limit =  $itemExt_infor[1];///$this->uri->segment(5);
				  
			 
						$itemExt_infor3 = explode("-", $itemExt_infor2[1]);
						 $itemExt_infor_count3 = count($itemExt_infor3);
						 
						 
				 for ($b = 0; $b < ($itemExt_infor_count-1); $b++) {  $itemExt_infor3[$b];  } 
				 
				 $policy_no =$itemExt_infor3[0];
				 
				 
				 $cover_period=   $this->endose_model->get_cover_period($policy_no,$limit);
				 
				 $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                $data['access_epf'] = $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

				 
				 
		   $this->endose_model->add_delete_data_from_iis($policy_no,$cover_period, $branch_state, $branch_code, $region_code, $zone_code,$access_epf,$act_name) ;
				
				
				$data['delete_data'] ='<div class="validation sucess" style="width:385px;padding-left:4px;"> Record Successfully Deleted ..!</class>';
				
                $this->load->view('main_page/header_view', $data);
                $this->load->view('manager/delete_old_data_view', $data);
                $this->load->view('main_page/footer_view');
            }		
			
			


      function authorize_for_print() {

			
				$auth_id    = trim($this->input->post('auth_id'));
				$auth_state = trim($this->input->post('auth_state'));
				
				
				$access_epf = $this->session->userdata('loguser_tbl_id');
				$this->load->model('user_model');
				$this->load->model('man_authorize_model');
		
				$access_epf = $this->session->userdata('loguser_tbl_id');
				$access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
		
  			    $act_name = $access_user_infor['users_name'];

                $dbResult =$this->man_authorize_model->update_authorize_card($access_epf,$auth_id,$auth_state,$act_name);


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }





    function daily_mic_list() {

        $this->check_logged_in();
        $data['sort_order'] = "B";

        $this->load->model('card_model');
        $this->load->model('user_model');
        $this->load->model('card_serial_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);



        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];

        $print_only = $access_user_infor['print_only'];
        $data['print_facility'] = $print_only;


        $rec_count = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $zone_code, $zone_code, "");
        $data['rec_count'] = $rec_count;




        $by_hand_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "", "B");
        $data['by_hand_count'] = $by_hand_count;

        $colombo_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "Y", "");
        $data['colombo_list_count'] = $colombo_list_count;

        $outstation_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "N", "");
        $data['outstation_list_count'] = $outstation_list_count;





        $data['search_type'] = 0;
        $data['seek_data'] = "";


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);


        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_list_view.php', $data); //user_dashboard_view
        $this->load->view('main_page/footer_view');
    }


     function card_alteration(){
	 
	 
	    $this->check_logged_in();
        $this->load->model('user_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $data['access_epf'] = $access_epf;


        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];

	

        $this->load->view('main_page/header_view', $data);
        $this->load->view('manager/card_alteration_view', $data);
        $this->load->view('main_page/footer_view');
	 
	 
	 }
	 
	 function ajax_alteration_cards() {
			
			    
				$this->load->model('card_alter_model');
				$this->load->model('user_model');
				//$this->load->model('re_print_model');
				
				/// display_daily_card_data_all_count_at_head_office
				$txt_search =trim($this->input->post('txt_search'));
				$access_epf = $this->session->userdata('loguser_tbl_id');
				////$data['access_epf'] = $access_epf;
		
		
				///echo$access_epf = $this->session->userdata('loguser_tbl_id');
				$access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
		
				$branch_state = $access_user_infor['brz_state'];
				$branch_code = $access_user_infor['branch'];
				$region_code = $access_user_infor['region'];
				$zone_code = $access_user_infor['zone'];
				$act_name = $access_user_infor['users_name'];

				
				$rec_count = $this->card_alter_model->serach_alter_card_count_at_head_office($branch_state, $branch_code, $region_code, $zone_code,$txt_search);
				
				$data['search_count'] = $rec_count;
				
				
			    //if ($serach_count >0 ) {
				  
				       $per_page = 0;
						$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
						$per_page = 12; //how much records you want to show
						$adjacents = 12; //gap between pages after number of adjacents
						$offset = ($page - 1) * $per_page;
						$data['offset_count'] = $offset;
				
				
						$reload = 'ajax_alteration_cards';
						$total_pages = ceil($rec_count / $per_page);
       $data['rec_count'] = $rec_count;
       $data['pages'] = $total_pages;
       $data['no_of_paginations'] = $total_pages;
       $data['per_page'] = $per_page;

				 
				 ///cho "sdsd";paginate_secondz
				 
				  $data['search_listing']= $this->card_alter_model->serach_alter_card_display($branch_state, $branch_code, $region_code, $zone_code,$txt_search,$offset,$per_page);
				   $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
				  ///print_r( $data['search_listing']);
				//}
				
								
			    $this->load->view('manager/ajax_card_view',$data);
			
			}
	  
	 
	 function print_mic_card_basket(){
	 

	 			$this->load->model('user_model');
				$this->load->model('card_infor_model');
				$this->load->model('card_alter_model');
				
				$data['display_my_msg']="";
				
				
				$access_epf = $this->session->userdata('loguser_tbl_id');
				////$data['access_epf'] = $access_epf;
		         $id = $this->uri->segment(3);
				 $data['id']=$id;
		
				$access_epf = $this->session->userdata('loguser_tbl_id');
				$access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
		
				$branch_state = $access_user_infor['brz_state'];
				$branch_code = $access_user_infor['branch'];
				$region_code = $access_user_infor['region'];
				$zone_code = $access_user_infor['zone'];
				$act_name = $access_user_infor['users_name'];
				
			     $card_infor = $this->card_infor_model->get_daily_card_policy_data($id);
                $data['title'] = $card_infor['MICTITL'];
                $data['name'] = $card_infor['MICNAME'];
                $data['address1'] = $card_infor['MICADD1'];
                $data['address2'] = $card_infor['MICADD2'];
                $data['policy_no'] = $card_infor['MICPLNO'];
                $data['vehile_no'] = $card_infor['MICVENO'];
                $data['cover_period'] = $card_infor['MICPCOV'];
                $data['print_date'] = $card_infor['PRINT_DATE'];
                $data['print_count'] = 1;
				
				       ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
 
				 if ($this->input->post('btn_re_confirm_print') != NULL) { //echo "ssss";

                      $post_branch_id   = $this->input->post('branch_id');
			          $hdd_pol_id  = $this->input->post('hdd_pol_id');
               // $re_enter_password = $this->input->post('re_enter_password');
               // $new_password_encript = md5($password);
               // $this->load->model('admin_model');

                    $change_status = $this->card_alter_model->update_buy_hand_basket($access_epf, $act_name,$branch_state, $branch_code, $region_code, $zone_code,$id,$post_branch_id);

                 if ($change_status > 0) {
				
                   $data['display_my_msg'] = '<div class="validation sucess" style="width:285px">Added to the buy hand list</class>';
                } else {
                   $data['display_my_msg'] = '<div class="validation fielderror" style="width:285px">PasswordUpdation Fails</class>';
                 }
				
                 }
				
				


	         $this->load->view('main_page/header_view', $data);
        $this->load->view('manager/man_card_view', $data);
        $this->load->view('main_page/footer_view');

	 
	 }

    /* here * /

      /******** */

    /* pagination class here */

    function paginate($reload, $page, $tpages, $adjacents) {

        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $out = '<div class="pagin green">';

        // previous label

        if ($page == 1) { ///if ($page == 1) 
            $out.= "<span>$prevlabel</span>";
        } else if ($page == 2) {
            $out.= "<a href='javascript:void(0);' onclick='load(1)'>$prevlabel</a>";
        } else {
            $out.= "<a href='javascript:void(0);' onclick='load(" . ($page - 1) . ")'>$prevlabel</a>";
        }

        // first label
        if ($page > ($adjacents + 1)) {
            $out.= "<a href='javascript:void(0);' onclick='load(1)'>1</a>";
        }
        // interval
        if ($page > ($adjacents + 2)) {
            $out.= "...\n";
        }

        // pages

        $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
        $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
        for ($i = $pmin; $i <= $pmax; $i++) {
            if ($i == $page) {
                $out.= "<span class='current'>$i</span>";
            } else if ($i == 1) {
                $out.= "<a href='javascript:void(0);' onclick='load(1)'>$i</a>";
            } else {
                $out.= "<a href='javascript:void(0);' onclick='load(" . $i . ")'>$i</a>";
            }
        }

        // interval

        if ($page < ($tpages - $adjacents - 1)) {
            $out.= "...\n";
        }

        // last

        if ($page < ($tpages - $adjacents)) {
            $out.= "<a href='javascript:void(0);' onclick='load($tpages)'>$tpages</a>";
        }

        // next

        if ($page < $tpages) {
            $out.= "<a href='javascript:void(0);' onclick='load(" . ($page + 1) . ")'>$nextlabel</a>";
        } // else {
        //$out.= "<span>$nextlabel</span>";
        //}
        $out.= "</div>";
        return $out;
    }

    function paginate_second($reload, $page, $tpages, $adjacents) {

        $prevlabel = "&lsaquo; Prev";
        $nextlabel = "Next &rsaquo;";
        $out = '<div class="pagin green">';

        // previous label

        if ($page == 1) { ///if ($page == 1) 
            $out.= "<span>$prevlabel</span>";
        } else if ($page == 2) {
            $out.= "<a href='javascript:void(0);' onclick='load_page(1)'>$prevlabel</a>";
        } else {
            $out.= "<a href='javascript:void(0);' onclick='load_page(" . ($page - 1) . ")'>$prevlabel</a>";
        }

        // first label
        if ($page > ($adjacents + 1)) {
            $out.= "<a href='javascript:void(0);' onclick='load_page(1)'>1</a>";
        }
        // interval
        if ($page > ($adjacents + 2)) {
            $out.= "...\n";
        }

        // pages

        $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
        $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
        for ($i = $pmin; $i <= $pmax; $i++) {
            if ($i == $page) {
                $out.= "<span class='current'>$i</span>";
            } else if ($i == 1) {
                $out.= "<a href='javascript:void(0);' onclick='load_page(1)'>$i</a>";
            } else {
                $out.= "<a href='javascript:void(0);' onclick='load_page(" . $i . ")'>$i</a>";
            }
        }

        // interval

        if ($page < ($tpages - $adjacents - 1)) {
            $out.= "...\n";
        }

        // last

        if ($page < ($tpages - $adjacents)) {
            $out.= "<a href='javascript:void(0);' onclick='load_page($tpages)'>$tpages</a>";
        }

        // next

        if ($page < $tpages) {
            $out.= "<a href='javascript:void(0);' onclick='load_page(" . ($page + 1) . ")'>$nextlabel</a>";
        } // else {
        //$out.= "<span>$nextlabel</span>";
        //}
        $out.= "</div>";
        return $out;
    }

    /* end of pagination */

    function fiter_numbers_only($_input) {
        $_output = preg_replace('/\D/', '', $_input);
        return $_output;
        unset($_input, $_output);
    }

    function daily_card_search_new() {


        $this->load->model('user_model');
        $this->load->model('card_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];

        if ($this->input->post('group_name')) {
            $data['sort_order'] = $this->input->post('group_name');
            $mys_sort = $this->input->post('group_name');
        } else {
            $data['sort_order'] = "B";
            $mys_sort = "B";
        }

        $data['sort_order'] = $mys_sort;



        //$data['rs_daily_print']   = $this->card_model->display_daily_card_data($branch_state,$branch_code,$region_code,$zone_code);
        // $total_rows               = $this->card_model->display_daily_card_data_count($branch_state,$branch_code,$zone_code,$zone_code);


        $per_page = 0;
        $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $per_page = 16; //how much records you want to show
        $adjacents = 16; //gap between pages after number of adjacents
        $offset = ($page - 1) * $per_page;
        $data['offset_count'] = $offset;


        $reload = 'daily_card_search_new';

        if ($this->input->post('group_name') == 'A') {

            $rec_count = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $region_code, $zone_code);
        } else {
            if ($this->input->post('group_name') == 'C') {
                $city = "Y";
                $by_hand = "";
            } else {

                if ($this->input->post('group_name') == 'O') {

                    $city = "N";
                    $by_hand = "";
                } else {

                    $city = "";
                    $by_hand = 'B';
                }
            }

            $rec_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand);
        }

        $total_pages = ceil($rec_count / $per_page);
        $data['rec_count'] = $rec_count;
        $data['pages'] = $total_pages;
        $data['no_of_paginations'] = $total_pages;
        $data['per_page'] = $per_page;


        if ($rec_count > 0) {


            if ($this->input->post('group_name') == 'A') {


                $data['rs_listing'] = $this->card_model->display_daily_card_data($branch_state, $branch_code, $region_code, $zone_code, $offset, $per_page);
                $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
            } else {

                $data['rs_listing'] = $this->card_model->display_daily_card_data_count_with_filter_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $offset, $per_page);
                $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
            }
        } else {
            $data['dsp_err'] = '<div class="validation fielderror" style="width:578px;">No Records Found ..!</div>';
        }

        $this->load->view('user/jx_daily_mic_list_view.php', $data); ///ajax_daily_list_view', $data);
    }

    function card_serail() {


        //$data['card_type'] = "M";
        $this->check_logged_in();

        $this->load->model('card_model');

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="validation fielderror" style="width:285px">', '</class>');
        $this->form_validation->set_rules('card_type', 'Card Type', 'trim|required');
        //$this->form_validation->set_rules('start_serail', 'Starting Serial', 'trim|required|callback_debtor_name_not_exists');
        //$this->form_validation->set_rules('end_serail',   'Ending Serial',   'trim|required|callback_debtor_name_not_exists');

        if ($this->input->post('card_type') == 'M') {

            $this->form_validation->set_rules('start_serail', 'Starting Serial', 'min_length[13]|trim|required|integer');
            $this->form_validation->set_rules('end_serail', 'Starting Serial', 'min_length[13]|trim|required|integer');
        } else {
            $this->form_validation->set_rules('start_serail', 'Starting Serial', 'min_length[16]');
            $this->form_validation->set_rules('end_serail', 'Starting Serial', 'min_length[16]');
        }



        if ($this->form_validation->run() === FALSE) {

            $data['start_serail'] = $this->input->post('start_serail');
            $data['end_serail'] = $this->input->post('end_serail');

            if ($this->input->post('card_type')) {
                $data['card_type'] = $this->input->post('card_type');
            } else {
                $data['card_type'] = "M";
            }



            $this->load->view('user/card_serail_view', $data);
        } else {

            if ($this->input->post('BtnSubmit') != NULL) {

                $data['start_serail'] = $this->input->post('start_serail');
                $data['end_serail'] = $this->input->post('end_serail');
                $data['card_type'] = $this->input->post('card_type');



                $add_serial = $this->card_model->add_serial($this->input->post('card_type'), $this->input->post('start_serail'), $this->input->post('end_serail'), $this->session->userdata('loguser_username'));

                if ($add_serial) {

                    echo "ddd";
                    //$this->load->view('main_page/header_view', $data);
                    $this->load->view('user/card_serail_view', $data);
                    // redirect('admin/ad_user_sucess', 'refresh');
                    //$this->load->view('main_page/footer_view', $data);
                } else {

                    //$this->load->view('main_page/header_view', $data);
                    $this->load->view('user/card_serail_view', $data);
                    //$this->load->view('main_page/footer_view', $data);
                }
            }
        }
    }

    /* function printer_spool(){

      $this->check_logged_in();
      $this->load->model('user_model');
      $this->load->model('card_model');

      $access_epf = $this->session->userdata('loguser_tbl_id');
      $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

      $branch_state = $access_user_infor['brz_state'];
      $branch_code  = $access_user_infor['branch'];
      $region_code  = $access_user_infor['region'];
      $zone_code    = $access_user_infor['zone'];

      echo $rec_count   = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $zone_code, $zone_code);

      //echo $test = $this->user_model->test($branch_state, $branch_code, $zone_code, $zone_code);
      //echo $data['rs_printer_spool']=$this->card_model->get_daily_card_data_to_printer($branch_state, $branch_code, $zone_code, $zone_code);
      //$data['rs_printer_spool']   =  $this->card_model->display_daily_card_data($branch_state, $branch_code, $region_code, $zone_code, $offset, $per_page);


      }
     */ ///daily_mic_by_hand_list

    function daily_mic_by_hand_list() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "I";
        $data['pg_transfer'] = "daily_mic_by_hand_list_serial";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_by_hand_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_mic_by_hand_list_serial() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "P";
        $data['pg_transfer'] = "daily_mic_by_hand_list_serial";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_by_hand_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_mic_by_colombo_list() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "I";
        $data['pg_transfer'] = "daily_mic_by_colombo_list_serial";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_colombo_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_mic_by_colombo_list_serial() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "P";
        $data['pg_transfer'] = "daily_mic_by_colombo_list_serial";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_colombo_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_mic_by_outstation_list() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "I";
        $data['pg_transfer'] = "daily_mic_by_outstation_list_serial";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_by_outstation_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_mic_by_outstation_list_serial() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "P";
        $data['pg_transfer'] = "daily_mic_by_outstation_list_serial";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/daily_mic_by_outstation_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_pinter_buy_hand_list() {


        $this->load->model('user_model');
        $this->load->model('card_printer_send_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];

        if ($this->input->post('printed_group_name') == "A") {

            $print_state = "N";
        } else {
            if ($this->input->post('printed_group_name') == "P") {

                $print_state = "Y";
            } else {
                $print_state = "N";
            }
        }



        $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, 'M');
        $data['print_count'] = $rec_count;

        $per_page = 0;
        $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $per_page = 16; //how much records you want to show
        $adjacents = 16; //gap between pages after number of adjacents
        $offset = ($page - 1) * $per_page;
        $data['offset_count'] = $offset;


        $reload = 'daily_pinter_buy_hand_list';



        if ($this->input->post('printed_group_name')) {
            $data['card_print_state'] = $this->input->post('printed_group_name');
            $card_print_state = $this->input->post('printed_group_name');
        } else {
            $data['card_print_state'] = "A";
            $card_print_state = "A";
        }

        $data['card_print_state'] = $card_print_state;








        $total_pages = ceil($rec_count / $per_page);
        $data['rec_count'] = $rec_count;
        $data['pages'] = $total_pages;
        $data['no_of_paginations'] = $total_pages;
        $data['per_page'] = $per_page;

        if ($rec_count > 0) {

            $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, $offset, $per_page, 'M');
            $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
        }



        $this->load->view('user/ajax_daily_print_spool_view', $data);
    }

    function daily_pinter_colombo_data_list() {


        $this->load->model('user_model');
        $this->load->model('card_printer_send_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];

        if ($this->input->post('printed_group_name') == "A") {

            $print_state = "N";
        } else {
            if ($this->input->post('printed_group_name') == "P") {

                $print_state = "Y";
            } else {
                $print_state = "N";
            }
        }



        $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', 'M');
        $data['print_count'] = $rec_count;

        $per_page = 0;
        $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $per_page = 16; //how much records you want to show
        $adjacents = 16; //gap between pages after number of adjacents
        $offset = ($page - 1) * $per_page;
        $data['offset_count'] = $offset;


        $reload = 'daily_pinter_buy_hand_list';



        if ($this->input->post('printed_group_name')) {
            $data['card_print_state'] = $this->input->post('printed_group_name');
            $card_print_state = $this->input->post('printed_group_name');
        } else {
            $data['card_print_state'] = "A";
            $card_print_state = "A";
        }

        $data['card_print_state'] = $card_print_state;








        $total_pages = ceil($rec_count / $per_page);
        $data['rec_count'] = $rec_count;
        $data['pages'] = $total_pages;
        $data['no_of_paginations'] = $total_pages;
        $data['per_page'] = $per_page;

        if ($rec_count > 0) {

            $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', $offset, $per_page, 'M');
            $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
        }



        $this->load->view('user/ajax_daily_print_spool_view', $data);
    }

    function daily_pinter_outstation_data_list() {


        $this->load->model('user_model');
        $this->load->model('card_printer_send_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];

        if ($this->input->post('printed_group_name') == "A") {

            $print_state = "N";
        } else {
            if ($this->input->post('printed_group_name') == "P") {

                $print_state = "Y";
            } else {
                $print_state = "N";
            }
        }



        $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', 'M');
        $data['print_count'] = $rec_count;

        $per_page = 0;
        $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $per_page = 16; //how much records you want to show
        $adjacents = 16; //gap between pages after number of adjacents
        $offset = ($page - 1) * $per_page;
        $data['offset_count'] = $offset;


        $reload = 'daily_pinter_outstation_data_list'; //daily_pinter_buy_hand_list';



        if ($this->input->post('printed_group_name')) {
            $data['card_print_state'] = $this->input->post('printed_group_name');
            $card_print_state = $this->input->post('printed_group_name');
        } else {
            $data['card_print_state'] = "A";
            $card_print_state = "A";
        }

        $data['card_print_state'] = $card_print_state;








        $total_pages = ceil($rec_count / $per_page);
        $data['rec_count'] = $rec_count;
        $data['pages'] = $total_pages;
        $data['no_of_paginations'] = $total_pages;
        $data['per_page'] = $per_page;

        if ($rec_count > 0) {

            $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', $offset, $per_page, 'M');
            $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
        }



        $this->load->view('user/ajax_daily_print_spool_view', $data);
    }

    function send_by_hand_information() {

        $this->load->model('user_model');
        $this->load->model('card_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];

        //$rec_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code,"","B");

        $dbResult = $this->card_model->update_by_hand_printer_list($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "B");


        if ($dbResult > 0) {

            $jason_output['result'] = $dbResult;
        } else {

            $jason_output['result'] = 0;
        }

        $jason_output['result'] = $dbResult;


        echo json_encode($jason_output);
    }

    function send_colombo_information() {

        $this->load->model('user_model');
        $this->load->model('card_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];


        // $rec_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code,"Y","");

        $dbResult = $this->card_model->update_by_hand_printer_list_colombo_none_colombo($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "", "Y");


        if ($dbResult > 0) {

            $jason_output['result'] = $dbResult;
        } else {

            $jason_output['result'] = 0;
        }

        $jason_output['result'] = $dbResult;


        echo json_encode($jason_output);
    }

    function send_out_station_information() {

        $this->load->model('user_model');
        $this->load->model('card_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];


        //$rec_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code,'N','');

        $dbResult = $this->card_model->update_by_hand_printer_list_colombo_none_colombo($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, '', 'N');


        if ($dbResult > 0) {

            $jason_output['result'] = $dbResult;
        } else {

            $jason_output['result'] = 0;
        }

        $jason_output['result'] = $dbResult;


        echo json_encode($jason_output);
    }

    function fleet_printer_policy_list() { //daily_mic_list_generate(){
        $this->check_logged_in();
        $data['rec_count'] = "0";
        $data['card_print_state'] = "I";
        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/fleet_printer_policy_list_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function daily_pinter_fleet_policy_list() {


        $this->load->model('user_model');
        $this->load->model('fleet_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];

        if ($this->input->post('printed_group_name') == "A") {

            $print_state = "N";
        } else {
            if ($this->input->post('printed_group_name') == "P") {

                $print_state = "Y";
            } else {
                $print_state = "N";
            }
        }



        $rec_count = $this->fleet_model->get_fleet_print_count($branch_state, $branch_code, $region_code, $zone_code, $print_state);
        $data['print_count'] = $rec_count;

        $per_page = 0;
        $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
        $per_page = 16; //how much records you want to show
        $adjacents = 16; //gap between pages after number of adjacents
        $offset = ($page - 1) * $per_page;
        $data['offset_count'] = $offset;


        $reload = 'daily_pinter_fleet_policy_list';



        if ($this->input->post('printed_group_name')) {
            $data['card_print_state'] = $this->input->post('printed_group_name');
            $card_print_state = $this->input->post('printed_group_name');
        } else {
            $data['card_print_state'] = "A";
            $card_print_state = "A";
        }

        $data['card_print_state'] = $card_print_state;








        $total_pages = ceil($rec_count / $per_page);
        $data['rec_count'] = $rec_count;
        $data['pages'] = $total_pages;
        $data['no_of_paginations'] = $total_pages;
        $data['per_page'] = $per_page;

        if ($rec_count > 0) {

            $data['rs_listing'] = $this->fleet_model->get_fleet_print_count_display_infor($branch_state, $branch_code, $region_code, $zone_code, $print_state, $offset, $per_page);
            $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
        }



        $this->load->view('user/ajax_daily_fleet_spool_view', $data);
    }

    function serach_records() {

        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);


        if ($this->input->post('txt_search')) {


            $search_txt = trim($this->input->post('txt_search'));
            $data['search_txt'] = $search_txt;

            $this->load->model('search_model');
            $this->load->model('user_model');
            $this->load->model('re_print_model');


            $access_epf = $this->session->userdata('loguser_tbl_id');
            $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

            $branch_state = $access_user_infor['brz_state'];
            $branch_code = $access_user_infor['branch'];
            $region_code = $access_user_infor['region'];
            $zone_code = $access_user_infor['zone'];
            $act_name = $access_user_infor['users_name'];
            $print_only = $access_user_infor['print_only'];
            $data['print_facility'] = $print_only;



            $search_count = $this->search_model->search_card_data_count($branch_state, $branch_code, $region_code, $zone_code, $search_txt);
            $data['search_count'] = $search_count;

            $data['print_count'] = $search_count;

            $per_page = 0;
            $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
            $per_page = 13; //how much records you want to show
            $adjacents = 13; //gap between pages after number of adjacents
            $offset = ($page - 1) * $per_page;
            $data['offset_count'] = $offset;

            $reload = 'serach_records';

            $total_pages = ceil($search_count / $per_page);
            $data['rec_count'] = $search_count;
            $data['pages'] = $total_pages;
            $data['no_of_paginations'] = $total_pages;
            $data['per_page'] = $per_page;


            if ($search_count > 0) {

                $data['search_listing'] = $this->search_model->search_card_data_display_data($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $offset, $per_page);
                $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
            }


            $this->load->view('user/ajax_search_view', $data);
        } else {
            echo '<br><div align=left style=padding-left:251px padding-top:241px ><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
        }
    }

    function search_print_mic_card() {

        $this->check_logged_in();
        $id = $this->uri->segment(3);
        $this->load->model('daily_policy_model');
        $this->load->model('user_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];



        $poliy_infor = $this->daily_policy_model->get_daily_policy_data($branch_state, $branch_code, $region_code, $zone_code, $id);
        $buy_hand_state = $poliy_infor['MICBYHND'];
        $city_state = $poliy_infor['MICCITY'];

        if ($buy_hand_state == 'B') { ///daily_mic_by_hand_list
            if ($this->daily_policy_model->update_by_search_and_proceeds($access_epf, $act_name, $buy_hand_state, $city_state, $id)) {

                redirect('user/daily_mic_by_hand_list');
            }
        } else {

            if ($city_state == 'Y') {


                if ($this->daily_policy_model->update_by_search_and_proceeds($access_epf, $act_name, $buy_hand_state, $city_state, $id)) {

                    redirect('user/daily_mic_by_colombo_list');
                }
            } else {

                if ($this->daily_policy_model->update_by_search_and_proceeds($access_epf, $act_name, $buy_hand_state, $city_state, $id)) {

                    redirect('user/daily_mic_by_outstation_list');
                }
            }
        }
    }

    function search_printed_cards() {

        $this->check_logged_in();
        $data['sort_order'] = "B";

        $this->load->model('card_model');
        $this->load->model('user_model');
        $this->load->model('card_serial_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);


        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];


        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/search_printed_cards_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function jx_search_printed_cards() { //padding-left:25px;
        $this->load->model('user_model');
        $this->load->model('card_printer_send_model');
        $this->load->model('search_model');
        $this->load->model('re_print_model');



        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $print_only = $access_user_infor['print_only'];
        $data['print_facility'] = $print_only;



        if ($this->input->post('seek_data') != NULL) {



            //<div align="right"><div class="my_close_note" > No Records Found in the Printer Spool</div></div>


            $text_search = trim($this->input->post('seek_data'));
            $search_type = trim($this->input->post('search_type'));


            $rec_count = $this->search_model->search_printed_card_count($branch_state, $branch_code, $region_code, $zone_code, $text_search, $search_type);
            $data['print_count'] = $rec_count;

            $per_page = 0;
            $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
            $per_page = 12; //how much records you want to show
            $adjacents = 12; //gap between pages after number of adjacents
            $offset = ($page - 1) * $per_page;
            $data['offset_count'] = $offset;


            $reload = 'jx_search_printed_cards';


            $total_pages = ceil($rec_count / $per_page);
            $data['rec_count'] = $rec_count;
            $data['pages'] = $total_pages;
            $data['no_of_paginations'] = $total_pages;
            $data['per_page'] = $per_page;

            if ($rec_count > 0) {

                $data['rs_listing'] = $this->search_model->search_printed_card_count_display($branch_state, $branch_code, $region_code, $zone_code, $text_search, $search_type, $offset, $per_page);
                $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
            }


            $this->load->view('user/jx_search_printed_cards_view', $data);
        } else {
            echo '<div align=left style=padding-left:241px><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
        }
    }

    function search_re_print_card() {



        $this->check_logged_in();
        $this->load->model('user_model');
        $this->load->model('re_print_model');

        $id = $this->uri->segment(3);
        $data['id'] = $id;
        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];



        $card_infor = $this->re_print_model->get_print_card_data($id);
        $data['title'] = $card_infor['PR_MICTITL'];
        $data['name'] = $card_infor['PR_MICNAME'];
        $data['address1'] = $card_infor['PR_MICADD1'];
        $data['address2'] = $card_infor['PR_MICADD2'];
        $data['policy_no'] = $card_infor['PR_MICPLNO'];
        $data['vehile_no'] = $card_infor['PR_MICVENO'];
        $data['cover_period'] = $card_infor['PR_MICPCOV'];
        $data['print_date'] = $card_infor['PR_PRINT_DATE'];
        $data['print_count'] = $card_infor['PR_COUNT'];


        $data['card_infor'] = $this->re_print_model->display_reason();




        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/search_re_print_card_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function jx_card_duplicate_send() {



        $policy_id = $this->input->post('policy_id');


        $this->load->model('user_model');
        $this->load->model('re_print_model');

        $id = $this->input->post('policy_id');
        $data['id'] = $this->input->post('policy_id');
        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $users_name = $access_user_infor['users_name'];



        //$card_infor           = $this->re_print_model->get_print_card_data($id);
        //$data['title']        = $card_infor['PR_MICTITL'];
        ////$data['name']         = $card_infor['PR_MICNAME'];
        //$data['address1']     = $card_infor['PR_MICADD1'];
        // $data['address2']     = $card_infor['PR_MICADD2'];
        //$data['policy_no']    = $card_infor['PR_MICPLNO'];
        //$data['vehile_no']    = $card_infor['PR_MICVENO'];
        //$data['cover_period'] = $card_infor['PR_MICPCOV'];
        //$data['print_date']   = $card_infor['PR_PRINT_DATE'];











        $reason_id = $this->input->post('search_type');
        $dbResult = $this->re_print_model->add_duplicate_cards($id, $branch_state, $branch_code, $region_code, $zone_code, $access_epf, $users_name, $reason_id);

        if ($dbResult > 0) {

            $jason_output['result'] = $dbResult;
        } else {

            $jason_output['result'] = 0;
        }

        $jason_output['result'] = $dbResult;


        echo json_encode($jason_output);
    }

    function search_confirm_card() {

        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);

        $this->check_logged_in();
        $this->load->model('user_model');
        $this->load->model('re_print_model');
        $this->load->model('daily_policy_model');

        $epf_user = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($epf_user);

        $users_name = $access_user_infor['users_name'];

        $pr_mic_id = $this->uri->segment(3);


        $card_infor = $this->re_print_model->get_print_card_data($pr_mic_id);
        $buy_hand_state = $card_infor['PR_MICBYHND'];
        $city_state = $card_infor['PR_MICCITY'];

        //echo  $this->re_print_model->send_duplicate_card_to_printer($pr_mic_id,$epf_user,$users_name);

        if ($buy_hand_state == 'B') { ///daily_mic_by_hand_list
            if ($this->re_print_model->send_duplicate_card_to_printer($pr_mic_id, $epf_user, $users_name)) {
                //
                redirect('user/daily_mic_by_hand_list');
            }
        } else {

            if ($city_state == 'Y') {


                if ($this->re_print_model->send_duplicate_card_to_printer($pr_mic_id, $epf_user, $users_name)) {

                    redirect('user/daily_mic_by_colombo_list');
                }
            } else {

                if ($this->re_print_model->send_duplicate_card_to_printer($pr_mic_id, $epf_user, $users_name)) {

                    redirect('user/daily_mic_by_outstation_list');
                }
            }
        }
    }

    function fleet_policy_infor() {


        $this->check_logged_in();
        $data[''] = "";

        $this->load->model('user_model');
        $this->load->model('excel_model');


        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
        $data['access_epf'] = $access_epf;

        $branch_state = $access_user_infor['brz_state'];
        $branch_code = $access_user_infor['branch'];
        $region_code = $access_user_infor['region'];
        $zone_code = $access_user_infor['zone'];
        $act_name = $access_user_infor['users_name'];
        $fleet_allow = $access_user_infor['upload_allow_fleet'];

        $no_records = $this->excel_model->export_excel_all_fleets($branch_state, $branch_code, $region_code, $zone_code);
        $data['no_records'] = $no_records;

        $this->load->view('main_page/header_view', $data);
        $this->load->view('user/fleet_policy_view', $data);
        $this->load->view('main_page/footer_view');
    }

    function upload_fleet_policy() {

        $this->check_logged_in();
        $data['upload_msg'] = "";
        $this->load->model('user_model');

        $access_epf = $this->session->userdata('loguser_tbl_id');
        $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
        $data['access_epf'] = $access_epf;
        $fleet_allow = $access_user_infor['upload_allow_fleet'];


        $this->load->view('main_page/header_view', $data);

        if ($fleet_allow == 0) {

            $data['err_msg'] = "<div class='validation fielderror'>You Don't Have Privileges To Upload</div>";

            $this->load->view('user/upload_fleet_policy_error', $data);
        } else {
            $this->load->view('user/upload_fleet_policy_view', array('error' => ' '));
        }
        $this->load->view('main_page/footer_view');
    }

    function do_upload_fleet_policy() {


        $this->check_logged_in();
        error_reporting(0);
        $this->load->model('fleet_model');


        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'xls';
        $config['max_size'] = '13085';


        $data['upload_msg'] = "";

        $name = $_FILES['userfile']['name']; // get file name from form
        $fileNameParts = explode('.', $name); // explode file name to two part
        $fileExtension = end($fileNameParts); // give extension
        $fileExtension = strtolower($fileExtension); // convert to lower case
        $now = time();
        $gmt = local_to_gmt($now);
        $cust_stamp = $gmt;
        $encripted_pic_name = md5($name . $cust_stamp) . '.' . $fileExtension;  // new file name
        $config['file_name'] = substr($encripted_pic_name, 0, 12); //set file name


        $this->load->library('upload', $config);

        $xx = array('upload_data' => $this->upload->data());
        $mimetype = $xx['upload_data']['file_type'];

        //var_dump('Mime: ' . $mimetype);
        //var_dump($_FILES);
        //$this->load->library('Upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());

            $data['upload_msg'] = '<div class="validation fielderror">' . $this->upload->display_errors() . '</div>';

            $this->load->view('main_page/header_view', $data);
            $this->load->view('user/upload_fleet_policy_view', $data);
            $this->load->view('main_page/footer_view');
        } else {
            $data = array('upload_data' => $this->upload->data());

            //print_r($data);

            error_reporting(E_ALL ^ E_NOTICE);


            $FilePath = './uploads/' . substr($encripted_pic_name, 0, 12) . '.' . $fileExtension;




            $parameters = array('file' => $FilePath,
                'store_extended_info' => true,
                'outputEncoding' => '');

            $this->load->library('Spreadsheet_Excel_Reader', $parameters);

            //$data['xls_data'] = $this->spreadsheet_excel_reader->dump(false, false);
            $list_data = $this->spreadsheet_excel_reader->dump(false, false);
            $myrows = $this->spreadsheet_excel_reader->sheets[0]['numRows'];
            $mynumCols = $this->spreadsheet_excel_reader->sheets[0]['numCols'];

            $data['myrows'] = $myrows;

            $this->load->view('main_page/header_view', $data);
            $this->load->view('user/upload_fleet_policy_sucess_view', $data);
            $this->load->view('main_page/footer_view');
        }
    }

    function upload_fleet_policy_submit() {


        $this->load->model('fleet_model');
        $this->load->model('user_model');

        $total = $this->input->post('total');

        for ($x = 1; $x <= $total; $x++) {


            $pol_no = $this->input->post('hdd_pol_no_' . $x);
            $veh_no = $this->input->post('hdd_veh_no_' . $x);
            $period = $this->input->post('hdd_period_' . $x);
            $name = $this->input->post('hdd_name_' . $x);

            $address1 = $this->input->post('hdd_add_1' . $x);
            $address2 = $this->input->post('hdd_add_2' . $x);

            $access_epf = $this->session->userdata('loguser_tbl_id');
            $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

            $branch_state = $access_user_infor['brz_state'];
            $branch_code = $access_user_infor['branch'];
            $region_code = $access_user_infor['region'];
            $zone_code = $access_user_infor['zone'];
            $act_name = $access_user_infor['users_name'];


            $itemExt = explode("TO", $period);
            $count = count($itemExt);
            $string_need = "";
            $data_item = array();

            for ($m = 0; $m < $count; $m++) {

                $data_item[$m] = str_replace("-", "", $itemExt[$m]);


                //$sh_miccmdt  = $data_item[0];
                // $sh_micexdt  = $data_item[1]; 			 
            }

            $sh_miccmdt = $data_item[0];
            $sh_micexdt = $data_item[1];

            $this->fleet_model->add_fleed_to_iis($pol_no, $veh_no, $period, $name, $address1, $address2, $access_epf, $act_name, $branch_state, $branch_code, $region_code, $zone_code, $sh_miccmdt, $sh_micexdt);
        }
        ?>
                <a href="<?php echo base_url() ?>index.php/user/fleet_printer_policy_list">Please click here</a> <?php
            }

            function nexus() {

                $this->check_logged_in();

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $data['access_epf'] = $access_epf;

                $this->load->model('fleet_model');
                $this->load->model('user_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];



                $data['rec_count'] = "0";
                $data['card_print_state'] = "A";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_list() {

                $this->check_logged_in();
                $data['sort_order'] = "A";

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                $this->load->model('card_model');
                $this->load->model('user_model');
                $this->load->model('card_serial_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);


                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $print_only = $access_user_infor['print_only'];
                $data['print_facility'] = $print_only;




                $rec_count = $this->nexus_model->display_daily_card_data_count_by_nexus_level($branch_state, $branch_code, $zone_code, $zone_code, "");
                $data['rec_count'] = $rec_count;


                $by_hand_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, "", "B");
                $data['by_hand_count'] = $by_hand_count;

                $colombo_list_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, "Y", "");
                $data['colombo_list_count'] = $colombo_list_count;

                $outstation_list_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, "N", "");
                $data['outstation_list_count'] = $outstation_list_count;




                $data['search_type'] = 0;
                $data['seek_data'] = "";


                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_card_search() {


                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('group_name')) {
                    $data['sort_order'] = $this->input->post('group_name');
                    $mys_sort = $this->input->post('group_name');
                } else {
                    $data['sort_order'] = "B";
                    $mys_sort = "B";
                }

                $data['sort_order'] = $mys_sort;


                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_nexus_card_search';

                if ($this->input->post('group_name') == 'A') {

                    $rec_count = $this->nexus_model->display_daily_card_data_count_for_nexus($branch_state, $branch_code, $region_code, $zone_code);
                } else {
                    if ($this->input->post('group_name') == 'C') {
                        $city = "Y";
                        $by_hand = "";
                    } else {

                        if ($this->input->post('group_name') == 'O') {

                            $city = "N";
                            $by_hand = "";
                        } else {

                            $city = "";
                            $by_hand = 'B';
                        }
                    }

                    $rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand);
                }

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;


                if ($rec_count > 0) {


                    if ($this->input->post('group_name') == 'A') {


                        $data['rs_listing'] = $this->nexus_model->display_daily_card_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                    } else {

                        $data['rs_listing'] = $this->nexus_model->display_daily_card_data_count_with_filter_display_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                    }
                } else {
                    $data['dsp_err'] = '<div class="validation fielderror" style="width:578px;">No Records Found ..!</div>';
                }

                $this->load->view('user/nexus/ajax_nexus_view', $data);
            }

            function serach_records_for_nexus() {

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                if ($this->input->post('txt_search')) {


                    $search_txt = trim($this->input->post('txt_search'));
                    $data['search_txt'] = $search_txt;

                    //$this->load->model('search_model');
                    $this->load->model('user_model');
                    $this->load->model('search_nexus_model');


                    $access_epf = $this->session->userdata('loguser_tbl_id');
                    $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                    $branch_state = $access_user_infor['brz_state'];
                    $branch_code = $access_user_infor['branch'];
                    $region_code = $access_user_infor['region'];
                    $zone_code = $access_user_infor['zone'];
                    $act_name = $access_user_infor['users_name'];
                    $print_only = $access_user_infor['print_only'];
                    $data['print_facility'] = $print_only;


                    $search_count = $this->search_nexus_model->search_card_data_count_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $search_txt);
                    $data['search_count'] = $search_count;

                    $data['print_count'] = $search_count;

                    $per_page = 0;
                    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                    $per_page = 14; //how much records you want to show
                    $adjacents = 14; //gap between pages after number of adjacents
                    $offset = ($page - 1) * $per_page;
                    $data['offset_count'] = $offset;

                    $reload = 'serach_records_for_nexus';

                    $total_pages = ceil($search_count / $per_page);
                    $data['rec_count'] = $search_count;
                    $data['pages'] = $total_pages;
                    $data['no_of_paginations'] = $total_pages;
                    $data['per_page'] = $per_page;


                    if ($search_count > 0) {

                        $data['search_listing'] = $this->search_nexus_model->search_card_data_display_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $offset, $per_page);
                        $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
                    }


                    $this->load->view('user/nexus/ajax_search_nexus_view', $data);
                } else {
                    echo '<br><div align=left style=padding-left:251px padding-top:241px ><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
                }
            }

            function search_print_nexus_card() {

                $this->check_logged_in();
                $nexus_id = $this->uri->segment(3);
                $this->load->model('daily_policy_model');
                $this->load->model('user_model');
                $this->load->model('daily_policy_nexus_model');


                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];



                $poliy_infor = $this->daily_policy_nexus_model->get_daily_policy_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $nexus_id);
                $buy_hand_state = $poliy_infor['MICBYHND'];
                $city_state = $poliy_infor['MICCITY'];

                if ($buy_hand_state == 'B') {


                    if ($this->daily_policy_nexus_model->update_by_search_and_proceeds_for_nexus($access_epf, $act_name, $buy_hand_state, $city_state, $nexus_id)) {

                        redirect('user/daily_nexus_by_hand_list');
                    }
                } else {

                    if ($city_state == 'Y') {


                        if ($this->daily_policy_nexus_model->update_by_search_and_proceeds_for_nexus($access_epf, $act_name, $buy_hand_state, $city_state, $nexus_id)) {

                            redirect('user/daily_nexus_by_colombo_list'); ////redirect('user/daily_mic_by_colombo_list');
                        }
                    } else {

                        if ($this->daily_policy_nexus_model->update_by_search_and_proceeds_for_nexus($access_epf, $act_name, $buy_hand_state, $city_state, $nexus_id)) {

                            redirect('user/daily_nexus_by_outstation_list'); ///redirect('user/daily_mic_by_outstation_list');
                        }
                    }
                }
            }

            function daily_nexus_by_hand_list() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
                $data['pg_transfer'] = "daily_nexus_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_by_hand_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_by_hand_list_serial() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
                $data['pg_transfer'] = "daily_nexus_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_by_hand_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_pinter_buy_hand_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {
                        $print_state = "N";
                    }
                }



                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, 'N');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_nexus_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {

                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;


                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {

                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, $offset, $per_page, 'N');
                    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/nexus/ajax_daily_nexus_bhand_view', $data); ///ajax_daily_print_spool_view',$data);
            }

            function daily_nexus_by_colombo_list() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
                $data['pg_transfer'] = "daily_nexus_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_colombo_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_by_colombo_list_serial() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
                $data['pg_transfer'] = "daily_nexus_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_colombo_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_pinter_nexus_colombo_data_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {
                        $print_state = "N";
                    }
                }



                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', 'N');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_nexus_colombo_data_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;








                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {

                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', $offset, $per_page, 'N');
                    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                //$this->load->view('user/ajax_daily_print_spool_view',$data);
                $this->load->view('user/nexus/ajax_daily_nexus_bhand_view', $data);
            }

            function daily_nexus_by_outstation_list() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
                $data['pg_transfer'] = "daily_nexus_by_outstation_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexsus_by_outstation_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_by_outstation_list_serial() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
                $data['pg_transfer'] = "daily_nexus_by_outstation_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexsus_by_outstation_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_pinter_nexus_outstation_data_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {
                        $print_state = "N";
                    }
                }



                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', 'N');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_nexus_outstation_data_list'; //daily_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;








                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {

                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', $offset, $per_page, 'N');
                    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                //$this->load->view('user/ajax_daily_print_spool_view',$data);
                // $this->load->view('user/nexus/ajax_nexus_outstation_data_list_view',$data);
                $this->load->view('user/nexus/ajax_nexus_outstation_data_list_view_update', $data);
            }

            function send_by_hand_information_for_nexus() {

                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

                //$rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code,"","B");

                $dbResult = $this->nexus_model->update_by_hand_printer_list_for_nexus($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "B");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function send_colombo_information_for_nexus() {

                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];


                //$rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code,"Y","");

                $dbResult = $this->nexus_model->update_by_hand_printer_list_colombo_none_colombo_for_nexus($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "", "Y");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function send_nexus_out_station_information() {

                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];


                //$rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code,'N','');

                $dbResult = $this->nexus_model->update_by_hand_printer_list_colombo_none_colombo_for_nexus($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, '', 'N');


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function daily_mic_list_alert() {

                $this->check_logged_in();
                $data['sort_order'] = "A";

                $this->load->model('card_model');
                $this->load->model('user_model');
                $this->load->model('card_serial_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);



                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                $fleet_allow = $access_user_infor['upload_allow_fleet'];
                $man_is_policy = $access_user_infor['upload_allow_nexus'];


                $rec_count = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $zone_code, $zone_code, "");
                $data['rec_count'] = $rec_count;




                $by_hand_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "", "B");
                $data['by_hand_count'] = $by_hand_count;

                $colombo_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "Y", "");
                $data['colombo_list_count'] = $colombo_list_count;

                $outstation_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "N", "");
                $data['outstation_list_count'] = $outstation_list_count;





                $data['search_type'] = 0;
                $data['seek_data'] = "";


                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                $this->load->view('main_page/header_view', $data);

                if ($man_is_policy == 0) {

                    $data['err_msg'] = "<div class='validation fielderror'>You don't sufficient have Privileges ..!</div>";

                    $this->load->view('user/man_is_policy_error', $data);
                } else {

                    $this->load->view('user/user_alert_view', $data);
                }
                $this->load->view('main_page/footer_view');
            }

            function list_card_information() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['MICPLNO'];
                $data['MICPCOV'] = $card_data['MICPCOV'];
                $data['MICVENO'] = $card_data['MICVENO'];
                $data['MICNAME'] = $card_data['MICNAME'];
                $data['MICADD1'] = $card_data['MICADD1'];
                $data['MICADD2'] = $card_data['MICADD2'];
                $data['MICPRDU'] = $card_data['MICPRDU'];


                $this->load->view('user/list_card_information_view', $data);
            }

            function list_card_information_daily() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_daily_card_policy_data($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['MICPLNO'];
                $data['MICPCOV'] = $card_data['MICPCOV'];
                $data['MICVENO'] = $card_data['MICVENO'];
                $data['MICNAME'] = $card_data['MICNAME'];
                $data['MICADD1'] = $card_data['MICADD1'];
                $data['MICADD2'] = $card_data['MICADD2'];
                $data['MICPRDU'] = $card_data['MICPRDU'];


                $this->load->view('user/list_card_information_view', $data);
            }

            function list_card_information_display() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['PR_MICPLNO'];
                $data['MICPCOV'] = $card_data['PR_MICPCOV'];
                $data['MICVENO'] = $card_data['PR_MICVENO'];
                $data['MICNAME'] = $card_data['PR_MICNAME'];
                $data['MICADD1'] = $card_data['PR_MICADD1'];
                $data['MICADD2'] = $card_data['PR_MICADD2'];
                $data['MICPRDU'] = $card_data['PR_MICPRDU'];
                $data['MICTYPE'] = $card_data['PR_TYPRE'];


                $this->load->view('user/list_card_information_spool_view', $data);
            }

            function list_card_printed_information() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['PR_MICPLNO'];
                $data['MICPCOV'] = $card_data['PR_MICPCOV'];
                $data['MICVENO'] = $card_data['PR_MICVENO'];
                $data['MICNAME'] = $card_data['PR_MICNAME'];
                $data['MICADD1'] = $card_data['PR_MICADD1'];
                $data['MICADD2'] = $card_data['PR_MICADD2'];
                $data['MICPRDU'] = $card_data['PR_MICPRDU'];
                $data['MICTYPE'] = $card_data['PR_TYPRE'];


                $this->load->view('user/printed_card_information_view', $data);
            }

            function card_nexus_information() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data_for_mic($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['PR_MICPLNO'];
                $data['MICPCOV'] = $card_data['PR_MICPCOV'];
                $data['MICVENO'] = $card_data['PR_MICVENO'];
                $data['TITLE'] = $card_data['PR_MICTITL'];
                $data['MICNAME'] = $card_data['PR_MICNAME'];
                $data['MICADD1'] = $card_data['PR_MICADD1'];
                $data['MICADD2'] = $card_data['PR_MICADD2'];
                $data['MICPRDU'] = $card_data['PR_MICPRDU'];
                $data['MICTYPE'] = $card_data['PR_TYPRE'];


                $this->load->view('user/nexus/card_nexus_view', $data);
            }

            function update_nexus_serial() {


                /////B1121040000008273^/                    ^1507701000000000000000000000000



                if (( $this->input->post('text_data') == NULL) && ( $this->input->post('nex_id') == NULL)) {

                    $jason_output['result'] = -3;
                    echo json_encode($jason_output);
                } else {
                    $this->load->model('card_infor_model');

                    $nex_text = trim($this->input->post('text_data'));
                    $length = strlen($nex_text);
                    $nex_id = $this->input->post('nex_id');


                    if ($length != 71) {

                        $dbResult = -10;
                    } else {
                        if ($this->card_infor_model->check_serial_exists($nex_text) == 0) {

                            $dbResult = $this->card_infor_model->update_mic_serial_data($nex_id, $nex_text);

                            if ($dbResult > 0) {


                                $jason_output['result'] = $dbResult;
                            } else {

                                $jason_output['result'] = 0;
                            }
                        } else {
                            $dbResult = -999;
                        }
                    }
                    $jason_output['result'] = $dbResult;


                    echo json_encode($jason_output);
                }
            }

            function update_mic_serial() {





                if (( $this->input->post('text_data') == NULL) && ( $this->input->post('mic_id') == NULL)) {

                    $jason_output['result'] = -3;
                    echo json_encode($jason_output);
                } else {
                    $this->load->model('card_infor_model');

                    $nex_text = trim($this->input->post('text_data'));
                    $length = strlen($nex_text);
                    $nex_id = $this->input->post('mic_id');


                    if ($length != 13) {

                        $dbResult = -10;
                    } else {
                        if ($this->card_infor_model->check_serial_exists($nex_text) == 0) {

                            $dbResult = $this->card_infor_model->update_mic_serial_data($nex_id, $nex_text);

                            if ($dbResult > 0) {


                                $jason_output['result'] = $dbResult;
                            } else {

                                $jason_output['result'] = 0;
                            }
                        } else {
                            $dbResult = -999;
                        }
                    }
                    $jason_output['result'] = $dbResult;


                    echo json_encode($jason_output);
                }
            }

        }
        ?>