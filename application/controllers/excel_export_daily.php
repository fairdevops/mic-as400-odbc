<?php

class Excel_export_daily extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url'); /////http://localhost/mic_db/index.php/excel_export/reports_transcation_summery
        $this->load->helper('date');
        $this->load->library('session');
    }
	
	
	 function export_daily_mic_buy_hand_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');
		
		

        $user_epf = $this->uri->segment(3);
        $card_type_p = $this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'export_mic_buy_hand_report';
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_nexus_buy_hand_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->daily_excel_model->export_mic_detail_buyhand_count($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);



        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');	
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_mic_detail_buyhand_display($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }


	 function export_daily_list_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');
		
		

        $user_epf = $this->uri->segment(3);
        $card_type_p = 1;
		$city=$this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
			
			if ($city =='Y') {
			
            $report_name = 'export_mic_colombo_report'; }else { $report_name = 'export_mic_outstation_report'; } 
			
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_nexus_buy_hand_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->daily_excel_model->export_mic_detail_city_count($my_branch_state, $branch_code, $region_code, $zone_code, $city);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_mic_detail_city_display($my_branch_state, $branch_code, $region_code, $zone_code, $city);


            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }


  ///
  
  
     function export_daily_list_all(){
	 
	    ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;
        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');		
	

        $user_epf = $this->uri->segment(3);
        $card_type_p = 1;
		$city=$this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
			
			if ($city =='Y') {
			
            $report_name = 'export_before_print_report_colombo'; }else { $report_name = 'export_before_print_report'; } 
			
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_nexus_buy_hand_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records =$this->daily_excel_model->before_print_all_count_at_head_office($my_branch_state, $branch_code, $region_code, $zone_code);
		
		

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_mic_detail_all_head_office($my_branch_state, $branch_code, $region_code, $zone_code);
			

            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
	     
	 
	 }
  
  
  	 function export_daily_list_out_station_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');
		
		

        $user_epf = $this->uri->segment(3);
        $card_type_p = 1;
		$city=$this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
			
			if ($city =='Y') {
			
            $report_name = 'export_mic_outstation_report'; }else { $report_name = 'export_mic_outstation_report'; } 
			
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_nexus_buy_hand_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->daily_excel_model->export_mic_detail_city_count($my_branch_state, $branch_code, $region_code, $zone_code, $city);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_mic_detail_out_station_display($my_branch_state, $branch_code, $region_code, $zone_code, $city);


            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
	
	
	 function export_daily_nexus_buy_hand_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');
		
		

        $user_epf = $this->uri->segment(3);
        $card_type_p = 2;//$this->uri->segment(4);

        if ($card_type_p == 1) {
            $card_type = 'M';
            $report_name = 'export_mic_buy_hand_report';
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_nexus_buy_hand_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->daily_excel_model->export_nexus_detail_buyhand_count($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_nexus_detail_buyhand_display($my_branch_state, $branch_code, $region_code, $zone_code, $card_type);


            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }



    function export_daily_nexus_list_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');
		$this->load->model('nexus_model');
		

        $user_epf = $this->uri->segment(3);
        $card_type_p = 1;
		$city=$this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
			
			if ($city =='Y') {
			
            $report_name = 'export_nexus_colombo_report_before_print_'; }else { $report_name = 'export_nexus_colombo_report_before_print_'; } 
			
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_colombo_report_before_print_';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($my_branch_state, $branch_code, $region_code, $zone_code,$city,'');
		//daily_excel_model->export_nexus_detail_city_count($my_branch_state, $branch_code, $region_code, $zone_code, $city);
    
	

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_nexus_detail_city_display($my_branch_state, $branch_code, $region_code, $zone_code, $city,'');


            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
	
	
	 function export_daily_nexus_out_station_data() {


        ini_set('display_errors', 1);
        ini_set('log_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('pxl');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
                ->setDescription("description");

        // Assign cell values
        $objPHPExcel->setActiveSheetIndex(0);

        $Y = 2;



        $this->load->model('excel_model');
		$this->load->model('daily_excel_model');
		
		

        $user_epf = $this->uri->segment(3);
        $card_type_p = 1;
		$city=$this->uri->segment(4);


        if ($card_type_p == 1) {
            $card_type = 'M';
			
			if ($city =='Y') {
			
            $report_name = 'export_nexus_outstation_before_print_report'; }else { $report_name = 'export_nexus_outstation_before_print_report'; } 
			
        } else {
            $card_type = 'N';
            $report_name = 'export_mic_nexus_buy_hand_report';
        }


        $user_infor = $this->excel_model->get_user_data($user_epf);
        $my_branch_state = $user_infor['brz_state'];
        $branch_code = $user_infor['branch'];
        $region_code = $user_infor['region'];
        $zone_code = $user_infor['zone'];

        $no_records = $this->daily_excel_model->export_nexus_detail_city_count($my_branch_state, $branch_code, $region_code, $zone_code, 'N','');

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'VEHICLE NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'POLICY NUMBER');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'PERIOD OF COVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'SERIAL');
		
		
		
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'INSURED NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ADDRESS 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ADDRESS 2');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'POSTAL TOWN');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'DEBIT ACCOUNT');//DEBTOR CODE');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'MARKETING CODE');//DEBTOR CODE');
		
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'CONTACT NUMBER');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'HANDOVER');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'DATE OF DOWNLOAD');


        if ($no_records > 0) {


            $rs_data = $this->daily_excel_model->export_nexus_detail_city_display($my_branch_state, $branch_code, $region_code, $zone_code, $city,'');
			

            foreach ($rs_data as $row) {
			 
			    if ($row['MICNAME2']=="" ) { $reg_state= "RP";} else { $reg_state= $row['MICNAME2'];} 

                $Y++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $Y, $row['MICVENO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $Y, $row['MICPLNO']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $Y, $row['MICPCOV']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $Y, $row['MICREFNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $Y, $row['MICNAME']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $Y, $row['MICADD1']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $Y, $row['MICADD2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $Y, $row['MICTOWN']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $Y, $row['MICSPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $Y, $row['MICMKCD']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $Y, $row['MICMPNO']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $Y, $reg_state);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $Y, date('d/m/Y'));				
			
            }
        } else {

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', "No Records Found");
        }


        $objPHPExcel->createSheet();
        $sheet = $objPHPExcel->getActiveSheet();


        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);

        $sheet->getStyle('A1:M1')->getFont()->setBold(true)->getColor()->setRGB('000000');


        $objWriter = IOFactory::createWriter($objPHPExcel, "Excel5");


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_name . '-' . date('d-m-Y') . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
	
}

?>