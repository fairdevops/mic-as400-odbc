<?php

/// vehicl no ,policy no ,period ,name ,address 
///upload_fleet_policy_single

class User extends CI_Controller {

    public function __construct() {
        parent :: __construct();

        $this->load->helper(array('url', 'form', 'security', 'date'));
        $this->load->library('session');
        $data['base_url'] = base_url();  //$this->output->enable_profiler(TRUE);
    }

    public function index() {

        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            redirect('admin/user_creation');
        } else {

            if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'U')) {


                redirect('user/daily_mic_list');
            } else {


                redirect('member/login');
                $data['message'] = "";
                $data['usr_login'] = 'N';
            }
        }
    }

    public function login() {

        $data['message'] = "";
        $data['usr_login'] = '';
        $access_epf_no = $this->input->post('epf');
        $access_level = $this->input->post('auth');

        $userdata = array(
            'loguser_username' => $access_epf_no,
            'loguser_login' => 'A',
            'loguser_level' => 'A',
            'loguser_first_name' => $access_epf_no,
            'loguser_last_name' => $access_epf_no,
            'loguser_tbl_id' => $access_epf_no
        );

        $this->session->set_userdata($userdata);


        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            //redirect('email_shedule_configure/assign_notifications'); 
            redirect('admin/user_creation');
        } else {


            redirect('http://link/');
        }
    }

    public function check_logged_in() {

        $access_level = $this->session->userdata('loguser_level');



        if (($this->session->userdata('loguser_username')) && ($access_level == 'U')) {

            $state = true;
            return $state;
        } else {

            $state = false;
            redirect('http://link/');
            exit();
        }
    }

    public function logout() {

        $user_data = array(
            'loguser_username' => '',
            'loguser_login' => '',
            'loguser_level' => '',
            'loguser_first_name' => '',
            'loguser_first_name' => '',
            'loguser_tbl_id' => ''
        );

        $this->session->unset_userdata($user_data);
        ?>
        <a href="http://link/">RE LOGIN<a>
                <?php
                ///redirect('member/login');
            }

          

            function user_dashboard() {

                $data[''] = "assign_notifications";
                $data['search_type'] = 0;
                $data['seek_data'] = "";
                $this->check_logged_in();

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);

                $total_rows = 0;
                $this->load->model('user_model');
                $this->load->model('excel_model');


                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                $data['access_epf'] = $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

              
               
				
				///if ( $branch_code == 'A') {

                $no_mic_card = $this->excel_model->export_excel_by_all_cards_count($branch_state, $branch_code, $region_code, $zone_code, 'M');
               
				if ( $branch_code == 'A') {
                  $no_nexus_card = $this->excel_model->export_nexus_points_list($branch_state, $branch_code, $region_code, $zone_code, 'N');
				  $no_fleet = $this->excel_model->export_excel_all_fleets($branch_state, $branch_code, $region_code, $zone_code);
				}
				else
				{
				  $no_nexus_card = 0;
				  $no_fleet = 0;
				
				} 
              

                $no_manual_card = $this->excel_model->export_excel_by_all_letters_for_manual_print($branch_state, $branch_code, $region_code, $zone_code, 'N', 'M');
              
				
				
				$no_mic_buyhand = $this->excel_model->export_excel_by_hand_count($branch_state,$branch_code,$region_code, $zone_code, $access_epf, 'B', 'Y', 'M');
                $data['no_mic_buyhand'] = $no_mic_buyhand;
				
				$data['branch_code'] = $branch_code;
				$data['no_fleet']   = $no_fleet;
				$data['no_mic_card'] = $no_mic_card;
				$data['no_nexus_card'] = $no_nexus_card;
				$data['no_manual_card'] = $no_manual_card;
				
				
				


                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/my_dashboard_view', $data);
                $this->load->view('main_page/footer_view');
                
            }
			
			
			
			function new_endosment() { //daily_mic_list_generate(){
                
				$this->check_logged_in(); /// ABMPC/00490
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/user_endosment_view', $data);
                $this->load->view('main_page/footer_view');
            }
			
			function ajax_new_endosment() {
			
			    
				$this->load->model('endose_model');
				
				$pol_no =trim($this->input->post('txt_search'));
				///$search_count=$this->endose_model->get_endosement_data_count($pol_no);
				
				//$data['search_count'] = $search_count;
				
				//if ($search_count >0 ) {
				        $data['search_listing']= $this->endose_model->get_endosement_data($pol_no);
				//}
				
								
			     $this->load->view('user/ajax_new_endosment_view',$data);
			
			}
			
			 function delete_old_data() { //daily_mic_list_generate(){
                               				$this->check_logged_in();
				$this->load->model('user_model');
				$this->load->model('endose_model');
				
				 $itemExt_infor = array();
				 $itemExt_infor2 = array();
				 $itemExt_infor3 = array();
				 
				 $itemExt_infor = explode("-", $_SERVER['REQUEST_URI']);
				 $itemExt_infor_count = count($itemExt_infor);
						
					
						for ($m = 0; $m < ($itemExt_infor_count-1); $m++) {  $itemExt_infor[$m];  }
									
									
						 $itemExt_infor2 = explode("/delete_old_data/", $_SERVER['REQUEST_URI']);
						 $itemExt_infor_count2 = count($itemExt_infor2);
						 
						 
						 for ($b = 0; $b < ($itemExt_infor_count-1); $b++) {  $itemExt_infor2[$b];  } 
				
				  $limit =  $itemExt_infor[1];///$this->uri->segment(5);
				  
			 
						$itemExt_infor3 = explode("-", $itemExt_infor2[1]);
						 $itemExt_infor_count3 = count($itemExt_infor3);
						 
						 
				 for ($b = 0; $b < ($itemExt_infor_count-1); $b++) {  $itemExt_infor3[$b];  } 
				 
				 $policy_no =$itemExt_infor3[0];
				 
				 
				 $cover_period=   $this->endose_model->get_cover_period($policy_no,$limit);
				 
				 $access_epf = $this->session->userdata('loguser_tbl_id');
                 $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                 $data['access_epf'] = $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

				 
				 
		     $this->endose_model->add_delete_data_from_iis($policy_no,$cover_period, $branch_state, $branch_code, $region_code, $zone_code,$access_epf,$act_name) ;
				
				
				$data['delete_data'] ='<div class="validation sucess" style="width:385px;padding-left:4px;"> Record Successfully Deleted ..!</class>';
				
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/delete_old_data_view', $data);
                $this->load->view('main_page/footer_view');
            }
			
			

            function daily_mic_list() {

                $this->check_logged_in();
                $data['sort_order'] = "B";
				
				

                $this->load->model('card_model');
                $this->load->model('user_model');
                $this->load->model('card_serial_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
				
               
				
                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                $print_only = $access_user_infor['print_only'];
                $data['print_facility'] = $print_only;
				
                $rec_count = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $zone_code, $zone_code, "");
                $data['rec_count'] = $rec_count;




                $by_hand_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "", "B");
                $data['by_hand_count'] = $by_hand_count;

               
			     if ($branch_code=='A'){
			   
                    $colombo_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "Y", "");
                    $data['colombo_list_count'] = $colombo_list_count;

                    $outstation_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "N", "");
                    $data['outstation_list_count'] = $outstation_list_count;

                 }else{
				    $colombo_list_count     = 0;
				    $outstation_list_count  = 0;
				}

                $data['colombo_list_count']    = $colombo_list_count;
                $data['outstation_list_count'] = $outstation_list_count;

                $data['search_type'] = 0;
                $data['seek_data'] = "";
				$data['branch_code']= $branch_code;


                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                $this->load->view('main_page/header_view', $data);
                /////////////$this->load->view('user/daily_mic_list_view', $data); //user_dashboard_view
				
				
				if ($branch_code=='A'){
				    // var_dump($access_user_infor);
                    $data['is_bullkprint_allowed'] = TRUE;
                    $this->load->view('user/daily_mic_list_view', $data); //user_dashboard_view 
					
				}
				else
				{
 			        $this->load->view('user/daily_mic_list_view_branch_view', $data);
				}	

				
				
                $this->load->view('main_page/footer_view');
            }







            function bulk_print(){

                $this->check_logged_in();
                $this->load->model('user_model');
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
                $this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);

                //$this->load->view('user/daily_mic_by_hand_list_view', $data);
                $this->load->view('user/bullk_print_list_view', $data);

                $this->load->view('main_page/footer_view');

            }

            /* pagination class here */

            function paginate($reload, $page, $tpages, $adjacents) {

                $prevlabel = "&lsaquo; Prev";
                $nextlabel = "Next &rsaquo;";
                $out = '<div class="pagin green">';

                // previous label

                if ($page == 1) { ///if ($page == 1) 
                    $out.= "<span>$prevlabel</span>";
                } else if ($page == 2) {
                    $out.= "<a href='javascript:void(0);' onclick='load(1)'>$prevlabel</a>";
                } else {
                    $out.= "<a href='javascript:void(0);' onclick='load(" . ($page - 1) . ")'>$prevlabel</a>";
                }

                // first label
                if ($page > ($adjacents + 1)) {
                    $out.= "<a href='javascript:void(0);' onclick='load(1)'>1</a>";
                }
                // interval
                if ($page > ($adjacents + 2)) {
                    $out.= "...\n";
                }

                // pages

                $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
                $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
                for ($i = $pmin; $i <= $pmax; $i++) {
                    if ($i == $page) {
                        $out.= "<span class='current'>$i</span>";
                    } else if ($i == 1) {
                        $out.= "<a href='javascript:void(0);' onclick='load(1)'>$i</a>";
                    } else {
                        $out.= "<a href='javascript:void(0);' onclick='load(" . $i . ")'>$i</a>";
                    }
                }

                // interval

                if ($page < ($tpages - $adjacents - 1)) {
                    $out.= "...\n";
                }

                // last

                if ($page < ($tpages - $adjacents)) {
                    $out.= "<a href='javascript:void(0);' onclick='load($tpages)'>$tpages</a>";
                }

                // next

                if ($page < $tpages) {
                    $out.= "<a href='javascript:void(0);' onclick='load(" . ($page + 1) . ")'>$nextlabel</a>";
                } // else {
                //$out.= "<span>$nextlabel</span>";
                //}
                $out.= "</div>";
                return $out;
            }

            function paginate_second($reload, $page, $tpages, $adjacents) {

                $prevlabel = "&lsaquo; Prev";
                $nextlabel = "Next &rsaquo;";
                $out = '<div class="pagin green">';

                // previous label

                if ($page == 1) { ///if ($page == 1) 
                    $out.= "<span>$prevlabel</span>";
                } else if ($page == 2) {
                    $out.= "<a href='javascript:void(0);' onclick='load_page(1)'>$prevlabel</a>";
                } else {
                    $out.= "<a href='javascript:void(0);' onclick='load_page(" . ($page - 1) . ")'>$prevlabel</a>";
                }

                // first label
                if ($page > ($adjacents + 1)) {
                    $out.= "<a href='javascript:void(0);' onclick='load_page(1)'>1</a>";
                }
                // interval
                if ($page > ($adjacents + 2)) {
                    $out.= "...\n";
                }

                // pages

                $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
                $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
                for ($i = $pmin; $i <= $pmax; $i++) {
                    if ($i == $page) {
                        $out.= "<span class='current'>$i</span>";
                    } else if ($i == 1) {
                        $out.= "<a href='javascript:void(0);' onclick='load_page(1)'>$i</a>";
                    } else {
                        $out.= "<a href='javascript:void(0);' onclick='load_page(" . $i . ")'>$i</a>";
                    }
                }

                // interval

                if ($page < ($tpages - $adjacents - 1)) {
                    $out.= "...\n";
                }

                // last

                if ($page < ($tpages - $adjacents)) {
                    $out.= "<a href='javascript:void(0);' onclick='load_page($tpages)'>$tpages</a>";
                }

                // next

                if ($page < $tpages) {
                    $out.= "<a href='javascript:void(0);' onclick='load_page(" . ($page + 1) . ")'>$nextlabel</a>";
                } // else {
                //$out.= "<span>$nextlabel</span>";
                //}
                $out.= "</div>";
                return $out;
            }

            /* end of pagination */

            function fiter_numbers_only($_input) {
                $_output = preg_replace('/\D/', '', $_input);
                return $_output;
                unset($_input, $_output);
            }

		 
		 
		     function daily_card_search_new_branch_level() {


                $this->load->model('user_model');
                $this->load->model('card_model');
				$by_hand="";
				$city="";

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
				$data['access_epf']= $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code  = $access_user_infor['branch'];
                $region_code  = $access_user_infor['region'];
                $zone_code    = $access_user_infor['zone'];

                if ($this->input->post('group_name')) {
                    $data['sort_order'] = $this->input->post('group_name');
                    $mys_sort = $this->input->post('group_name');
                } else {
                    $data['sort_order'] = "B";
                    $mys_sort = "B";
                }

                $data['sort_order'] = $mys_sort;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 18; //how much records you want to show
                $adjacents = 18; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_card_search_new_branch_level';

                if ($this->input->post('group_name') == 'A') {

                    $rec_count = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $region_code, $zone_code);
					
                } else {
                    if ($this->input->post('group_name') == 'C') {
                        $city = "Y";
                        $by_hand = "";
                    } else {

                        if ($this->input->post('group_name') == 'O') {

                            $city = "N";
                            $by_hand = "";
                        } else {

                            $city = "";
                            $by_hand = 'B';
                        }
                    }

                 $rec_count = $this->card_model->display_daily_card_data_count_with_filter_branch_level($branch_state,$branch_code,$region_code,$zone_code,$city,$by_hand);
               
			   }

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;


                if ($rec_count > 0) {


                    //if ($this->input->post('group_name') == 'A') {

                       // $data['rs_listing'] = $this->card_model->display_daily_card_data($branch_state, $branch_code, $region_code, $zone_code, $offset, $per_page);
                       // $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
						//$city = "";
                       // $by_hand = "";
						
						
                    //} else { 
					//echo "dsdsd";

                        $data['rs_listing'] = $this->card_model->display_daily_card_data_count_with_filter_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
						
                   // }
                } else {
                    $data['dsp_err'] = '<div class="validation fielderror" style="width:578px;">No Records Found ..!</div>';
                }
                
				$data['by_hand']     = $by_hand ;
				$data['city']        = $city ;
                $this->load->view('user/jx_daily_mic_list_edit_branch_level_view',$data);//jx_daily_mic_list_view', $data); ///ajax_daily_list_view', $data);
            }

		 
		 
		 
		 


            function daily_card_search_new() {


                $this->load->model('user_model');
                $this->load->model('card_model');
				$by_hand="";
				$city="";

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
				$data['access_epf']= $access_epf;


              //  var_dump($this->input->post('group_name'));


                $branch_state = $access_user_infor['brz_state'];
                $branch_code  = $access_user_infor['branch'];
                $region_code  = $access_user_infor['region'];
                $zone_code    = $access_user_infor['zone'];

                if ($this->input->post('group_name')) {
                    $data['sort_order'] = $this->input->post('group_name');
                    $mys_sort = $this->input->post('group_name');
                } else {
                    $data['sort_order'] = "B";
                    $mys_sort = "B";
                }

                $data['sort_order'] = $mys_sort;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 18; //how much records you want to show
                $adjacents = 18; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_card_search_new';
		
		//var_dump($this->input->post('group_name'));
               if ($this->input->post('group_name') == 'A') {  

                     $rec_count = $this->card_model->display_daily_card_data_all_count_at_head_office($branch_state, $branch_code, $region_code, $zone_code);
					
                } else {
                    if ($this->input->post('group_name') == 'C') {  //echo "COLOMBO" ;
                        $city = "Y";
                        $by_hand = "";
                    } else {

                        if ($this->input->post('group_name') == 'O') { //echo "OUT STATION" ;

                            $city = "N";
                            $by_hand = "";
                        } else {

                            $city = "";
                            $by_hand = 'B';  ///echo "BUY HAND" ;
                        }
                    }

                $rec_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state,$branch_code,$region_code,$zone_code,$city,$by_hand);
               
			   }

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;


                if ($rec_count > 0) {


                    if ($this->input->post('group_name') == 'A') { //echo "ALL2" ;

                        $data['rs_listing'] = $this->card_model->display_daily_card_data_all_at_head_office($branch_state, $branch_code, $region_code, $zone_code, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
						$city = "";
                        $by_hand = "";
						
						
                    } else { 
                        
                        $data['rs_listing'] = $this->card_model->display_daily_card_data_count_with_filter_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
						
                    }
                } else {
                    $data['dsp_err'] = '<div class="validation fielderror" style="width:578px;">No Records Found ..!</div>';
                }
                
				$data['by_hand']     = $by_hand ;
				$data['city']        = $city ;
                $this->load->view('user/jx_daily_mic_list_edit_view',$data);//jx_daily_mic_list_view', $data); ///ajax_daily_list_view', $data);
            }

            function card_serail() {


               
                $this->check_logged_in();

                $this->load->model('card_model');

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div class="validation fielderror" style="width:285px">', '</class>');
                $this->form_validation->set_rules('card_type', 'Card Type', 'trim|required');
                //$this->form_validation->set_rules('start_serail', 'Starting Serial', 'trim|required|callback_debtor_name_not_exists');
                //$this->form_validation->set_rules('end_serail',   'Ending Serial',   'trim|required|callback_debtor_name_not_exists');

                if ($this->input->post('card_type') == 'M') {

                    $this->form_validation->set_rules('start_serail', 'Starting Serial', 'min_length[13]|trim|required|integer');
                    $this->form_validation->set_rules('end_serail', 'Starting Serial', 'min_length[13]|trim|required|integer');
                } else {
                    $this->form_validation->set_rules('start_serail', 'Starting Serial', 'min_length[16]');
                    $this->form_validation->set_rules('end_serail', 'Starting Serial', 'min_length[16]');
                }



                if ($this->form_validation->run() === FALSE) {

                    $data['start_serail'] = $this->input->post('start_serail');
                    $data['end_serail'] = $this->input->post('end_serail');

                    if ($this->input->post('card_type')) {
                        $data['card_type'] = $this->input->post('card_type');
                    } else {
                        $data['card_type'] = "M";
                    }



                    $this->load->view('user/card_serail_view', $data);
                } else {

                    if ($this->input->post('BtnSubmit') != NULL) {

                        $data['start_serail'] = $this->input->post('start_serail');
                        $data['end_serail'] = $this->input->post('end_serail');
                        $data['card_type'] = $this->input->post('card_type');



                        $add_serial = $this->card_model->add_serial($this->input->post('card_type'), $this->input->post('start_serail'), $this->input->post('end_serail'), $this->session->userdata('loguser_username'));

                        if ($add_serial) {

                            //echo "ddd";
                            //$this->load->view('main_page/header_view', $data);
                            $this->load->view('user/card_serail_view', $data);
                            // redirect('admin/ad_user_sucess', 'refresh');
                            //$this->load->view('main_page/footer_view', $data);
                        } else {

                            //$this->load->view('main_page/header_view', $data);
                            $this->load->view('user/card_serail_view', $data);
                            //$this->load->view('main_page/footer_view', $data);
                        }
                    }
                }
            }

           	 ///daily_mic_by_hand_list

            function daily_mic_by_hand_list() { //daily_mic_list_generate(){
                $this->check_logged_in();
				$this->load->model('user_model');
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/daily_mic_by_hand_list_view', $data);
                $this->load->view('main_page/footer_view');
            }


            /**
            * CR : Debtor wise bulk card printing
            * Developer : Gayan C <gayanch@fairfirst.lk>
            */
            function serach_bulklist_by_debtor(){


                $this->load->model('user_model');
                $this->load->model('card_model');
                $this->load->model('re_print_model');
                $by_hand="";
                $city="";

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

              //  var_dump($access_user_infor);
                $data['access_epf']= $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code  = $access_user_infor['branch'];
                $region_code  = $access_user_infor['region'];
                $zone_code    = $access_user_infor['zone'];
                $print_only   = $access_user_infor['print_only'];

                if ($this->input->post('group_name')) {
                    $data['sort_order'] = $this->input->post('group_name');
                    $mys_sort = $this->input->post('group_name');
                } else {
                    $data['sort_order'] = "B";
                    $mys_sort = "B";
                }

                $data['sort_order'] = $mys_sort;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 10; //how much records you want to show
                $adjacents = 18; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'bulk_print';
        
        
                $debtor_code = $this->input->post('txt_search');
                $txn_type    = $this->input->post('txn_type');

                $rec_count = $this->card_model->display_debtor_card_data_all_count($debtor_code,$txn_type); 


                $total_pages        = ceil($rec_count / $per_page);
                $data['rec_count']  = $rec_count;
                $data['pages']      = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page']   = $per_page;
                $data['print_facility'] = $print_only;

                if ($rec_count > 0) {
                       
                        $data['rs_listing'] = $this->card_model->display_debtor_card_data_all_display($debtor_code,$txn_type,$offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                        

                } else {
                    $data['dsp_err'] = '<div class="validation fielderror" style="width:578px;">No Records Found ..!</div>';
                }
                
                $data['by_hand']     = $by_hand ;
                $data['city']        = $city ;
                $this->load->view('user/jx_debtor_card_list_edit_view',$data);//jx_daily_mic_list_view', $data); ///ajax_daily_list_view', $data);

            }


            function daily_mic_by_hand_list_serial() { //daily_mic_list_generate(){
                $this->check_logged_in();
                $this->load->model('user_model');
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
				 
                $data['pg_transfer'] = "daily_mic_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/daily_mic_by_hand_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_mic_by_colombo_list() { //daily_mic_list_generate(){
                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				 $this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/daily_mic_colombo_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_mic_by_colombo_list_serial() { //daily_mic_list_generate(){
                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/daily_mic_colombo_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_mic_by_outstation_list() { //daily_mic_list_generate(){
                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_outstation_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/daily_mic_by_outstation_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_mic_by_outstation_list_serial() { //daily_mic_list_generate(){
                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_mic_by_outstation_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/daily_mic_by_outstation_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_pinter_buy_hand_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {

                        if ($this->input->post('printed_group_name') == "F") {


                            $print_state = "F";
                        } else {

                            $print_state = "N";
                        }
                    }
                }

                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, 'M');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {
				
				    if ($print_state == 'N' ){

                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, $offset, $per_page, 'M');
					
					}
					else
					{
                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_display_order($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, $offset, $per_page, 'M');
					}
					
                    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/ajax_daily_print_spool_view', $data);
            }

            function daily_pinter_colombo_data_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

               if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {

                        if ($this->input->post('printed_group_name') == "F") {


                            $print_state = "F";
                        } else {

                            $print_state = "N";
                        }
                    }
                }



                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', 'M');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {
                    
					 if ($print_state == 'N' ){
					
                        $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', $offset, $per_page, 'M');
                     }
					 else
					 {
                        $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display_order($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', $offset, $per_page, 'M');
					 } 
					    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/ajax_daily_print_spool_view', $data);
            }

            function daily_pinter_outstation_data_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

               	
				
				if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {

                        if ($this->input->post('printed_group_name') == "F") {


                            $print_state = "F";
                        } else {

                            $print_state = "N";
                        }
                    }
                }




                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', 'M');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_outstation_data_list'; //daily_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }
				
							

                $data['card_print_state'] = $card_print_state;

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {

                    if ($print_state == 'N' ){
				  
				    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', $offset, $per_page, 'M');
                    }
					else
					{
				    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display_order($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', $offset, $per_page, 'M');
					}
					
					$data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/ajax_daily_print_spool_view', $data);
            }

            function send_by_hand_information() {

                $this->load->model('user_model');
                $this->load->model('card_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

                $dbResult = $this->card_model->update_by_hand_printer_list($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "B");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }


            function send_debtor_bulk_information() {

                //var_dump($_POST);

                $this->load->model('user_model');
                $this->load->model('card_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

                $dbResult = $this->card_model->update_debtor_bulk_printer_list($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "B");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function send_colombo_information() {

                $this->load->model('user_model');
                $this->load->model('card_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];


                $dbResult = $this->card_model->update_by_hand_printer_list_colombo_none_colombo($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "", "Y");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function send_out_station_information() {

                $this->load->model('user_model');
                $this->load->model('card_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];


                $dbResult = $this->card_model->update_by_hand_printer_list_colombo_none_colombo($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "", 'N');


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function printer_shedule() {

                $this->check_logged_in();

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $data['access_epf'] = $access_epf;

                $this->load->model('fleet_model');
                $this->load->model('user_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

                $reports_allow = $access_user_infor['reports_allow'];
                $data['reports_allow'] = $reports_allow;


                $data['shedule_fleet_count'] = $this->fleet_model->get_fleet_print_count($branch_state, $branch_code, $region_code, $zone_code, 'N');


                $data['rec_count'] = "0";
                $data['card_print_state'] = "A";
                $this->load->view('main_page/header_view', $data);
				
				if ($branch_code=='A'){
				
                $this->load->view('user/printer_shedule_view', $data);
				
				}
				else
				{
				
				 $this->load->view('user/printer_shedule_view_branch', $data);
				
				}
				
				
				
				
                $this->load->view('main_page/footer_view');
            }

            function fleet_printer_policy_list() { //daily_mic_list_generate(){
                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				 $this->load->model('user_model');
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/fleet_printer_policy_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_pinter_fleet_policy_list() {


                $this->load->model('user_model');
                $this->load->model('fleet_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
					
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {
                        $print_state = "N";
                    }
                }



                $rec_count = $this->fleet_model->get_fleet_print_count($branch_state, $branch_code, $region_code, $zone_code, $print_state);
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_fleet_policy_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;








                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {

                    $data['rs_listing'] = $this->fleet_model->get_fleet_print_count_display_infor($branch_state, $branch_code, $region_code, $zone_code, $print_state, $offset, $per_page);
                    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/ajax_daily_fleet_spool_view', $data);
            }
			
			
			function serach_records_branch_level(){
			
			                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                if ($this->input->post('txt_search')) {


                    $search_txt = trim($this->input->post('txt_search'));
                    $data['search_txt'] = $search_txt;

                    $this->load->model('search_model');
                    $this->load->model('user_model');
                    $this->load->model('re_print_model');


                    $access_epf = $this->session->userdata('loguser_tbl_id');
                    $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                    $branch_state = $access_user_infor['brz_state'];
                    $branch_code = $access_user_infor['branch'];
                    $region_code = $access_user_infor['region'];
                    $zone_code = $access_user_infor['zone'];
                    $act_name = $access_user_infor['users_name'];
                    $print_only = $access_user_infor['print_only'];
                    $data['print_facility'] = $print_only;



                    $search_count = $this->search_model->search_card_data_branch_level_count($branch_state, $branch_code, $region_code, $zone_code, $search_txt);
                    $data['search_count'] = $search_count;

                    $data['print_count'] = $search_count;

                    $per_page = 0;
                    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                    $per_page = 13; //how much records you want to show
                    $adjacents = 13; //gap between pages after number of adjacents
                    $offset = ($page - 1) * $per_page;
                    $data['offset_count'] = $offset;

                    $reload = 'serach_records_branch_level';

                    $total_pages = ceil($search_count / $per_page);
                    $data['rec_count'] = $search_count;
                    $data['pages'] = $total_pages;
                    $data['no_of_paginations'] = $total_pages;
                    $data['per_page'] = $per_page;


                    if ($search_count > 0) {

                        $data['search_listing'] = $this->search_model->search_card_data_branch_level_display_data($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $offset, $per_page);
                        $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
                    }


                    $this->load->view('user/ajax_search_br_split_view',$data);//ajax_search_view', $data);
                } else {
                    echo '<br><div align=left style=padding-left:251px padding-top:241px ><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
                }

			
			
			
			
			
			}
			
			

            function serach_records() {

               // ini_set('display_errors', 1);
                //ini_set('log_errors', 1);
                //error_reporting(E_ALL);


                if ($this->input->post('txt_search')) {


                    $search_txt = trim($this->input->post('txt_search'));
                    $data['search_txt'] = $search_txt;

                    $this->load->model('search_model');
                    $this->load->model('user_model');
                    $this->load->model('re_print_model');


                    $access_epf = $this->session->userdata('loguser_tbl_id');
                    $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                    $branch_state = $access_user_infor['brz_state'];
                    $branch_code = $access_user_infor['branch'];
                    $region_code = $access_user_infor['region'];
                    $zone_code = $access_user_infor['zone'];
                    $act_name = $access_user_infor['users_name'];
                    $print_only = $access_user_infor['print_only'];
                    $data['print_facility'] = $print_only;



                    $search_count = $this->search_model->search_card_data_count($branch_state, $branch_code, $region_code, $zone_code, $search_txt);
                    $data['search_count'] = $search_count;

                    $data['print_count'] = $search_count;

                    $per_page = 0;
                    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                    $per_page = 13; //how much records you want to show
                    $adjacents = 13; //gap between pages after number of adjacents
                    $offset = ($page - 1) * $per_page;
                    $data['offset_count'] = $offset;

                    $reload = 'serach_records';

                    $total_pages = ceil($search_count / $per_page);
                    $data['rec_count'] = $search_count;
                    $data['pages'] = $total_pages;
                    $data['no_of_paginations'] = $total_pages;
                    $data['per_page'] = $per_page;


                    if ($search_count > 0) {

                        $data['search_listing'] = $this->search_model->search_card_data_display_data($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $offset, $per_page);
                        $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
                    }


                    $this->load->view('user/ajax_search_br_split_view',$data);//ajax_search_view', $data);
                } else {
                    echo '<br><div align=left style=padding-left:251px padding-top:241px ><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
                }
            }
			
			
			
			
			   function serach_records_alert() {

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                if ($this->input->post('txt_search')) {


                    $search_txt = trim($this->input->post('txt_search'));
                    $data['search_txt'] = $search_txt;

                    $this->load->model('search_model');
                    $this->load->model('user_model');
                    $this->load->model('re_print_model');


                    $access_epf = $this->session->userdata('loguser_tbl_id');
                    $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                    $branch_state = $access_user_infor['brz_state'];
                    $branch_code = $access_user_infor['branch'];
                    $region_code = $access_user_infor['region'];
                    $zone_code = $access_user_infor['zone'];
                    $act_name = $access_user_infor['users_name'];
                    $print_only = $access_user_infor['print_only'];
                    $data['print_facility'] = $print_only;



                    $search_count = $this->search_model->search_card_data_count_mannual_alert($branch_state, $branch_code, $region_code, $zone_code, $search_txt);
                    $data['search_count'] = $search_count;

                    $data['print_count'] = $search_count;

                    $per_page = 0;
                    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                    $per_page = 13; //how much records you want to show
                    $adjacents = 13; //gap between pages after number of adjacents
                    $offset = ($page - 1) * $per_page;
                    $data['offset_count'] = $offset;

                    $reload = 'serach_records_alert';

                    $total_pages = ceil($search_count / $per_page);
                    $data['rec_count'] = $search_count;
                    $data['pages'] = $total_pages;
                    $data['no_of_paginations'] = $total_pages;
                    $data['per_page'] = $per_page;


                    if ($search_count > 0) {

                        $data['search_listing'] = $this->search_model->search_card_data_display_for_mannual($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $offset, $per_page);
                        $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
                    }


                    $this->load->view('user/ajax_search_br_split_view',$data);//ajax_search_view', $data);
                } else {
                    echo '<br><div align=left style=padding-left:251px padding-top:241px ><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
                }
            }

			
			
			
			
			
			
			
			
			
			

            function search_print_mic_card() {

                $this->check_logged_in();
                $id = $this->uri->segment(3);
                $this->load->model('daily_policy_model');
                $this->load->model('user_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];



                $poliy_infor = $this->daily_policy_model->get_daily_policy_data($branch_state, $branch_code, $region_code, $zone_code, $id);

                //var_dump($poliy_infor);
                $buy_hand_state = $poliy_infor['MICBYHND'];
                $city_state = $poliy_infor['MICCITY'];

                if ($buy_hand_state == 'B') { ///daily_mic_by_hand_list
                    if ($this->daily_policy_model->update_by_search_and_proceeds($access_epf, $act_name, $buy_hand_state, $city_state, $id)) {

                        redirect('user/daily_mic_by_hand_list');
                    }
                } else {

                    if ($city_state == 'Y') {

                        if ($this->daily_policy_model->update_by_search_and_proceeds($access_epf, $act_name, $buy_hand_state, $city_state, $id)) {

                            redirect('user/daily_mic_by_colombo_list');
                        }
                    } else {

                        if ($this->daily_policy_model->update_by_search_and_proceeds($access_epf, $act_name, $buy_hand_state, $city_state, $id)) {

                            redirect('user/daily_mic_by_outstation_list');
                        }
                    }
                }
            }

            function search_printed_cards() {

                $this->check_logged_in();
                $data['sort_order'] = "B";

                $this->load->model('card_model');
                $this->load->model('user_model');
                $this->load->model('card_serial_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);


                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];


                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/search_printed_cards_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function jx_search_printed_cards() { //padding-left:25px;
                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');
                $this->load->model('search_model');
                $this->load->model('re_print_model');



                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $print_only = $access_user_infor['print_only'];
                $data['print_facility'] = $print_only;



                if ($this->input->post('seek_data') != NULL) {



                    //<div align="right"><div class="my_close_note" > No Records Found in the Printer Spool</div></div>


                    $text_search = trim($this->input->post('seek_data'));
                    $search_type = trim($this->input->post('search_type'));


                    $rec_count = $this->search_model->search_printed_card_count2($branch_state, $branch_code, $region_code, $zone_code, $text_search, $search_type);
                    $data['print_count'] = $rec_count;

                    $per_page = 0;
                    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                    $per_page = 12; //how much records you want to show
                    $adjacents = 12; //gap between pages after number of adjacents
                    $offset = ($page - 1) * $per_page;
                    $data['offset_count'] = $offset;


                    $reload = 'jx_search_printed_cards';


                    $total_pages = ceil($rec_count / $per_page);
                    $data['rec_count'] = $rec_count;
                    $data['pages'] = $total_pages;
                    $data['no_of_paginations'] = $total_pages;
                    $data['per_page'] = $per_page;

                    if ($rec_count > 0) {

                        $data['rs_listing'] = $this->search_model->search_printed_card_count_display2($branch_state, $branch_code, $region_code, $zone_code, $text_search, $search_type, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                    }


                    $this->load->view('user/jx_search_printed_cards_view', $data);
                } else {
                    echo '<div align=left style=padding-left:157px><div class=my_close_note style=width:492px;> Please enter search criteria ..!</div></div>';
                }
            }

            function search_re_print_card() {



                $this->check_logged_in();
                $this->load->model('user_model');
                $this->load->model('re_print_model');

                $id = $this->uri->segment(3);
                $data['id'] = $id;
                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];



                $card_infor = $this->re_print_model->get_print_card_data($id);
                $data['title'] = $card_infor['PR_MICTITL'];
                $data['name'] = $card_infor['PR_MICNAME'];
                $data['address1'] = $card_infor['PR_MICADD1'];
                $data['address2'] = $card_infor['PR_MICADD2'];
                $data['policy_no'] = $card_infor['PR_MICPLNO'];
                $data['vehile_no'] = $card_infor['PR_MICVENO'];
                $data['cover_period'] = $card_infor['PR_MICPCOV'];
                $data['print_date'] = $card_infor['PR_PRINT_DATE'];
                $data['print_count'] = $card_infor['PR_COUNT'];


                $data['card_infor'] = $this->re_print_model->display_reason();




                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/search_re_print_card_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function jx_card_duplicate_send() {



                $policy_id = $this->input->post('policy_id');


                $this->load->model('user_model');
                $this->load->model('re_print_model');

                $id = $this->input->post('policy_id');
                $data['id'] = $this->input->post('policy_id');
                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $users_name = $access_user_infor['users_name'];



                //$card_infor           = $this->re_print_model->get_print_card_data($id);
                //$data['title']        = $card_infor['PR_MICTITL'];
                ////$data['name']         = $card_infor['PR_MICNAME'];
                //$data['address1']     = $card_infor['PR_MICADD1'];
                // $data['address2']     = $card_infor['PR_MICADD2'];
                //$data['policy_no']    = $card_infor['PR_MICPLNO'];
                //$data['vehile_no']    = $card_infor['PR_MICVENO'];
                //$data['cover_period'] = $card_infor['PR_MICPCOV'];
                //$data['print_date']   = $card_infor['PR_PRINT_DATE'];



                $reason_id = $this->input->post('search_type');
                $dbResult = $this->re_print_model->add_duplicate_cards($id, $branch_state, $branch_code, $region_code, $zone_code, $access_epf, $users_name, $reason_id);

                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function search_confirm_card() {

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);

                $this->check_logged_in();
                $this->load->model('user_model');
				$this->load->model('region_model');
				$this->load->model('branch_model');
                $this->load->model('re_print_model');
                $this->load->model('daily_policy_model');
				
				

                $epf_user = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($epf_user);

                $users_name = $access_user_infor['users_name'];
				$pbranch    = $access_user_infor['branch'];
				$pregion    = $this->branch_model->get_regon_code($pbranch);//$access_user_infor['region'];
				$pzone      = $this->region_model->get_zone_code($pregion);////$access_user_infor['zone'];
				
				///branch,region,zone

                $pr_mic_id = $this->uri->segment(3);


                $card_infor = $this->re_print_model->get_print_card_data($pr_mic_id);
                $buy_hand_state = $card_infor['PR_MICBYHND'];
                $city_state = $card_infor['PR_MICCITY'];

                //echo  $this->re_print_model->send_duplicate_card_to_printer($pr_mic_id,$epf_user,$users_name);

                if ($buy_hand_state == 'B') { ///daily_mic_by_hand_list
                    if ($this->re_print_model->send_duplicate_card_to_printer_branch_level($pr_mic_id, $epf_user, $users_name,$pbranch,$pregion,$pzone)){ //// ealier ///send_duplicate_card_to_printer($pr_mic_id, $epf_user, $users_name)) {
                        //send_duplicate_card_to_printer_branch_level($pr_mic_id, $epf_user, $users_name,$branch,$reagon,$zone)
                        redirect('user/daily_mic_by_hand_list');
						
                    }
                } else {

                    if ($city_state == 'Y') {


                        if ($this->re_print_model->send_duplicate_card_to_printer_branch_level($pr_mic_id, $epf_user, $users_name,$pbranch,$pregion,$pzone)) {

                            redirect('user/daily_mic_by_colombo_list');
                        }
                    } else {

                        if ($this->re_print_model->send_duplicate_card_to_printer_branch_level($pr_mic_id, $epf_user, $users_name,$pbranch,$pregion,$pzone)) {

                            redirect('user/daily_mic_by_outstation_list');
                        }
                    }
                }
            }

            function fleet_policy_infor() {


                $this->check_logged_in();
                $data[''] = "";

                $this->load->model('user_model');
                $this->load->model('excel_model');


                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                $data['access_epf'] = $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];
                $fleet_allow = $access_user_infor['upload_allow_fleet'];

                $no_records = $this->excel_model->export_excel_all_fleets($branch_state, $branch_code, $region_code, $zone_code);
                $data['no_records'] = $no_records;

                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/fleet_policy_view', $data);
                $this->load->view('main_page/footer_view');
            }
			
			
			function upload_fleet_policy_single(){
			
			
			
			    $this->check_logged_in();
                $data[''] = "";

                $this->load->model('user_model');
                $this->load->model('excel_model');
				$this->load->model('fleet_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                $data['access_epf'] = $access_epf;

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];
                $fleet_allow = $access_user_infor['upload_allow_fleet'];

                $no_records = $this->excel_model->export_excel_all_fleets($branch_state, $branch_code, $region_code, $zone_code);
                $data['no_records'] = $no_records;
				
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<div class="validation fielderror" style="width:278px;">', '</class>');
				$this->form_validation->set_rules('vehicle_no', 'Vehicle number', 'trim|required|xss_clean|max_length[10]');
				$this->form_validation->set_rules('policy_no', 'Policy Number', 'trim|required|xss_clean|max_length[16]');
				$this->form_validation->set_rules('periode', 'Policy Commencement Date', 'trim|required|xss_clean|max_length[10]');
				$this->form_validation->set_rules('periods', 'Policy Expiry Date', 'trim|required|xss_clean|max_length[10]');
                $this->form_validation->set_rules('name', 'Customer Name', 'trim|required|xss_clean|max_length[200]');
                $this->form_validation->set_rules('address', 'Customer Address', 'trim|required|xss_clean|max_length[220]');
				
				if ($this->form_validation->run() == FALSE) {
				
					
						$this->load->view('main_page/header_view', $data);

                                       if ($fleet_allow == 0) {

                    $data['err_msg'] = "<div class='validation fielderror'>You Don't Have Privileges To Upload</div>";

                    $this->load->view('user/upload_fleet_policy_error', $data);

                     }else{




						$this->load->view('user/fleet_policy_single_view', $data);

}
						$this->load->view('main_page/footer_view');

					
				} else {
					
						$vehicle_no = $this->input->post('vehicle_no');
						$policy_no  = $this->input->post('policy_no');
						$periods     = $this->input->post('periods');
						$periode     = $this->input->post('periode');
						$name       = $this->input->post('name');
						$address1    = $this->input->post('address');
						
						$address2   = $this->input->post('address2');
						$remarks     = $this->input->post('reason');
						
						
						$period     = $periods." to ".$periode;
						
						
						$iis_start  = trim(str_replace("-", "", $periods));
						$iis_starte = trim(str_replace("-", "", $periode));
					   
						$update=$this->fleet_model->add_fleed_to_iis($policy_no, $vehicle_no, $period, $name, $address1, $address2, $access_epf, $act_name, $branch_state, $branch_code, $region_code, $zone_code, $iis_start, $iis_starte,$remarks);

                        if ($update >0 ) {
						
						 redirect('user/fleet_printer_policy_list'); 
						 
						} 
						///http://localhost/mic/index.php/user/fleet_printer_policy_list
						
						/*
						
						          $itemExt = explode("to", $period);
                    $count = count($itemExt);
                    $string_need = "";
                    $data_item = array();

                    for ($m = 0; $m < $count; $m++) {

                        $data_item[$m] = trim(str_replace("-", "", $itemExt[$m]));///2014-07-03 to 2015-07-02


                        //$sh_miccmdt  = $data_item[0];
                        // $sh_micexdt  = $data_item[1]; 			 
                    }

                      $sh_miccmdt       = $data_item[0]; 
					  $excel_sh_miccmdt = $this->_query_date_format($sh_miccmdt);//print"<br>";	
                      $sh_micexdt = $data_item[1];
					
					  $excel_sh_micexdt =  $this->_query_date_format($sh_micexdt) ;///print"<br>";	
					   
					  $excel_period = $excel_sh_miccmdt." to ".$excel_sh_micexdt;
					  
					 $iis_d1=$this->_query_date_format_iis($sh_miccmdt);
					 $iis_d2=$this->_query_date_format_iis($sh_micexdt);
					  
INSERT INTO `shedule_fleet_policy_data` (`sh_policy_no`, `sh_vehicle_no`, `sh_period`, `sh_cust_name`, `sh_address_1`, `sh_address_2`, `upload_epf`, `upload_user`, `sh_br_state`, `sh_br_code`, `sh_re_code`, `sh_zo_code`, `sh_miccmdt`, `sh_micexdt`, `back_end_update`, `sh_db_update`) VALUES ('RMMPCP/C00001448', 'WP-GB-7764', '2014-11-20 to 2015-11-19', 'THAVARAPPERUMA �H D', '76, SOYSA MW KORALAINNA', 'GONAPOLA_JUNCTION', '000568', 'Shevon', 'B', 'A', '0', '0', '20141120', '20151119', 'N', '2014-10-30 17:55:36')
                     echo $this->fleet_model->add_fleed_to_iis($pol_no, $veh_no, $excel_period, $name, $address1, $address2, $access_epf, $act_name, $branch_state, $branch_code, $region_code, $zone_code, $iis_d1, $iis_d2);

						
						
						*/
                      //$sh_miccmdt       = $data_item[0]; ///
					// echo  $excel_sh_miccmdt = $this->_query_date_format($sh_miccmdt);//print"<br>";	
                     // $sh_micexdt = $data_item[1];
					
					 // $excel_sh_micexdt =  $this->_query_date_format($sh_micexdt) ;///print"<br>";	
					 //  
					 // $excel_period = $excel_sh_miccmdt." to ".$excel_sh_micexdt;
					//  
					 //$iis_d1=$this->_query_date_format_iis($sh_miccmdt);
					 // $iis_d2=$this->_query_date_format_iis($sh_micexdt);
					 
					 
					 
						
						
						$this->load->view('main_page/header_view', $data);
						$this->load->view('user/fleet_policy_single_view', $data);
						$this->load->view('main_page/footer_view');
				
				
				}

			
			
			}

            function upload_fleet_policy() {

                $this->check_logged_in();
                $data['upload_msg'] = "";
                $this->load->model('user_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                $data['access_epf'] = $access_epf;
                $fleet_allow = $access_user_infor['upload_allow_fleet'];
				$branch_code = $access_user_infor['branch'];


                $this->load->view('main_page/header_view', $data);
				
				if ($branch_code !="A"){
				
				    $data['err_msg'] = "<div class='validation fielderror'>You Don't Have Privileges To Upload</div>";

                    $this->load->view('user/upload_fleet_policy_error', $data);
				
				}
				else {
				

                if ($fleet_allow == 0) {

                    $data['err_msg'] = "<div class='validation fielderror'>You Don't Have Privileges To Upload</div>";

                    $this->load->view('user/upload_fleet_policy_error', $data);
                } else {
                    $this->load->view('user/upload_fleet_policy_view', array('error' => ' '));
                }
				
				}
				
				
                $this->load->view('main_page/footer_view');
            }

            function do_upload_fleet_policy() {


                $this->check_logged_in();
                error_reporting(0);
                $this->load->model('fleet_model');
				 $this->load->model('user_model');


                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'xls';
                $config['max_size'] = '13085';


                $data['upload_msg'] = "";

                $name = $_FILES['userfile']['name']; // get file name from form
                $fileNameParts = explode('.', $name); // explode file name to two part
                $fileExtension = end($fileNameParts); // give extension
                $fileExtension = strtolower($fileExtension); // convert to lower case
                $now = time();
                $gmt = local_to_gmt($now);
                $cust_stamp = $gmt;
                $encripted_pic_name = md5($name . $cust_stamp) . '.' . $fileExtension;  // new file name
                $config['file_name'] = substr($encripted_pic_name, 0, 12); //set file name


                $this->load->library('upload', $config);

                $xx = array('upload_data' => $this->upload->data());
                $mimetype = $xx['upload_data']['file_type'];

                //var_dump('Mime: ' . $mimetype);
                //var_dump($_FILES);
                //$this->load->library('Upload', $config);

                if (!$this->upload->do_upload()) {
                    $error = array('error' => $this->upload->display_errors());

                    $data['upload_msg'] = '<div class="validation fielderror">' . $this->upload->display_errors() . '</div>';

                    $this->load->view('main_page/header_view', $data);
                    $this->load->view('user/upload_fleet_policy_view', $data);
                    $this->load->view('main_page/footer_view');
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    //print_r($data);

                    error_reporting(E_ALL ^ E_NOTICE);


                    $FilePath = './uploads/' . substr($encripted_pic_name, 0, 12) . '.' . $fileExtension;




                    $parameters = array('file' => $FilePath,
                        'store_extended_info' => true,
                        'outputEncoding' => '');

                    $this->load->library('Spreadsheet_Excel_Reader', $parameters);

                    //$data['xls_data'] = $this->spreadsheet_excel_reader->dump(false, false);
                    $list_data = $this->spreadsheet_excel_reader->dump(false, false);
                    $myrows = $this->spreadsheet_excel_reader->sheets[0]['numRows'];
                    $mynumCols = $this->spreadsheet_excel_reader->sheets[0]['numCols'];

                    $data['myrows'] = $myrows;

                    $this->load->view('main_page/header_view', $data);
                    $this->load->view('user/upload_fleet_policy_sucess_view', $data);
                    $this->load->view('main_page/footer_view');
                }
            }
            

            function _query_date_format($report_date) {

        			        $date_from_yy = substr($report_date, 6,4);
					$date_from_dd = substr($report_date, 0, 2);
					$date_from_mm = substr($report_date, 3, 2);
			
					$query_date = $date_from_yy . '-' . $date_from_mm . '-' . $date_from_dd;
			
					return $query_date;
                   }
				   
				   
				 function _query_date_format_iis($report_date) {

        			        $date_from_yy = substr($report_date, 6,4);
					$date_from_dd = substr($report_date, 0, 2);
					$date_from_mm = substr($report_date, 3, 2);
			
					$query_date = $date_from_yy . $date_from_mm . $date_from_dd;
			
					return $query_date;
                   }   

            function upload_fleet_policy_submit() {

		                $this->load->model('fleet_model');
                $this->load->model('user_model');

                $total = ceil(count($_POST)/6);  //$this->input->post('total');
               // print('<pre>');print_r($_POST); echo $total; exit();
                for ($x = 1; $x <= $total; $x++) {


                    $pol_no = trim($this->input->post('hdd_pol_no_' . $x));
                    $veh_no = trim($this->input->post('hdd_veh_no_' . $x));
                    //$period = trim($this->input->post('hdd_period_' . $x));
		            $period = trim(strtolower($this->input->post('hdd_period_' . $x))); print"<br>";	///2013-12-09 to 2015-12-08
                    $name = $this->input->post('hdd_name_' . $x);

                    $address1 = $this->input->post('hdd_add_1' . $x);
                    $address2 = $this->input->post('hdd_add_2' . $x);

                    $access_epf = $this->session->userdata('loguser_tbl_id');
                    $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                    $branch_state = $access_user_infor['brz_state'];
                    $branch_code = $access_user_infor['branch'];
                    $region_code = $access_user_infor['region'];
                    $zone_code = $access_user_infor['zone'];
                    $act_name = $access_user_infor['users_name'];


                    $itemExt = explode("to", $period);
                    $count = count($itemExt);
                    $string_need = "";
                    $data_item = array();

                    for ($m = 0; $m < $count; $m++) {

                        $data_item[$m] = trim(str_replace("-", "", $itemExt[$m]));///2014-07-03 to 2015-07-02


                        //$sh_miccmdt  = $data_item[0];
                        // $sh_micexdt  = $data_item[1]; 			 
                    }

                      $sh_miccmdt       = $data_item[0]; 
					  $excel_sh_miccmdt = $this->_query_date_format($sh_miccmdt);//print"<br>";	
                      $sh_micexdt = $data_item[1];
					
					  $excel_sh_micexdt =  $this->_query_date_format($sh_micexdt) ;///print"<br>";	
					   
					  $excel_period = $excel_sh_miccmdt." to ".$excel_sh_micexdt;
					  
					 $iis_d1=$this->_query_date_format_iis($sh_miccmdt);
					 $iis_d2=$this->_query_date_format_iis($sh_micexdt);
					 
					 $remarks = "FLEET";
					  

                     $this->fleet_model->add_fleed_to_iis($pol_no, $veh_no, $excel_period, $name, $address1, $address2, $access_epf, $act_name, $branch_state, $branch_code, $region_code, $zone_code, $iis_d1, $iis_d2,$remarks);







                }


                ?>
                <a href="<?php echo base_url()?>index.php/user/fleet_printer_policy_list">Please click here</a> <?php
            }

            function nexus() {

                $this->check_logged_in();

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $data['access_epf'] = $access_epf;

                $this->load->model('fleet_model');
                $this->load->model('user_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];
				$nexus_tab = $access_user_infor['nexus_tab_allow'];



                $data['rec_count'] = "0";
                $data['card_print_state'] = "A";
                $this->load->view('main_page/header_view', $data);
				
				if ( $nexus_tab == 'Y' ){ 
				
                $this->load->view('user/nexus_view', $data);
				}
				else
				{
				 
				 
                    $data['err_msg'] = "<div class='validation fielderror'>You Don't Have Privileges To Access</div>";

                    $this->load->view('user/nexus_policy_error', $data);
				
				}
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_list() { //echo $myVar = HO_SQL_PATH; 

                $this->check_logged_in();
                $data['sort_order'] = "A";

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(0);//E_ALL);
		$by_hand="";
		$city="";


                $this->load->model('card_model');
                $this->load->model('user_model');
                $this->load->model('card_serial_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);


                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $print_only = $access_user_infor['print_only'];
                $data['print_facility'] = $print_only;




                $rec_count = $this->nexus_model->display_daily_card_data_count_by_nexus_level($branch_state, $branch_code, $zone_code, $zone_code, "");
                $data['rec_count'] = $rec_count;


                $by_hand_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, "", "B");
                $data['by_hand_count'] = $by_hand_count;

                $colombo_list_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, "Y", "");
                $data['colombo_list_count'] = $colombo_list_count;

                $outstation_list_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, "N", "");
                $data['outstation_list_count'] = $outstation_list_count;




                $data['search_type'] = 0;
                $data['seek_data'] = "";


                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_card_search() {


                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('group_name')) {
                    $data['sort_order'] = $this->input->post('group_name');
                    $mys_sort = $this->input->post('group_name');
                } else {
                    $data['sort_order'] = "B";
                    $mys_sort = "B";
                }

                $data['sort_order'] = $mys_sort;


                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_nexus_card_search';

                if ($this->input->post('group_name') == 'A') {

                    $rec_count = $this->nexus_model->display_daily_card_data_count_for_nexus($branch_state, $branch_code, $region_code, $zone_code);
					
                } else {
				
                    if ($this->input->post('group_name') == 'C') {
                        $city = "Y";
                        $by_hand = "";
						
                    } else {

                        if ($this->input->post('group_name') == 'O') {

                            $city = "N";
                            $by_hand = "";
                        } else {

                            $city = "";
                            $by_hand = 'B';
                        }
                    }

                    $rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand);
                }

                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;


                if ($rec_count > 0) {


                    if ($this->input->post('group_name') == 'A') {


                        $data['rs_listing'] = $this->nexus_model->display_daily_card_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
						$city = "";
                        $by_hand = "";
						
                    } else {

                        $data['rs_listing'] = $this->nexus_model->display_daily_card_data_count_with_filter_display_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $offset, $per_page);
                        $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                    }
                } else {
                    $data['dsp_err'] = '<div class="validation fielderror" style="width:578px;">No Records Found ..!</div>';
                }
                
				$data['city']       = $city; 
				$data['by_hand']    = $by_hand; 
				$data['access_epf'] = $access_epf;
                $this->load->view('user/nexus/ajax_nexus_edited_view',$data);//ajax_nexus_view', $data);
            }

            function serach_records_for_nexus() {

                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                if ($this->input->post('txt_search')) {


                    $search_txt = trim($this->input->post('txt_search'));
                    $data['search_txt'] = $search_txt;

                    //$this->load->model('search_model');
                    $this->load->model('user_model');
                    $this->load->model('search_nexus_model');


                    $access_epf = $this->session->userdata('loguser_tbl_id');
                    $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                    $branch_state = $access_user_infor['brz_state'];
                    $branch_code = $access_user_infor['branch'];
                    $region_code = $access_user_infor['region'];
                    $zone_code = $access_user_infor['zone'];
                    $act_name = $access_user_infor['users_name'];
                    $print_only = $access_user_infor['print_only'];
                    $data['print_facility'] = $print_only;


                    $search_count = $this->search_nexus_model->search_card_data_count_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $search_txt);
                    $data['search_count'] = $search_count;

                    $data['print_count'] = $search_count;

                    $per_page = 0;
                    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                    $per_page = 14; //how much records you want to show
                    $adjacents = 14; //gap between pages after number of adjacents
                    $offset = ($page - 1) * $per_page;
                    $data['offset_count'] = $offset;

                    $reload = 'serach_records_for_nexus';

                    $total_pages = ceil($search_count / $per_page);
                    $data['rec_count'] = $search_count;
                    $data['pages'] = $total_pages;
                    $data['no_of_paginations'] = $total_pages;
                    $data['per_page'] = $per_page;


                    if ($search_count > 0) {

                        $data['search_listing'] = $this->search_nexus_model->search_card_data_display_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $offset, $per_page);
                        $data['paginater'] = $this->paginate_second($reload, $page, $total_pages, 6);
                    }


                    $this->load->view('user/nexus/ajax_search_nexus_view', $data);
                } else {
                    echo '<br><div align=left style=padding-left:251px padding-top:241px ><div class=my_close_note style=width:322px;> Please enter search criteria ..!</div></div>';
                }
            }

            function search_print_nexus_card() {

                $this->check_logged_in();
                $nexus_id = $this->uri->segment(3);
                $this->load->model('daily_policy_model');
                $this->load->model('user_model');
                $this->load->model('daily_policy_nexus_model');


                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];



                $poliy_infor = $this->daily_policy_nexus_model->get_daily_policy_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $nexus_id);
                $buy_hand_state = $poliy_infor['MICBYHND'];
                $city_state = $poliy_infor['MICCITY'];

                if ($buy_hand_state == 'B') {


                    if ($this->daily_policy_nexus_model->update_by_search_and_proceeds_for_nexus($access_epf, $act_name, $buy_hand_state, $city_state, $nexus_id)) {

                        redirect('user/daily_nexus_by_hand_list');
                    }
                } else {

                    if ($city_state == 'Y') {


                        if ($this->daily_policy_nexus_model->update_by_search_and_proceeds_for_nexus($access_epf, $act_name, $buy_hand_state, $city_state, $nexus_id)) {

                            redirect('user/daily_nexus_by_colombo_list'); ////redirect('user/daily_mic_by_colombo_list');
                        }
                    } else {

                        if ($this->daily_policy_nexus_model->update_by_search_and_proceeds_for_nexus($access_epf, $act_name, $buy_hand_state, $city_state, $nexus_id)) {

                            redirect('user/daily_nexus_by_outstation_list'); ///redirect('user/daily_mic_by_outstation_list');
                        }
                    }
                }
            }

            function daily_nexus_by_hand_list() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_nexus_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_by_hand_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_by_hand_list_serial() {

                $this->check_logged_in();
				$this->load->model('user_model');
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
                $data['pg_transfer'] = "daily_nexus_by_hand_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_by_hand_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_pinter_buy_hand_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

               	
				
				if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {

                        if ($this->input->post('printed_group_name') == "F") {


                            $print_state = "F";
                        } else {

                            $print_state = "N";
                        }
                    }
                }




                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, 'N');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_nexus_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {

                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;


                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {
				
				
				     if ($print_state == 'N' ){

                         $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, $offset, $per_page, 'N');
                     }
					 else
					 {
                         $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_display_order($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "B", $print_state, $offset, $per_page, 'N');
					 }    
						 $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/nexus/ajax_daily_nexus_bhand_view', $data); ///ajax_daily_print_spool_view',$data);
            }
			
			
			
			function upload_card_serial(){
			
			    $this->check_logged_in();
                $data['upload_msg'] = "";
                $this->load->model('user_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);
                $data['access_epf'] = $access_epf;
                $fleet_allow = $access_user_infor['upload_allow_fleet'];


                $this->load->view('main_page/header_view', $data);

                $this->load->view('user/upload_card_serial_view', array('error' => ' '));
                $this->load->view('main_page/footer_view');			
			
			}
			
			function do_upload_card_serial(){
			
			   
			                   $this->check_logged_in();
							    $this->load->model('user_model');
                error_reporting(0);
				$data['upload_serial']="";
                $this->load->model('fleet_model');
				$this->load->model('upload_serial_model');
				

                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'xls';
                $config['max_size'] = '13085';


                $data['upload_msg'] = "";

                $name = $_FILES['userfile']['name']; // get file name from form
                $fileNameParts = explode('.', $name); // explode file name to two part
                $fileExtension = end($fileNameParts); // give extension
                $fileExtension = strtolower($fileExtension); // convert to lower case
                $now = time();
                $gmt = local_to_gmt($now);
                $cust_stamp = $gmt;
                $encripted_pic_name = md5($name . $cust_stamp) . '.' . $fileExtension;  // new file name
                $config['file_name'] = "serail_".substr($encripted_pic_name, 0, 12); //set file name


                $this->load->library('upload', $config);

                $xx = array('upload_data' => $this->upload->data());
                $mimetype = $xx['upload_data']['file_type'];

                //var_dump('Mime: ' . $mimetype);
                //var_dump($_FILES);
                //$this->load->library('Upload', $config);

                if (!$this->upload->do_upload()) {
                    $error = array('error' => $this->upload->display_errors());

                    $data['upload_msg'] = '<div class="validation fielderror">' . $this->upload->display_errors() . '</div>';

                    $this->load->view('main_page/header_view', $data);
                    $this->load->view('user/upload_card_serial_view', $data);
                    $this->load->view('main_page/footer_view');
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    //print_r($data);

                    error_reporting(E_ALL ^ E_NOTICE);


                    $FilePath = './uploads/' ."serail_". substr($encripted_pic_name, 0, 12) . '.' . $fileExtension;




                    $parameters = array('file' => $FilePath,
                        'store_extended_info' => true,
                        'outputEncoding' => '');

                    $this->load->library('Spreadsheet_Excel_Reader', $parameters);

                    //$data['xls_data'] = $this->spreadsheet_excel_reader->dump(false, false);
                    $list_data = $this->spreadsheet_excel_reader->dump(false, false);
                    $myrows = $this->spreadsheet_excel_reader->sheets[0]['numRows'];
                    $mynumCols = $this->spreadsheet_excel_reader->sheets[0]['numCols'];

                 
					for ($a = 1; $a < $myrows; $a++){
					
					$serial      = $this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][1];
					$pol_no      = $this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][3];
					$vehicle_no  = $this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][2];
					$period      = $this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][4];
					$this->upload_serial_model->get_upload_card_id($pol_no,$vehicle_no,$period,$serial);
					 
					}

                    $this->load->view('main_page/header_view', $data);
					$data['upload_serial'] ='<div class="validation sucess" style="width:385px;padding-left:4px;"> Serial Successfully Uploaded ..!</class>';
					
                    $this->load->view('user/do_upload_card_serial_view_sucess', $data);
                    $this->load->view('main_page/footer_view');
                }

			
			
			
			}
			

            function daily_nexus_by_colombo_list() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
                $data['card_print_state'] = "I";
				$this->load->model('user_model');
                $data['pg_transfer'] = "daily_nexus_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_colombo_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_by_colombo_list_serial() {

                $this->check_logged_in();
				$this->load->model('user_model');
                $data['rec_count'] = "0";
                $data['card_print_state'] = "P";
                $data['pg_transfer'] = "daily_nexus_by_colombo_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexus_colombo_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_pinter_nexus_colombo_data_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {
                        $print_state = "N";
                    }
                }



                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', 'N');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_nexus_colombo_data_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;








                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {
				
				     if ($print_state == 'N' ){

                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', $offset, $per_page, 'N');
                    }
					else
					{
                    $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display_order($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'Y', $offset, $per_page, 'N');
					}
				   
				    $data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                //$this->load->view('user/ajax_daily_print_spool_view',$data);
                $this->load->view('user/nexus/ajax_daily_nexus_bhand_view', $data);
            }

            function daily_nexus_by_outstation_list() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
				$this->load->model('user_model');
                $data['card_print_state'] = "I";
                $data['pg_transfer'] = "daily_nexus_by_outstation_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexsus_by_outstation_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_nexus_by_outstation_list_serial() {

                $this->check_logged_in();
                $data['rec_count'] = "0";
				$this->load->model('user_model');
                $data['card_print_state'] = "P";
                $data['pg_transfer'] = "daily_nexus_by_outstation_list_serial";
                $this->load->view('main_page/header_view', $data);
                $this->load->view('user/nexus/daily_nexsus_by_outstation_list_view', $data);
                $this->load->view('main_page/footer_view');
            }

            function daily_pinter_nexus_outstation_data_list() {


                $this->load->model('user_model');
                $this->load->model('card_printer_send_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                if ($this->input->post('printed_group_name') == "A") {

                    $print_state = "N";
                } else {
                    if ($this->input->post('printed_group_name') == "P") {

                        $print_state = "Y";
                    } else {
                        $print_state = "N";
                    }
                }



                $rec_count = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', 'N');
                $data['print_count'] = $rec_count;

                $per_page = 0;
                $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                $per_page = 16; //how much records you want to show
                $adjacents = 16; //gap between pages after number of adjacents
                $offset = ($page - 1) * $per_page;
                $data['offset_count'] = $offset;


                $reload = 'daily_pinter_nexus_outstation_data_list'; //daily_pinter_buy_hand_list';



                if ($this->input->post('printed_group_name')) {
                    $data['card_print_state'] = $this->input->post('printed_group_name');
                    $card_print_state = $this->input->post('printed_group_name');
                } else {
                    $data['card_print_state'] = "A";
                    $card_print_state = "A";
                }

                $data['card_print_state'] = $card_print_state;








                $total_pages = ceil($rec_count / $per_page);
                $data['rec_count'] = $rec_count;
                $data['pages'] = $total_pages;
                $data['no_of_paginations'] = $total_pages;
                $data['per_page'] = $per_page;

                if ($rec_count > 0) {
                    
					 if ($print_state == 'N' ){
                         $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', $offset, $per_page, 'N');
                     }
					 else
					 {
                         $data['rs_listing'] = $this->card_printer_send_model->get_daily_card_print_send_count_for_none_by_hand_display_order($branch_state, $branch_code, $region_code, $zone_code, $access_epf, "", $print_state, 'N', $offset, $per_page, 'N');
					 }
					
					$data['paginater'] = $this->paginate($reload, $page, $total_pages, 6);
                }



                $this->load->view('user/nexus/ajax_nexus_outstation_data_list_view_update', $data);
            }

            function send_by_hand_information_for_nexus() {

                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];

                //$rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code,"","B");

                $dbResult = $this->nexus_model->update_by_hand_printer_list_for_nexus($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "B");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function send_colombo_information_for_nexus() {

                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];


                //$rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code,"Y","");

                $dbResult = $this->nexus_model->update_by_hand_printer_list_colombo_none_colombo_for_nexus($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, "", "Y");


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function send_nexus_out_station_information() {

                $this->load->model('user_model');
                $this->load->model('nexus_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);

                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];
                $act_name = $access_user_infor['users_name'];


                //$rec_count = $this->nexus_model->display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code,'N','');

                $dbResult = $this->nexus_model->update_by_hand_printer_list_colombo_none_colombo_for_nexus($this->session->userdata('loguser_tbl_id'), $act_name, $branch_state, $branch_code, $region_code, $zone_code, '', 'N');


                if ($dbResult > 0) {

                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function daily_mic_list_alert() {

                $this->check_logged_in();
                $data['sort_order'] = "A";

                $this->load->model('card_model');
                $this->load->model('user_model');
                $this->load->model('card_serial_model');

                $access_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($access_epf);



                $branch_state = $access_user_infor['brz_state'];
                $branch_code = $access_user_infor['branch'];
                $region_code = $access_user_infor['region'];
                $zone_code = $access_user_infor['zone'];

                $fleet_allow = $access_user_infor['upload_allow_fleet'];
                $man_is_policy = 1;// $access_user_infor['upload_allow_nexus'];


                $rec_count = $this->card_model->display_daily_card_data_count($branch_state, $branch_code, $zone_code, $zone_code, "");
                $data['rec_count'] = $rec_count;




                $by_hand_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "", "B");
                $data['by_hand_count'] = $by_hand_count;

                $colombo_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "Y", "");
                $data['colombo_list_count'] = $colombo_list_count;

                $outstation_list_count = $this->card_model->display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, "N", "");
                $data['outstation_list_count'] = $outstation_list_count;





                $data['search_type'] = 0;
                $data['seek_data'] = "";


                ini_set('display_errors', 1);
                ini_set('log_errors', 1);
                error_reporting(E_ALL);


                $this->load->view('main_page/header_view', $data);

                if ($man_is_policy == 0) {

                    $data['err_msg'] = "<div class='validation fielderror'>You don't sufficient have Privileges ..!</div>";

                    $this->load->view('user/man_is_policy_error', $data);
                } else {

                    $this->load->view('user/user_alert_view', $data);
                }
                $this->load->view('main_page/footer_view');
            }

            function list_card_information() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['MICPLNO'];
                $data['MICPCOV'] = $card_data['MICPCOV'];
                $data['MICVENO'] = $card_data['MICVENO'];
                $data['MICNAME'] = $card_data['MICNAME'];
                $data['MICADD1'] = $card_data['MICADD1'];
                $data['MICADD2'] = $card_data['MICADD2'];
                $data['MICPRDU'] = $card_data['MICPRDU'];


                $this->load->view('user/list_card_information_view', $data);
            }

            function list_card_information_daily() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_daily_card_policy_data($this->uri->segment(4));

                $data['MICPLNO']  = $card_data['MICPLNO'];
                $data['MICPCOV']  = $card_data['MICPCOV'];
                $data['MICVENO']  = $card_data['MICVENO'];
                $data['MICNAME']  = $card_data['MICNAME'];
                $data['MICADD1']  = $card_data['MICADD1'];
                $data['MICADD2']  = $card_data['MICADD2'];
                $data['MICPRDU']  = $card_data['MICPRDU'];
				$data['MICNAME2'] = $card_data['MICNAME2'];
                $data['PR_COUNT'] = 0;


                $this->load->view('user/list_card_information_view', $data);
            }

            function list_card_information_display() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['PR_MICPLNO'];
                $data['MICPCOV'] = $card_data['PR_MICPCOV'];
                $data['MICVENO'] = $card_data['PR_MICVENO'];
                $data['MICNAME'] = $card_data['PR_MICNAME'];
                $data['MICADD1'] = $card_data['PR_MICADD1'];
                $data['MICADD2'] = $card_data['PR_MICADD2'];
                $data['MICPRDU'] = $card_data['PR_MICPRDU'];
                $data['MICTYPE'] = $card_data['PR_TYPRE'];


                $this->load->view('user/list_card_information_spool_view', $data);
            }

            function list_card_printed_information() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');
				$this->load->model('user_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data($this->uri->segment(4));

                $data['MICPLNO']    = $card_data['PR_MICPLNO'];
                $data['MICPCOV']    = $card_data['PR_MICPCOV'];
                $data['MICVENO']    = $card_data['PR_MICVENO'];
                $data['MICNAME']    = $card_data['PR_MICNAME'];
                $data['MICADD1']    = $card_data['PR_MICADD1'];
                $data['MICADD2']    = $card_data['PR_MICADD2'];
                $data['MICPRDU']    = $card_data['PR_MICPRDU'];
                $data['MICTYPE']    = $card_data['PR_TYPRE'];
				$data['PR_COUNT']   = $card_data['PR_CARD_PRINT'];
				$mic_id             = $card_data['PR_MIC_ID'];
                $data['PR_DATE']    = $card_data['PR_PRINT_DATE'];
				$data['PR_MICNAME2']= $card_data['PR_MICNAME2'];
				$data['PR_ACT_NAME']= $card_data['PR_ACT_NAME'];
				
				$dat['PR_MICBRCD']  = $card_data['PR_MICBRCD'];
				$data['usr_print_branch']  = $card_data['PR_MICBRCD'];
				
								
				if ($card_data['PR_MICRESEPF']!=""){
				
				    $usr_branch=$this->user_model->get_user_print_branch($card_data['PR_MICRESEPF']);
					
					if ($usr_branch=='A'){
					    
						$dat['usr_print_branch']  = "HEAD OFFICE";
					}
					else
					{
					   $this->load->model('branch_model');
					   
					   $dat['usr_print_branch']  = $this->branch_model->get_branch_name($usr_branch);
					}
				
				}
				
				else
				{
				$dat['usr_print_branch']  = "";
				}
				
				
								
				if ($card_data['PR_CARD_PRINT'] > 0 ) {
				    
				    $data['reason']  =	$this->card_infor_model->card_duplicate_reason($mic_id) ;
				
				} 


                $this->load->view('user/printed_card_information_view', $data);
            }

            function card_nexus_information() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_printed_card_policy_data_for_mic($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['PR_MICPLNO'];
                $data['MICPCOV'] = $card_data['PR_MICPCOV'];
                $data['MICVENO'] = $card_data['PR_MICVENO'];
                $data['TITLE']   = $card_data['PR_MICTITL'];
                $data['MICNAME'] = $card_data['PR_MICNAME'];
                $data['MICADD1'] = $card_data['PR_MICADD1'];
                $data['MICADD2'] = $card_data['PR_MICADD2'];
                $data['MICPRDU'] = $card_data['PR_MICPRDU'];
                $data['MICTYPE'] = $card_data['PR_TYPRE'];


                $this->load->view('user/nexus/card_nexus_view', $data);
            }
			
			
			function card_nexus_information_daily() {



                $id = $this->uri->segment(3);

                $this->load->model('card_infor_model');

                $card_data = $this->card_infor_model->get_nexus_card_policy_data_daily($this->uri->segment(4));

                $data['MICPLNO'] = $card_data['MICPLNO'];
                $data['MICPCOV'] = $card_data['MICPCOV'];
                $data['MICVENO'] = $card_data['MICVENO'];
                $data['TITLE']   = $card_data['MICTITL'];
                $data['MICNAME'] = $card_data['MICNAME'];
                $data['MICADD1'] = $card_data['MICADD1'];
                $data['MICADD2'] = $card_data['MICADD2'];
                $data['MICPRDU'] = $card_data['MICPRDU'];
                $data['MICTYPE'] = "N";


                $this->load->view('user/nexus/card_nexus_view', $data);
            }
			
			

            function update_nexus_serial() {

              
             if (( $this->input->post('text_data') == NULL) && ( $this->input->post('nex_id') == NULL)) {

                    $jason_output['result'] = -3;
                    echo json_encode($jason_output);
                } else {
                    $this->load->model('card_infor_model');

                    $nex_text = trim($this->input->post('text_data'));
                    $length = strlen($nex_text);
                    $nex_id = $this->input->post('nex_id');


                    if ($length != 71) {

                        $dbResult = -10;
                    } else {
                        if ($this->card_infor_model->check_serial_exists($nex_text) == 0) {

                            $dbResult = $this->card_infor_model->update_mic_serial_data($nex_id, $nex_text);

                            if ($dbResult > 0) {


                                $jason_output['result'] = $dbResult;
                            } else {

                                $jason_output['result'] = 0;
                            }
                        } else {
                            $dbResult = -999;
                        }
                    }
                    $jason_output['result'] = $dbResult;


                    echo json_encode($jason_output);
                }
            }

            function re_send_to_printer() {

                $this->load->model('card_printer_send_model');
				$this->load->model('re_print_model'); 
				$this->load->model('user_model'); 
				

                $pudh_id        = trim($this->input->post('pudh_id'));
				
				$re_request_epf = $this->session->userdata('loguser_tbl_id');
                $access_user_infor = $this->user_model->get_user_information_by_epf($re_request_epf);

                    //$branch_state = $access_user_infor['brz_state'];
                   // $branch_code = $access_user_infor['branch'];
                    //$region_code = $access_user_infor['region'];
                    //$zone_code = $access_user_infor['zone'];
					 $request_name = $access_user_infor['users_name'];

				
				
					
				

                $dbResult  = $this->card_printer_send_model->push_to_printer($pudh_id);
				


                if ($dbResult > 0) {
                    
                    $this->re_print_model->push_to_printer_cards($pudh_id,$re_request_epf,$request_name);					
                    $jason_output['result'] = $dbResult;
                } else {

                    $jason_output['result'] = 0;
                }

                $jason_output['result'] = $dbResult;


                echo json_encode($jason_output);
            }

            function update_mic_serial() {





                if (( $this->input->post('text_data') == NULL) && ( $this->input->post('mic_id') == NULL)) {

                    $jason_output['result'] = -3;
                    echo json_encode($jason_output);
                } else {
                    $this->load->model('card_infor_model');

                    $nex_text = trim($this->input->post('text_data'));
                    $length = strlen($nex_text);
                    $nex_id = $this->input->post('mic_id');


                    if ($length != 13) {

                        $dbResult = -10;
                    } else {
                        if ($this->card_infor_model->check_serial_exists($nex_text) == 0) {

                            $dbResult = $this->card_infor_model->update_mic_serial_data($nex_id, $nex_text);

                            if ($dbResult > 0) {


                                $jason_output['result'] = $dbResult;
                            } else {

                                $jason_output['result'] = 0;
                            }
                        } else {
                            $dbResult = -999;
                        }
                    }
                    $jason_output['result'] = $dbResult;


                    echo json_encode($jason_output);
                }
            }

        }
        ?>