<?php

class Member extends CI_Controller {

    public function __construct() {
        parent :: __construct();

        $this->load->helper(array('url', 'form', 'security', 'date'));
        $this->load->library('session');
        $data['base_url'] = base_url();
    }

    public function index() {

        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            redirect('admin/index');
        } else {

            redirect('member/login');
            $data['message'] = "";
            $data['usr_login'] = 'N';
        }
    }

    public function login() {

        $data['message'] = "";
        $data['usr_login'] = '';
        $access_epf_no = $this->input->post('EPF');
        $access_level = $this->input->post('auth');

        // $userdata = array(
        //'loguser_username' => $access_epf_no,
        //'loguser_login' => 'A',
        ////'loguser_level' => $access_level,
        //'loguser_first_name' => $access_epf_no,
        //'loguser_last_name' => $access_epf_no,
        //'loguser_tbl_id' => $access_epf_no
        ///);
        ///$this->session->set_userdata($userdata);


        if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {

            redirect('admin/index');
        } else {



            if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'U')) {

           
                redirect('user/daily_mic_list');
            } else {

                if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'M')) {

                    if ($this->session->userdata('loguser_branch') == 'A') {

                        redirect('manager/manager_dashboard');
                    } else {
                        redirect('manager/authorize_print');
                    }

                    
                } else {
                    echo "access denied";
                }
            }



            ///redirect('http://link/');
        }
    }

    public function check_logged_in() {

        $access_level = $this->session->userdata('loguser_level');

        if (($this->session->userdata('loguser_username')) && ($access_level != '')) {
            $state = true;
            return $state;
        } else {

            $state = false;
            redirect('http://work.uageneral.com');
            exit();
        }
    }

    public function logout() {

        $user_data = array(
            'loguser_username' => '',
            'loguser_login' => '',
            'loguser_level' => '',
            'loguser_first_name' => '',
            'loguser_first_name' => '',
            'loguser_branch' =>'',
            'loguser_tbl_id' => ''
        );

        $this->session->unset_userdata($user_data);
        ?>
        <a href="http://work.uageneral.com">RE LOGIN<a>
        <?php
        ///redirect('member/login');
    }

   
}
?>