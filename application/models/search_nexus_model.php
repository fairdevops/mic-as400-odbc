<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_nexus_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('array');
    }

    function search_card_data_count_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $search_txt) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= "AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= "AND MICZONE='$zone_code'";
                }
            }
        }


        if ($search_txt != "") {

            $sql .= "AND MICVENO LIKE '$search_txt%' OR MICPLNO LIKE '$search_txt%'  OR  MICNAME LIKE '$search_txt%' GROUP BY MIC_ID_NEXUS ";
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    function search_card_data_display_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $start, $per_page) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy_nexus  WHERE MICREFNO=0 ";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= "AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= "AND MICZONE='$zone_code'";
                }
            }
        }


        if ($search_txt != "") {

            $sql .= "AND MICVENO LIKE '$search_txt%' OR MICPLNO LIKE '$search_txt%'  OR  MICNAME LIKE '$search_txt%' GROUP BY MIC_ID_NEXUS ";
        }


        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    /* public function search_printed_card_count($branch_state, $branch_code, $region_code, $zone_code,$text_search,$search_type) {

      $sql ="";

      $pdate= date("Y-m-d");

      if ($branch_state == 'B') {

      $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_CARD_PRINT='Y' ";

      if ($branch_code!='A'){

      $sql .= " AND PR_MICBRCD='$branch_code'";

      }


      } else {
      if ($branch_state == 'R') {


      $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_CARD_PRINT='Y' ";

      if ($region_code!='A'){
      $sql .= " AND PR_MICREGN='$region_code'";
      }


      } else {

      $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE WHERE PR_CARD_PRINT='Y' ";

      if ($zone_code!='A'){
      $sql .= " AND PR_MICZONE='$zone_code'";
      }
      }
      }


      if ( $search_type =="P" ) {

      $sql .= " AND  PR_MICPLNO LIKE'$text_search%'";

      }
      else
      {
      if ( $search_type =="V" ) {

      $sql .= " AND  PR_MICVENO LIKE '$text_search%'";

      }

      else
      {

      $sql .= " AND  PR_MICNAME LIKE '$text_search%'";

      }




      }


      $sql .= " GROUP BY PR_MIC_ID";



      ///echo $sql;

      $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
      return $query->num_rows();
      } else {

      return 0;
      }

      $this->load->database()->close();
      }


      public function search_printed_card_count_display($branch_state, $branch_code, $region_code, $zone_code,$text_search,$search_type,$start, $per_page) {

      $sql ="";

      $pdate= date("Y-m-d");

      if ($branch_state == 'B') {

      $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,
      DATE_FORMAT(PR_PRINT_DATE, '%d%/%m%/%Y') AS printed_date
      FROM daily_policy_to_printer WHERE PR_CARD_PRINT='Y' ";

      if ($branch_code!='A'){

      $sql .= " AND PR_MICBRCD='$branch_code'";

      }


      } else {
      if ($branch_state == 'R') {


      $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT
      DATE_FORMAT(PR_PRINT_DATE, '%d%/%m%/%Y') AS printed_date
      FROM daily_policy_to_printer WHERE PR_CARD_PRINT='Y' ";

      if ($region_code!='A'){
      $sql .= " AND PR_MICREGN='$region_code'";
      }


      } else {

      $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT
      DATE_FORMAT(PR_PRINT_DATE, '%d%/%m%/%Y') AS printed_date
      FROM daily_policy_to_printer WHERE WHERE PR_CARD_PRINT='Y' ";

      if ($zone_code!='A'){
      $sql .= " AND PR_MICZONE='$zone_code'";
      }
      }
      }


      if ( $search_type =="P" ) {

      $sql .= " AND  PR_MICPLNO LIKE'$text_search%'";

      }
      else
      {
      if ( $search_type =="V" ) {

      $sql .= " AND  PR_MICVENO LIKE '$text_search%'";

      }

      else
      {

      $sql .= " AND  PR_MICNAME LIKE '$text_search%'";

      }




      }


      $sql .= " GROUP BY PR_MIC_ID";

      $sql.=" LIMIT $start, $per_page";

      //echo $sql;

      $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
      return $query->result_array();
      } else {

      return 0;
      }

      $this->load->database()->close();
      }






      function search_card_data_count($branch_state, $branch_code, $region_code, $zone_code,$search_txt) {

      $sql ="";

      if ($branch_state == 'B') {

      $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

      if ($branch_code!='A'){

      $sql .= " AND MICBRCD='$branch_code'";

      }


      } else {
      if ($branch_state == 'R') {


      $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

      if ($region_code!='A'){
      $sql .= "AND MICREGN='$region_code'";
      }


      } else {

      $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

      if ($zone_code!='A'){
      $sql .= "AND MICZONE='$zone_code'";
      }
      }
      }


      if ( $search_txt != "" ){

      $sql .= "AND MICVENO LIKE '$search_txt%' OR MICPLNO LIKE '$search_txt%'  OR  MICNAME LIKE '$search_txt%' GROUP BY MIC_ID ";
      }

      $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
      return $query->num_rows();
      } else {

      return 0;
      }

      $this->load->database()->close();
      }




      function search_card_data_display($branch_state, $branch_code, $region_code, $zone_code,$search_txt,$start,$per_page) {

      $sql ="";

      if ($branch_state == 'B') {

      $sql .= "SELECT PR_MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy  WHERE MICREFNO=0 ";

      if ($branch_code!='A'){

      $sql .= " AND MICBRCD='$branch_code'";

      }


      } else {
      if ($branch_state == 'R') {


      $sql .= "SELECT PR_MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy  WHERE MICREFNO=0 ";

      if ($region_code!='A'){
      $sql .= "AND MICREGN='$region_code'";
      }


      } else {

      $sql .= "SELECT PR_MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy  WHERE MICREFNO=0 ";

      if ($zone_code!='A'){
      $sql .= "AND MICZONE='$zone_code'";
      }
      }
      }


      if ( $search_txt != "" ){

      $sql .= "AND MICVENO LIKE '$search_txt%' OR MICPLNO LIKE '$search_txt%'  OR  MICNAME LIKE '$search_txt%' GROUP BY MIC_ID ";
      }


      $sql.=" LIMIT $start, $per_page";

      $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
      return $query->result_array();
      } else {

      return 0;
      }

      $this->load->database()->close();
      }


      function search_card_data_display_data($branch_state, $branch_code, $region_code, $zone_code,$search_txt,$start,$per_page) {

      $sql ="";

      if ($branch_state == 'B') {

      $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy  WHERE MICREFNO=0 ";

      if ($branch_code!='A'){

      $sql .= " AND MICBRCD='$branch_code'";

      }


      } else {
      if ($branch_state == 'R') {


      $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy  WHERE MICREFNO=0 ";

      if ($region_code!='A'){
      $sql .= "AND MICREGN='$region_code'";
      }


      } else {

      $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN FROM daily_policy  WHERE MICREFNO=0 ";

      if ($zone_code!='A'){
      $sql .= "AND MICZONE='$zone_code'";
      }
      }
      }


      if ( $search_txt != "" ){

      $sql .= "AND MICVENO LIKE '$search_txt%' OR MICPLNO LIKE '$search_txt%'  OR  MICNAME LIKE '$search_txt%' GROUP BY MIC_ID ";
      }


      $sql.=" LIMIT $start, $per_page";

      $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
      return $query->result_array();
      } else {

      return 0;
      }

      $this->load->database()->close();
      }

     */
}

?>