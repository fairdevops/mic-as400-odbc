<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nexus_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function display_daily_card_data_count_by_nexus_level($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= "AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= "AND MICZONE='$zone_code'";
                }
            }
        }

//echo $sql;


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data_count_for_nexus($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= "AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= "AND MICZONE='$zone_code'";
                }
            }
        }

//echo $sql;


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $start, $per_page) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
		             FROM daily_policy_nexus WHERE MICREFNO=0";

            if ($branch_code != 'A') {
                $sql .= " AND MICBRCD='$branch_code' ";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
		                 FROM daily_policy_nexus WHERE MICREFNO=0";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'	";
                }
            } else {

                $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
		                 FROM daily_policy_nexus WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .=" AND MICZONE='$zone_code'";
                }
            }
        }


        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy_nexus AND MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy_nexus AND MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND='' ";
        }

//echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter_display_for_nexus($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $start, $per_page) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID_NEXUS,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND=''";
        }

        if ($by_hand == 'B') {

            $sql .= " ORDER BY MICNAME2 ";
        }



//echo $sql;
        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    function update_by_hand_printer_list_for_nexus($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;


        $this->db->select('*');
        $this->db->from('daily_policy_nexus');
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', 'B');

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $this->db->where('MICBRCD', '$branch_code');
            }
        } else {
            if ($branch_state == 'R') {


                if ($region_code != 'A') {
                    $this->db->where('MICREGN', '$region_code');
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', '$zone_code');
                }
            }
        }


        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID_NEXUS'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = $row['MICNAME2'];
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];
                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];


                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'N',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                $g++;
            }
        }

        //return $g;



        $this->db->where('MICBYHND', $by_hand);
        $this->db->where('MICREFNO', 0);

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {
                $this->db->where('MICBRCD', $branch_code);
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }







        $arr = array('MICRESEPF' => $epf_user,
            'MICREFNO ' => $epf_user,
            'MICCDUS' => $print_user);

        $result = $this->db->update('daily_policy_nexus', $arr);
        return $this->db->affected_rows();


        //($branch_state, $branch_code, $region_code, $zone_code,$city,$by_hand)
        //display_daily_card_data_count_with_filter_display($branch_state, $branch_code, $region_code, $zone_code,$city,$by_hand, $start, $per_page)
    }

    function update_by_hand_printer_list_colombo_none_colombo_for_nexus($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand, $city) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;


        $this->db->select('*');
        $this->db->from('daily_policy_nexus');
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', $by_hand); ///MICCITY='$city' AND MICBYHND=''
        $this->db->where('MICCITY', $city);

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $this->db->where('MICBRCD', '$branch_code');
            }
        } else {
            if ($branch_state == 'R') {


                if ($region_code != 'A') {
                    $this->db->where('MICREGN', '$region_code');
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', '$zone_code');
                }
            }
        }


        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID_NEXUS'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = $row['MICNAME2'];
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];
                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];



                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'N',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                $g++;
            }
        }

        //return $g;



        $this->db->where('MICBYHND', $by_hand);
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICCITY', $city);




        if ($branch_state == 'B') {


            if ($branch_code != 'A') {
                $this->db->where('MICBRCD', $branch_code);
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }




        $arr = array('MICRESEPF' => $epf_user,
            'MICREFNO ' => $epf_user,
            'MICCDUS' => $print_user);

        $result = $this->db->update('daily_policy_nexus', $arr);
        return $this->db->affected_rows();
    }

}

?>
