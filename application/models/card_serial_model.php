
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_serial_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function get_card_serail_count($code) {


        $this->db->select('ser_id');
        $this->db->from('mic_card_serial');
        $this->db->where('serial_state', 1);
        $this->db->where('code', $code);

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
        } else {
            return 0;
        }
    }

    function get_card_serail_first($code) {

        $query_str = "SELECT serial FROM mic_card_serial WHERE code=? limit 0,1";
        $result = $this->db->query($query_str, array($code));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['serial'];
        } else {
            return false;
        }
    }

    function get_card_serail_last($code) {

        $last_count = $this->get_card_serail_count($code);

        $query_str = "SELECT serial FROM mic_card_serial WHERE code=? limit 0,$last_count";
        $result = $this->db->query($query_str, array($code));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['serial'];
        } else {
            return false;
        }
    }

	 
	 
}

?>
