
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('array');
    }


     /************************** CR

      Debtor bulk print functions 

              $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', 'B');
        $this->db->where('MICMNFL !=', 'M'); ///// nov 09
        $this->db->where('MICSPNO',$debtor_code);
        $this->db->where('MICRNCT',$rad_val);

    ***********************/


    public function display_debtor_card_data_all_count($debtor_code,$txn_type){
        $sql = "";
        $sql .= "SELECT MICPLNO FROM daily_policy WHERE MICREFNO=0 AND  MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='B' AND MICSPNO = '$debtor_code' AND MICRNCT = '$txn_type' ORDER BY MICBRCD ASC" ;

        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();


    }


    function display_debtor_card_data_all_display($debtor_code,$txn_type, $start, $per_page){

        $sql = "";
        $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV,MICNAME2,MICENCT,MICRNCT,MICPCOV  FROM daily_policy WHERE MICREFNO=0 AND  MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='B' AND MICSPNO = '$debtor_code' AND MICRNCT = '$txn_type' ORDER BY MICBRCD ASC" ;

         $sql.=" LIMIT $start, $per_page"; 

          $query = $this->db->query($sql);;

         // echo  $sql;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();

    }



    function update_debtor_bulk_printer_list($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;


        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
        

        $rad_val            = $_POST['rad_val'];
        $debtor_code        = $_POST['dc'];

        $this->db->select('*');
        $this->db->from('daily_policy');
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', 'B');
        $this->db->where('MICMNFL !=', 'M'); ///// nov 09
        $this->db->where('MICSPNO',$debtor_code);
        $this->db->where('MICRNCT',$rad_val);

        //$this->db->order_by('MICSPNO', 'ASC'); 

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $this->db->where('MICBRCD', $branch_code);
                
            } 
            else {

                $this->db->where('MICBRCD !=', $select_br_1);
                
                if ($select_br_2 != "") {
                    $this->db->where('MICBRCD !=', $select_br_2);
                }
                if ($select_br_3 != "") {
                    $this->db->where('MICBRCD !=', $select_br_3);
                }
                if ($select_br_4 != "") {
                    $this->db->where('MICBRCD !=', $select_br_4);
                }
                if ($select_br_5 != "") {
                    $this->db->where('MICBRCD !=', $select_br_5);
                }

                if ($select_br_6 != "") {
                    $this->db->where('MICBRCD !=', $select_br_6);
                }
                if ($select_br_7 != "") {
                    $this->db->where('MICBRCD !=', $select_br_7);
                }
                if ($select_br_8 != "") {
                    $this->db->where('MICBRCD !=', $select_br_8);
                }
                if ($select_br_9 != "") {
                    $this->db->where('MICBRCD !=', $select_br_9);
                }
                if ($select_br_10 != "") {
                    $this->db->where('MICBRCD !=', $select_br_10);
                }
                if ($select_br_11 != "") {
                    $this->db->where('MICBRCD !=', $select_br_11);
                }
                if ($select_br_12 != "") {
                    $this->db->where('MICBRCD !=', $select_br_12);
                }
                if ($select_br_13 != "") {
                    $this->db->where('MICBRCD !=', $select_br_13);
                }
                if ($select_br_14 != "") {
                    $this->db->where('MICBRCD !=', $select_br_14);
                }
                if ($select_br_15 != "") {
                    $this->db->where('MICBRCD !=', $select_br_15);
                }
                if ($select_br_16 != "") {
                    $this->db->where('MICBRCD !=', $select_br_16);
                }
                if ($select_br_17 != "") {
                    $this->db->where('MICBRCD !=', $select_br_17);
                }
                if ($select_br_18 != "") {
                    $this->db->where('MICBRCD !=', $select_br_18);
                }
                if ($select_br_19 != "") {
                    $this->db->where('MICBRCD !=', $select_br_19);
                }

                if ($select_br_20 != "") {
                    $this->db->where('MICBRCD !=', $select_br_20);
                }
                if ($select_br_21 != "") {
                    $this->db->where('MICBRCD !=', $select_br_21);
                }
                if ($select_br_22 != "") {
                    $this->db->where('MICBRCD !=', $select_br_22);
                }
                if ($select_br_23 != "") {
                    $this->db->where('MICBRCD !=', $select_br_23);
                }
                if ($select_br_24 != "") {
                    $this->db->where('MICBRCD !=', $select_br_24);
                }
                if ($select_br_25 != "") {
                    $this->db->where('MICBRCD !=', $select_br_25);
                }
                if ($select_br_26 != "") {
                    $this->db->where('MICBRCD !=', $select_br_26);
                }
                if ($select_br_27 != "") {
                    $this->db->where('MICBRCD !=', $select_br_27);
                }
                if ($select_br_28 != "") {
                    $this->db->where('MICBRCD !=', $select_br_28);
                }
                if ($select_br_29 != "") {
                    $this->db->where('MICBRCD !=', $select_br_29);
                }
                if ($select_br_30 != "") {
                    $this->db->where('MICBRCD !=', $select_br_30);
                }
                
                if ($select_br_31 != "") {
                    $this->db->where('MICBRCD !=', $select_br_31);
                }
                
                if ($select_br_32 != "") {
                    $this->db->where('MICBRCD !=', $select_br_32);
                }
                
                if ($select_br_33 != "") {
                    $this->db->where('MICBRCD !=', $select_br_33);
                }
                
                if ($select_br_34 != "") {
                    $this->db->where('MICBRCD !=', $select_br_34);
                }
                
                if ($select_br_35 != "") {
                    $this->db->where('MICBRCD !=', $select_br_35);
                }
                
                if ($select_br_36 != "") {
                    $this->db->where('MICBRCD !=', $select_br_36);
                }
                
                if ($select_br_37 != "") {
                    $this->db->where('MICBRCD !=', $select_br_37);
                }
                
                if ($select_br_38 != "") {
                    $this->db->where('MICBRCD !=', $select_br_38);
                }
                
                if ($select_br_39 != "") {
                    $this->db->where('MICBRCD !=', $select_br_39);
                }
                
                if ($select_br_40 != "") {
                    $this->db->where('MICBRCD !=', $select_br_40);
                }
                
                if ($select_br_41 != "") {
                    $this->db->where('MICBRCD !=', $select_br_41);
                }
                
                if ($select_br_42 != "") {
                    $this->db->where('MICBRCD !=', $select_br_42);
                }
                
                if ($select_br_43 != "") {
                    $this->db->where('MICBRCD !=', $select_br_43);
                }
                if ($select_br_44 != "") {
                    $this->db->where('MICBRCD !=', $select_br_44);
                }
                
                if ($select_br_45 != "") {
                    $this->db->where('MICBRCD !=', $select_br_45);
                }
                if ($select_br_46 != "") {
                    $this->db->where('MICBRCD !=', $select_br_46);
                }
                if ($select_br_47 != "") {
                    $this->db->where('MICBRCD !=', $select_br_47);
                }
                if ($select_br_48 != "") {
                    $this->db->where('MICBRCD !=', $select_br_48);
                }
                if ($select_br_49 != "") {
                    $this->db->where('MICBRCD !=', $select_br_49);
                }
                if ($select_br_50 != "") {
                    $this->db->where('MICBRCD !=', $select_br_50);
                }



                if ($select_br_50 != "") {
                    $this->db->where('MICBRCD !=', $select_br_50);
                }
                if ($select_br_51 != "") {
                    $this->db->where('MICBRCD !=', $select_br_51);
                }
                if ($select_br_52 != "") {
                    $this->db->where('MICBRCD !=', $select_br_52);
                }
                if ($select_br_53 != "") {
                    $this->db->where('MICBRCD !=', $select_br_53);
                }
                if ($select_br_54 != "") {
                    $this->db->where('MICBRCD !=', $select_br_54);
                }
                if ($select_br_55 != "") {
                    $this->db->where('MICBRCD !=', $select_br_55);
                }
                if ($select_br_56 != "") {
                    $this->db->where('MICBRCD !=', $select_br_56);
                }
                if ($select_br_57 != "") {
                    $this->db->where('MICBRCD !=', $select_br_57);
                }
                if ($select_br_58 != "") {
                    $this->db->where('MICBRCD !=', $select_br_58);
                }
                if ($select_br_59 != "") {
                    $this->db->where('MICBRCD !=', $select_br_59);
                }
                if ($select_br_60 != "") {
                    $this->db->where('MICBRCD !=', $select_br_60);
                }



                if ($select_br_61 != "") {
                    $this->db->where('MICBRCD !=', $select_br_61);
                }
                if ($select_br_62 != "") {
                    $this->db->where('MICBRCD !=', $select_br_62);
                }
                if ($select_br_63 != "") {
                    $this->db->where('MICBRCD !=', $select_br_63);
                }
                if ($select_br_64 != "") {
                    $this->db->where('MICBRCD !=', $select_br_64);
                }
                if ($select_br_65 != "") {
                    $this->db->where('MICBRCD !=', $select_br_65);
                }
                if ($select_br_66 != "") {
                    $this->db->where('MICBRCD !=', $select_br_66);
                }
                if ($select_br_67 != "") {
                    $this->db->where('MICBRCD !=', $select_br_67);
                }
                if ($select_br_68 != "") {
                    $this->db->where('MICBRCD !=', $select_br_68);
                }
                if ($select_br_69 != "") {
                    $this->db->where('MICBRCD !=', $select_br_69);
                }
                if ($select_br_70 != "") {
                    $this->db->where('MICBRCD !=', $select_br_70);
                }
    
            }
        } else {

            if ($branch_state == 'R') {


                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }


        $this->db->order_by('MICNAME2', 'ASC');
        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = trim($row['MICNAME2']);
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];
                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];

                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'M',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                $g++;
            }
        }

        //return $g;



        $this->db->where('MICBYHND', $by_hand);
        $this->db->where('MICREFNO', 0);

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {
                $this->db->where('MICBRCD', $branch_code);
            } else {

                $this->db->where('MICBRCD !=', $select_br_1);

                if ($select_br_2 != "") {
                    $this->db->where('MICBRCD !=', $select_br_2);
                }

                if ($select_br_3 != "") {

                    $this->db->where('MICBRCD !=', $select_br_3); //oct 15
                }

                if ($select_br_4 != "") {

                    $this->db->where('MICBRCD !=', $select_br_4); //oct 15
                }

                if ($select_br_5 != "") {

                    $this->db->where('MICBRCD !=', $select_br_5); //oct 15
                }

                if ($select_br_6 != "") {

                    $this->db->where('MICBRCD !=', $select_br_6); //oct 15
                }
                if ($select_br_7 != "") {

                    $this->db->where('MICBRCD !=', $select_br_7); //oct 15
                }

                if ($select_br_8 != "") {

                    $this->db->where('MICBRCD !=', $select_br_8); //oct 15
                }

                if ($select_br_9 != "") {

                    $this->db->where('MICBRCD !=', $select_br_9); //oct 15
                }
                if ($select_br_10 != "") {

                    $this->db->where('MICBRCD !=', $select_br_10); //oct 15
                }

                if ($select_br_11 != "") {

                    $this->db->where('MICBRCD !=', $select_br_11); //oct 15
                }
                if ($select_br_12 != "") {

                    $this->db->where('MICBRCD !=', $select_br_12); //oct 15
                }

                if ($select_br_13 != "") {

                    $this->db->where('MICBRCD !=', $select_br_13); //oct 15
                }
                if ($select_br_14 != "") {

                    $this->db->where('MICBRCD !=', $select_br_14); //oct 15
                }
                if ($select_br_15 != "") {

                    $this->db->where('MICBRCD !=', $select_br_15); //oct 15
                }
                if ($select_br_16 != "") {

                    $this->db->where('MICBRCD !=', $select_br_16); //oct 15
                }
                if ($select_br_17 != "") {

                    $this->db->where('MICBRCD !=', $select_br_17); //oct 15
                }

                if ($select_br_18 != "") {

                    $this->db->where('MICBRCD !=', $select_br_18); //oct 15
                }

                if ($select_br_19 != "") {

                    $this->db->where('MICBRCD !=', $select_br_19); //oct 15
                }
                if ($select_br_20 != "") {

                    $this->db->where('MICBRCD !=', $select_br_20); //oct 15
                }
                
                if ($select_br_21 != "") {
                    $this->db->where('MICBRCD !=', $select_br_21);
                }
                
                if ($select_br_22 != "") {
                    $this->db->where('MICBRCD !=', $select_br_22);
                }
                
                if ($select_br_23 != "") {
                    $this->db->where('MICBRCD !=', $select_br_23);
                }
                
                if ($select_br_24 != "") {
                    $this->db->where('MICBRCD !=', $select_br_24);
                }
                if ($select_br_25 != "") {
                    $this->db->where('MICBRCD !=', $select_br_25);
                }
                if ($select_br_26 != "") {
                    $this->db->where('MICBRCD !=', $select_br_26);
                }
                if ($select_br_27 != "") {
                    $this->db->where('MICBRCD !=', $select_br_27);
                }
                if ($select_br_28 != "") {
                    $this->db->where('MICBRCD !=', $select_br_28);
                }
                
                if ($select_br_29 != "") {
                    $this->db->where('MICBRCD !=', $select_br_29);
                }
                if ($select_br_30 != "") {
                    $this->db->where('MICBRCD !=', $select_br_30);
                }
                
                if ($select_br_31 != "") {
                    $this->db->where('MICBRCD !=', $select_br_31);
                }
                
                if ($select_br_32 != "") {
                    $this->db->where('MICBRCD !=', $select_br_32);
                }
                
                if ($select_br_33 != "") {
                    $this->db->where('MICBRCD !=', $select_br_33);
                }
                
                if ($select_br_34 != "") {
                    $this->db->where('MICBRCD !=', $select_br_34);
                }
                
                if ($select_br_35 != "") {
                    $this->db->where('MICBRCD !=', $select_br_35);
                }
                
                if ($select_br_36 != "") {
                    $this->db->where('MICBRCD !=', $select_br_36);
                }
                
                if ($select_br_37 != "") {
                    $this->db->where('MICBRCD !=', $select_br_37);
                }
                
                if ($select_br_38 != "") {
                    $this->db->where('MICBRCD !=', $select_br_38);
                }
                
                if ($select_br_39 != "") {
                    $this->db->where('MICBRCD !=', $select_br_39);
                }
                if ($select_br_40 != "") {
                    $this->db->where('MICBRCD !=', $select_br_40);
                }
                
                if ($select_br_41 != "") {
                    $this->db->where('MICBRCD !=', $select_br_41);
                }
                
                if ($select_br_42 != "") {
                    $this->db->where('MICBRCD !=', $select_br_42);
                }
                
                if ($select_br_43 != "") {
                    $this->db->where('MICBRCD !=', $select_br_43);
                }
                
                if ($select_br_44 != "") {
                    $this->db->where('MICBRCD !=', $select_br_44);
                }
                
                if ($select_br_45 != "") {
                    $this->db->where('MICBRCD !=', $select_br_45);
                }
                
                if ($select_br_46 != "") {
                    $this->db->where('MICBRCD !=', $select_br_46);
                }
                
                if ($select_br_47 != "") {
                    $this->db->where('MICBRCD !=', $select_br_47);
                }
                
                if ($select_br_48 != "") {
                    $this->db->where('MICBRCD !=', $select_br_48);
                }
                
                if ($select_br_49 != "") {
                    $this->db->where('MICBRCD !=', $select_br_49);
                }
                
                if ($select_br_50 != "") {
                    $this->db->where('MICBRCD !=', $select_br_50);
                }

                if ($select_br_51 != "") {
                    $this->db->where('MICBRCD !=', $select_br_51);
                }
                if ($select_br_52 != "") {
                    $this->db->where('MICBRCD !=', $select_br_52);
                }
                if ($select_br_53 != "") {
                    $this->db->where('MICBRCD !=', $select_br_53);
                }
                if ($select_br_54 != "") {
                    $this->db->where('MICBRCD !=', $select_br_54);
                }
                if ($select_br_55 != "") {
                    $this->db->where('MICBRCD !=', $select_br_55);
                }
                if ($select_br_56 != "") {
                    $this->db->where('MICBRCD !=', $select_br_56);
                }
                if ($select_br_57 != "") {
                    $this->db->where('MICBRCD !=', $select_br_57);
                }
                if ($select_br_58 != "") {
                    $this->db->where('MICBRCD !=', $select_br_58);
                }
                if ($select_br_59 != "") {
                    $this->db->where('MICBRCD !=', $select_br_59);
                }
                if ($select_br_60 != "") {
                    $this->db->where('MICBRCD !=', $select_br_60);
                }



                if ($select_br_61 != "") {
                    $this->db->where('MICBRCD !=', $select_br_61);
                }
                if ($select_br_62 != "") {
                    $this->db->where('MICBRCD !=', $select_br_62);
                }
                if ($select_br_63 != "") {
                    $this->db->where('MICBRCD !=', $select_br_63);
                }
                if ($select_br_64 != "") {
                    $this->db->where('MICBRCD !=', $select_br_64);
                }
                if ($select_br_65 != "") {
                    $this->db->where('MICBRCD !=', $select_br_65);
                }
                if ($select_br_66 != "") {
                    $this->db->where('MICBRCD !=', $select_br_66);
                }
                if ($select_br_67 != "") {
                    $this->db->where('MICBRCD !=', $select_br_67);
                }
                if ($select_br_68 != "") {
                    $this->db->where('MICBRCD !=', $select_br_68);
                }
                if ($select_br_69 != "") {
                    $this->db->where('MICBRCD !=', $select_br_69);
                }
                if ($select_br_70 != "") {
                    $this->db->where('MICBRCD !=', $select_br_70);
                }               
                
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }





        $arr = array('MICRESEPF' => $epf_user,
                     'MICRML3'   => 'D',
                     'MICREFNO ' => $epf_user,
                     'MICCDUS' => $print_user);



        $result = $this->db->update('daily_policy', $arr);
       ///echo  $str = $this->db->last_query();  
        return $this->db->affected_rows();
    }


    /************************** EOF Debtor bulk print functions ***********************/
    




    public function display_daily_card_data_all_count_at_head_office($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";



        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;


        
        
        
        //DEBTOR EXCLUDE : excluded Debtor codes : B26016 and B26001

        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy WHERE MICREFNO=0 AND ( MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND 
            MICSPNO !='B26001' AND  MICSPNO !='B26016'  AND MICBYHND='' OR ";

            $sql .= "( MICBYHND='B' AND  ";


            if ($select_br_1 != "") {

                $sql .= "  MICBRCD<>'$select_br_1'  ";
            }

            if ($select_br_2 != "") {

                $sql .= " AND MICBRCD<>'$select_br_2' ";
            }

            if ($select_br_3 != "") {

                $sql .= " AND MICBRCD<>'$select_br_3' ";
            }

            if ($select_br_4 != "") {

                $sql .= "AND  MICBRCD<>'$select_br_4' ";
            }


            if ($select_br_5 != "") {

                $sql .= " AND MICBRCD<>'$select_br_5' ";
            }

            if ($select_br_6 != "") {

                $sql .= " AND MICBRCD<>'$select_br_6' ";
            }

            if ($select_br_7 != "") {

                $sql .= " AND MICBRCD<>'$select_br_7' ";
            }

            if ($select_br_8 != "") {

                $sql .= " AND MICBRCD<>'$select_br_8' ";
            }

            if ($select_br_9 != "") {

                $sql .= " AND MICBRCD<>'$select_br_9' ";
            }

            if ($select_br_10 != "") {

                $sql .= " AND MICBRCD<>'$select_br_10' ";
            }
            if ($select_br_11 != "") {

                $sql .= " AND MICBRCD<>'$select_br_11' ";
            }

            if ($select_br_12 != "") {

                $sql .= " AND MICBRCD<>'$select_br_12' ";
            }
            if ($select_br_13 != "") {

                $sql .= " AND MICBRCD<>'$select_br_13' ";
            }

            if ($select_br_14 != "") {

                $sql .= " AND MICBRCD<>'$select_br_14' ";
            }

            if ($select_br_15 != "") {

                $sql .= " AND MICBRCD<>'$select_br_15' ";
            }
            if ($select_br_16 != "") {

                $sql .= " AND MICBRCD<>'$select_br_16' ";
            }

            if ($select_br_17 != "") {

                $sql .= " AND MICBRCD<>'$select_br_17' ";
            }
            if ($select_br_18 != "") {

                $sql .= " AND MICBRCD<>'$select_br_18' ";
            }

            if ($select_br_19 != "") {

                $sql .= " AND MICBRCD<>'$select_br_19' ";
            }
            if ($select_br_20 != "") {

                $sql .= " AND MICBRCD<>'$select_br_20' ";
            }
            
            if ($select_br_21 != "") {

                $sql .= " AND MICBRCD<>'$select_br_21' ";
            }
            if ($select_br_22 != "") {

                $sql .= " AND MICBRCD<>'$select_br_22' ";
            }
            
            if ($select_br_23 != "") {

                $sql .= " AND MICBRCD<>'$select_br_23' ";
            }
            if ($select_br_24 != "") {

                $sql .= " AND MICBRCD<>'$select_br_24' ";
            }
            
            if ($select_br_25 != "") {

                $sql .= " AND MICBRCD<>'$select_br_25' ";
            }
            if ($select_br_26 != "") {

                $sql .= " AND MICBRCD<>'$select_br_26' ";
            }
            
            if ($select_br_27 != "") {

                $sql .= " AND MICBRCD<>'$select_br_27' ";
            }
            if ($select_br_28 != "") {

                $sql .= " AND MICBRCD<>'$select_br_28' ";
            }
            
            if ($select_br_29 != "") {

                $sql .= " AND MICBRCD<>'$select_br_29' ";
            }
            if ($select_br_30 != "") {

                $sql .= " AND MICBRCD<>'$select_br_30' ";
            }
            
            if ($select_br_31 != "") {

                $sql .= " AND MICBRCD<>'$select_br_31' ";
            }
            
            if ($select_br_32 != "") {

                $sql .= " AND MICBRCD<>'$select_br_32' ";
            }
            
            if ($select_br_33 != "") {

                $sql .= " AND MICBRCD<>'$select_br_33' ";
            }
            
            if ($select_br_34 != "") {

                $sql .= " AND MICBRCD<>'$select_br_34' ";
            }
            
            if ($select_br_35 != "") {

                $sql .= " AND MICBRCD<>'$select_br_35' ";
            }
            
            
            if ($select_br_36 != "") {

                $sql .= " AND MICBRCD<>'$select_br_36' ";
            }
            
            if ($select_br_37 != "") {

                $sql .= " AND MICBRCD<>'$select_br_37' ";
            }
            
            if ($select_br_38 != "") {

                $sql .= " AND MICBRCD<>'$select_br_38' ";
            }
            
            if ($select_br_39 != "") {

                $sql .= " AND MICBRCD<>'$select_br_39' ";
            }
            
            if ($select_br_40 != "") {

                $sql .= " AND MICBRCD<>'$select_br_40' ";
            }
            
            if ($select_br_41 != "") {

                $sql .= " AND MICBRCD<>'$select_br_41' ";
            }
            
            if ($select_br_42 != "") {

                $sql .= " AND MICBRCD<>'$select_br_42' ";
            }
            
            if ($select_br_43 != "") {

                $sql .= " AND MICBRCD<>'$select_br_43' ";
            }
            
            if ($select_br_44 != "") {

                $sql .= " AND MICBRCD<>'$select_br_44' ";
            }
            
            if ($select_br_45 != "") {

                $sql .= " AND MICBRCD<>'$select_br_45' ";
            }
            
            if ($select_br_46 != "") {

                $sql .= " AND MICBRCD<>'$select_br_46' ";
            }
            
            if ($select_br_47 != "") {

                $sql .= " AND MICBRCD<>'$select_br_47' ";
            }
            
            if ($select_br_48 != "") {

                $sql .= " AND MICBRCD<>'$select_br_48' ";
            }
            
            if ($select_br_49 != "") {

                $sql .= " AND MICBRCD<>'$select_br_49' ";
            }
            
            if ($select_br_50 != "") {

                $sql .= " AND MICBRCD<>'$select_br_50' ";
            }

            if ($select_br_51 != "") {

                $sql .= " AND MICBRCD<>'$select_br_51' ";
            }

            if ($select_br_52 != "") {

                $sql .= " AND MICBRCD<>'$select_br_52' ";
            }

            if ($select_br_53 != "") {

                $sql .= " AND MICBRCD<>'$select_br_53' ";
            }    

            if ($select_br_54 != "") {

                $sql .= " AND MICBRCD<>'$select_br_54' ";
            }

            if ($select_br_55 != "") {

                $sql .= " AND MICBRCD<>'$select_br_55' ";
            }

            if ($select_br_56 != "") {

                $sql .= " AND MICBRCD<>'$select_br_56' ";
            }
            

            if ($select_br_57 != "") {

                $sql .= " AND MICBRCD<>'$select_br_57' ";
            }


            if ($select_br_58 != "") {

                $sql .= " AND MICBRCD<>'$select_br_58' ";
            }


            if ($select_br_59 != "") {

                $sql .= " AND MICBRCD<>'$select_br_59' ";
            }

            if ($select_br_60 != "") {

                $sql .= " AND MICBRCD<>'$select_br_60' ";
            }


            if ($select_br_61 != "") {

                $sql .= " AND MICBRCD<>'$select_br_61' ";
            }
            
            if ($select_br_62 != "") {

                $sql .= " AND MICBRCD<>'$select_br_62' ";
            }

            if ($select_br_63 != "") {

                $sql .= " AND MICBRCD<>'$select_br_63' ";
            }

            if ($select_br_64 != "") {

                $sql .= " AND MICBRCD<>'$select_br_64' ";
            }

            if ($select_br_65 != "") {

                $sql .= " AND MICBRCD<>'$select_br_65' ";
            }

            if ($select_br_66 != "") {

                $sql .= " AND MICBRCD<>'$select_br_66' ";
            }

            if ($select_br_67 != "") {

                $sql .= " AND MICBRCD<>'$select_br_67' ";
            }

            if ($select_br_68 != "") {

                $sql .= " AND MICBRCD<>'$select_br_68' ";
            }

            if ($select_br_69 != "") {

                $sql .= " AND MICBRCD<>'$select_br_69' ";
            }


            if ($select_br_70 != "") {

                $sql .= " AND MICBRCD<>'$select_br_70' ";
            }

            $sql .= " ) ) ";

            //echo $sql;
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data_all_at_head_office($branch_state, $branch_code, $region_code, $zone_code, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;
        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;

        
        

        
        

        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
                     FROM daily_policy WHERE 

                     MICPLNO NOT IN (SELECT t2.MICPLNO FROM daily_policy AS t2 WHERE t2.MICSPNO  IN('B26016','B26001') AND t2.MICRNCT != 999 AND t2.MICENCT  = 999 AND t2.MICREFNO=0 AND t2.MICBYHND='$by_hand' AND t2.MICMNFL<>'M')
                     AND
                     MICREFNO=0 AND  ( MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='' OR ";


            $sql .= "( MICBYHND='B' AND  ";

            if ($select_br_1 != "") {

                $sql .= " MICBRCD<>'$select_br_1' ";
            }

            if ($select_br_2 != "") {

                $sql .= " AND MICBRCD<>'$select_br_2' ";
            }

            if ($select_br_3 != "") {

                $sql .= " AND MICBRCD<>'$select_br_3' ";
            }

            if ($select_br_4 != "") {

                $sql .= " AND MICBRCD<>'$select_br_4' ";
            }


            if ($select_br_5 != "") {

                $sql .= " AND MICBRCD<>'$select_br_5' ";
            }

            if ($select_br_6 != "") {

                $sql .= " AND MICBRCD<>'$select_br_6' ";
            }
            if ($select_br_7 != "") {

                $sql .= " AND MICBRCD<>'$select_br_7' ";
            }
            if ($select_br_8 != "") {

                $sql .= " AND MICBRCD<>'$select_br_8' ";
            }
            if ($select_br_9 != "") {

                $sql .= " AND MICBRCD<>'$select_br_9' ";
            }
            if ($select_br_10 != "") {

                $sql .= " AND MICBRCD<>'$select_br_10' ";
            }
            if ($select_br_11 != "") {

                $sql .= " AND MICBRCD<>'$select_br_11' ";
            }
            if ($select_br_12 != "") {

                $sql .= " AND MICBRCD<>'$select_br_12' ";
            }

            if ($select_br_13 != "") {

                $sql .= " AND MICBRCD<>'$select_br_13' ";
            }
            if ($select_br_14 != "") {

                $sql .= " AND MICBRCD<>'$select_br_14' ";
            }

            if ($select_br_15 != "") {

                $sql .= " AND MICBRCD<>'$select_br_15' ";
            }

            if ($select_br_16 != "") {

                $sql .= " AND MICBRCD<>'$select_br_16' ";
            }
            if ($select_br_17 != "") {

                $sql .= " AND MICBRCD<>'$select_br_17' ";
            }
            if ($select_br_18 != "") {

                $sql .= " AND MICBRCD<>'$select_br_18' ";
            }
            if ($select_br_19 != "") {

                $sql .= " AND MICBRCD<>'$select_br_19' ";
            }
            if ($select_br_20 != "") {

                $sql .= " AND MICBRCD<>'$select_br_20' ";
            }
            
            if ($select_br_21 != "") {

                $sql .= " AND MICBRCD<>'$select_br_21' ";
            }
            
            if ($select_br_22 != "") {

                $sql .= " AND MICBRCD<>'$select_br_22' ";
            }
            
            if ($select_br_23 != "") {

                $sql .= " AND MICBRCD<>'$select_br_23' ";
            }
            
            if ($select_br_24 != "") {

                $sql .= " AND MICBRCD<>'$select_br_24' ";
            }
            
            if ($select_br_25 != "") {

                $sql .= " AND MICBRCD<>'$select_br_25' ";
            }
            
            if ($select_br_26 != "") {

                $sql .= " AND MICBRCD<>'$select_br_26' ";
            }
            
            if ($select_br_27 != "") {

                $sql .= " AND MICBRCD<>'$select_br_27' ";
            }
            
            if ($select_br_28 != "") {

                $sql .= " AND MICBRCD<>'$select_br_28' ";
            }
            
            if ($select_br_29 != "") {

                $sql .= " AND MICBRCD<>'$select_br_29' ";
            }
            
            if ($select_br_30 != "") {

                $sql .= " AND MICBRCD<>'$select_br_30' ";
            }
            
            if ($select_br_31 != "") {

                $sql .= " AND MICBRCD<>'$select_br_31' ";
            }
            
            if ($select_br_32 != "") {

                $sql .= " AND MICBRCD<>'$select_br_32' ";
            }
            
            if ($select_br_33 != "") {

                $sql .= " AND MICBRCD<>'$select_br_33' ";
            }
            
            if ($select_br_34 != "") {

                $sql .= " AND MICBRCD<>'$select_br_34' ";
            }
            
            if ($select_br_35 != "") {

                $sql .= " AND MICBRCD<>'$select_br_35' ";
            }
            
            if ($select_br_36 != "") {

                $sql .= " AND MICBRCD<>'$select_br_36' ";
            }
            
            if ($select_br_37 != "") {

                $sql .= " AND MICBRCD<>'$select_br_37' ";
            }
            
            if ($select_br_38 != "") {

                $sql .= " AND MICBRCD<>'$select_br_38' ";
            }
            
            if ($select_br_39 != "") {

                $sql .= " AND MICBRCD<>'$select_br_39' ";
            }
            
            if ($select_br_40 != "") {

                $sql .= " AND MICBRCD<>'$select_br_40' ";
            }
            
            if ($select_br_41 != "") {

                $sql .= " AND MICBRCD<>'$select_br_41' ";
            }
            
            if ($select_br_42 != "") {

                $sql .= " AND MICBRCD<>'$select_br_42' ";
            }
            
            if ($select_br_43 != "") {

                $sql .= " AND MICBRCD<>'$select_br_43' ";
            }
            
            if ($select_br_44 != "") {

                $sql .= " AND MICBRCD<>'$select_br_44' ";
            }
            
            if ($select_br_45 != "") {

                $sql .= " AND MICBRCD<>'$select_br_45' ";
            }
            
            if ($select_br_46 != "") {

                $sql .= " AND MICBRCD<>'$select_br_46' ";
            }
            
            if ($select_br_47 != "") {

                $sql .= " AND MICBRCD<>'$select_br_47' ";
            }
            
            if ($select_br_48 != "") {

                $sql .= " AND MICBRCD<>'$select_br_48' ";
            }
            
            if ($select_br_49 != "") {

                $sql .= " AND MICBRCD<>'$select_br_49' ";
            }
            
            if ($select_br_50 != "") {

                $sql .= " AND MICBRCD<>'$select_br_50' ";
            }

            if ($select_br_51 != "") {

                $sql .= " AND MICBRCD<>'$select_br_51' ";
            }
            

            if ($select_br_52 != "") {

                $sql .= " AND MICBRCD<>'$select_br_52' ";
            }
            

            if ($select_br_53 != "") {

                $sql .= " AND MICBRCD<>'$select_br_53' ";
            }
            
            if ($select_br_54 != "") {

                $sql .= " AND MICBRCD<>'$select_br_54' ";
            }

            if ($select_br_55 != "") {

                $sql .= " AND MICBRCD<>'$select_br_55' ";
            }

            if ($select_br_56 != "") {

                $sql .= " AND MICBRCD<>'$select_br_56' ";
            }

            if ($select_br_57 != "") {

                $sql .= " AND MICBRCD<>'$select_br_57' ";
            }

            if ($select_br_58 != "") {

                $sql .= " AND MICBRCD<>'$select_br_58' ";
            }

            if ($select_br_59 != "") {

                $sql .= " AND MICBRCD<>'$select_br_59' ";
            }

            if ($select_br_60 != "") {

                $sql .= " AND MICBRCD<>'$select_br_60' ";
            }

            if ($select_br_61 != "") {

                $sql .= " AND MICBRCD<>'$select_br_62' ";
            }

            if ($select_br_63 != "") {

                $sql .= " AND MICBRCD<>'$select_br_63' ";
            }

            if ($select_br_64 != "") {

                $sql .= " AND MICBRCD<>'$select_br_64' ";
            }

            if ($select_br_65 != "") {

                $sql .= " AND MICBRCD<>'$select_br_65' ";
            }

            if ($select_br_66 != "") {

                $sql .= " AND MICBRCD<>'$select_br_66' ";
            }

            if ($select_br_67 != "") {

                $sql .= " AND MICBRCD<>'$select_br_67' ";
            }

            if ($select_br_68 != "") {

                $sql .= " AND MICBRCD<>'$select_br_68' ";
            }

            if ($select_br_69 != "") {

                $sql .= " AND MICBRCD<>'$select_br_69' ";
            }

            if ($select_br_70 != "") {

                $sql .= " AND MICBRCD<>'$select_br_70' ";
            }

            $sql .= " ) ) ";

        //$sql .= " ORDER BY MICNAME2 ASC ";    
            $sql.=" LIMIT $start, $per_page";  ///echo $sql;
      
  }

    //echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data($branch_state, $branch_code, $region_code, $zone_code, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;

        


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
                     FROM daily_policy WHERE MICREFNO=0 ";



            if ($branch_code != 'A') {
                $sql .= " AND MICBRCD='$branch_code' ";
            } else {
                /* ///oct 15
                  if ($select_br_1 !="" ) {

                  $sql .= " AND MICBRCD <>'$select_br_1' ";

                  }

                  if ($select_br_2 !="" ) {

                  $sql .= " AND MICBRCD <>'$select_br_2' ";

                  }

                  if ($select_br_3 !="" ) {

                  $sql .= " AND MICBRCD <>'$select_br_3' ";

                  }

                 */
            }
        } else {

            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
                          FROM daily_policy  WHERE MICREFNO=0";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'    ";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
                         FROM daily_policy   WHERE MICREFNO=0";

                if ($zone_code != 'A') {
                    $sql .=" AND MICZONE='$zone_code'";
                }
            }
        }


        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;
        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
       
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
        





        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 "; ///SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }


                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }
                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }
                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
                
                if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
                
                if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
                
                if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
                
                if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
                
                if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
                
                if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
                
                if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
                
                if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
                
                if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
                
                if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
                
                if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
                
                
                if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
                
                if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
                
                if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
                
                if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
                
                
                if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
                
                if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
                
                if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
                
                
                if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
                
                if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
                
                if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
                
                if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
                
                if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
                
                if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
                
                if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
                
                if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
                
                if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
                
                if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
                
                if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
                
                if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }

                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_70' ";
                }
                    
                
            }
        } else {
            
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= "AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= "AND MICZONE='$zone_code'";
                }
            }
        }
//echo $sql;


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    function add_serial($type, $start_serial, $end_serial, $addded_epf) {

        $now_date = mysql_datetime();
        $b = 0;

        $data = array(
            'serail_id_type' => $type,
            'start_serial' => $start_serial,
            'end_serial' => $end_serial,
            'addded_epf' => $addded_epf,
            'added_date' => $now_date
        );

        $result = $this->db->insert('mic_card_serial_summery', $data);

        if ($result = $this->db->insert_id()) {


            for ($x = $start_serial; $x <= $end_serial; $x++) {

                $data2 = array(
                    'ser_type' => $type,
                    'serial' => $x,
                    'serial_state' => 1,
                    'print_state' => 'N',
                    'serai_enter_epf' => $addded_epf,
                    'added_date' => $now_date
                );

                $b++;

                $result = $this->db->insert('mic_card_serial', $data2);
            }
        }

        if ($b > 0) {
            return 1;
        } else {
            return 0;
        }
        // $this->load->database()->close();
    }

    public function get_all_daily_card_data_to_printer_by_hand_list($branch_state, $branch_code, $region_code, $zone_code) { ////not in used
        $sql = "";
        $sql .= "SELECT * FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='B'";

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {



                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    ///MICREFNO



    function update_by_hand_printer_list($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand) { //done sep 26 

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;


        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
        

        $this->db->select('*');
        $this->db->from('daily_policy');

        //DEBTOR EXCLUDE : excluded Debtor codes : B26016 and B26001 for renewals
        $this->db->where("MICPLNO NOT IN (SELECT t2.MICPLNO FROM daily_policy AS t2 WHERE t2.MICSPNO  IN('B26016','B26001') AND t2.MICRNCT != 999 AND t2.MICENCT  = 999 AND t2.MICREFNO=0 AND t2.MICBYHND='$by_hand' AND t2.MICMNFL<>'M') ");
       
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', 'B');
        $this->db->where('MICMNFL !=', 'M'); ///// nov 09



        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $this->db->where('MICBRCD', $branch_code);
                
            } else {

                $this->db->where('MICBRCD !=', $select_br_1);
                
                if ($select_br_2 != "") {
                    $this->db->where('MICBRCD !=', $select_br_2);
                }
                if ($select_br_3 != "") {
                    $this->db->where('MICBRCD !=', $select_br_3);
                }
                if ($select_br_4 != "") {
                    $this->db->where('MICBRCD !=', $select_br_4);
                }
                if ($select_br_5 != "") {
                    $this->db->where('MICBRCD !=', $select_br_5);
                }

                if ($select_br_6 != "") {
                    $this->db->where('MICBRCD !=', $select_br_6);
                }
                if ($select_br_7 != "") {
                    $this->db->where('MICBRCD !=', $select_br_7);
                }
                if ($select_br_8 != "") {
                    $this->db->where('MICBRCD !=', $select_br_8);
                }
                if ($select_br_9 != "") {
                    $this->db->where('MICBRCD !=', $select_br_9);
                }
                if ($select_br_10 != "") {
                    $this->db->where('MICBRCD !=', $select_br_10);
                }
                if ($select_br_11 != "") {
                    $this->db->where('MICBRCD !=', $select_br_11);
                }
                if ($select_br_12 != "") {
                    $this->db->where('MICBRCD !=', $select_br_12);
                }
                if ($select_br_13 != "") {
                    $this->db->where('MICBRCD !=', $select_br_13);
                }
                if ($select_br_14 != "") {
                    $this->db->where('MICBRCD !=', $select_br_14);
                }
                if ($select_br_15 != "") {
                    $this->db->where('MICBRCD !=', $select_br_15);
                }
                if ($select_br_16 != "") {
                    $this->db->where('MICBRCD !=', $select_br_16);
                }
                if ($select_br_17 != "") {
                    $this->db->where('MICBRCD !=', $select_br_17);
                }
                if ($select_br_18 != "") {
                    $this->db->where('MICBRCD !=', $select_br_18);
                }
                if ($select_br_19 != "") {
                    $this->db->where('MICBRCD !=', $select_br_19);
                }

                if ($select_br_20 != "") {
                    $this->db->where('MICBRCD !=', $select_br_20);
                }
                if ($select_br_21 != "") {
                    $this->db->where('MICBRCD !=', $select_br_21);
                }
                if ($select_br_22 != "") {
                    $this->db->where('MICBRCD !=', $select_br_22);
                }
                if ($select_br_23 != "") {
                    $this->db->where('MICBRCD !=', $select_br_23);
                }
                if ($select_br_24 != "") {
                    $this->db->where('MICBRCD !=', $select_br_24);
                }
                if ($select_br_25 != "") {
                    $this->db->where('MICBRCD !=', $select_br_25);
                }
                if ($select_br_26 != "") {
                    $this->db->where('MICBRCD !=', $select_br_26);
                }
                if ($select_br_27 != "") {
                    $this->db->where('MICBRCD !=', $select_br_27);
                }
                if ($select_br_28 != "") {
                    $this->db->where('MICBRCD !=', $select_br_28);
                }
                if ($select_br_29 != "") {
                    $this->db->where('MICBRCD !=', $select_br_29);
                }
                if ($select_br_30 != "") {
                    $this->db->where('MICBRCD !=', $select_br_30);
                }
                
                if ($select_br_31 != "") {
                    $this->db->where('MICBRCD !=', $select_br_31);
                }
                
                if ($select_br_32 != "") {
                    $this->db->where('MICBRCD !=', $select_br_32);
                }
                
                if ($select_br_33 != "") {
                    $this->db->where('MICBRCD !=', $select_br_33);
                }
                
                if ($select_br_34 != "") {
                    $this->db->where('MICBRCD !=', $select_br_34);
                }
                
                if ($select_br_35 != "") {
                    $this->db->where('MICBRCD !=', $select_br_35);
                }
                
                if ($select_br_36 != "") {
                    $this->db->where('MICBRCD !=', $select_br_36);
                }
                
                if ($select_br_37 != "") {
                    $this->db->where('MICBRCD !=', $select_br_37);
                }
                
                if ($select_br_38 != "") {
                    $this->db->where('MICBRCD !=', $select_br_38);
                }
                
                if ($select_br_39 != "") {
                    $this->db->where('MICBRCD !=', $select_br_39);
                }
                
                if ($select_br_40 != "") {
                    $this->db->where('MICBRCD !=', $select_br_40);
                }
                
                if ($select_br_41 != "") {
                    $this->db->where('MICBRCD !=', $select_br_41);
                }
                
                if ($select_br_42 != "") {
                    $this->db->where('MICBRCD !=', $select_br_42);
                }
                
                if ($select_br_43 != "") {
                    $this->db->where('MICBRCD !=', $select_br_43);
                }
                if ($select_br_44 != "") {
                    $this->db->where('MICBRCD !=', $select_br_44);
                }
                
                if ($select_br_45 != "") {
                    $this->db->where('MICBRCD !=', $select_br_45);
                }
                if ($select_br_46 != "") {
                    $this->db->where('MICBRCD !=', $select_br_46);
                }
                if ($select_br_47 != "") {
                    $this->db->where('MICBRCD !=', $select_br_47);
                }
                if ($select_br_48 != "") {
                    $this->db->where('MICBRCD !=', $select_br_48);
                }
                if ($select_br_49 != "") {
                    $this->db->where('MICBRCD !=', $select_br_49);
                }
                if ($select_br_50 != "") {
                    $this->db->where('MICBRCD !=', $select_br_50);
                }



                if ($select_br_50 != "") {
                    $this->db->where('MICBRCD !=', $select_br_50);
                }
                if ($select_br_51 != "") {
                    $this->db->where('MICBRCD !=', $select_br_51);
                }
                if ($select_br_52 != "") {
                    $this->db->where('MICBRCD !=', $select_br_52);
                }
                if ($select_br_53 != "") {
                    $this->db->where('MICBRCD !=', $select_br_53);
                }
                if ($select_br_54 != "") {
                    $this->db->where('MICBRCD !=', $select_br_54);
                }
                if ($select_br_55 != "") {
                    $this->db->where('MICBRCD !=', $select_br_55);
                }
                if ($select_br_56 != "") {
                    $this->db->where('MICBRCD !=', $select_br_56);
                }
                if ($select_br_57 != "") {
                    $this->db->where('MICBRCD !=', $select_br_57);
                }
                if ($select_br_58 != "") {
                    $this->db->where('MICBRCD !=', $select_br_58);
                }
                if ($select_br_59 != "") {
                    $this->db->where('MICBRCD !=', $select_br_59);
                }
                if ($select_br_60 != "") {
                    $this->db->where('MICBRCD !=', $select_br_60);
                }



                if ($select_br_61 != "") {
                    $this->db->where('MICBRCD !=', $select_br_61);
                }
                if ($select_br_62 != "") {
                    $this->db->where('MICBRCD !=', $select_br_62);
                }
                if ($select_br_63 != "") {
                    $this->db->where('MICBRCD !=', $select_br_63);
                }
                if ($select_br_64 != "") {
                    $this->db->where('MICBRCD !=', $select_br_64);
                }
                if ($select_br_65 != "") {
                    $this->db->where('MICBRCD !=', $select_br_65);
                }
                if ($select_br_66 != "") {
                    $this->db->where('MICBRCD !=', $select_br_66);
                }
                if ($select_br_67 != "") {
                    $this->db->where('MICBRCD !=', $select_br_67);
                }
                if ($select_br_68 != "") {
                    $this->db->where('MICBRCD !=', $select_br_68);
                }
                if ($select_br_69 != "") {
                    $this->db->where('MICBRCD !=', $select_br_69);
                }
                if ($select_br_70 != "") {
                    $this->db->where('MICBRCD !=', $select_br_70);
                }

    
                
            }
        } else {
            if ($branch_state == 'R') {


                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }


        $this->db->order_by('MICNAME2', 'ASC');
        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = trim($row['MICNAME2']);
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];
                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];

                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'M',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                $g++;
            }
        }

        //return $g;



        $this->db->where('MICBYHND', $by_hand);
        $this->db->where('MICREFNO', 0);

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {
                $this->db->where('MICBRCD', $branch_code);
            } else {

                $this->db->where('MICBRCD !=', $select_br_1);

                if ($select_br_2 != "") {
                    $this->db->where('MICBRCD !=', $select_br_2);
                }

                if ($select_br_3 != "") {

                    $this->db->where('MICBRCD !=', $select_br_3); //oct 15
                }

                if ($select_br_4 != "") {

                    $this->db->where('MICBRCD !=', $select_br_4); //oct 15
                }

                if ($select_br_5 != "") {

                    $this->db->where('MICBRCD !=', $select_br_5); //oct 15
                }

                if ($select_br_6 != "") {

                    $this->db->where('MICBRCD !=', $select_br_6); //oct 15
                }
                if ($select_br_7 != "") {

                    $this->db->where('MICBRCD !=', $select_br_7); //oct 15
                }

                if ($select_br_8 != "") {

                    $this->db->where('MICBRCD !=', $select_br_8); //oct 15
                }

                if ($select_br_9 != "") {

                    $this->db->where('MICBRCD !=', $select_br_9); //oct 15
                }
                if ($select_br_10 != "") {

                    $this->db->where('MICBRCD !=', $select_br_10); //oct 15
                }

                if ($select_br_11 != "") {

                    $this->db->where('MICBRCD !=', $select_br_11); //oct 15
                }
                if ($select_br_12 != "") {

                    $this->db->where('MICBRCD !=', $select_br_12); //oct 15
                }

                if ($select_br_13 != "") {

                    $this->db->where('MICBRCD !=', $select_br_13); //oct 15
                }
                if ($select_br_14 != "") {

                    $this->db->where('MICBRCD !=', $select_br_14); //oct 15
                }
                if ($select_br_15 != "") {

                    $this->db->where('MICBRCD !=', $select_br_15); //oct 15
                }
                if ($select_br_16 != "") {

                    $this->db->where('MICBRCD !=', $select_br_16); //oct 15
                }
                if ($select_br_17 != "") {

                    $this->db->where('MICBRCD !=', $select_br_17); //oct 15
                }

                if ($select_br_18 != "") {

                    $this->db->where('MICBRCD !=', $select_br_18); //oct 15
                }

                if ($select_br_19 != "") {

                    $this->db->where('MICBRCD !=', $select_br_19); //oct 15
                }
                if ($select_br_20 != "") {

                    $this->db->where('MICBRCD !=', $select_br_20); //oct 15
                }
                
                if ($select_br_21 != "") {
                    $this->db->where('MICBRCD !=', $select_br_21);
                }
                
                if ($select_br_22 != "") {
                    $this->db->where('MICBRCD !=', $select_br_22);
                }
                
                if ($select_br_23 != "") {
                    $this->db->where('MICBRCD !=', $select_br_23);
                }
                
                if ($select_br_24 != "") {
                    $this->db->where('MICBRCD !=', $select_br_24);
                }
                if ($select_br_25 != "") {
                    $this->db->where('MICBRCD !=', $select_br_25);
                }
                if ($select_br_26 != "") {
                    $this->db->where('MICBRCD !=', $select_br_26);
                }
                if ($select_br_27 != "") {
                    $this->db->where('MICBRCD !=', $select_br_27);
                }
                if ($select_br_28 != "") {
                    $this->db->where('MICBRCD !=', $select_br_28);
                }
                
                if ($select_br_29 != "") {
                    $this->db->where('MICBRCD !=', $select_br_29);
                }
                if ($select_br_30 != "") {
                    $this->db->where('MICBRCD !=', $select_br_30);
                }
                
                if ($select_br_31 != "") {
                    $this->db->where('MICBRCD !=', $select_br_31);
                }
                
                if ($select_br_32 != "") {
                    $this->db->where('MICBRCD !=', $select_br_32);
                }
                
                if ($select_br_33 != "") {
                    $this->db->where('MICBRCD !=', $select_br_33);
                }
                
                if ($select_br_34 != "") {
                    $this->db->where('MICBRCD !=', $select_br_34);
                }
                
                if ($select_br_35 != "") {
                    $this->db->where('MICBRCD !=', $select_br_35);
                }
                
                if ($select_br_36 != "") {
                    $this->db->where('MICBRCD !=', $select_br_36);
                }
                
                if ($select_br_37 != "") {
                    $this->db->where('MICBRCD !=', $select_br_37);
                }
                
                if ($select_br_38 != "") {
                    $this->db->where('MICBRCD !=', $select_br_38);
                }
                
                if ($select_br_39 != "") {
                    $this->db->where('MICBRCD !=', $select_br_39);
                }
                if ($select_br_40 != "") {
                    $this->db->where('MICBRCD !=', $select_br_40);
                }
                
                if ($select_br_41 != "") {
                    $this->db->where('MICBRCD !=', $select_br_41);
                }
                
                if ($select_br_42 != "") {
                    $this->db->where('MICBRCD !=', $select_br_42);
                }
                
                if ($select_br_43 != "") {
                    $this->db->where('MICBRCD !=', $select_br_43);
                }
                
                if ($select_br_44 != "") {
                    $this->db->where('MICBRCD !=', $select_br_44);
                }
                
                if ($select_br_45 != "") {
                    $this->db->where('MICBRCD !=', $select_br_45);
                }
                
                if ($select_br_46 != "") {
                    $this->db->where('MICBRCD !=', $select_br_46);
                }
                
                if ($select_br_47 != "") {
                    $this->db->where('MICBRCD !=', $select_br_47);
                }
                
                if ($select_br_48 != "") {
                    $this->db->where('MICBRCD !=', $select_br_48);
                }
                
                if ($select_br_49 != "") {
                    $this->db->where('MICBRCD !=', $select_br_49);
                }
                
                if ($select_br_50 != "") {
                    $this->db->where('MICBRCD !=', $select_br_50);
                }

                if ($select_br_51 != "") {
                    $this->db->where('MICBRCD !=', $select_br_51);
                }
                if ($select_br_52 != "") {
                    $this->db->where('MICBRCD !=', $select_br_52);
                }
                if ($select_br_53 != "") {
                    $this->db->where('MICBRCD !=', $select_br_53);
                }
                if ($select_br_54 != "") {
                    $this->db->where('MICBRCD !=', $select_br_54);
                }
                if ($select_br_55 != "") {
                    $this->db->where('MICBRCD !=', $select_br_55);
                }
                if ($select_br_56 != "") {
                    $this->db->where('MICBRCD !=', $select_br_56);
                }
                if ($select_br_57 != "") {
                    $this->db->where('MICBRCD !=', $select_br_57);
                }
                if ($select_br_58 != "") {
                    $this->db->where('MICBRCD !=', $select_br_58);
                }
                if ($select_br_59 != "") {
                    $this->db->where('MICBRCD !=', $select_br_59);
                }
                if ($select_br_60 != "") {
                    $this->db->where('MICBRCD !=', $select_br_60);
                }



                if ($select_br_61 != "") {
                    $this->db->where('MICBRCD !=', $select_br_61);
                }
                if ($select_br_62 != "") {
                    $this->db->where('MICBRCD !=', $select_br_62);
                }
                if ($select_br_63 != "") {
                    $this->db->where('MICBRCD !=', $select_br_63);
                }
                if ($select_br_64 != "") {
                    $this->db->where('MICBRCD !=', $select_br_64);
                }
                if ($select_br_65 != "") {
                    $this->db->where('MICBRCD !=', $select_br_65);
                }
                if ($select_br_66 != "") {
                    $this->db->where('MICBRCD !=', $select_br_66);
                }
                if ($select_br_67 != "") {
                    $this->db->where('MICBRCD !=', $select_br_67);
                }
                if ($select_br_68 != "") {
                    $this->db->where('MICBRCD !=', $select_br_68);
                }
                if ($select_br_69 != "") {
                    $this->db->where('MICBRCD !=', $select_br_69);
                }
                if ($select_br_70 != "") {
                    $this->db->where('MICBRCD !=', $select_br_70);
                }               
                
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }





        $arr = array('MICRESEPF' => $epf_user,
                     'MICRML3'   => 'D',
                     'MICREFNO ' => $epf_user,
                     'MICCDUS' => $print_user);



        $result = $this->db->update('daily_policy', $arr);
       ///echo  $str = $this->db->last_query();  
        return $this->db->affected_rows();
    }

    function update_by_hand_printer_list_colombo_none_colombo($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand, $city) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
        


        $this->db->select('*');
        $this->db->from('daily_policy');
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', $by_hand); ///MICCITY='$city' AND MICBYHND=''
        $this->db->where('MICCITY', $city);
        $this->db->where('MICMNFL !=', 'M');
        //$this->db->order_by('MICNAME', 'ASC'); ///tuesday aug 18 

        /* if ($branch_state == 'B') {


          if ($branch_code!='A'){

          $this->db->where('MICBRCD', $branch_code);


          }
          else
          {
          $this->db->where('MICBRCD !=', $select_br_1);

          if ($select_br_2!=""){

          $this->db->where('MICBRCD !=', $select_br_2);
          }

          if ($select_br_3!=""){


          $this->db->where('MICBRCD !=', $select_br_3);
          }


          }


          } else {
          if ($branch_state == 'R') {


          if ($region_code!='A'){
          $this->db->where('MICREGN', $region_code);
          }


          } else {


          if ($zone_code!='A'){
          $this->db->where('MICZONE', $zone_code);
          }
          }
          }

         */

        $this->db->order_by('MICNAME', 'ASC');

        $query_1 = $this->db->get();

        ///echo "ccc".$query_1->num_rows();
        //echo  $str = $this->db->last_query();  



        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = $row['MICNAME2'];
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];

                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];



                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'M',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                //$g++;
            }
        }

        //echo  $str = $this->db->last_query(); 




        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', $by_hand); ////$by_hand
        //$this->db->where('MICCITY', $city);

        /*


          if ($branch_state == 'B') {


          if ($branch_code !='A') {
          $this->db->where('MICBRCD', $branch_code);
          }
          else
          {
          $this->db->where('MICBRCD !=', $select_br_1);

          if ($select_br_2!=""){
          $this->db->where('MICBRCD !=', $select_br_2);
          }

          if ($select_br_3!=""){
          $this->db->where('MICBRCD !=', $select_br_3);
          }
          }


          } else {
          if ($branch_state == 'R') {



          if ($region_code !='A') {
          $this->db->where('MICREGN', $region_code);
          }

          } else {


          if ($zone_code !='A') {
          $this->db->where('MICZONE', $zone_code);
          }
          }
          }

         */

        $this->db->where('MICCITY', $city);
        $this->db->where('MICMNFL !=', 'M'); ////nov 09
        ///$this->db->where('MICBYHND', $by_hand);


        $arr = array('MICRESEPF' => $epf_user,
            'MICREFNO ' => $epf_user); ///,
        ////// 'MICCDUS'       => $print_user );

        $result = $this->db->update('daily_policy', $arr);

        //echo $str = $this->db->last_query(); 
        return $this->db->affected_rows();
    }

    public function display_daily_card_data_count_with_filter_branch_level($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
        
        





        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }
                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }



                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }
                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
               
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }
                
                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }
                
                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }
                
                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }
                
                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }
                
                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }
                
                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }
                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }
                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }
                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
                
                if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
                
                if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
                if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
                
                if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
                if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
                
                if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
                
                if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
                
                if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
                
                if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
                
                if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
                
                if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
                
                if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
                
                if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
                
                if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
                
                if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
                
                if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
                
                if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
                
                if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
                
                if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
                
                if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
                
                if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
                
                if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
                
                if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
                
                
                if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
                
                if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
                
                if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
                
                
                if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
                
                
                if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
                
                if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
                
                if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }


                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_70' ";
                }
                
                
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND='' ";
        }

//echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {


        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
        
        


        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand' AND MICMNFL<>'M' ";


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($by_hand != "") {

                    if ($select_br_1 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_1' ";
                    }
                    if ($select_br_2 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_2' ";
                    }

                    if ($select_br_3 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_3' ";
                    }

                    if ($select_br_4 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_4' ";
                    }

                    if ($select_br_5 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_5' ";
                    }

                    if ($select_br_6 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_6' ";
                    }

                    if ($select_br_7 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_7' ";
                    }

                    if ($select_br_8 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_8' ";
                    }

                    if ($select_br_9 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_9' ";
                    }

                    if ($select_br_10 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_10' ";
                    }

                     if ($select_br_11 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_11' ";
                    }
                    
                     if ($select_br_12 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_12' ";
                    }
                    
                     if ($select_br_13 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_13' ";
                    }
                    
                     if ($select_br_14 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_14' ";
                    }
                    
                     if ($select_br_15 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_15' ";
                    }
                    
                     if ($select_br_16 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_16' ";
                    }
                    
                     if ($select_br_17 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_17' ";
                    }
                    
                     if ($select_br_18 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_18' ";
                    }
                    
                     if ($select_br_19 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_19' ";
                    }
                    
                     if ($select_br_20 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_20' ";
                    }
                    
                     if ($select_br_21 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_21' ";
                    }
                    
                     if ($select_br_22 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_22' ";
                    }
                    
                     if ($select_br_23 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_23' ";
                    }
                    
                     if ($select_br_24 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_24' ";
                    }
                    
                     if ($select_br_25 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_25' ";
                    }
                    
                     if ($select_br_26 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_26' ";
                    }
                    
                     if ($select_br_27 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_27' ";
                    }
                    
                     if ($select_br_28 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_28' ";
                    }
                    
                     if ($select_br_29 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_29' ";
                    }
                    
                     if ($select_br_30 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_30' ";
                    }
                    
                    if ($select_br_31 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_31' ";
                    }
                    
                    if ($select_br_32 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_32' ";
                    }
                    
                    if ($select_br_33 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_33' ";
                    }
                    
                    if ($select_br_34 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_34' ";
                    }
                    
                    if ($select_br_35 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_35' ";
                    }
                    
                    if ($select_br_36 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_36' ";
                    }
                    
                    if ($select_br_37 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_37' ";
                    }
                    
                    if ($select_br_38 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_38' ";
                    }
                    
                    if ($select_br_39 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_39' ";
                    }
                    
                    if ($select_br_40 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_40' ";
                    }
                    
                    if ($select_br_41 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_41' ";
                    }
                    
                    if ($select_br_42 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_42' ";
                    }
                    
                    if ($select_br_43 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_43' ";
                    }
                    
                    if ($select_br_44 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_44' ";
                    }
                    
                    if ($select_br_45 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_45' ";
                    }
                    
                    
                    if ($select_br_46 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_46' ";
                    }
                    
                    if ($select_br_47 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_47' ";
                    }
                    
                    if ($select_br_48 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_48' ";
                    }
                    
                    if ($select_br_49 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_49' ";
                    }
                    
                    
                    if ($select_br_50 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_50' ";
                    }


                    if ($select_br_51 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_51' ";
                    }
                    

                    if ($select_br_52 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_52' ";
                    }
                    

                    if ($select_br_53 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_53' ";
                    }
                    
                    if ($select_br_54 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_54' ";
                    }

                    if ($select_br_55 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_55' ";
                    }

                    if ($select_br_56 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_56' ";
                    }

                    if ($select_br_57 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_57' ";
                    }

                    if ($select_br_58 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_58' ";
                    }

                    if ($select_br_59 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_59' ";
                    }

                    if ($select_br_60 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_60' ";
                    }

                    if ($select_br_61 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_62' ";
                    }

                    if ($select_br_63 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_63' ";
                    }

                    if ($select_br_64 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_64' ";
                    }

                    if ($select_br_65 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_65' ";
                    }

                    if ($select_br_66 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_66' ";
                    }

                    if ($select_br_67 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_67' ";
                    }

                    if ($select_br_68 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_68' ";
                    }

                    if ($select_br_69 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_69' ";
                    }

                    if ($select_br_70 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_70' ";
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND='' ";
        }

//echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;



        ///BR_CLIENT_1


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV,MICNAME2 FROM daily_policy  WHERE 


             MICPLNO NOT IN (SELECT t2.MICPLNO FROM daily_policy AS t2 WHERE t2.MICSPNO  IN('B26016','B26001') AND t2.MICRNCT != 999 AND t2.MICENCT  = 999 AND t2.MICREFNO=0 AND t2.MICBYHND='$by_hand' AND t2.MICMNFL<>'M')
            AND
            MICREFNO=0 AND MICBYHND='$by_hand' AND MICMNFL<>'M'";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($by_hand == "B") {
                    if ($select_br_1 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_1' ";
                    }

                    if ($select_br_2 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_2' ";
                    }

                    if ($select_br_3 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_3' ";
                    }


                    if ($select_br_4 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_4' ";
                    }


                    if ($select_br_5 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_5' ";
                    }


                    if ($select_br_6 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_6' ";
                    }

                    if ($select_br_7 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_7' ";
                    }

                    if ($select_br_8 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_8' ";
                    }

                    if ($select_br_9 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_9' ";
                    }

                    if ($select_br_10 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_10' ";
                    }

                    if ($select_br_11 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_11' ";
                    }

                    if ($select_br_12 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_12' ";
                    }


                    if ($select_br_13 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_13' ";
                    }

                    if ($select_br_14 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_14' ";
                    }

                    if ($select_br_15 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_15' ";
                    }

                    if ($select_br_16 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_16' ";
                    }


                    if ($select_br_17 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_17' ";
                    }


                    if ($select_br_18 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_18' ";
                    }

                    if ($select_br_19 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_19' ";
                    }

                    if ($select_br_20 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_20' ";
                    }
                    
                    if ($select_br_21 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_21' ";
                    }
                    
                      if ($select_br_22 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_22' ";
                    }
                    
                    if ($select_br_23 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_23' ";
                    }
                    
                      if ($select_br_24 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_24' ";
                    }
                    
                    if ($select_br_25 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_25' ";
                    }
                    
                      if ($select_br_26 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_26' ";
                    }
                    
                    if ($select_br_27 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_27' ";
                    }
                    
                      if ($select_br_28 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_28' ";
                    }
                    
                    if ($select_br_29 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_29' ";
                    }
                    
                    if ($select_br_30 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_30' ";
                    }
                    
                    if ($select_br_31 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_31' ";
                    }
                    
                    if ($select_br_32 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_32' ";
                    }
                    
                    if ($select_br_33 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_33' ";
                    }
                    
                    if ($select_br_34 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_34' ";
                    }
                    
                    
                    if ($select_br_35 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_35' ";
                    }
                    
                    if ($select_br_36 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_36' ";
                    }
                    
                    if ($select_br_37 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_37' ";
                    }
                    
                    if ($select_br_38 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_38' ";
                    }
                    
                    if ($select_br_39 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_39' ";
                    }
                    
                    if ($select_br_40 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_40' ";
                    }
                    
                    if ($select_br_41 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_41' ";
                    }
                    
                    if ($select_br_42 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_42' ";
                    }
                    
                    if ($select_br_43 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_43' ";
                    }
                    
                    if ($select_br_44 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_44' ";
                    }
                    
                    if ($select_br_45 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_45' ";
                    }
                    
                    if ($select_br_46 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_46' ";
                    }
                    
                    if ($select_br_47 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_47' ";
                    }
                    
                    
                    if ($select_br_48 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_48' ";
                    }
                    
                    if ($select_br_49 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_49' ";
                    }
                    
                    if ($select_br_50 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_50' ";
                    }


                    if ($select_br_51 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_51' ";
                    }
                    

                    if ($select_br_52 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_52' ";
                    }
                    

                    if ($select_br_53 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_53' ";
                    }
                    
                    if ($select_br_54 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_54' ";
                    }

                    if ($select_br_55 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_55' ";
                    }

                    if ($select_br_56 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_56' ";
                    }

                    if ($select_br_57 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_57' ";
                    }

                    if ($select_br_58 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_58' ";
                    }

                    if ($select_br_59 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_59' ";
                    }

                    if ($select_br_60 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_60' ";
                    }

                    if ($select_br_61 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_62' ";
                    }

                    if ($select_br_63 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_63' ";
                    }

                    if ($select_br_64 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_64' ";
                    }

                    if ($select_br_65 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_65' ";
                    }

                    if ($select_br_66 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_66' ";
                    }

                    if ($select_br_67 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_67' ";
                    }

                    if ($select_br_68 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_68' ";
                    }

                    if ($select_br_69 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_69' ";
                    }

                    if ($select_br_70 != "") {

                        $sql .= " AND MICBRCD<>'$select_br_70' ";
                    }
                    
                    
                    
                    
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND=''";
        }

        if ($by_hand == 'B' && $branch_code == 'A') {

            $sql .= " ORDER BY MICNAME2 ";
        } else {
            $sql .= " ORDER BY MICSPNO ASC ";
        }



//echo $sql;
        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter_branch_level_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
        
        $select_br_21 = BR_CLIENT_21;
        $select_br_22 = BR_CLIENT_22;
        $select_br_23 = BR_CLIENT_23;
        $select_br_24 = BR_CLIENT_24;
        $select_br_25 = BR_CLIENT_25;
        $select_br_26 = BR_CLIENT_26;
        $select_br_27 = BR_CLIENT_27;
        $select_br_28 = BR_CLIENT_28;
        $select_br_29 = BR_CLIENT_29;
        $select_br_30 = BR_CLIENT_30;
        
        
        $select_br_31 = BR_CLIENT_31;
        $select_br_32 = BR_CLIENT_32;
        $select_br_33 = BR_CLIENT_33;
        $select_br_34 = BR_CLIENT_34;
        $select_br_35 = BR_CLIENT_35;
        $select_br_36 = BR_CLIENT_36;
        $select_br_37 = BR_CLIENT_37;
        $select_br_38 = BR_CLIENT_38;
        $select_br_39 = BR_CLIENT_39;
        $select_br_40 = BR_CLIENT_40;
        $select_br_41 = BR_CLIENT_41;
        $select_br_42 = BR_CLIENT_42;
        $select_br_43 = BR_CLIENT_43;
        $select_br_44 = BR_CLIENT_44;
        $select_br_45 = BR_CLIENT_45;
        
        $select_br_46 = BR_CLIENT_46;
        $select_br_47 = BR_CLIENT_47;
        $select_br_48 = BR_CLIENT_48;
        $select_br_49 = BR_CLIENT_49;
        $select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;




        ///BR_CLIENT_1


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }



                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }


                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }
                
                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }
                
                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }
                
                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }
                
                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }
                
                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }
                
                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }
                
                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }
                
                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }
                
                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
                
                if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
                
                if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
                
                if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
                
                
                if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
                
                
                if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
                
                if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
                
                if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
                
                if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
                
                if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
                
                if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
                
                if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
                
                if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
                
                if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
                
                if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
                
                if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
                
                if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
                
                if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
                
                if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
                
                
                if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
                

               if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
                
               if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
                
                if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
                    
                
                if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
                
                if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
                
                if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
                
                if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
                
                if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
                if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }


                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD<>'$select_br_70' ";
                }
                
                
                
               
               
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND=''";
        }

        if ($by_hand == 'B') {

            $sql .= " ORDER BY MICNAME2 ";
        } else {
            $sql .= " ORDER BY MICNAME ";
        }



//echo $sql;
        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

}

?>
