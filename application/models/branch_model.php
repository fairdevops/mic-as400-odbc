
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branch_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    ////sfbrdes

    function get_branch_codes() {

        $this->db->select('BRCODE,BRDESCRP');
        $this->db->from('mic_branches');
        $this->db->order_by('BRCODE', 'ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function get_regon_code($BRCODE) {

        $query_str = "SELECT REGIONMASTER_RGCODE FROM mic_branches WHERE BRCODE=? ";
        $result = $this->db->query($query_str, array($BRCODE));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['REGIONMASTER_RGCODE'];
        } else {
            return false;
        }
    }

    public function get_branch_name($BRCODE) {

        $query_str = "SELECT BRDESCRP FROM mic_branches WHERE BRCODE=? ";
        $result = $this->db->query($query_str, array($BRCODE));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['BRDESCRP'];
        } else {
            return false;
        }
    }

}

?>
