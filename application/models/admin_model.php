
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function check_user_epf_no_to_edit($client_epf_no, $usr_id) {

        $this->db->select('epf_no');
        $this->db->from('mic_users');
        $this->db->where('usr_id', $usr_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $data = $query->row_array();
            if ($data['epf_no'] == $client_epf_no) {
                return 'Y';
            } else {
                return 'N';
            }
        } else {
            return 'N';
        }
    }

    function add_previous_user_data($p_epf_no, $p_users_name, $p_brz_state, $p_branch, $p_region, $p_zone, $p_view_only, $p_print_only, $p_reports_allow, $p_search_allow, $p_upload_allow_fleet, $p_upload_allow_nexus, $p_add_user_epf, $p_user_level, $p_now_date) {

        $now_date = mysql_datetime();

        $data = array(
            'prev_epf_no' => $p_epf_no,
            'prev_users_name' => $p_users_name,
            'prev_brz_state' => $p_brz_state,
            'prev_branch' => $p_branch,
            'prev_region' => $p_region,
            'prev_zone' => $p_zone,
            'prev_rec_state' => 1,
            'prev_view_only' => $p_view_only,
            'prev_print_only' => $p_print_only,
            'prev_reports_allow' => $p_reports_allow,
            'prev_search_allow' => $p_search_allow,
            'prev_upload_allow_fleet' => $p_upload_allow_fleet,
            'prev_upload_allow_nexus' => $p_upload_allow_nexus,
            'edit_user_epf' => $p_add_user_epf,
            'prev_user_level' => $p_user_level,
            'update_date' => $p_now_date
        );

        $result = $this->db->insert('mic_users_edit', $data);
        return $result = $this->db->insert_id();

        $this->load->database()->close();
    }

    function update_user_creation_data($epf_no, $users_name, $brz_state, $branch, $region, $zone, $view_only, $print_only, $reports_allow, $search_allow, $upload_allow_fleet, $upload_allow_nexus, $add_user_epf, $user_level, $usr_id) {

        $update_date = mysql_datetime();
        $this->load->model('user_model');

        $user_infor = $this->user_model->get_user_information($usr_id);

        $p_epf_no = $user_infor['epf_no'];
        $p_users_name = $user_infor['users_name'];
        $p_brz_state = $user_infor['brz_state'];
        $p_branch = $user_infor['branch'];
        $p_region = $user_infor['region'];
        $p_zone = $user_infor['zone'];
        $p_view_only = $user_infor['view_only'];
        $p_print_only = $user_infor['print_only'];
        $p_reports_allow = $user_infor['reports_allow'];
        $p_search_allow = $user_infor['search_allow'];
        $p_upload_allow_fleet = $user_infor['upload_allow_fleet'];
        $p_upload_allow_nexus = $user_infor['upload_allow_nexus'];
        $p_add_user_epf = $user_infor['add_user_epf'];
        $p_user_level = $user_infor['user_level'];
        $p_now_date = $user_infor['add_date'];



        $this->add_previous_user_data($p_epf_no, $p_users_name, $p_brz_state, $p_branch, $p_region, $p_zone, $p_view_only, $p_print_only, $p_reports_allow, $p_search_allow, $p_upload_allow_fleet, $p_upload_allow_nexus, $p_add_user_epf, $p_user_level, $p_now_date);



        $this->db->where('usr_id', $usr_id);
        $arr = array(
            'epf_no' => $epf_no,
            'users_name' => $users_name,
            'brz_state' => $brz_state,
            'branch' => $branch,
            'region' => $region,
            'zone' => $zone,
            'rec_state' => 1,
            'view_only' => $view_only,
            'print_only' => $print_only,
            'reports_allow' => $reports_allow,
            'search_allow' => $search_allow,
            'upload_allow_fleet' => $upload_allow_fleet,
            'upload_allow_nexus' => $upload_allow_nexus,
            'add_user_epf' => $add_user_epf,
            'user_level' => $user_level,
            'add_date' => $update_date
        );

        return $result = $this->db->update('mic_users', $arr);
    }

    function search_count_users($search_type, $seek_data) {

        $this->db->select('usr_id');
        $this->db->from('mic_users');
        //$this->db->where('rec_state', 1);

        if ($search_type == 1) {
            $this->db->like('epf_no', $seek_data);
        } else {
            $this->db->like('users_name', $seek_data);
        }

        $this->db->order_by('epf_no', 'desc');

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
        } else {
            return 0;
        }
    }

    function get_all_search_count_users($no_perpage, $search_type, $seek_data) {

        $this->db->select('usr_id,epf_no,users_name,brz_state,user_level');
        $this->db->from('mic_users');

        if ($search_type == 1) {
            $this->db->like('epf_no', $seek_data);
        } else {
            $this->db->like('users_name', $seek_data);
        }

        $this->db->order_by('epf_no', 'desc');

        $this->db->limit($no_perpage, $this->uri->segment(3));

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->result_array();
        }
    }

    function get_all_active_debtor_codes($no_perpage) {

        $this->db->select('debtor_id,debtor_name');
        $this->db->from('mic_debtor_names');
        $this->db->where('debtor_state', 1);
        $this->db->order_by('debtor_name', 'desc');

        $this->db->limit($no_perpage, $this->uri->segment(3));

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->result_array();
        }
    }

    function count_active_debtor_rec_count() {

        $this->db->select('debtor_id');
        $this->db->from('mic_debtor_names');
        $this->db->where('debtor_state', 1);
        $this->db->order_by('debtor_name', 'desc');

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
        } else {
            return 0;
        }
    }

    function get_all_search_count_debtor_codes($no_perpage, $search_type, $seek_data) {

        $this->db->select('debtor_id,debtor_name');
        $this->db->from('mic_debtor_names');

        if ($search_type == 1) {
            $this->db->like('debtor_name', $seek_data);
        }

        $this->db->order_by('debtor_name', 'desc');

        $this->db->limit($no_perpage, $this->uri->segment(3));

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->result_array();
        }
    }

    function search_count_debtor_codes($search_type, $seek_data) {

        $this->db->select('debtor_id');
        $this->db->from('mic_debtor_names');
        if ($search_type == 1) {
            $this->db->like('debtor_name', $seek_data);
        }

        $this->db->order_by('debtor_name', 'desc');

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
        } else {
            return 0;
        }
    }

}

?>
