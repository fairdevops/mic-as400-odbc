
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fleet_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function add_fleed_to_iis($sh_policy_no, $sh_vehicle_no, $sh_period, $sh_cust_name, $sh_address_1, $sh_address_2, $upload_epf, $upload_user, $sh_br_state, $sh_br_code, $sh_re_code, $sh_zo_code, $sh_miccmdt, $sh_micexdt, $remarks) {

        $now_date = mysql_datetime();
        $my_date = mysql_date();

        $data = array(
            'sh_policy_no' => $sh_policy_no,
            'sh_vehicle_no' => $sh_vehicle_no,
            'sh_period' => $sh_period,
            'sh_cust_name' => $sh_cust_name,
            'sh_address_1' => $sh_address_1,
            'sh_address_2' => $sh_address_2,
            'upload_epf' => $upload_epf,
            'upload_user' => $upload_user,
            'sh_br_state' => $sh_br_state,
            'sh_br_code' => $sh_br_code,
            'sh_re_code' => $sh_re_code,
            'sh_zo_code' => $sh_zo_code,
            'sh_miccmdt' => $sh_miccmdt,
            'sh_micexdt' => $sh_micexdt,
            'back_end_update' => 'N',
            'sh_db_update' => $now_date
        );

        $result = $this->db->insert('shedule_fleet_policy_data', $data);
        $last_id = $this->db->insert_id();


        $data3 = array(
            'PR_MIC_INIT_ID' => $last_id,
            'PR_MICPLNO' => $sh_policy_no,
            'PR_MICPCOV' => trim($sh_period),
            'PR_MICVENO' => $sh_vehicle_no,
            //'PR_MICCMDT'           => trim($sh_miccmdt),
            //'PR_MICEXDT'           => trim($sh_micexdt),
            'PR_MICTITL' => "",
            'PR_MICNAME' => $sh_cust_name,
            'PR_MICADD1' => $sh_address_1,
            'PR_MICADD2' => $sh_address_2,
            'PR_MICTOWN' => "",
            'PR_MICCITY' => "",
            'PR_MICBRCD' => $sh_br_code,
            'PR_MICZONE' => $sh_zo_code,
            'PR_MICREGN' => $sh_re_code,
            'PR_MICMNFL' => 'F',
            'PR_MICRESEPF' => $upload_epf,
            'PR_MICNAME2' => $sh_cust_name . " " . $remarks,
            'PR_CARD_PRINT' => 'N',
            'PR_PRINT_DATE' => $my_date,
            'PR_COUNT' => 1,
            'PR_ACT_NAME' => $upload_user,
            'PR_TYPRE' => 'M',
            'PR_B_STATE' => $sh_br_state,
            'PR_BRANCH_CODE' => $sh_br_code,
            'PR_REGION_CODE' => $sh_re_code,
            'PR_ZONE_CODE' => $sh_zo_code
        );


        $result = $this->db->insert('daily_policy_to_printer', $data3);
        return $this->db->insert_id();
        //echo  $str = $this->db->last_query();  print"<br>";
    }

    public function get_fleet_count($sh_vehicle_no, $sh_miccmdt, $sh_micexdt) {


        $this->db->select('fleet_id');
        $this->db->from('shedule_fleet_policy_data');
        $this->db->where('sh_vehicle_no', $sh_vehicle_no);
        $this->db->where('sh_miccmdt', $sh_miccmdt);
        $this->db->where('sh_micexdt', $sh_micexdt);

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
        } else {
            return 0;
        }
    }

    public function get_fleet_count_in_printer_file($sh_vehicle_no, $sh_miccmdt, $sh_micexdt) {


        $this->db->select('PR_MIC_ID');
        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MICVENO', $sh_vehicle_no);
        $this->db->where('PR_MICCMDT', $sh_miccmdt);
        $this->db->where('PR_MICEXDT', $sh_micexdt);

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
        } else {
            return 0;
        }
    }

    public function get_fleet_print_count($branch_state, $branch_code, $region_code, $zone_code, $pstate) {

        $pdate = date("Y-m-d");
        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICMNFL='F' AND PR_PRINT_DATE ='$pdate'  AND PR_CARD_PRINT='$pstate'";

            if ($branch_code != 'A') {
                $sql .= " AND PR_BRANCH_CODE='$branch_code' ";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICMNFL='F' AND PR_PRINT_DATE ='$pdate'  AND PR_CARD_PRINT='$pstate'  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_REGION_CODE='$region_code'	";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICMNFL='F' AND PR_PRINT_DATE ='$pdate'  AND PR_CARD_PRINT='$pstate'";

                if ($zone_code != 'A') {
                    $sql .=" AND PR_ZONE_CODE='$zone_code'";
                }
            }
        }



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function get_fleet_print_count_display_infor($branch_state, $branch_code, $region_code, $zone_code, $pstate, $start, $per_page) {

        $pdate = date("Y-m-d");
        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICPLNO,PR_MICVENO,PR_MICSPNO,PR_CARD_PRINT,PR_MICPCOV,PR_MICNAME FROM daily_policy_to_printer WHERE PR_MICMNFL='F' AND PR_PRINT_DATE ='$pdate'  AND PR_CARD_PRINT='$pstate'";

            if ($branch_code != 'A') {
                $sql .= " AND PR_BRANCH_CODE='$branch_code' ";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICPLNO,PR_MICVENO,PR_MICSPNO,PR_CARD_PRINT,PR_MICPCOV,PR_MICNAME FROM daily_policy_to_printer WHERE PR_MICMNFL='F' AND PR_PRINT_DATE ='$pdate'  AND PR_CARD_PRINT='$pstate'  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_REGION_CODE='$region_code'	";
                }
            } else {

                $sql .= "SELECT PR_MICPLNO,PR_MICVENO,PR_MICSPNO,PR_CARD_PRINT,PR_MICPCOV,PR_MICNAME FROM daily_policy_to_printer WHERE PR_MICMNFL='F' AND PR_PRINT_DATE ='$pdate'  AND PR_CARD_PRINT='$pstate'";

                if ($zone_code != 'A') {
                    $sql .=" AND PR_ZONE_CODE='$zone_code'";
                }
            }
        }

        $sql .=" LIMIT $start, $per_page";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

}

?>
