
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('array');
    }

    public function display_daily_card_data_all_count_at_head_office($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";



        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;

        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy WHERE MICREFNO=0 AND ( MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='' OR ";

            $sql .= "( MICBYHND='B' AND  ";


            if ($select_br_1 != "") {

                $sql .= "  MICBRCD<>'$select_br_1'  ";
            }

            if ($select_br_2 != "") {

                $sql .= " AND MICBRCD<>'$select_br_2' ";
            }

            if ($select_br_3 != "") {

                $sql .= " AND MICBRCD<>'$select_br_3' ";
            }

            if ($select_br_4 != "") {

                $sql .= "AND  MICBRCD<>'$select_br_4' ";
            }


            if ($select_br_5 != "") {

                $sql .= " AND MICBRCD<>'$select_br_5' ";
            }

            if ($select_br_6 != "") {

                $sql .= " AND MICBRCD<>'$select_br_6' ";
            }

            if ($select_br_7 != "") {

                $sql .= " AND MICBRCD<>'$select_br_7' ";
            }

            if ($select_br_8 != "") {

                $sql .= " AND MICBRCD<>'$select_br_8' ";
            }

            if ($select_br_9 != "") {

                $sql .= " AND MICBRCD<>'$select_br_9' ";
            }

            if ($select_br_10 != "") {

                $sql .= " AND MICBRCD<>'$select_br_10' ";
            }
            if ($select_br_11 != "") {

                $sql .= " AND MICBRCD<>'$select_br_11' ";
            }

            if ($select_br_12 != "") {

                $sql .= " AND MICBRCD<>'$select_br_12' ";
            }
            if ($select_br_13 != "") {

                $sql .= " AND MICBRCD<>'$select_br_13' ";
            }

            if ($select_br_14 != "") {

                $sql .= " AND MICBRCD<>'$select_br_14' ";
            }

            if ($select_br_15 != "") {

                $sql .= " AND MICBRCD<>'$select_br_15' ";
            }
            if ($select_br_16 != "") {

                $sql .= " AND MICBRCD<>'$select_br_16' ";
            }

            if ($select_br_17 != "") {

                $sql .= " AND MICBRCD<>'$select_br_17' ";
            }
            if ($select_br_18 != "") {

                $sql .= " AND MICBRCD<>'$select_br_18' ";
            }

            if ($select_br_19 != "") {

                $sql .= " AND MICBRCD<>'$select_br_19' ";
            }
            if ($select_br_20 != "") {

                $sql .= " AND MICBRCD<>'$select_br_20' ";
            }


            $sql .= " ) ) ";

            //echo $sql;
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data_all_at_head_office($branch_state, $branch_code, $region_code, $zone_code, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;

        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
			         FROM daily_policy WHERE MICREFNO=0 AND  ( MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='' OR ";

            $sql .= "( MICBYHND='B' AND  ";

            if ($select_br_1 != "") {

                $sql .= " MICBRCD<>'$select_br_1' ";
            }

            if ($select_br_2 != "") {

                $sql .= " AND MICBRCD<>'$select_br_2' ";
            }

            if ($select_br_3 != "") {

                $sql .= " AND MICBRCD<>'$select_br_3' ";
            }

            if ($select_br_4 != "") {

                $sql .= " AND MICBRCD<>'$select_br_4' ";
            }


            if ($select_br_5 != "") {

                $sql .= " AND MICBRCD<>'$select_br_5' ";
            }

            if ($select_br_6 != "") {

                $sql .= " AND MICBRCD<>'$select_br_6' ";
            }
            if ($select_br_7 != "") {

                $sql .= " AND MICBRCD<>'$select_br_7' ";
            }
            if ($select_br_8 != "") {

                $sql .= " AND MICBRCD<>'$select_br_8' ";
            }
            if ($select_br_9 != "") {

                $sql .= " AND MICBRCD<>'$select_br_9' ";
            }
            if ($select_br_10 != "") {

                $sql .= " AND MICBRCD<>'$select_br_10' ";
            }
            if ($select_br_11 != "") {

                $sql .= " AND MICBRCD<>'$select_br_11' ";
            }
            if ($select_br_12 != "") {

                $sql .= " AND MICBRCD<>'$select_br_12' ";
            }

            if ($select_br_13 != "") {

                $sql .= " AND MICBRCD<>'$select_br_13' ";
            }
            if ($select_br_14 != "") {

                $sql .= " AND MICBRCD<>'$select_br_14' ";
            }

            if ($select_br_15 != "") {

                $sql .= " AND MICBRCD<>'$select_br_15' ";
            }

            if ($select_br_16 != "") {

                $sql .= " AND MICBRCD<>'$select_br_16' ";
            }
            if ($select_br_17 != "") {

                $sql .= " AND MICBRCD<>'$select_br_17' ";
            }
            if ($select_br_18 != "") {

                $sql .= " AND MICBRCD<>'$select_br_18' ";
            }
            if ($select_br_19 != "") {

                $sql .= " AND MICBRCD<>'$select_br_19' ";
            }
            if ($select_br_20 != "") {

                $sql .= " AND MICBRCD<>'$select_br_20' ";
            }


            $sql .= " ) ) ";

            $sql.=" LIMIT $start, $per_page";  ///echo $sql;
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data($branch_state, $branch_code, $region_code, $zone_code, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
		             FROM daily_policy WHERE MICREFNO=0 ";



            if ($branch_code != 'A') {
                $sql .= " AND MICBRCD='$branch_code' ";
            } else {
                /* ///oct 15
                  if ($select_br_1 !="" ) {

                  $sql .= " AND MICBRCD <>'$select_br_1' ";

                  }

                  if ($select_br_2 !="" ) {

                  $sql .= " AND MICBRCD <>'$select_br_2' ";

                  }

                  if ($select_br_3 !="" ) {

                  $sql .= " AND MICBRCD <>'$select_br_3' ";

                  }

                 */
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
		                  FROM daily_policy  WHERE MICREFNO=0";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'	";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV
		                 FROM daily_policy   WHERE MICREFNO=0";

                if ($zone_code != 'A') {
                    $sql .=" AND MICZONE='$zone_code'";
                }
            }
        }


        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;
        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;




        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 "; ///SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }


                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }
                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }
                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= "AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= "AND MICZONE='$zone_code'";
                }
            }
        }
//echo $sql;


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    function add_serial($type, $start_serial, $end_serial, $addded_epf) {

        $now_date = mysql_datetime();
        $b = 0;

        $data = array(
            'serail_id_type' => $type,
            'start_serial' => $start_serial,
            'end_serial' => $end_serial,
            'addded_epf' => $addded_epf,
            'added_date' => $now_date
        );

        $result = $this->db->insert('mic_card_serial_summery', $data);

        if ($result = $this->db->insert_id()) {


            for ($x = $start_serial; $x <= $end_serial; $x++) {

                $data2 = array(
                    'ser_type' => $type,
                    'serial' => $x,
                    'serial_state' => 1,
                    'print_state' => 'N',
                    'serai_enter_epf' => $addded_epf,
                    'added_date' => $now_date
                );

                $b++;

                $result = $this->db->insert('mic_card_serial', $data2);
            }
        }

        if ($b > 0) {
            return 1;
        } else {
            return 0;
        }
        // $this->load->database()->close();
    }

    public function get_all_daily_card_data_to_printer_by_hand_list($branch_state, $branch_code, $region_code, $zone_code) { ////not in used
        $sql = "";
        $sql .= "SELECT * FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='B'";

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {



                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    ///MICREFNO



    function update_by_hand_printer_list($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;


        $this->db->select('*');
        $this->db->from('daily_policy');
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', 'B');
        $this->db->where('MICMNFL !=', 'M'); ///// nov 09
        //$this->db->order_by('MICNAME2', 'ASC'); 

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {

                $this->db->where('MICBRCD', $branch_code);
            } else {

                $this->db->where('MICBRCD !=', $select_br_1);
                if ($select_br_2 != "") {
                    $this->db->where('MICBRCD !=', $select_br_2);
                }
                if ($select_br_3 != "") {
                    $this->db->where('MICBRCD !=', $select_br_3);
                }
                if ($select_br_4 != "") {
                    $this->db->where('MICBRCD !=', $select_br_4);
                }
                if ($select_br_5 != "") {
                    $this->db->where('MICBRCD !=', $select_br_5);
                }

                if ($select_br_6 != "") {
                    $this->db->where('MICBRCD !=', $select_br_6);
                }
                if ($select_br_7 != "") {
                    $this->db->where('MICBRCD !=', $select_br_7);
                }
                if ($select_br_8 != "") {
                    $this->db->where('MICBRCD !=', $select_br_8);
                }
                if ($select_br_9 != "") {
                    $this->db->where('MICBRCD !=', $select_br_9);
                }
                if ($select_br_10 != "") {
                    $this->db->where('MICBRCD !=', $select_br_10);
                }
                if ($select_br_11 != "") {
                    $this->db->where('MICBRCD !=', $select_br_11);
                }
                if ($select_br_12 != "") {
                    $this->db->where('MICBRCD !=', $select_br_12);
                }
                if ($select_br_13 != "") {
                    $this->db->where('MICBRCD !=', $select_br_13);
                }
                if ($select_br_14 != "") {
                    $this->db->where('MICBRCD !=', $select_br_14);
                }
                if ($select_br_15 != "") {
                    $this->db->where('MICBRCD !=', $select_br_15);
                }
                if ($select_br_16 != "") {
                    $this->db->where('MICBRCD !=', $select_br_16);
                }
                if ($select_br_17 != "") {
                    $this->db->where('MICBRCD !=', $select_br_17);
                }
                if ($select_br_18 != "") {
                    $this->db->where('MICBRCD !=', $select_br_18);
                }
                if ($select_br_19 != "") {
                    $this->db->where('MICBRCD !=', $select_br_19);
                }

                if ($select_br_20 != "") {
                    $this->db->where('MICBRCD !=', $select_br_20);
                }
            }
        } else {
            if ($branch_state == 'R') {


                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }

        $this->db->order_by('MICNAME2', 'ASC');
        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = trim($row['MICNAME2']);
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];
                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];

                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'M',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                $g++;
            }
        }

        //return $g;



        $this->db->where('MICBYHND', $by_hand);
        $this->db->where('MICREFNO', 0);

        if ($branch_state == 'B') {


            if ($branch_code != 'A') {
                $this->db->where('MICBRCD', $branch_code);
            } else {

                $this->db->where('MICBRCD !=', $select_br_1);

                if ($select_br_2 != "") {
                    $this->db->where('MICBRCD !=', $select_br_2);
                }

                if ($select_br_3 != "") {

                    $this->db->where('MICBRCD !=', $select_br_3); //oct 15
                }

                if ($select_br_4 != "") {

                    $this->db->where('MICBRCD !=', $select_br_4); //oct 15
                }

                if ($select_br_5 != "") {

                    $this->db->where('MICBRCD !=', $select_br_5); //oct 15
                }

                if ($select_br_6 != "") {

                    $this->db->where('MICBRCD !=', $select_br_6); //oct 15
                }
                if ($select_br_7 != "") {

                    $this->db->where('MICBRCD !=', $select_br_7); //oct 15
                }

                if ($select_br_8 != "") {

                    $this->db->where('MICBRCD !=', $select_br_8); //oct 15
                }

                if ($select_br_9 != "") {

                    $this->db->where('MICBRCD !=', $select_br_9); //oct 15
                }
                if ($select_br_10 != "") {

                    $this->db->where('MICBRCD !=', $select_br_10); //oct 15
                }

                if ($select_br_11 != "") {

                    $this->db->where('MICBRCD !=', $select_br_11); //oct 15
                }
                if ($select_br_12 != "") {

                    $this->db->where('MICBRCD !=', $select_br_12); //oct 15
                }

                if ($select_br_13 != "") {

                    $this->db->where('MICBRCD !=', $select_br_13); //oct 15
                }
                if ($select_br_14 != "") {

                    $this->db->where('MICBRCD !=', $select_br_14); //oct 15
                }
                if ($select_br_15 != "") {

                    $this->db->where('MICBRCD !=', $select_br_15); //oct 15
                }
                if ($select_br_16 != "") {

                    $this->db->where('MICBRCD !=', $select_br_16); //oct 15
                }
                if ($select_br_17 != "") {

                    $this->db->where('MICBRCD !=', $select_br_17); //oct 15
                }

                if ($select_br_18 != "") {

                    $this->db->where('MICBRCD !=', $select_br_18); //oct 15
                }

                if ($select_br_19 != "") {

                    $this->db->where('MICBRCD !=', $select_br_19); //oct 15
                }
                if ($select_br_20 != "") {

                    $this->db->where('MICBRCD !=', $select_br_20); //oct 15
                }
            }
        } else {
            if ($branch_state == 'R') {



                if ($region_code != 'A') {
                    $this->db->where('MICREGN', $region_code);
                }
            } else {


                if ($zone_code != 'A') {
                    $this->db->where('MICZONE', $zone_code);
                }
            }
        }





        $arr = array('MICRESEPF' => $epf_user,
            'MICREFNO ' => $epf_user,
            'MICCDUS' => $print_user);



        $result = $this->db->update('daily_policy', $arr);
        //echo  $str = $this->db->last_query();  
        return $this->db->affected_rows();
    }

    function update_by_hand_printer_list_colombo_none_colombo($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $by_hand, $city) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;


        $this->db->select('*');
        $this->db->from('daily_policy');
        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', $by_hand); ///MICCITY='$city' AND MICBYHND=''
        $this->db->where('MICCITY', $city);
        $this->db->where('MICMNFL !=', 'M');
        //$this->db->order_by('MICNAME', 'ASC'); ///tuesday aug 18 

        /* if ($branch_state == 'B') {


          if ($branch_code!='A'){

          $this->db->where('MICBRCD', $branch_code);


          }
          else
          {
          $this->db->where('MICBRCD !=', $select_br_1);

          if ($select_br_2!=""){

          $this->db->where('MICBRCD !=', $select_br_2);
          }

          if ($select_br_3!=""){


          $this->db->where('MICBRCD !=', $select_br_3);
          }


          }


          } else {
          if ($branch_state == 'R') {


          if ($region_code!='A'){
          $this->db->where('MICREGN', $region_code);
          }


          } else {


          if ($zone_code!='A'){
          $this->db->where('MICZONE', $zone_code);
          }
          }
          }

         */

        $this->db->order_by('MICNAME', 'ASC');

        $query_1 = $this->db->get();

        ///echo "ccc".$query_1->num_rows();
        //echo  $str = $this->db->last_query();  



        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = $row['MICNAME2'];
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];

                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];



                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PR_MIC_INIT_ID' => $MIC_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICPRDU' => $MICPRDU,
                    'PR_MICREMK' => $MICREMK,
                    'PR_MICGNDR' => $MICGNDR,
                    'PR_MICMPNO' => $MICMPNO,
                    'PR_MICDOBT' => $MICDOBT,
                    'PR_MICNICN' => $MICNICN,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => 1,
                    'PR_ACT_NAME' => $print_user,
                    'PR_TYPRE' => 'M',
                    'PR_B_STATE' => $branch_state,
                    'PR_BRANCH_CODE' => $branch_code,
                    'PR_REGION_CODE' => $region_code,
                    'PR_ZONE_CODE' => $zone_code
                );


                $result = $this->db->insert('daily_policy_to_printer', $data3);
                //$g++;
            }
        }

        //echo  $str = $this->db->last_query(); 




        $this->db->where('MICREFNO', 0);
        $this->db->where('MICBYHND', $by_hand); ////$by_hand
        //$this->db->where('MICCITY', $city);

        /*


          if ($branch_state == 'B') {


          if ($branch_code !='A') {
          $this->db->where('MICBRCD', $branch_code);
          }
          else
          {
          $this->db->where('MICBRCD !=', $select_br_1);

          if ($select_br_2!=""){
          $this->db->where('MICBRCD !=', $select_br_2);
          }

          if ($select_br_3!=""){
          $this->db->where('MICBRCD !=', $select_br_3);
          }
          }


          } else {
          if ($branch_state == 'R') {



          if ($region_code !='A') {
          $this->db->where('MICREGN', $region_code);
          }

          } else {


          if ($zone_code !='A') {
          $this->db->where('MICZONE', $zone_code);
          }
          }
          }

         */

        $this->db->where('MICCITY', $city);
        $this->db->where('MICMNFL !=', 'M'); ////nov 09
        ///$this->db->where('MICBYHND', $by_hand);


        $arr = array('MICRESEPF' => $epf_user,
            'MICREFNO ' => $epf_user); ///,
        ////// 'MICCDUS'       => $print_user );

        $result = $this->db->update('daily_policy', $arr);

        //echo $str = $this->db->last_query(); 
        return $this->db->affected_rows();
    }

    public function display_daily_card_data_count_with_filter_branch_level($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;



        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }
                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }



                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }
                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $this->db->where('MICBRCD !=', $select_br_11); //oct 15
                }
                if ($select_br_12 != "") {

                    $this->db->where('MICBRCD !=', $select_br_12); //oct 15
                }

                if ($select_br_13 != "") {

                    $this->db->where('MICBRCD !=', $select_br_13); //oct 15
                }
                if ($select_br_14 != "") {

                    $this->db->where('MICBRCD !=', $select_br_14); //oct 15
                }
                if ($select_br_15 != "") {

                    $this->db->where('MICBRCD !=', $select_br_15); //oct 15
                }
                if ($select_br_16 != "") {

                    $this->db->where('MICBRCD !=', $select_br_16); //oct 15
                }
                if ($select_br_17 != "") {

                    $this->db->where('MICBRCD !=', $select_br_17); //oct 15
                }

                if ($select_br_18 != "") {

                    $this->db->where('MICBRCD !=', $select_br_18); //oct 15
                }

                if ($select_br_19 != "") {

                    $this->db->where('MICBRCD !=', $select_br_19); //oct 15
                }
                if ($select_br_20 != "") {

                    $this->db->where('MICBRCD !=', $select_br_20); //oct 15
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND='' ";
        }

//echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {


        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;


        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand' AND MICMNFL<>'M' ";


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($by_hand != "") {

                    if ($select_br_1 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_1' ";
                    }
                    if ($select_br_2 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_2' ";
                    }

                    if ($select_br_3 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_3' ";
                    }

                    if ($select_br_4 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_4' ";
                    }

                    if ($select_br_5 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_5' ";
                    }

                    if ($select_br_6 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_6' ";
                    }

                    if ($select_br_7 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_7' ";
                    }

                    if ($select_br_8 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_8' ";
                    }

                    if ($select_br_9 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_9' ";
                    }

                    if ($select_br_10 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_10' ";
                    }

                    if ($select_br_11 != "") {

                        $this->db->where('MICBRCD !=', $select_br_11); //oct 15
                    }
                    if ($select_br_12 != "") {

                        $this->db->where('MICBRCD !=', $select_br_12); //oct 15
                    }

                    if ($select_br_13 != "") {

                        $this->db->where('MICBRCD !=', $select_br_13); //oct 15
                    }
                    if ($select_br_14 != "") {

                        $this->db->where('MICBRCD !=', $select_br_14); //oct 15
                    }
                    if ($select_br_15 != "") {

                        $this->db->where('MICBRCD !=', $select_br_15); //oct 15
                    }
                    if ($select_br_16 != "") {

                        $this->db->where('MICBRCD !=', $select_br_16); //oct 15
                    }
                    if ($select_br_17 != "") {

                        $this->db->where('MICBRCD !=', $select_br_17); //oct 15
                    }

                    if ($select_br_18 != "") {

                        $this->db->where('MICBRCD !=', $select_br_18); //oct 15
                    }

                    if ($select_br_19 != "") {

                        $this->db->where('MICBRCD !=', $select_br_19); //oct 15
                    }
                    if ($select_br_20 != "") {

                        $this->db->where('MICBRCD !=', $select_br_20); //oct 15
                    }
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICPLNO FROM daily_policy AND MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND='' ";
        }

//echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;



        ///BR_CLIENT_1


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand' AND MICMNFL<>'M'";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($by_hand == "B") {
                    if ($select_br_1 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_1' ";
                    }

                    if ($select_br_2 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_2' ";
                    }

                    if ($select_br_3 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_3' ";
                    }


                    if ($select_br_4 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_4' ";
                    }


                    if ($select_br_5 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_5' ";
                    }


                    if ($select_br_6 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_6' ";
                    }

                    if ($select_br_7 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_7' ";
                    }

                    if ($select_br_8 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_8' ";
                    }

                    if ($select_br_9 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_9' ";
                    }

                    if ($select_br_10 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_10' ";
                    }

                    if ($select_br_11 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_11' ";
                    }

                    if ($select_br_12 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_12' ";
                    }


                    if ($select_br_13 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_13' ";
                    }

                    if ($select_br_14 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_14' ";
                    }

                    if ($select_br_15 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_15' ";
                    }

                    if ($select_br_16 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_16' ";
                    }


                    if ($select_br_17 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_17' ";
                    }


                    if ($select_br_18 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_18' ";
                    }

                    if ($select_br_19 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_19' ";
                    }

                    if ($select_br_20 != "") {

                        $sql .= " AND MICBRCD <>'$select_br_20' ";
                    }
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND=''";
        }

        if ($by_hand == 'B') {

            $sql .= " ORDER BY MICNAME2 ";
        } else {
            $sql .= " ORDER BY MICNAME ";
        }



//echo $sql;
        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function display_daily_card_data_count_with_filter_branch_level_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand, $start, $per_page) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;



        ///BR_CLIENT_1


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }



                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }


                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $this->db->where('MICBRCD !=', $select_br_11); //oct 15
                }
                if ($select_br_12 != "") {

                    $this->db->where('MICBRCD !=', $select_br_12); //oct 15
                }

                if ($select_br_13 != "") {

                    $this->db->where('MICBRCD !=', $select_br_13); //oct 15
                }
                if ($select_br_14 != "") {

                    $this->db->where('MICBRCD !=', $select_br_14); //oct 15
                }
                if ($select_br_15 != "") {

                    $this->db->where('MICBRCD !=', $select_br_15); //oct 15
                }
                if ($select_br_16 != "") {

                    $this->db->where('MICBRCD !=', $select_br_16); //oct 15
                }
                if ($select_br_17 != "") {

                    $this->db->where('MICBRCD !=', $select_br_17); //oct 15
                }

                if ($select_br_18 != "") {

                    $this->db->where('MICBRCD !=', $select_br_18); //oct 15
                }

                if ($select_br_19 != "") {

                    $this->db->where('MICBRCD !=', $select_br_19); //oct 15
                }
                if ($select_br_20 != "") {

                    $this->db->where('MICBRCD !=', $select_br_20); //oct 15
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND=''";
        }

        if ($by_hand == 'B') {

            $sql .= " ORDER BY MICNAME2 ";
        } else {
            $sql .= " ORDER BY MICNAME ";
        }



//echo $sql;
        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

}

?>
