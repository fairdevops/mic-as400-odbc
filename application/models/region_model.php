
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Region_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    ////sfbrdes

    function get_region_codes() {

        $this->db->select('RGCODE,RGDESCRP');
        $this->db->from('mic_regions');
        $this->db->order_by('RGCODE', 'ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function get_zone_code($RGCODE) {

        $query_str = "SELECT ZONEMASTER_ZNCODE FROM mic_regions WHERE RGCODE=? ";
        $result = $this->db->query($query_str, array($RGCODE));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['ZONEMASTER_ZNCODE'];
        } else {
            return false;
        }
    }

}

?>
