
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function add_new_user($epf_no, $users_name, $brz_state, $branch, $region, $zone, $view_only, $print_only, $reports_allow, $search_allow, $upload_allow_fleet, $upload_allow_nexus, $add_user_epf, $user_level) {

        $now_date = mysql_datetime();

        if ($branch != 'A') {
            $show_nexus = 'N';
        } else {
            $show_nexus = 'Y';
        }

        $data = array(
            'epf_no' => $epf_no,
            'users_name' => $users_name,
            'act_epf_no' => $users_name,
            'brz_state' => $brz_state,
            'branch' => $branch,
            'region' => $region,
            'zone' => $zone,
            'rec_state' => 1,
            'view_only' => $view_only,
            'print_only' => $print_only,
            'reports_allow' => $reports_allow,
            'search_allow' => $search_allow,
            'upload_allow_fleet' => $upload_allow_fleet,
            'upload_allow_nexus' => $upload_allow_nexus,
            'nexus_tab_allow' => $show_nexus,
            'add_user_epf' => $add_user_epf,
            'user_level' => $user_level,
            'add_date' => $now_date
        );

        $result = $this->db->insert('mic_users', $data);
        return $result = $this->db->insert_id();

        $this->load->database()->close();
    }

    function user_epf_not_exists($epf_no) {

        $query_str = "SELECT usr_id FROM mic_users WHERE epf_no=?";
        $result = $this->db->query($query_str, array($epf_no));

        if ($result->num_rows() > 0) {

            return true;
        } else {
            return false;
        }
    }

    function get_nexus_tab_allow($epf_no) {

        $query_str = "SELECT nexus_tab_allow FROM mic_users WHERE epf_no=? ";
        $result = $this->db->query($query_str, array($epf_no));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['nexus_tab_allow'];
        } else {
            return false;
        }
    }

    function user_epf_count($epf_no) {

        $query_str = "SELECT usr_id FROM mic_users WHERE epf_no=?";
        $result = $this->db->query($query_str, array($epf_no));

        if ($result->num_rows() > 0) {

            return $result->num_rows();
        } else {
            return 0;
        }
    }

    function get_all_active_users($no_perpage) {

        $this->db->select('usr_id,epf_no,users_name,brz_state,user_level');
        $this->db->from('mic_users');
        $this->db->where('rec_state', 1);
        $this->db->order_by('epf_no', 'desc');

        $this->db->limit($no_perpage, $this->uri->segment(3));

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->result_array();
        }
    }

    function count_users_count() {

        $this->db->select('usr_id');
        $this->db->from('mic_users');
        $this->db->where('rec_state', 1);
        $this->db->order_by('epf_no', 'desc');

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            return $rs->num_rows();
            $this->load->database()->close();
        } else {
            return 0;
        }
    }

    function delete_user($ID) {


        $this->db->where('usr_id', $ID);
        $arr = array('rec_state' => 0);

        $result = $this->db->update('mic_users', $arr);


        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $result = false;
        } else {
            $this->db->trans_commit();
            $result = true;
        }



        return $result;
    }

    public function get_user_information($usr_id) {

        $this->db->select('epf_no,users_name,brz_state,branch,region,zone,view_only,print_only,
		                   reports_allow,search_allow,upload_allow_fleet,upload_allow_nexus,user_level,add_date,add_user_epf');

        $this->db->from('mic_users');
        $this->db->where('usr_id', $usr_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function get_user_information_by_epf($epf_no) {

        $this->db->select('brz_state,branch,region,zone,view_only,print_only,
		                   reports_allow,search_allow,upload_allow_fleet,upload_allow_nexus,user_level,users_name,nexus_tab_allow');

        $this->db->from('mic_users');
        $this->db->where('epf_no', $epf_no);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    function get_user_print_branch($epf_no) {

        $query_str = "SELECT branch FROM mic_users WHERE epf_no=? ";
        $result = $this->db->query($query_str, array($epf_no));

        if ($result->num_rows() > 0) {
            $data = $result->row_array();
            return $data['branch'];
        } else {
            return false;
        }
    }
	
	
	function auth_gen_user_availability($epf_no) {

        $query_str = "SELECT usr_id FROM mic_users WHERE act_epf_no=?";
        $result = $this->db->query($query_str, array($epf_no));

        if ($result->num_rows() > 0) {

            return $result->num_rows();
        } else {
            return 0;
        }
    }

}

?>
