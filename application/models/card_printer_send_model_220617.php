
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_printer_send_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function push_to_printer($PR_MIC_ID) {


        $this->db->where('PR_MIC_ID', $PR_MIC_ID);
        $arr = array('PR_CARD_PRINT' => 'N');

        $result = $this->db->update('daily_policy_to_printer', $arr);
        return $this->db->affected_rows();
    }

    public function get_daily_card_print_send_count_for_none_by_hand($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {

            $sql .= " AND  PR_MICCITY='$city'";
        }

//echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function get_daily_card_print_send_count_for_none_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $start, $per_page, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO
			         FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {

            $sql .= " AND  PR_MICCITY='$city'";
        }

        $sql .=" LIMIT $start, $per_page";
//echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function get_daily_card_print_send_count_for_none_by_hand_display_order($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $start, $per_page, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO
			         FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {

            $sql .= " AND  PR_MICCITY='$city'";
        }

        $sql .=" ORDER BY PR_MIC_ID DESC LIMIT $start, $per_page";
//echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function get_daily_card_print_send_count($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $card_type) {

        $sql = ""; ///SELECT * FROM daily_policy_to_printer WHERE   PR_MICBYHND='B' AND PR_PRINT_DATE='2014-05-26' AND PR_MICRESEPF=2159

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		



        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_1'";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_2'";
                }


                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_3'";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_4'";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_5'";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_6'";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_7'";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_8'";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_9'";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_10'";
                }
				
				 if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_11'";
                }
				
				 if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_12'";
                }
				
				 if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_13'";
                }
					
				 if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_14'";
                }
						
				 if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_15'";
                }
	
				 if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_16'";
                }
				
			    if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_17'";
                }

			    if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_18'";
                }
				
			    if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_19'";
                }

			    if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_20'";
                }


			    if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_21'";
                }
				

			    if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_22'";
                }
				

			    if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_23'";
                }
				
				
			    if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_24'";
                }


			    if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_25'";
                }
				
				

			    if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_26'";
                }
				

			    if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_27'";
                }
				

			    if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_28'";
                }
				

			    if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_29'";
                }
				

			    if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_30'";
                }
				
				 if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_31'";
                }
				
				 if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_32'";
                }
				
				 if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_33'";
                }
				
				 if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_34'";
                }
				
				 if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_35'";
                }
				
				
				 if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_36'";
                }
				
				 if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_37'";
                }
				
				
				 if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_38'";
                }
				
				 if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_39'";
                }
				
				 if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_40'";
                }
				
				
				 if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_41'";
                }
				
				
				 if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_42'";
                }
				
				 if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_43'";
                }
				
				
				
				 if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_44'";
                }
				
				 if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_45'";
                }
				
				
				 if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_46'";
                }
				
				 if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_47'";
                }
				
				 if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_48'";
                }
				
				 if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_49'";
                }
				
				 if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_50'";
                }
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


//echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function get_daily_card_print_send_count_display_order($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $start, $per_page, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
 	    $select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
	    $select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

		
		

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO,PR_CARD_PRINT
			         FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_1'";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_2'";
                }


                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_3'";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_4'";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_5'";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_6'";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_7'";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_8'";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_9'";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_10'";
                }
				
				  if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_11'";
                }

				  if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_12'";
                }
				  if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_13'";
                }
				
				  if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_14'";
                }
				
				  if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_15'";
                }
				
				  if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_16'";
                }
				
				  if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_17'";
                }
				
				  if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_18'";
                }
				
				
				  if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_19'";
                }
				
				  if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_20'";
                }
				
				  if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_21'";
                }
				
				  if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_22'";
                }
				
				  if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_23'";
                }
				
				  if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_24'";
                }
				
				  if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_25'";
                }
				
				  if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_26'";
                }
				
				  if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_27'";
                }
				
				  if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_28'";
                }
				
				  if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_29'";
                }
				
				  if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_30'";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_31'";
                }
				
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_32'";
                }
				
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_33'";
                }
				
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_34'";
                }
				
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_35'";
                }
				
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_36'";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_37'";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_38'";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_39'";
                }
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_40'";
                }
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_41'";
                }
				
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_42'";
                }
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_43'";
                }
				
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_44'";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_45'";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_46'";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_47'";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_48'";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_49'";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <> '$select_br_50'";
                }
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO,PR_CARD_PRINT
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO,PR_CARD_PRINT
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }




        $sql.=" ORDER BY PR_MIC_ID DESC LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function get_daily_card_print_send_count_display($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $start, $per_page, $card_type) {

        $sql = "";



        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO,PR_CARD_PRINT
			         FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO,PR_CARD_PRINT
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID,PR_MICPLNO,PR_MICRNCT,PR_MICTITL,PR_MICNAME,PR_MICVENO,PR_MICSPNO,PR_MICTOWN,PR_CARD_PRINT,PR_MICREFNO,PR_CARD_PRINT
			             FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' AND PR_TYPRE='$card_type'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }




        $sql.=" LIMIT $start, $per_page";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

}

?>
