<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Daily_excel_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    public function before_print_all_count_at_head_office($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
		
		


        if ($branch_state == 'B') {

            $sql .= "SELECT MICPLNO FROM daily_policy WHERE MICREFNO=0 AND MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='' OR ";

            $sql .= "( MICBYHND='B'   ";

            if ($select_br_3 != "") {

                $sql .= "  AND MICBRCD <> '$select_br_3' ";
            }

            if ($select_br_2 != "") {

                $sql .= " AND MICBRCD <> '$select_br_2' ";
            }

            if ($select_br_1 != "") {

                $sql .= " AND MICBRCD <> '$select_br_1' ";
            }
            if ($select_br_4 != "") {

                $sql .= " AND MICBRCD <> '$select_br_4' ";
            }

            if ($select_br_5 != "") {

                $sql .= " AND MICBRCD <> '$select_br_5' ";
            }

            if ($select_br_6 != "") {

                $sql .= " AND MICBRCD <> '$select_br_6' ";
            }
            if ($select_br_7 != "") {

                $sql .= " AND MICBRCD <> '$select_br_7' ";
            }
            if ($select_br_8 != "") {

                $sql .= " AND MICBRCD<>'$select_br_8' ";
            }
            if ($select_br_9 != "") {

                $sql .= " AND MICBRCD <> '$select_br_9' ";
            }
            if ($select_br_10 != "") {

                $sql .= " AND MICBRCD <> '$select_br_10' ";
            }
            if ($select_br_11 != "") {

                $sql .= " AND MICBRCD <> '$select_br_11' ";
            }
			
			if ($select_br_12 != "") {

                $sql .= " AND MICBRCD <> '$select_br_12' ";
            }
			
			if ($select_br_13 != "") {

                $sql .= " AND MICBRCD <> '$select_br_13' ";
            }
			
			if ($select_br_14 != "") {

                $sql .= " AND MICBRCD <> '$select_br_14' ";
            }
			
			if ($select_br_15 != "") {

                $sql .= " AND MICBRCD <> '$select_br_15' ";
            }
			
			if ($select_br_16 != "") {

                $sql .= " AND MICBRCD <> '$select_br_16' ";
            }
			
			if ($select_br_17 != "") {

                $sql .= " AND MICBRCD <> '$select_br_17' ";
            }
			
			if ($select_br_18 != "") {

                $sql .= " AND MICBRCD <> '$select_br_18' ";
            }
			
			if ($select_br_19 != "") {

                $sql .= " AND MICBRCD <> '$select_br_19' ";
            }
			
			if ($select_br_20 != "") {

                $sql .= " AND MICBRCD <> '$select_br_20' ";
            }
			
			if ($select_br_21 != "") {

                $sql .= " AND MICBRCD <> '$select_br_21' ";
            }
			
			if ($select_br_22 != "") {

                $sql .= " AND MICBRCD <> '$select_br_22' ";
            }
			
			if ($select_br_23 != "") {

                $sql .= " AND MICBRCD <> '$select_br_23' ";
            }
			
			if ($select_br_24 != "") {

                $sql .= " AND MICBRCD <> '$select_br_24' ";
            }
			
			if ($select_br_25 != "") {

                $sql .= " AND MICBRCD <> '$select_br_25' ";
            }
			
			if ($select_br_26 != "") {

                $sql .= " AND MICBRCD <> '$select_br_26' ";
            }
			
			if ($select_br_27 != "") {

                $sql .= " AND MICBRCD <> '$select_br_27' ";
            }
			
			if ($select_br_28 != "") {

                $sql .= " AND MICBRCD <> '$select_br_28' ";
            }
			
			if ($select_br_29 != "") {

                $sql .= " AND MICBRCD <> '$select_br_29' ";
            }
			
			if ($select_br_30 != "") {

                $sql .= " AND MICBRCD <> '$select_br_30' ";
            }
			
			if ($select_br_31 != "") {

                $sql .= " AND MICBRCD <> '$select_br_31' ";
            }
			
			if ($select_br_32 != "") {

                $sql .= " AND MICBRCD <> '$select_br_32' ";
            }
			
			if ($select_br_33 != "") {

                $sql .= " AND MICBRCD <> '$select_br_33' ";
            }
			
			
			if ($select_br_33 != "") {

                $sql .= " AND MICBRCD <> '$select_br_33' ";
            }
			
			
			if ($select_br_34 != "") {

                $sql .= " AND MICBRCD <> '$select_br_34' ";
            }
			
			if ($select_br_34 != "") {

                $sql .= " AND MICBRCD <> '$select_br_34' ";
            }
			
			if ($select_br_35 != "") {

                $sql .= " AND MICBRCD <> '$select_br_35' ";
            }
			
			if ($select_br_36 != "") {

                $sql .= " AND MICBRCD <> '$select_br_36' ";
            }
			
			if ($select_br_37 != "") {

                $sql .= " AND MICBRCD <> '$select_br_37' ";
            }
			
			if ($select_br_38 != "") {

                $sql .= " AND MICBRCD <> '$select_br_38' ";
            }
			
			if ($select_br_39 != "") {

                $sql .= " AND MICBRCD <> '$select_br_39' ";
            }
			
			if ($select_br_40 != "") {

                $sql .= " AND MICBRCD <> '$select_br_40' ";
            }
			
			if ($select_br_41 != "") {

                $sql .= " AND MICBRCD <> '$select_br_41' ";
            }
			
			if ($select_br_42 != "") {

                $sql .= " AND MICBRCD <> '$select_br_42' ";
            }
			
			
			if ($select_br_43 != "") {

                $sql .= " AND MICBRCD <> '$select_br_43' ";
            }
			
			if ($select_br_44 != "") {

                $sql .= " AND MICBRCD <> '$select_br_44' ";
            }
			
			
			if ($select_br_35 != "") {

                $sql .= " AND MICBRCD <> '$select_br_35' ";
            }
			
			
			if ($select_br_36 != "") {

                $sql .= " AND MICBRCD <> '$select_br_36' ";
            }
			
			if ($select_br_37 != "") {

                $sql .= " AND MICBRCD <> '$select_br_37' ";
            }
			
			if ($select_br_38 != "") {

                $sql .= " AND MICBRCD <> '$select_br_38' ";
            }
			
			if ($select_br_39 != "") {

                $sql .= " AND MICBRCD <> '$select_br_39' ";
            }
			
			if ($select_br_40 != "") {

                $sql .= " AND MICBRCD <> '$select_br_40' ";
            }
			
			if ($select_br_41 != "") {

                $sql .= " AND MICBRCD <> '$select_br_41' ";
            }
			
			if ($select_br_42 != "") {

                $sql .= " AND MICBRCD <> '$select_br_42' ";
            }
			
			if ($select_br_43 != "") {

                $sql .= " AND MICBRCD <> '$select_br_43' ";
            }
			
			if ($select_br_44 != "") {

                $sql .= " AND MICBRCD <> '$select_br_44' ";
            }
			
			if ($select_br_45 != "") {

                $sql .= " AND MICBRCD <> '$select_br_45' ";
            }
			
			if ($select_br_46 != "") {

                $sql .= " AND MICBRCD <> '$select_br_46' ";
            }
			
			if ($select_br_47 != "") {

                $sql .= " AND MICBRCD <> '$select_br_47' ";
            }
			
			if ($select_br_48 != "") {

                $sql .= " AND MICBRCD <> '$select_br_48' ";
            }
			
			if ($select_br_49 != "") {

                $sql .= " AND MICBRCD <> '$select_br_49' ";
            }
			
			if ($select_br_50 != "") {

                $sql .= " AND MICBRCD <> '$select_br_50' ";
            }


            if ($select_br_51 != "") {

                $sql .= " AND MICBRCD <> '$select_br_51' ";
            }
            

            if ($select_br_52 != "") {

                $sql .= " AND MICBRCD <> '$select_br_52' ";
            }
            

            if ($select_br_53 != "") {

                $sql .= " AND MICBRCD <> '$select_br_53' ";
            }
            
            if ($select_br_54 != "") {

                $sql .= " AND MICBRCD <> '$select_br_54' ";
            }

            if ($select_br_55 != "") {

                $sql .= " AND MICBRCD <> '$select_br_55' ";
            }

            if ($select_br_56 != "") {

                $sql .= " AND MICBRCD <> '$select_br_56' ";
            }

            if ($select_br_57 != "") {

                $sql .= " AND MICBRCD <> '$select_br_57' ";
            }

            if ($select_br_58 != "") {

                $sql .= " AND MICBRCD <> '$select_br_58' ";
            }

            if ($select_br_59 != "") {

                $sql .= " AND MICBRCD <> '$select_br_59' ";
            }

            if ($select_br_60 != "") {

                $sql .= " AND MICBRCD <> '$select_br_60' ";
            }

            if ($select_br_61 != "") {

                $sql .= " AND MICBRCD <> '$select_br_62' ";
            }

            if ($select_br_63 != "") {

                $sql .= " AND MICBRCD <> '$select_br_63' ";
            }

            if ($select_br_64 != "") {

                $sql .= " AND MICBRCD <> '$select_br_64' ";
            }

            if ($select_br_65 != "") {

                $sql .= " AND MICBRCD <> '$select_br_65' ";
            }

            if ($select_br_66 != "") {

                $sql .= " AND MICBRCD <> '$select_br_66' ";
            }

            if ($select_br_67 != "") {

                $sql .= " AND MICBRCD <> '$select_br_67' ";
            }

            if ($select_br_68 != "") {

                $sql .= " AND MICBRCD <> '$select_br_68' ";
            }

            if ($select_br_69 != "") {

                $sql .= " AND MICBRCD <> '$select_br_69' ";
            }

            if ($select_br_70 != "") {

                $sql .= " AND MICBRCD <> '$select_br_70' ";
            }


			
			


            $sql .= " ) ";
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function export_mic_detail_all_head_office($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $pdate = date("Y-m-d");

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
		
		


        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0 AND MICMNFL<>'M' AND( MICCITY='N' OR MICCITY='Y' ) AND MICBYHND='' OR ";

            $sql .= "( MICBYHND='B'   ";
			
			
			
			            if ($select_br_51 != "") {

                $sql .= " AND MICBRCD <> '$select_br_51' ";
            }
            

            if ($select_br_52 != "") {

                $sql .= " AND MICBRCD <> '$select_br_52' ";
            }
            

            if ($select_br_53 != "") {

                $sql .= " AND MICBRCD <> '$select_br_53' ";
            }
            
            if ($select_br_54 != "") {

                $sql .= " AND MICBRCD <> '$select_br_54' ";
            }

            if ($select_br_55 != "") {

                $sql .= " AND MICBRCD <> '$select_br_55' ";
            }

            if ($select_br_56 != "") {

                $sql .= " AND MICBRCD <> '$select_br_56' ";
            }

            if ($select_br_57 != "") {

                $sql .= " AND MICBRCD <> '$select_br_57' ";
            }

            if ($select_br_58 != "") {

                $sql .= " AND MICBRCD <> '$select_br_58' ";
            }

            if ($select_br_59 != "") {

                $sql .= " AND MICBRCD <> '$select_br_59' ";
            }

            if ($select_br_60 != "") {

                $sql .= " AND MICBRCD <> '$select_br_60' ";
            }

            if ($select_br_61 != "") {

                $sql .= " AND MICBRCD <> '$select_br_62' ";
            }

            if ($select_br_63 != "") {

                $sql .= " AND MICBRCD <> '$select_br_63' ";
            }

            if ($select_br_64 != "") {

                $sql .= " AND MICBRCD <> '$select_br_64' ";
            }

            if ($select_br_65 != "") {

                $sql .= " AND MICBRCD <> '$select_br_65' ";
            }

            if ($select_br_66 != "") {

                $sql .= " AND MICBRCD <> '$select_br_66' ";
            }

            if ($select_br_67 != "") {

                $sql .= " AND MICBRCD <> '$select_br_67' ";
            }

            if ($select_br_68 != "") {

                $sql .= " AND MICBRCD <> '$select_br_68' ";
            }

            if ($select_br_69 != "") {

                $sql .= " AND MICBRCD <> '$select_br_69' ";
            }

            if ($select_br_70 != "") {

                $sql .= " AND MICBRCD <> '$select_br_70' ";
            }

			if ($select_br_50 != "") {

                $sql .= " AND MICBRCD<>'$select_br_50' ";
            }
			
			if ($select_br_49 != "") {

                $sql .= " AND MICBRCD<>'$select_br_49' ";
            }
			
			
			if ($select_br_48 != "") {

                $sql .= " AND MICBRCD<>'$select_br_48' ";
            }
			
			
			if ($select_br_47 != "") {

                $sql .= " AND MICBRCD<>'$select_br_47' ";
            }
			
			
			if ($select_br_46 != "") {

                $sql .= " AND MICBRCD<>'$select_br_46' ";
            }
			
			
			if ($select_br_45 != "") {

                $sql .= " AND MICBRCD<>'$select_br_45' ";
            }
			
			
			if ($select_br_44 != "") {

                $sql .= " AND MICBRCD<>'$select_br_44' ";
            }
			
			
			
			if ($select_br_43 != "") {

                $sql .= " AND MICBRCD<>'$select_br_43' ";
            }
			
			if ($select_br_42 != "") {

                $sql .= " AND MICBRCD<>'$select_br_42' ";
            }
			
			if ($select_br_41 != "") {

                $sql .= " AND MICBRCD<>'$select_br_41' ";
            }
			
			
			if ($select_br_40 != "") {

                $sql .= " AND MICBRCD<>'$select_br_40' ";
            }
			
			
			if ($select_br_39 != "") {

                $sql .= " AND MICBRCD<>'$select_br_39' ";
            }
			
			
			if ($select_br_38 != "") {

                $sql .= " AND MICBRCD<>'$select_br_38' ";
            }
			
			if ($select_br_37 != "") {

                $sql .= " AND MICBRCD<>'$select_br_37' ";
            }
			
			if ($select_br_36 != "") {

                $sql .= " AND MICBRCD<>'$select_br_36' ";
            }
			
			
			if ($select_br_35 != "") {

                $sql .= " AND MICBRCD<>'$select_br_35' ";
            }
			
			
			if ($select_br_34 != "") {

                $sql .= " AND MICBRCD<>'$select_br_34' ";
            }
			
			
			
			
			if ($select_br_33 != "") {

                $sql .= " AND MICBRCD<>'$select_br_33' ";
            }
			
			
			
			if ($select_br_32 != "") {

                $sql .= " AND MICBRCD<>'$select_br_32' ";
            }
			
			
			if ($select_br_31 != "") {

                $sql .= " AND MICBRCD<>'$select_br_31' ";
            }
			
			
			if ($select_br_30 != "") {

                $sql .= " AND MICBRCD<>'$select_br_30' ";
            }
			
			if ($select_br_29 != "") {

                $sql .= " AND MICBRCD<>'$select_br_29' ";
            }
			
			if ($select_br_28 != "") {

                $sql .= " AND MICBRCD<>'$select_br_28' ";
            }
			
			if ($select_br_27 != "") {

                $sql .= " AND MICBRCD<>'$select_br_27' ";
            }
			
			if ($select_br_26 != "") {

                $sql .= " AND MICBRCD<>'$select_br_26' ";
            }
			
			if ($select_br_25 != "") {

                $sql .= " AND MICBRCD<>'$select_br_25' ";
            }
			
			if ($select_br_24 != "") {

                $sql .= " AND MICBRCD<>'$select_br_24' ";
            }
			
			if ($select_br_23 != "") {

                $sql .= " AND MICBRCD<>'$select_br_23' ";
            }
			
			if ($select_br_21 != "") {

                $sql .= " AND MICBRCD<>'$select_br_21' ";
            }


            if ($select_br_20 != "") {

                $sql .= " AND MICBRCD<>'$select_br_20' ";
            }

            if ($select_br_19 != "") {

                $sql .= " AND MICBRCD<>'$select_br_19' ";
            }

            if ($select_br_18 != "") {

                $sql .= " AND MICBRCD<>'$select_br_18' ";
            }


            if ($select_br_17 != "") {

                $sql .= " AND MICBRCD<>'$select_br_17' ";
            }

            if ($select_br_16 != "") {

                $sql .= " AND MICBRCD<>'$select_br_16' ";
            }


            if ($select_br_15 != "") {

                $sql .= " AND MICBRCD<>'$select_br_15' ";
            }


            if ($select_br_14 != "") {

                $sql .= " AND MICBRCD<>'$select_br_14' ";
            }


            if ($select_br_13 != "") {

                $sql .= " AND MICBRCD<>'$select_br_13' ";
            }


            if ($select_br_12 != "") {

                $sql .= " AND MICBRCD<>'$select_br_12' ";
            }

            if ($select_br_11 != "") {

                $sql .= " AND MICBRCD<>'$select_br_11' ";
            }

            if ($select_br_10 != "") {

                $sql .= " AND MICBRCD<>'$select_br_10' ";
            }

            if ($select_br_9 != "") {

                $sql .= " AND MICBRCD<>'$select_br_9' ";
            }

            if ($select_br_8 != "") {

                $sql .= " AND MICBRCD<>'$select_br_8' ";
            }

            if ($select_br_7 != "") {

                $sql .= " AND MICBRCD<>'$select_br_7' ";
            }

            if ($select_br_6 != "") {

                $sql .= " AND MICBRCD<>'$select_br_6' ";
            }

            if ($select_br_5 != "") {

                $sql .= " AND MICBRCD<>'$select_br_5' ";
            }

            if ($select_br_4 != "") {

                $sql .= " AND MICBRCD<>'$select_br_4' ";
            }


            if ($select_br_3 != "") {

                $sql .= " AND MICBRCD<>'$select_br_3' ";
            }

            if ($select_br_2 != "") {

                $sql .= " AND MICBRCD<>'$select_br_2' ";
            }

            if ($select_br_1 != "") {

                $sql .= " AND MICBRCD<>'$select_br_1' ";
            }

            $sql .= " ) ";
        }

//echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function display_card_only_city_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {

        $sql = "";

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;


        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;




        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }
                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
				
				
				if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
				
				if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
				
				
				if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
				
				if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
				
				if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
				
				if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
				
				
				if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
				
				
				if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }
				

                 if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_70' ";
                }
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($region_code != 'A') {
                    $sql .= " AND  MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";

                if ($zone_code != 'A') {
                    $sql .= " AND  MICZONE='$zone_code'";
                }
            }
        }

        if ($city != "") {
            $sql .= " AND  MICCITY='$city' AND MICBYHND=''";
        }

        if ($by_hand == 'B') {

            $sql .= " ORDER BY MICNAME2 ";
        } else {
            $sql .= " ORDER BY MICNAME ";
        }



//echo $sql;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function export_mic_detail_buyhand_display($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;
        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
		



        if ($branch_state == 'B') {

            $sql .= "SELECT *
			         FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B' ";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }
                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }
                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
							
				if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }			
							
				if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }			
							
			  	if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }				
							
				if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }				
				
				if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }				
							
				if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }				
							
				if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }				
							
				if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }	
				
				if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }	
				
				if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }	
				
				if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }	
				
				
				if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }	
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }	
				
				
				if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }	
				
				if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }	
				
				if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }	
				
				if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }	
				
				if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }	
				
				if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }	
				
				
				if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }


                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_70' ";
                }	
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT *
				         FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B'  ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT *
				         FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='B' ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY MICSPNO ASC ";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_buyhand_count($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
	    $select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;
		



        if ($branch_state == 'B') {

            $sql .= "SELECT MICREFNO   FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B' ";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }
                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }
                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }
                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }
                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }
                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
				if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
				
				if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
				
				
				if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
				
				if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
				
				
				if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
				
				
				if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
				
				
				if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }


                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_70' ";
                }
				
				
				
				
				
				
				
				
				
				
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICREFNO  FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B'  ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICREFNO  FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='B' ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY MICNAME2 ";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_city_count($branch_state, $branch_code, $region_code, $zone_code, $city) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		


        if ($branch_state == 'B') {

            $sql .= "SELECT MICREFNO   FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='' AND MICMNFL<>'M'";


            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
            /* else
              {

              if ($select_br_1 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_1' ";

              }

              if ($select_br_2 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_2' ";

              }

              if ($select_br_3 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_3' ";

              }




              }

             */
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICREFNO  FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICREFNO  FROM daily_policy  WHERE MICREFNO=0 AND MICCITY='$city' ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }



        $sql .= " ORDER BY MICNAME2 ";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_city_display($branch_state, $branch_code, $region_code, $zone_code, $city) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;
        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;


        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='' ";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
            /* else
              {
              if ($select_br_1 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_1' ";

              }

              if ($select_br_2 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_2' ";

              }

              if ($select_br_3 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_3' ";

              }

              } */
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy  WHERE MICREFNO=0 AND MICCITY='$city' ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY MICNAME"; ////aug 17 sunday 



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_out_station_display($branch_state, $branch_code, $region_code, $zone_code, $city) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;
        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;



        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND=''  AND MICMNFL<>'M' ";




            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
            /* else
              {
              if ($select_br_1 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_1' ";

              }

              if ($select_br_2 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_2' ";

              }

              if ($select_br_3 !="" ) {

              $sql .= " AND MICBRCD <>'$select_br_3' ";

              }

              }

             */
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city'  AND MICBYHND='' ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy  WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND=''";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }




        $sql .= " ORDER BY MICNAME "; ///august 17 2014 sunday set





        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function display_daily_nexus_before_print($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
		             FROM daily_policy_nexus WHERE MICREFNO=0";

            if ($branch_code != 'A') {
                $sql .= " AND MICBRCD='$branch_code' ";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
		                 FROM daily_policy_nexus WHERE MICREFNO=0";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code' ";
                }
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
		                 FROM daily_policy_nexus WHERE MICREFNO=0 ";

                if ($zone_code != 'A') {
                    $sql .=" AND MICZONE='$zone_code'";
                }
            }
        }



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }

    public function export_mic_detail_all($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $pdate = date("Y-m-d");

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;
        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;

		
		
		

        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0  ";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }


                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }
                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }
                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }
                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }
                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }
                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }
                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
				if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
				if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
				if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
				if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
				if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
				
				if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
				
				
				if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
				
				
				if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
				
				
				
				if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
				
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
				
				
				
				if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
				
				
				
				if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
				
				
				if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
				
				
				
				if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
				
				
				if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }


                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_70' ";
                }
				
				
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy WHERE MICREFNO=0  ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy  WHERE MICREFNO=0  ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        //$sql .= " ORDER BY MICNAME2 ASC";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_all_data_count($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $pdate = date("Y-m-d");

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
     
	    $select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;

        $select_br_51 = BR_CLIENT_51;
        $select_br_52 = BR_CLIENT_52;
        $select_br_53 = BR_CLIENT_53;
        $select_br_54 = BR_CLIENT_54;
        $select_br_55 = BR_CLIENT_55;
        $select_br_56 = BR_CLIENT_56;
        $select_br_57 = BR_CLIENT_57;
        $select_br_58 = BR_CLIENT_58;
        $select_br_59 = BR_CLIENT_59;
        $select_br_60 = BR_CLIENT_60;
        $select_br_61 = BR_CLIENT_61;
        $select_br_62 = BR_CLIENT_62;
        $select_br_63 = BR_CLIENT_63;
        $select_br_64 = BR_CLIENT_64;
        $select_br_65 = BR_CLIENT_65;
        $select_br_66 = BR_CLIENT_66;
        $select_br_67 = BR_CLIENT_67;
        $select_br_68 = BR_CLIENT_68;
        $select_br_69 = BR_CLIENT_69;
        $select_br_70 = BR_CLIENT_70;

		


        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO FROM daily_policy WHERE MICREFNO=0  ";



            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            } else {
                if ($select_br_1 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_4' ";
                }


                if ($select_br_5 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_5' ";
                }


                if ($select_br_6 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_12' ";
                }
                if ($select_br_13 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_20' ";
                }
				
				 if ($select_br_21 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_21' ";
                }
				 if ($select_br_22 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_22' ";
                }
				
				 if ($select_br_23 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_23' ";
                }
				 if ($select_br_24 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_24' ";
                }
				
				 if ($select_br_25 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_25' ";
                }
				
				 if ($select_br_26 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_26' ";
                }
				
				 if ($select_br_27 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_27' ";
                }
				
				 if ($select_br_28 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_28' ";
                }
				
				 if ($select_br_29 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_29' ";
                }
				
				 if ($select_br_30 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_30' ";
                }
				
				 if ($select_br_31 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_31' ";
                }
				 if ($select_br_32 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_32' ";
                }
				
				 if ($select_br_33 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_33' ";
                }
				
				 if ($select_br_34 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_34' ";
                }
				
				 if ($select_br_35 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_35' ";
                }
				
				 if ($select_br_36 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_36' ";
                }
				
				 if ($select_br_37 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_37' ";
                }
				
				 if ($select_br_38 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_38' ";
                }
				
				
				 if ($select_br_39 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_39' ";
                }
				
				 if ($select_br_40 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_40' ";
                }
				
				 if ($select_br_41 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_41' ";
                }
				
				 if ($select_br_42 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_42' ";
                }
				
				
				 if ($select_br_43 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_43' ";
                }
				
				 if ($select_br_44 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_44' ";
                }
				
				
				 if ($select_br_45 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_45' ";
                }
				
				
				 if ($select_br_46 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_46' ";
                }
				
				 if ($select_br_47 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_47' ";
                }
				
				 if ($select_br_48 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_48' ";
                }
				
				 if ($select_br_49 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_49' ";
                }
				
				 if ($select_br_50 != "") {

                    $sql .= " AND MICBRCD <>'$select_br_50' ";
                }


                if ($select_br_51 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_51' ";
                }
                

                if ($select_br_52 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_52' ";
                }
                

                if ($select_br_53 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_53' ";
                }
                
                if ($select_br_54 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_54' ";
                }

                if ($select_br_55 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_55' ";
                }

                if ($select_br_56 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_56' ";
                }

                if ($select_br_57 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_57' ";
                }

                if ($select_br_58 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_58' ";
                }

                if ($select_br_59 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_59' ";
                }

                if ($select_br_60 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_60' ";
                }

                if ($select_br_61 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_62' ";
                }

                if ($select_br_63 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_63' ";
                }

                if ($select_br_64 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_64' ";
                }

                if ($select_br_65 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_65' ";
                }

                if ($select_br_66 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_66' ";
                }

                if ($select_br_67 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_67' ";
                }

                if ($select_br_68 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_68' ";
                }

                if ($select_br_69 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_69' ";
                }

                if ($select_br_70 != "") {

                    $sql .= " AND MICBRCD <> '$select_br_70' ";
                }
				
				
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO  FROM daily_policy WHERE MICREFNO=0  ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICVENO  FROM daily_policy  WHERE MICREFNO=0  ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        //$sql .= " ORDER BY MICNAME2 ASC";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_nexus_detail_buyhand_count($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";


        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID_NEXUS   FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B' ";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID_NEXUS  FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B'  ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MIC_ID_NEXUS  FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICBYHND='B' ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY MICNAME2 ";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_nexus_detail_buyhand_display($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT *
			         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B' ";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT *
				         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B'  ";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT *
				         FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICBYHND='B' ";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY MICNAME2 ";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_nexus_detail_city_count($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT MICREFNO   FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICREFNO  FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICREFNO  FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY MICNAME2 ";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_nexus_detail_city_display($branch_state, $branch_code, $region_code, $zone_code, $city, $by_hand) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";

            if ($branch_code != 'A') {

                $sql .= " AND MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";

                if ($region_code != 'A') {
                    $sql .= " AND MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";

                if ($zone_code != 'A') {
                    $sql .= " AND MICZONE='$zone_code'";
                }
            }
        }



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

}

?>
