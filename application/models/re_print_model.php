<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Re_print_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function check_daily_policy_mannual($MICPLNO) {

        $this->db->select("mic_id", FALSE);
        $this->db->from('daily_policy');
        $this->db->where('MICPLNO', $MICPLNO);
        $this->db->where('MICRESEPF', 0);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function get_print_card_data($id_no) {

        $this->db->select("PR_MICTITL,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICCITY,
		                   DATE_FORMAT(PR_PRINT_DATE, '%d%/%m%/%Y') AS PR_PRINT_DATE,PR_MICPLNO,PR_MICPCOV,PR_MICVENO,
						   PR_MICPCOV,PR_MICBRCD,PR_MICZONE,PR_MICREGN,PR_MICCMDT,PR_MICEXDT,PR_MICBYHND,PR_MICRNCT,PR_MICENCT,PR_COUNT", FALSE);

        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MIC_ID', $id_no);
        $this->db->where('PR_CARD_PRINT', 'Y');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function get_print_my_push_card_data_disply($re_mic_id) {

        $this->db->select('PR_BRANCH_CODE,PR_REGION_CODE,PR_ZONE_CODE,PR_MICVENO,PR_MICPLNO,PR_MICTITL,PR_MICNAME,PR_MICTOWN,PR_MICCITY,PR_MICCMDT,
						   PR_MICEXDT,PR_MICPCOV,PR_MICBYHND,PR_COUNT,PR_MICMPNO');

        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MIC_ID', $re_mic_id);


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    function push_to_printer_cards($re_mic_id, $re_request_epf, $re_request_users_name) {

        $now_date = mysql_datetime();

        $card_infor = $this->get_print_my_push_card_data_disply($re_mic_id);

        $title = $card_infor['PR_MICTITL'];
        $name = $card_infor['PR_MICNAME'];
        $re_policy_no = $card_infor['PR_MICPLNO'];
        $re_veh_no = $card_infor['PR_MICVENO'];

        $town = $card_infor['PR_MICTOWN'];
        $city = $card_infor['PR_MICCITY'];

        $miccmdt = $card_infor['PR_MICCMDT'];
        $micexdt = $card_infor['PR_MICEXDT'];
        $period = $card_infor['PR_MICPCOV'];
        $buy_hand = $card_infor['PR_MICBYHND'];
        $col_city = $card_infor['PR_MICCITY'];

        $pr_count = $card_infor['PR_COUNT'];

        $br_code = $card_infor['PR_BRANCH_CODE'];
        $reg_code = $card_infor['PR_REGION_CODE'];
        $zone_code = $card_infor['PR_ZONE_CODE'];
        $mob_number = $card_infor['PR_MICMPNO'];




        $data = array(
            're_br_code' => $br_code,
            're_reg_code' => $reg_code,
            're_zo_code' => $zone_code,
            're_request_epf' => $re_request_epf,
            're_request_user' => $re_request_users_name,
            're_mic_id' => $re_mic_id,
            're_veh_no' => $re_veh_no,
            're_policy_no' => $re_policy_no,
            're_title' => $title,
            're_cust_name' => $name,
            're_town' => $town,
            're_city' => $city,
            're_miccmdte' => $miccmdt,
            're_micexdte' => $micexdt,
            're_cover_period' => $period,
            're_buy_hand' => $buy_hand,
            're_dup_count' => $pr_count,
            're_mob_number' => $mob_number,
            're_request_date' => $now_date
        );

        $result = $this->db->insert('tbl_resend_printer_fail_cards', $data);
        return $result = $this->db->insert_id();

        $this->load->database()->close();
    }

    public function check_printer_card_count_data($PR_MICPLNO, $PR_MICRNCT, $PR_MICENCT) {

        $today_mannual_count = $this->check_daily_policy_mannual($PR_MICPLNO);
        $this->db->select('PR_MIC_ID');

        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MICPLNO', $PR_MICPLNO);
        $this->db->where('PR_MICRNCT', $PR_MICRNCT);
        $this->db->where('PR_MICENCT', $PR_MICENCT);

        $this->db->where('PR_CARD_PRINT', 'Y');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {  if ($today_mannual_count >0 ) {  return 0; } else{

        return $query->num_rows();
        }
        } else {
         return "";
        }
    }

    public function get_print_card_data_duplicate_count($PR_MICPLNO, $PR_MICRNCT, $PR_MICENCT) {

        $sql = "SELECT ( MAX(PR_COUNT)+1 ) AS NEW_COUNT FROM daily_policy_to_printer WHERE PR_MICPLNO='$PR_MICPLNO'
			   AND PR_MICRNCT= $PR_MICRNCT AND PR_MICENCT=$PR_MICENCT";
        $query = $this->db->query($sql);
        $data = $query->row_array();
        return $data['NEW_COUNT'];
    }

    function display_reason() {

        $this->db->select('re_id,reason');
        $this->db->from('mic_card_duplicates_reason');
        $this->db->where('rec_state', 1);
        $this->db->order_by('list_order', 'asc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return "";
        }
    }

    function display_reason_name($re_id) {

        $this->db->select('reason');
        $this->db->from('mic_card_duplicates_reason');
        $this->db->where('re_id', $re_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data['reason'];
        } else {
            return "";
        }
    }

    function add_duplicate_cards($id, $br_state, $br_code, $re_code, $zo_code, $request_epf, $request_user, $reason_id) {

        $now_date = mysql_datetime();

        $card_infor = $this->get_print_card_data($id);

        $title = $card_infor['PR_MICTITL'];
        $name = $card_infor['PR_MICNAME'];
        $address1 = $card_infor['PR_MICADD1'];
        $address2 = $card_infor['PR_MICADD2'];
        $policy_no = $card_infor['PR_MICPLNO'];
        $veh_no = $card_infor['PR_MICVENO'];

        $town = $card_infor['PR_MICTOWN'];

        $miccmdt = $card_infor['PR_MICCMDT'];
        $micexdt = $card_infor['PR_MICEXDT'];
        $period = $card_infor['PR_MICPCOV'];
        $buy_hand = $card_infor['PR_MICBYHND'];
        $col_city = $card_infor['PR_MICCITY'];
        $micrnct = $card_infor['PR_MICRNCT'];
        $micenct = $card_infor['PR_MICENCT'];


        $data = array(
            'br_state' => $br_state,
            'br_code' => $br_code,
            're_code' => $re_code,
            'zo_code' => $zo_code,
            'request_state' => 'P',
            'request_epf' => $request_epf,
            'request_user' => $request_user,
            'request_date' => $now_date,
            'pr_sp_mic_id' => $id,
            'pr_sp_mic_id_new_id' => 0,
            'veh_no' => $veh_no,
            'policy_no' => $policy_no,
            'title' => $title,
            'cust_name' => $name,
            'address_1' => $address1,
            'address_2' => $address2,
            'town' => $town,
            'miccmdte' => $miccmdt,
            'micexdte' => $micexdt,
            'cover_period' => $period,
            'reason_id' => $reason_id,
            'reason' => $this->display_reason_name($reason_id),
            'buy_hand' => $buy_hand,
            'col_city' => $col_city,
            'dup_count' => $this->get_print_card_data_duplicate_count($policy_no, $micrnct, $micenct)
        );

        $result = $this->db->insert('tbl_request_print', $data);
        return $result = $this->db->insert_id();

        $this->load->database()->close();
    }

    function check_request_made($pr_sp_mic_id) {

        $this->db->select('id');
        $this->db->from('tbl_request_print');
        $this->db->where('pr_sp_mic_id', $pr_sp_mic_id);


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }


    function check_before_duplicate_cards($pol_no){
	
	  $sql=" SELECT request_state FROM `tbl_request_print` WHERE `policy_no`='$pol_no'  ORDER BY id  desc limit 0,1";
	  $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data['request_state'];
			
        } else {

            return "P";
        }
	  
	
	}


    function check_request_made_state($pr_sp_mic_id) {
        

        $this->db->select('request_state');
        $this->db->from('tbl_request_print');
        $this->db->where('pr_sp_mic_id', $pr_sp_mic_id);
	$this->db->order_by('id', 'desc');
        $this->db->limit(0,1);
		
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data['request_state'];
        } else {
           return $data['request_state'];// return "P";
        }


    }

    function send_duplicate_card_to_printer($pr_mic_id, $epf_user, $print_user) {


        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;


        $this->db->select('*');
        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MIC_ID', $pr_mic_id);

        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {


                $PR_MIC_ID = $row['PR_MIC_ID'];
                $PR_MIC_INIT_ID = $row['PR_MIC_INIT_ID'];
                $MICPLNO = $row['PR_MICPLNO'];
                $MICRNCT = $row['PR_MICRNCT'];
                $MICENCT = $row['PR_MICENCT'];
                $MICPCOV = $row['PR_MICPCOV'];

                $MICVENO = $row['PR_MICVENO'];
                $MICCMDT = $row['PR_MICCMDT'];
                $MICEXDT = $row['PR_MICEXDT'];
                $MICTITL = $row['PR_MICTITL'];
                $MICNAME = $row['PR_MICNAME'];

                $MICADD1 = $row['PR_MICADD1'];
                $MICADD2 = $row['PR_MICADD2'];
                $MICTOWN = $row['PR_MICTOWN'];
                $MICCITY = $row['PR_MICCITY'];
                $MICSPNO = $row['PR_MICSPNO'];
                $MICMKCD = $row['PR_MICMKCD'];

                $MICQQCD = $row['PR_MICQQCD'];
                $MICTELN = $row['PR_MICTELN'];
                $MICBRCD = $row['PR_MICBRCD'];
                $MICZONE = $row['PR_MICZONE'];

                $MICREGN = $row['PR_MICREGN'];
                $MICISDT = $row['PR_MICISDT'];
                $MICISUS = $row['PR_MICISUS'];
                $MICVLCD = $row['PR_MICVLCD'];
                $MICCDFL = $row['PR_MICCDFL'];
                $MICCDDT = $row['PR_MICCDDT'];
                $MICCDUS = $row['PR_MICCDUS'];
                $MICCRDT = $row['PR_MICCRDT'];
                $MICCRUS = $row['PR_MICCRUS'];

                $MICMNFL = $row['PR_MICMNFL'];
                $MICRESEPF = $row['PR_MICRESEPF'];
                $MICREFNO = $row['PR_MICREFNO'];
                $MICBYHND = $row['PR_MICBYHND'];
                $MICNAME2 = $row['PR_MICNAME2'];

                $MICFLG1 = $row['PR_MICFLG1'];
                $MICFLG2 = $row['PR_MICFLG2'];
                $MICCOD1 = $row['PR_MICCOD1'];
                $MICCOD2 = $row['PR_MICCOD2'];
                $MICRMK2 = $row['PR_MICRMK2'];
                $MICRML3 = $row['PR_MICRML3'];
                $MICNUM1 = $row['PR_MICNUM1'];
                $MICNUM2 = $row['PR_MICNUM2'];


                $data = array(
                    'PR_MIC_INIT_ID' => $PR_MIC_INIT_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $MICBRCD,
                    'PR_MICZONE' => $MICZONE,
                    'PR_MICREGN' => $MICREGN,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => $this->get_print_card_data_duplicate_count($MICPLNO, $MICRNCT, $MICENCT),
                    'PR_ACT_NAME' => $print_user
                );


                $result = $this->db->insert('daily_policy_to_printer', $data);
                $insert_id = $this->db->insert_id();
                
            }
        }


        $this->db->where('pr_sp_mic_id', $PR_MIC_ID);
        $arr4 = array('pr_sp_mic_id_new_id' => $insert_id,
                      'request_state ' => "S"); /// A - authorized , P -pedning , S -Success

        $result = $this->db->update('tbl_request_print', $arr4);

        return $this->db->affected_rows();
    }
	
	
	
	    function send_duplicate_card_to_printer_branch_level($pr_mic_id, $epf_user, $print_user,$pbranch,$preagon,$pzone) {


        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;


        $this->db->select('*');
        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MIC_ID', $pr_mic_id);

        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {


                $PR_MIC_ID = $row['PR_MIC_ID'];
                $PR_MIC_INIT_ID = $row['PR_MIC_INIT_ID'];
                $MICPLNO = $row['PR_MICPLNO'];
                $MICRNCT = $row['PR_MICRNCT'];
                $MICENCT = $row['PR_MICENCT'];
                $MICPCOV = $row['PR_MICPCOV'];

                $MICVENO = $row['PR_MICVENO'];
                $MICCMDT = $row['PR_MICCMDT'];
                $MICEXDT = $row['PR_MICEXDT'];
                $MICTITL = $row['PR_MICTITL'];
                $MICNAME = $row['PR_MICNAME'];

                $MICADD1 = $row['PR_MICADD1'];
                $MICADD2 = $row['PR_MICADD2'];
                $MICTOWN = $row['PR_MICTOWN'];
                $MICCITY = $row['PR_MICCITY'];
                $MICSPNO = $row['PR_MICSPNO'];
                $MICMKCD = $row['PR_MICMKCD'];

                $MICQQCD = $row['PR_MICQQCD'];
                $MICTELN = $row['PR_MICTELN'];
				
                $MICBRCD = $row['PR_MICBRCD'];
                $MICZONE = $row['PR_MICZONE'];
                $MICREGN = $row['PR_MICREGN'];
                
				$MICISDT = $row['PR_MICISDT'];
                $MICISUS = $row['PR_MICISUS'];
                $MICVLCD = $row['PR_MICVLCD'];
                $MICCDFL = $row['PR_MICCDFL'];
                $MICCDDT = $row['PR_MICCDDT'];
                $MICCDUS = $row['PR_MICCDUS'];
                $MICCRDT = $row['PR_MICCRDT'];
                $MICCRUS = $row['PR_MICCRUS'];

                $MICMNFL = $row['PR_MICMNFL'];
                $MICRESEPF = $row['PR_MICRESEPF'];
                $MICREFNO = $row['PR_MICREFNO'];
                $MICBYHND = $row['PR_MICBYHND'];
                $MICNAME2 = $row['PR_MICNAME2'];

                $MICFLG1 = $row['PR_MICFLG1'];
                $MICFLG2 = $row['PR_MICFLG2'];
                $MICCOD1 = $row['PR_MICCOD1'];
                $MICCOD2 = $row['PR_MICCOD2'];
                $MICRMK2 = $row['PR_MICRMK2'];
                $MICRML3 = $row['PR_MICRML3'];
                $MICNUM1 = $row['PR_MICNUM1'];
                $MICNUM2 = $row['PR_MICNUM2'];


                $data = array(
                    'PR_MIC_INIT_ID' => $PR_MIC_INIT_ID,
                    'PR_MICPLNO' => $MICPLNO,
                    'PR_MICRNCT' => $MICRNCT,
                    'PR_MICENCT' => $MICENCT,
                    'PR_MICPCOV' => $MICPCOV,
                    'PR_MICVENO' => $MICVENO,
                    'PR_MICCMDT' => $MICCMDT,
                    'PR_MICEXDT' => $MICEXDT,
                    'PR_MICTITL' => $MICTITL,
                    'PR_MICNAME' => $MICNAME,
                    'PR_MICADD1' => $MICADD1,
                    'PR_MICADD2' => $MICADD2,
                    'PR_MICTOWN' => $MICTOWN,
                    'PR_MICCITY' => $MICCITY,
                    'PR_MICSPNO' => $MICSPNO,
                    'PR_MICMKCD' => $MICMKCD,
                    'PR_MICQQCD' => $MICQQCD,
                    'PR_MICTELN' => $MICTELN,
                    'PR_MICBRCD' => $pbranch,
                    'PR_MICZONE' => $pzone,
                    'PR_MICREGN' => $preagon,
                    'PR_MICISDT' => $MICISDT,
                    'PR_MICISUS' => $MICISUS,
                    'PR_MICVLCD' => $MICVLCD,
                    'PR_MICCDFL' => $MICCDFL,
                    'PR_MICCDDT' => $MICCDDT,
                    'PR_MICCDUS' => $MICCDUS,
                    'PR_MICCRDT' => $MICCRDT,
                    'PR_MICCRUS' => $MICCRUS,
                    'PR_MICMNFL' => $MICMNFL,
                    'PR_MICRESEPF' => $epf_user,
                    'PR_MICREFNO' => $MICREFNO,
                    'PR_MICBYHND' => $MICBYHND,
                    'PR_MICNAME2' => $MICNAME2,
                    'PR_MICFLG1' => $MICFLG1,
                    'PR_MICFLG2' => $MICFLG2,
                    'PR_MICCOD1' => $MICCOD1,
                    'PR_MICCOD2' => $MICCOD2,
                    'PR_MICRMK2' => $MICRMK2,
                    'PR_MICRML3' => $MICRML3,
                    'PR_MICNUM1' => $MICNUM1,
                    'PR_MICNUM2' => $MICNUM2,
                    'PR_CARD_PRINT' => 'N',
                    'PR_PRINT_DATE' => $print_date,
                    'PR_COUNT' => $this->get_print_card_data_duplicate_count($MICPLNO, $MICRNCT, $MICENCT),
                    'PR_ACT_NAME' => $print_user
                );


                $result = $this->db->insert('daily_policy_to_printer', $data);
                $insert_id = $this->db->insert_id();
                
            }
        }


        $this->db->where('pr_sp_mic_id', $PR_MIC_ID);
        $arr4 = array('pr_sp_mic_id_new_id' => $insert_id,
                      'request_state ' => "S"); /// A - authorized , P -pedning , S -Success

        $result = $this->db->update('tbl_request_print', $arr4);

        return $this->db->affected_rows();
    }


}

?>