
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_infor_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('array');
    }

    public function get_mic_card_policy_data($MIC_ID) {

        $this->db->select('MIC_ID,MICPLNO,MICPCOV,MICVENO,MICTITL,MICNAME,MICADD1,
		                   MICADD2,MICTOWN,MICPRDU,PR_TYPRE');

        $this->db->from('daily_policy');
        $this->db->where('MIC_ID', $MIC_ID);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function get_nexus_card_policy_data_daily($MIC_ID_NEXUS) {

        $this->db->select('MIC_ID_NEXUS,MICPLNO,MICPCOV,MICVENO,MICTITL,MICNAME,MICADD1,
		                   MICADD2,MICTOWN,MICPRDU');

        $this->db->from('daily_policy_nexus');
        $this->db->where('MIC_ID_NEXUS', $MIC_ID_NEXUS);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function get_printed_card_policy_data($PR_MIC_ID) {

        $this->db->select('PR_MIC_ID,PR_MICPLNO,PR_MICPCOV,PR_MICVENO,PR_MICTITL,PR_MICNAME,PR_MICADD1,
		                   PR_MICADD2,PR_MICTOWN,PR_PRINT_DATE,PR_TYPRE,PR_ACT_NAME,PR_MICPRDU,PR_TYPRE,PR_CARD_PRINT,PR_MICNAME2,PR_ACT_NAME,PR_MICBRCD,PR_MICRESEPF');

        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MIC_ID', $PR_MIC_ID);
        //$this->db->where('PR_CARD_PRINT', 'Y');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function get_daily_card_policy_data($MIC_ID) {

        $this->db->select('MIC_ID,MICPLNO,MICPCOV,MICVENO,MICTITL,MICNAME,MICADD1,
		                   MICADD2,MICTOWN,MICPRDU,MICNAME2');

        $this->db->from('daily_policy');
        $this->db->where('MIC_ID', $MIC_ID);
        //$this->db->where('PR_CARD_PRINT', 'Y');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function get_printed_card_policy_data_for_mic($PR_MIC_ID) {

        $this->db->select('PR_MIC_ID,PR_MICPLNO,PR_MICPCOV,PR_MICVENO,PR_MICTITL,PR_MICNAME,PR_MICADD1,
		                   PR_MICADD2,PR_MICTOWN,PR_PRINT_DATE,PR_TYPRE,PR_ACT_NAME,PR_MICPRDU,PR_TYPRE,PR_COUNT,PR_MICBRCD,PR_MICRESEPF');

        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MIC_ID', $PR_MIC_ID);


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    function update_mic_serial_data($PR_MIC_ID, $ref_serial) {

        $act_ref = str_replace("B", "", substr($ref_serial, 0, 17));

        $this->db->where('PR_MIC_ID', $PR_MIC_ID);

        $arr = array('PR_MICRML3' => $ref_serial,
            'PR_MICREFNO' => $act_ref);

        $result = $this->db->update('daily_policy_to_printer', $arr);
        return $result; //->db->affected_rows(); 

        $this->load->database()->close();
    }

    function check_serial_exists($ref_serial) {

        $act_ref = str_replace("B", "", substr($ref_serial, 0, 17));

        $this->db->select('PR_MICREFNO');

        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MICREFNO', $act_ref);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return "";
        }
    }

    function card_duplicate_reason($mic_id_new_id) {

        $this->db->select('reason');

        $this->db->from('tbl_request_print');
        $this->db->where('pr_sp_mic_id_new_id', $mic_id_new_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data['reason'];
        } else {
            return "";
        }
    }

}

?>
