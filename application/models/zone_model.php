
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Zone_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_zone_codes() {

        $this->db->select('ZNCODE,ZNDESCRP');
        $this->db->from('mic_zones');
        $this->db->order_by('ZNCODE', 'ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

}

?>
