<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Excel_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    public function get_user_data($epf_no) {

        $this->db->select('brz_state,branch,region,zone,users_name');
        $this->db->from('mic_users');
        $this->db->where('epf_no', $epf_no);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function export_excel_all_fleets($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  AND PR_MICMNFL='F' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  AND PR_MICMNFL='F' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  AND PR_MICMNFL='F' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY PR_MICNAME";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_all_fleets_display($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICMNFL='F' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICMNFL='F' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICMNFL='F'  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY PR_MICNAME";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_hand_count($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state'  ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42'";
                }
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_hand_display($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		
		


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICSPNO,PR_MICMKCD,PR_MICNAME2 FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {
                if ($card_type == 'M') {
                    if ($select_br_1 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                    }

                    if ($select_br_2 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                    }


                    if ($select_br_3 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                    }

                    if ($select_br_4 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                    }


                    if ($select_br_5 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                    }

                    if ($select_br_6 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                    }

                    if ($select_br_7 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                    }

                    if ($select_br_8 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                    }

                    if ($select_br_9 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                    }

                    if ($select_br_10 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                    }

                    if ($select_br_11 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                    }


                    if ($select_br_12 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                    }


                    if ($select_br_13 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                    }


                    if ($select_br_14 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                    }


                    if ($select_br_15 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                    }


                    if ($select_br_16 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                    }

                    if ($select_br_17 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                    }

                    if ($select_br_18 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                    }

                    if ($select_br_19 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                    }


                    if ($select_br_20 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                    }
					
					if ($select_br_21 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                    }
					
					if ($select_br_22 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                    }
					
					if ($select_br_23 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                    }
					
					if ($select_br_24 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                    }
					
					if ($select_br_25 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                    }
					
					if ($select_br_26 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                    }
					if ($select_br_27 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                    }
					if ($select_br_28 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                    }
					
					if ($select_br_29 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                    }
					
					if ($select_br_30 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                    }
					
					
					if ($select_br_31 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                    }
					
					if ($select_br_32 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                    }
					
					if ($select_br_33 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                    }
					
					if ($select_br_34 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                    }
					
					
					if ($select_br_35 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                    }
					
					if ($select_br_36 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                    }
					
					if ($select_br_37 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                    }
					
					if ($select_br_38 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                    }
					if ($select_br_39 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                    }
					
					if ($select_br_40 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                    }
					
					if ($select_br_41 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                    }
					
					
					if ($select_br_42 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                    }
					
					if ($select_br_43 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                    }
					
					
					if ($select_br_44 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                    }
					
					if ($select_br_45 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                    }
					
					if ($select_br_46 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                    }
					
					if ($select_br_47 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                    }
					
					if ($select_br_48 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                    }
					
					if ($select_br_49 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                    }
					
					if ($select_br_50 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                    }
					
					
					
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICSPNO,PR_MICMKCD,PR_MICNAME2 FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= " SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICSPNO,PR_MICMKCD,PR_MICNAME2 FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_list_print_coolombo($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		
		
		



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }


                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }


                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }
                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }
                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                }
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        $sql .= " AND  PR_MICCITY='$city' ";

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MICTOWN";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_list_print_colombo_for_head_office($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            /* if ($branch_code!='A'){

              $sql .= " AND PR_MICBRCD='$branch_code'";

              }
              else
              {

              if ($select_br_1 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_1' ";

              }


              if ($select_br_2 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_2' ";

              }


              if ($select_br_3 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_3' ";

              }


              }



              }


             * else {
              if ($branch_state == 'R') {


              $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

              if ($region_code!='A'){
              $sql .= " AND PR_MICREGN='$region_code'";
              }


              } else {

              $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

              if ($zone_code!='A'){
              $sql .= " AND PR_MICZONE='$zone_code'";
              }
              }
              }

             */
        }

        $sql .= " AND  PR_MICCITY='$city' ";

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MICTOWN";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_list_print_colombo_display_ho_use($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            /* if ($branch_code!='A'){

              $sql .= " AND PR_MICBRCD='$branch_code'";

              }
              else

              {

              if ($select_br_1 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_1' ";

              }


              if ($select_br_2 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_2' ";

              }


              if ($select_br_3 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_3' ";

              }

              }



              } else {
              if ($branch_state == 'R') {


              $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

              if ($region_code!='A'){
              $sql .= " AND PR_MICREGN='$region_code'";
              }


              } else {

              $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state'  ";

              if ($zone_code!='A'){
              $sql .= " AND PR_MICZONE='$zone_code'";
              }
              }
             */
        }


        $sql .= " AND  PR_MICCITY='$city' ";


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_list_print_coolombo_display($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		
		
		
		
		
		



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }


                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }


                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                }
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
				
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state'  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        $sql .= " AND  PR_MICCITY='$city' ";


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_list_print_colombo_display_for_ho($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            /* if ($branch_code!='A'){

              $sql .= " AND PR_MICBRCD='$branch_code'";

              }
              else

              {

              if ($select_br_1 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_1' ";

              }


              if ($select_br_2 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_2' ";

              }


              if ($select_br_3 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_3' ";

              }

              }



              } else {
              if ($branch_state == 'R') {


              $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

              if ($region_code!='A'){
              $sql .= " AND PR_MICREGN='$region_code'";
              }


              } else {

              $sql .= "SELECT PR_MICNAME,PR_MICTOWN FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state'  ";

              if ($zone_code!='A'){
              $sql .= " AND PR_MICZONE='$zone_code'";
              }
              }
             */
        }


        $sql .= " AND  PR_MICCITY='$city' ";


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_list_print_colombo_speration_branch($branch_state, $branch_code, $region_code, $zone_code, $epf, $bstate, $print_state, $city, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

            /* if ($branch_code!='A'){

              $sql .= " AND PR_MICBRCD='$branch_code'";

              }
              else
              {

              if ($select_br_1 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_1' ";

              }


              if ($select_br_2 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_2' ";

              }


              if ($select_br_3 !="" ) {

              $sql .= " AND PR_MICBRCD <>'$select_br_3' ";

              }


              }
             */
        } /* else {
          if ($branch_state == 'R') {


          $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

          if ($region_code!='A'){
          $sql .= " AND PR_MICREGN='$region_code'";
          }


          } else {

          $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICBYHND='$bstate'  AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='$print_state' ";

          if ($zone_code!='A'){
          $sql .= " AND PR_MICZONE='$zone_code'";
          }
          }
          }
         */

        $sql .= " AND  PR_MICCITY='$city' ";

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MICTOWN";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_letters($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		
		
		



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }
                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }
                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                }
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_letters_ho($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

            ///if ($branch_code!='A'){
            ///$/sql .= " AND PR_MICBRCD='$branch_code'";
            ///}
            //else
            //{
            //if ($select_br_1 !="" ) {
            ///$sql .= " AND PR_MICBRCD <>'$select_br_1' ";
            // }
            //if ($select_br_2 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_2' ";
            // }
            //if ($select_br_3 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_3' ";
            // }
            //}
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_count($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }
                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }
                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }
                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }
                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                }
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_count_ho($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_count_branch($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B' ";


            $sql .= " AND PR_MICBRCD='$branch_code'";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B'  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_count_for_ho($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;




        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

            ///if ($branch_code!='A'){
            //$sql .= " AND PR_MICBRCD='$branch_code'";
            //}
            //else
            //{
            ///if ($select_br_1 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_1' ";
            // }
            // if ($select_br_2 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_2' ";
            // }
            // 
            //if ($select_br_3 !="" ) {
            //
			//$sql .= " AND PR_MICBRCD <>'$select_br_3' ";
            /// }
            //}
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_count_for_branch($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B'";

            ///if ($branch_code!='A'){

            $sql .= " AND PR_MICBRCD='$branch_code'";

            //}
            //else
            //{
            ///if ($select_br_1 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_1' ";
            // }
            // if ($select_br_2 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_2' ";
            // }
            // 
            //if ($select_br_3 !="" ) {
            //
			//$sql .= " AND PR_MICBRCD <>'$select_br_3' ";
            /// }
            //}
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }


        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_letters_for_manual_print($branch_state, $branch_code, $region_code, $zone_code, $card_type, $manualType) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICMNFL='$manualType' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= "  ORDER BY PR_MICNAME";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_letters_display($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }



                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }


                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }


                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }


                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }

                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }

                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }

                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }

                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				  if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				  if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				
				  if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				  if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				  if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				  if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				  if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				
				  if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				  if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				
				  if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				 if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				 if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				
				
				 if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				
				 if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				
				 if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				
				 if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				
				 if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				 if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				 if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				 if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				 if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				 if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                }
				
				 if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				
				 if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				 if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				 if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				 if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				
				 if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				
				 if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				
				 if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICMPNO,PR_MICNICN,PR_MICRML3  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'   ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_letters_display_ho($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

            ///if ($branch_code!='A'){
            //$sql .= " AND PR_MICBRCD='$branch_code'";
            //}
            //else
            //{
            //if ($select_br_1 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_1' ";
            /// }
            //if ($select_br_2 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_2' ";
            //}
            //if ($select_br_3 !="" ) {
            //$sql .= " AND PR_MICBRCD <>'$select_br_3' ";
            // 
            // }
            ///}
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICMPNO,PR_MICNICN,PR_MICRML3  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'   ";

                //if ($region_code!='A'){
                //$sql .= " AND PR_MICREGN='$region_code'";
                //}
            } else {

                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

                //if ($zone_code!='A'){
                //$sql .= " AND PR_MICZONE='$zone_code'";
                //}
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_letters_for_nexus_only($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICMPNO,PR_MICNICN,PR_MICRML3  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_display_data($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		
		
		
		
		


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {

                if ($select_br_1 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                }

                if ($select_br_2 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                }

                if ($select_br_3 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                }

                if ($select_br_4 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                }

                if ($select_br_5 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                }

                if ($select_br_6 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                }

                if ($select_br_7 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                }

                if ($select_br_8 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                }

                if ($select_br_9 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                }
                if ($select_br_10 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                }
                if ($select_br_11 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                }
                if ($select_br_12 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                }

                if ($select_br_13 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                }

                if ($select_br_14 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                }

                if ($select_br_15 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                }

                if ($select_br_16 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                }


                if ($select_br_17 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                }

                if ($select_br_18 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                }

                if ($select_br_19 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                }

                if ($select_br_20 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                }
				
				if ($select_br_21 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                }
				
				if ($select_br_22 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                }
				
				if ($select_br_23 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                }
				
				if ($select_br_24 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                }
				
				if ($select_br_25 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                }
				
				if ($select_br_26 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                }
				
				if ($select_br_27 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                }
				
				if ($select_br_28 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                }
				
				if ($select_br_29 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                }
				
				if ($select_br_30 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                }
				
				if ($select_br_31 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                }
				
				
				if ($select_br_32 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                }
				
				
				if ($select_br_33 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                }
				
				if ($select_br_34 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                }
				
				if ($select_br_35 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                }
				
				if ($select_br_36 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                }
				
				
				if ($select_br_37 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                }
				
				
				if ($select_br_38 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                }
				
				if ($select_br_39 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                }
				
				if ($select_br_40 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                }
				
				if ($select_br_41 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                }
				
				if ($select_br_42 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                }
				
				
				if ($select_br_43 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                }
				
				if ($select_br_44 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                }
				
				if ($select_br_45 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                }
				
				if ($select_br_46 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                }
				
				if ($select_br_47 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                }
				
				if ($select_br_48 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                }
				
				if ($select_br_49 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                }
				
				if ($select_br_50 != "") {

                    $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                }
				
				
				
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_display_data_ho($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B' ";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND!='B'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_cards_display_data_branch_view($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B' ";

            $sql .= " AND PR_MICBRCD='$branch_code'";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_serail_data($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;




        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {
                if ($card_type == 'M') {

                    if ($select_br_1 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                    }

                    if ($select_br_2 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                    }

                    if ($select_br_3 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                    }

                    if ($select_br_4 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                    }

                    if ($select_br_5 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                    }

                    if ($select_br_6 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                    }
                    if ($select_br_7 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                    }
                    if ($select_br_8 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                    }
                    if ($select_br_9 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                    }

                    if ($select_br_10 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                    }

                    if ($select_br_11 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                    }

                    if ($select_br_12 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                    }

                    if ($select_br_13 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                    }

                    if ($select_br_14 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                    }

                    if ($select_br_15 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                    }
                    if ($select_br_16 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                    }
                    if ($select_br_17 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                    }
                    if ($select_br_18 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                    }

                    if ($select_br_19 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                    }

                    if ($select_br_20 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                    }
					 if ($select_br_21 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                    }
					
					 if ($select_br_22 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                    }
					
					 if ($select_br_23 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                    }
					
					 if ($select_br_24 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                    }
					
					 if ($select_br_25 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                    }
					
					 if ($select_br_26 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                    }
					
					 if ($select_br_27 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                    }
					
					 if ($select_br_28 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                    }
					
					 if ($select_br_29 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                    }
					
					if ($select_br_30 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                    }
					
					 if ($select_br_31 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                    }
					
					
					 if ($select_br_32 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                    }
					
					
					 if ($select_br_33 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                    }
					
					
					 if ($select_br_34 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                    }
					
					 if ($select_br_35 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                    }
					
					 if ($select_br_36 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                    }
					
					 if ($select_br_37 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                    }
					
					
					 if ($select_br_38 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                    }
					
					
					 if ($select_br_39 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                    }
					
					
					 if ($select_br_40 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                    }
					
					
					 if ($select_br_41 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                    }
					
					 if ($select_br_42 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                    }
					
					 if ($select_br_43 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                    }
					
					 if ($select_br_44 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                    }
					
					
					 if ($select_br_45 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                    }
					
					 if ($select_br_46 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                    }
					
					 if ($select_br_47 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                    }
					
					 if ($select_br_48 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                    }
					
					
					 if ($select_br_49 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                    }
					
					 if ($select_br_50 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                    }
					
					
					
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_serail_data_ho($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_excel_by_all_serail_data_branch($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;
        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;



        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B' ";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICREFNO,PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' AND PR_MICBYHND='B'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " AND PR_MICBRCD='$branch_code'";

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }


        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_nexus_points_list($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE  PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MIC_ID FROM daily_policy_to_printer  ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function export_nexus_points_list_display($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICMPNO,PR_MICNICN,PR_MICRML3  FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT  PR_MICTITL,PR_MICREFNO,PR_MICVENO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICREFNO,PR_MICPLNO,PR_MICPCOV,PR_MICPRDU,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICGNDR,PR_MICMPNO,PR_MICNICN,PR_MICRML3 FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_report_data_display($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;

        $select_br_4 = BR_CLIENT_4;
        $select_br_5 = BR_CLIENT_5;
        $select_br_6 = BR_CLIENT_6;
        $select_br_7 = BR_CLIENT_7;

        $select_br_8 = BR_CLIENT_8;
        $select_br_9 = BR_CLIENT_9;
        $select_br_10 = BR_CLIENT_10;
        $select_br_11 = BR_CLIENT_11;
        $select_br_12 = BR_CLIENT_12;
        $select_br_13 = BR_CLIENT_13;
        $select_br_14 = BR_CLIENT_14;
        $select_br_15 = BR_CLIENT_15;
        $select_br_16 = BR_CLIENT_16;
        $select_br_17 = BR_CLIENT_17;
        $select_br_18 = BR_CLIENT_18;
        $select_br_19 = BR_CLIENT_19;
        $select_br_20 = BR_CLIENT_20;
		
		$select_br_21 = BR_CLIENT_21;
		$select_br_22 = BR_CLIENT_22;
		$select_br_23 = BR_CLIENT_23;
		$select_br_24 = BR_CLIENT_24;
		$select_br_25 = BR_CLIENT_25;
		$select_br_26 = BR_CLIENT_26;
		$select_br_27 = BR_CLIENT_27;
		$select_br_28 = BR_CLIENT_28;
		$select_br_29 = BR_CLIENT_29;
		$select_br_30 = BR_CLIENT_30;
		
		
		$select_br_31 = BR_CLIENT_31;
		$select_br_32 = BR_CLIENT_32;
		$select_br_33 = BR_CLIENT_33;
		$select_br_34 = BR_CLIENT_34;
		$select_br_35 = BR_CLIENT_35;
		$select_br_36 = BR_CLIENT_36;
		$select_br_37 = BR_CLIENT_37;
		$select_br_38 = BR_CLIENT_38;
		$select_br_39 = BR_CLIENT_39;
		$select_br_40 = BR_CLIENT_40;
		$select_br_41 = BR_CLIENT_41;
		$select_br_42 = BR_CLIENT_42;
		$select_br_43 = BR_CLIENT_43;
		$select_br_44 = BR_CLIENT_44;
		$select_br_45 = BR_CLIENT_45;
		
		$select_br_46 = BR_CLIENT_46;
		$select_br_47 = BR_CLIENT_47;
		$select_br_48 = BR_CLIENT_48;
		$select_br_49 = BR_CLIENT_49;
		$select_br_50 = BR_CLIENT_50;
		
		
		
		
		


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            } else {
                if ($card_type == 'M') {
                    if ($select_br_1 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_1' ";
                    }

                    if ($select_br_2 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_2' ";
                    }

                    if ($select_br_3 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_3' ";
                    }

                    if ($select_br_4 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_4' ";
                    }

                    if ($select_br_5 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_5' ";
                    }

                    if ($select_br_6 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_6' ";
                    }

                    if ($select_br_7 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_7' ";
                    }


                    if ($select_br_8 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_8' ";
                    }


                    if ($select_br_9 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_9' ";
                    }


                    if ($select_br_10 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_10' ";
                    }


                    if ($select_br_11 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_11' ";
                    }



                    if ($select_br_12 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_12' ";
                    }


                    if ($select_br_13 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_13' ";
                    }


                    if ($select_br_14 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_14' ";
                    }


                    if ($select_br_15 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_15' ";
                    }



                    if ($select_br_16 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_16' ";
                    }


                    if ($select_br_17 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_17' ";
                    }

                    if ($select_br_18 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_18' ";
                    }


                    if ($select_br_19 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_19' ";
                    }



                    if ($select_br_20 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_20' ";
                    }
					
					if ($select_br_21 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_21' ";
                    }
					if ($select_br_22 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_22' ";
                    }
					if ($select_br_23 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_23' ";
                    }
					if ($select_br_24 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_24' ";
                    }
					
					if ($select_br_25 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_25' ";
                    }
					
					if ($select_br_26 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_26' ";
                    }
					
					if ($select_br_27 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_27' ";
                    }
					
					if ($select_br_28 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_28' ";
                    }
					
					if ($select_br_29 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_29' ";
                    }
					
					if ($select_br_30 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_30' ";
                    }
					
					if ($select_br_31 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_31' ";
                    }
					
					if ($select_br_32 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_32' ";
                    }
					
					if ($select_br_33 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_33' ";
                    }
					
					
					if ($select_br_34 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_34' ";
                    }
					
					
					if ($select_br_35 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_35' ";
                    }
					
					if ($select_br_36 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_36' ";
                    }
					
					if ($select_br_37 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_37' ";
                    }
					
					if ($select_br_38 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_38' ";
                    }
					
					if ($select_br_39 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_39' ";
                    }
					
					if ($select_br_40 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_40' ";
                    }
					
					if ($select_br_41 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_41' ";
                    }
					
					if ($select_br_42 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_42' ";
                    }
					
					
					if ($select_br_43 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_43' ";
                    }
					
					if ($select_br_44 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_44' ";
                    }
					
					if ($select_br_45 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_45' ";
                    }
					
					if ($select_br_46 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_46' ";
                    }
					
					if ($select_br_47 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_47' ";
                    }
					
					if ($select_br_48 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_48' ";
                    }
					
					
					if ($select_br_49 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_49' ";
                    }
					
					if ($select_br_50 != "") {

                        $sql .= " AND PR_MICBRCD <>'$select_br_50' ";
                    }
					
					
					
					
                }
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_report_data_display_ho_level($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_detail_report_data_display_branch_split_level($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  AND PR_MICBYHND='B'";

            $sql .= " AND PR_MICBRCD='$branch_code'";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  AND PR_MICBYHND='B' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'  AND PR_MICBYHND='B'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_mic_daily_detail_report_all_cards($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
        $select_br_1 = BR_CLIENT_1;
        $select_br_2 = BR_CLIENT_2;
        $select_br_3 = BR_CLIENT_3;


        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICREFNO,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICSPNO,PR_MICMKCD,PR_MICTELN,PR_MICTITL,PR_MICPRDU,PR_MICNAME2,PR_MICMPNO
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        if ($card_type == 'M') {
            $sql .= " AND PR_MICMNFL<>'M' ";
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    public function export_ntb_report_data($branch_state, $branch_code, $region_code, $zone_code, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");

        if ($branch_state == 'B') {

            $sql .= "SELECT PR_MICTITL,PR_MICNAME,PR_MICGNDR,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICMPNO,PR_MICNICN,PR_MICRML3,PR_MICREFNO,PR_MICPCOV
			         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

            if ($branch_code != 'A') {

                $sql .= " AND PR_MICBRCD='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT PR_MICTITL,PR_MICNAME,PR_MICGNDR,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICMPNO,PR_MICNICN,PR_MICRML3,PR_MICREFNO,PR_MICPCOV
				         FROM daily_policy_to_printer WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y' ";

                if ($region_code != 'A') {
                    $sql .= " AND PR_MICREGN='$region_code'";
                }
            } else {

                $sql .= "SELECT PR_MICTITL,PR_MICNAME,PR_MICGNDR,PR_MICADD1,PR_MICADD2,PR_MICTOWN,PR_MICMPNO,PR_MICNICN,PR_MICRML3,PR_MICREFNO,PR_MICPCOV
				         FROM daily_policy_to_printer  WHERE PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";

                if ($zone_code != 'A') {
                    $sql .= " AND PR_MICZONE='$zone_code'";
                }
            }
        }

        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }
	
	 public function export_excel_by_all_cards_count_branch_for_mannaul_print($epf, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
		
		$sql  = " SELECT PR_MIC_ID FROM daily_policy_to_printer WHERE PR_MICRESEPF='$epf' 
		          AND PR_MICMNFL='M' AND PR_TYPRE='M'AND PR_PRINT_DATE='$pdate' AND PR_CARD_PRINT='Y'";
    
        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }
	
	    public function export_excel_by_all_cards_count_branch_for_mannaul_print_details($epf, $card_type) {

        $sql = "";

        $pdate = date("Y-m-d");
		
		$sql  = " SELECT PR_MICVENO,PR_MICPLNO,PR_MICPCOV,PR_MICNAME,PR_MICADD1,PR_MICADD2,PR_MICSPNO,PR_MICMKCD,PR_MICNAME2,PR_MICREFNO
		          FROM daily_policy_to_printer WHERE PR_MICRESEPF='$epf' AND PR_MICMNFL='M' AND PR_TYPRE='M'AND PR_PRINT_DATE='$pdate' 
				  AND PR_CARD_PRINT='Y'";
    
        $sql .= " AND PR_TYPRE='$card_type' ORDER BY PR_MIC_ID";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }


}

?>
