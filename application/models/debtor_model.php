
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Debtor_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function add_new_debtor_name($debtor_name, $creator_epf) {

        $now_date = mysql_datetime();

        $data = array(
            'debtor_name' => $debtor_name,
            'debtor_state' => 1,
            'debtor_add_epf_no' => $creator_epf,
            'debtor_create_date' => $now_date
        );

        $result = $this->db->insert('mic_debtor_names', $data);
        return $result = $this->db->insert_id();

        $this->load->database()->close();
    }

    public function debtor_name_not_exists($debtor_name) {


        $query_str = "SELECT debtor_id FROM mic_debtor_names WHERE debtor_name=?";
        $result = $this->db->query($query_str, array($debtor_name));

        if ($result->num_rows() > 0) {

            return true;
        } else {
            return false;
        }
    }

}

?>
