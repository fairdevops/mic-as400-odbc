<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daily_excel_model extends CI_Model
 {
    
	public function __construct()
	{
	   parent::__construct();
	   $this->load->database(); 
	   $this->load->helper('date');
	  
	}
	
 
 		 public function display_card_only_city_display($branch_state, $branch_code, $region_code, $zone_code,$city,$by_hand) {
	         
                        $sql ="";
			
			$select_br_1 = BR_CLIENT_1;
			$select_br_2 = BR_CLIENT_2;
	    	$select_br_3 = BR_CLIENT_3;

			
			///BR_CLIENT_1
			
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='$by_hand'";
			
		
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			
		    	if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";
				
				if ($region_code!='A'){
				$sql .= " AND  MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MIC_ID,MICPLNO,MICRNCT,MICTITL,MICNAME,MICVENO,MICSPNO,MICTOWN,MICPCOV FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='$by_hand' ";
				
				if ($zone_code!='A'){
				$sql .= " AND  MICZONE='$zone_code'";
				}
            }
        }

       if ($city!=""){
				$sql .= " AND  MICCITY='$city' AND MICBYHND=''";
				}
		
	   if ( $by_hand == 'B' ) {
	   
	            $sql .= " ORDER BY MICNAME2 ";
	   }
	   else
	   {
	             $sql .= " ORDER BY MICNAME ";
	   
	   }	
				
				

//echo $sql;
 
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

 	

  public function export_mic_detail_buyhand_display($branch_state, $branch_code, $region_code, $zone_code,$card_type) {
	         
            $sql =""; 
			
			$pdate       = date("Y-m-d");
			$select_br_1 = BR_CLIENT_1;
			
            
        if ($branch_state == 'B') {

            $sql .= "SELECT *
			         FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B' ";
					 
							 
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			  if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT *
				         FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B'  ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT *
				         FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='B' ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  $sql .= " ORDER BY MICNAME2 ";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

       
    }	
	
  public function export_mic_detail_buyhand_count($branch_state, $branch_code, $region_code, $zone_code,$card_type) {
	         
            $sql =""; 
			
			$pdate       = date("Y-m-d");
			$select_br_1 = BR_CLIENT_1;
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICREFNO   FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B' ";
			
				
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			
			if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICREFNO  FROM daily_policy WHERE MICREFNO=0 AND MICBYHND='B'  ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICREFNO  FROM daily_policy  WHERE MICREFNO=0 AND MICBYHND='B' ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  $sql .= " ORDER BY MICNAME2 ";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

       
    }	
	
	
	 public function export_mic_detail_city_count($branch_state, $branch_code, $region_code, $zone_code,$city) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
			$select_br_1 = BR_CLIENT_1;
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICREFNO   FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='' ";
			
					
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			
			  if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICREFNO  FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICREFNO  FROM daily_policy  WHERE MICREFNO=0 AND MICCITY='$city' ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  $sql .= " ORDER BY MICNAME2 ";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

       
    }	
	
	
	
	 public function export_mic_detail_city_display($branch_state, $branch_code, $region_code, $zone_code,$city) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
			$select_br_1 = BR_CLIENT_1;
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='' ";
					 
			 
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			   	if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			  
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy  WHERE MICREFNO=0 AND MICCITY='$city' ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  //$sql .= " ORDER BY MICNAME2 ASC";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

       
    }	
	
	
	
	 public function export_mic_detail_out_station_display($branch_state, $branch_code, $region_code, $zone_code,$city) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
			$select_br_1 = BR_CLIENT_1;
			
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='' ";
					 
					 
					 
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			   if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy WHERE MICREFNO=0 AND MICCITY='$city'  AND MICBYHND='' ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy  WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND=''";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  //$sql .= " ORDER BY MICNAME2 ASC";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

       
    }	
	
	
	public function display_daily_nexus_before_print($branch_state, $branch_code, $region_code, $zone_code) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
		             FROM daily_policy_nexus WHERE MICREFNO=0";
			
			if ($branch_code !='A') {		 
			    $sql .= " AND MICBRCD='$branch_code' ";	
				}	 
					 
					 
        } else {
            if ($branch_state == 'R') {


                 $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
		                 FROM daily_policy_nexus WHERE MICREFNO=0";
				
				if ($region_code !='A') {	
				    $sql .= " AND MICREGN='$region_code' ";	
				 } 
						 
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
		                 FROM daily_policy_nexus WHERE MICREFNO=0 ";
				
				if ($zone_code !='A') {			 
				$sql .=" AND MICZONE='$zone_code'";
				}
            }
        }



        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        //$this->load->database()->close();
    }
	
	
	
	
	
	
	
	
	 public function export_mic_detail_all($branch_state, $branch_code, $region_code, $zone_code) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
            
    		$select_br_1 = BR_CLIENT_1;
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy WHERE MICREFNO=0  ";
					 
			 
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			   	if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			  
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy WHERE MICREFNO=0  ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy  WHERE MICREFNO=0  ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  //$sql .= " ORDER BY MICNAME2 ASC";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

       
    }	
	
	
	
		 public function export_mic_detail_all_data_count($branch_state, $branch_code, $region_code, $zone_code) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
            
    		$select_br_1 = BR_CLIENT_1;
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO FROM daily_policy WHERE MICREFNO=0  ";
					 
			 
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			else
			{
			   	if ($select_br_1 !="" ) {
			
			$sql .= " AND MICBRCD <>'$select_br_1' ";
			
			}
			  
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO  FROM daily_policy WHERE MICREFNO=0  ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICVENO  FROM daily_policy  WHERE MICREFNO=0  ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  //$sql .= " ORDER BY MICNAME2 ASC";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

       
    }	

	
	
	
	 public function export_nexus_detail_buyhand_count($branch_state, $branch_code, $region_code, $zone_code,$card_type) {
	         
            $sql =""; 
			
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MIC_ID_NEXUS   FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B' ";
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MIC_ID_NEXUS  FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B'  ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MIC_ID_NEXUS  FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICBYHND='B' ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  $sql .= " ORDER BY MICNAME2 ";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

       
    }	
	


 public function export_nexus_detail_buyhand_display($branch_state, $branch_code, $region_code, $zone_code,$card_type) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
            
        if ($branch_state == 'B') {

            $sql .= "SELECT *
			         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B' ";
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT *
				         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICBYHND='B'  ";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT *
				         FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICBYHND='B' ";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		  $sql .= " ORDER BY MICNAME2 ";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

       
    }		
	
	

public function export_nexus_detail_city_count($branch_state, $branch_code, $region_code, $zone_code,$city,$by_hand) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICREFNO   FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICREFNO  FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICREFNO  FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		   $sql .= " ORDER BY MICNAME2 ";
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

       
    }
	
	
  public function export_nexus_detail_city_display($branch_state, $branch_code, $region_code, $zone_code,$city,$by_hand) {
	         
            $sql =""; 
			
			$pdate= date("Y-m-d");
            
        if ($branch_state == 'B') {

            $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
			         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";
			
			if ($branch_code!='A'){
			    
				$sql .= " AND MICBRCD='$branch_code'";
				
			}
			
			
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy_nexus WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";
				
				if ($region_code!='A'){
				$sql .= " AND MICREGN='$region_code'";
				}
				
				
            } else {

                $sql .= "SELECT MICVENO,MICPLNO,MICPCOV,MICREFNO,MICNAME,MICADD1,MICADD2,MICTOWN,MICSPNO,MICMKCD,MICMPNO,MICNAME2
				         FROM daily_policy_nexus  WHERE MICREFNO=0 AND MICCITY='$city' AND MICBYHND='$by_hand'";
				
				if ($zone_code!='A'){
				$sql .= " AND MICZONE='$zone_code'";
				}
            }
        }
           
		 
		   


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

       
    }		
	
	
	
	
			

}


?>
