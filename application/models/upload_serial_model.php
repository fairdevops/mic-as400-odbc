
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload_serial_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    function get_upload_card_id($pol_no, $veh_no, $period, $serial) {


        $this->db->select('PR_MIC_ID');
        $this->db->from('daily_policy_to_printer');
        $this->db->where('PR_MICPLNO', $pol_no);
        $this->db->where('PR_MICVENO', $veh_no);
        $this->db->where('PR_MICPCOV', $period);
        $this->db->where('PR_TYPRE', 'M');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            $pass_id = $data['PR_MIC_ID'];
            $this->db->where('PR_MIC_ID', $pass_id);
            $arr = array('PR_MICREFNO' => $serial);

            $result = $this->db->update('daily_policy_to_printer', $arr);
            return $result; //->db->affected_rows(); 
        } else {
            return 0;
        }
    }

}

?>
