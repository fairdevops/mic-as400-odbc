<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Man_authorize_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    public function count_authorize_pending_record_count($branch_state, $branch_code, $region_code, $zone_code, $request_state) {
        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT id FROM tbl_request_print WHERE  request_state='$request_state'  ";

            if ($branch_code != 'A') {

                $sql .= " AND br_code='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT id FROM tbl_request_print WHERE  request_state='$request_state' ";

                if ($region_code != 'A') {
                    $sql .= " AND re_code='$region_code'";
                }
            } else {

                $sql .= "SELECT id FROM tbl_request_print WHERE  request_state='$request_state' ";

                if ($zone_code != 'A') {
                    $sql .= " AND zo_code='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY request_date";


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }
    }

    public function display_authorize_pending_records($branch_state, $branch_code, $region_code, $zone_code, $request_state) {

        $sql = "";

        if ($branch_state == 'B') {

            $sql .= "SELECT id,policy_no,veh_no,cover_period,cust_name ,DATE_FORMAT(request_date, '%d%/%m%/%Y %h%:%i%:%s% %p') AS request_date FROM tbl_request_print WHERE  request_state='$request_state'  ";

            if ($branch_code != 'A') {

                $sql .= " AND br_code='$branch_code'";
            }
        } else {
            if ($branch_state == 'R') {


                $sql .= "SELECT id,policy_no,veh_no,cover_period,cust_name,DATE_FORMAT(request_date, '%d%/%m%/%Y %h%:%i%:%s% %p') AS request_date FROM tbl_request_print WHERE  request_state='$request_state' ";

                if ($region_code != 'A') {
                    $sql .= " AND re_code='$region_code'";
                }
            } else {

                $sql .= "SELECT id,policy_no,veh_no,cover_period,cust_name, DATE_FORMAT(request_date, '%d%/%m%/%Y %h%:%i%:%s% %p') AS request_date FROM tbl_request_print WHERE  request_state='$request_state' ";

                if ($zone_code != 'A') {
                    $sql .= " AND zo_code='$zone_code'";
                }
            }
        }

        $sql .= " ORDER BY request_date";




        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }
    }

    function update_authorize_card($auth_epf, $id, $auth_state, $auth_user_name) {

        $now_date = mysql_datetime();

        $this->db->where('id', $id);
        $arr = array('request_state' => $auth_state,
					 'auth_date ' => $now_date,
					 'auth_epf ' => $auth_epf,
					 'auth_user_name' => $auth_user_name); /// A - authorized , P -pedning , S -Success

        $result = $this->db->update('tbl_request_print', $arr);

        return $this->db->affected_rows();
    }

}

?>