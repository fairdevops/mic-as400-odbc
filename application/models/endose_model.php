
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Endose_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_endosement_data($pol_no) {

        $this->db->select('MICPCOV,MICPLNO');
        $this->db->from('mic_famiclh');

        $this->db->where('MICPLNO', $pol_no);
        $this->db->group_by('MICPCOV', 'ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {
            return 0;
        }
    }

    function get_endosement_data_count($pol_no) {

        $this->db->select('MICPCOV');
        $this->db->from('mic_famiclh');

        $this->db->where('MICPLNO', $pol_no);
        $this->db->group_by('MICPCOV', 'ASC');

        $query = $this->db->get();

        return $query->num_rows();
    }

    function get_cover_period($pol_no, $limit) {
        $start = $limit - 1;



        $want = array();
        $query_str = "SELECT MICPCOV FROM mic_famiclh WHERE  MICPLNO =? GROUP BY MICPCOV ASC ";
        $result = $this->db->query($query_str, array($pol_no));

        $x = 1;





        if ($result->num_rows() > 0) {

            foreach ($result->result_array() as $row) {
                $x++;

                $MICPCOV = $row['MICPCOV'];
                $want[$x - 1] = $MICPCOV;
            }
        }


        return $want[$limit];
    }

    function add_delete_data_from_iis($policy_no, $cover, $brz_state, $branch, $region, $zone, $access_user_epf, $accesser_name) {

        $now_date = mysql_datetime();


        $data = array(
            'policy_no' => $policy_no,
            'cover' => $cover,
            'brz_state' => $brz_state,
            'branch' => $branch,
            'region' => $region,
            'zone' => $zone,
            'access_user_epf' => $access_user_epf,
            'accesser_name' => $accesser_name,
            'db2_update' => 'N',
            'access_date' => $now_date
        );

        $result = $this->db->insert('mic_famiclh_delete', $data);
        $id = $this->db->insert_id();

        if ($id > 0) {

            return $this->db->delete('mic_famiclh', array('MICPLNO' => $policy_no, 'MICPCOV' => $cover));
        }
    }

}

?>
