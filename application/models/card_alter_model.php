
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_alter_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('array');
    }

    function update_buy_hand_basket($epf_user, $print_user, $branch_state, $branch_code, $region_code, $zone_code, $MIC_ID, $post_branch_id) {

        $now_date = mysql_as400_date();
        $print_date = mysql_date();
        $g = 0;


        $this->load->model('branch_model');
        $new_region = $this->branch_model->get_regon_code($post_branch_id);

        $this->load->model('region_model');
        $new_zone_code = $this->region_model->get_zone_code($new_region);



        $this->db->select('*');
        $this->db->from('daily_policy');
        $this->db->where('MIC_ID', $MIC_ID);

        $query_1 = $this->db->get();

        if ($query_1->num_rows() > 0) {

            foreach ($query_1->result_array() as $row) {

                $MIC_ID = $row['MIC_ID'];
                $MICPLNO = $row['MICPLNO'];
                $MICRNCT = $row['MICRNCT'];
                $MICENCT = $row['MICENCT'];
                $MICPCOV = $row['MICPCOV'];

                $MICVENO = $row['MICVENO'];
                $MICCMDT = $row['MICCMDT'];
                $MICEXDT = $row['MICEXDT'];
                $MICTITL = $row['MICTITL'];
                $MICNAME = $row['MICNAME'];

                $MICADD1 = $row['MICADD1'];
                $MICADD2 = $row['MICADD2'];
                $MICTOWN = $row['MICTOWN'];
                $MICCITY = $row['MICCITY'];
                $MICSPNO = $row['MICSPNO'];
                $MICMKCD = $row['MICMKCD'];

                $MICQQCD = $row['MICQQCD'];
                $MICTELN = $row['MICTELN'];
                $MICBRCD = $row['MICBRCD'];
                $MICZONE = $row['MICZONE'];

                $MICREGN = $row['MICREGN'];
                $MICISDT = $row['MICISDT'];
                $MICISUS = $row['MICISUS'];
                $MICVLCD = $row['MICVLCD'];
                $MICCDFL = $row['MICCDFL'];
                $MICCDDT = $row['MICCDDT'];
                $MICCDUS = $row['MICCDUS'];
                $MICCRDT = $row['MICCRDT'];
                $MICCRUS = $row['MICCRUS'];

                $MICMNFL = $row['MICMNFL'];
                $MICRESEPF = $row['MICRESEPF'];
                $MICREFNO = $row['MICREFNO'];
                $MICBYHND = $row['MICBYHND'];
                $MICNAME2 = trim($row['MICNAME2']);
                $MICPRDU = $row['MICPRDU'];
                $MICREMK = $row['MICREMK'];
                $MICGNDR = $row['MICGNDR'];
                $MICMPNO = $row['MICMPNO'];
                $MICDOBT = $row['MICDOBT'];
                $MICNICN = $row['MICNICN'];

                $MICFLG1 = $row['MICFLG1'];
                $MICFLG2 = $row['MICFLG2'];
                $MICCOD1 = $row['MICCOD1'];
                $MICCOD2 = $row['MICCOD2'];
                $MICRMK2 = $row['MICRMK2'];
                $MICRML3 = $row['MICRML3'];
                $MICNUM1 = $row['MICNUM1'];
                $MICNUM2 = $row['MICNUM2'];


                $data3 = array(
                    'PSH_MIC_ID' => $MIC_ID,
                    'PSH_MICPLNO' => $MICPLNO,
                    'PSH_MICRNCT' => $MICRNCT,
                    'PSH_MICENCT' => $MICENCT,
                    'PSH_MICPCOV' => $MICPCOV,
                    'PSH_MICVENO' => $MICVENO,
                    'PSH_MICCMDT' => $MICCMDT,
                    'PSH_MICEXDT' => $MICEXDT,
                    'PSH_MICTITL' => $MICTITL,
                    'PSH_MICNAME' => $MICNAME,
                    'PSH_MICADD1' => $MICADD1,
                    'PSH_MICADD2' => $MICADD2,
                    'PSH_MICTOWN' => $MICTOWN,
                    'PSH_MICCITY' => $MICCITY,
                    'PSH_MICSPNO' => $MICSPNO,
                    'PSH_MICMKCD' => $MICMKCD,
                    'PSH_MICQQCD' => $MICQQCD,
                    'PSH_MICTELN' => $MICTELN,
                    'PSH_MICBRCD' => $MICBRCD,
                    'PSH_MICZONE' => $MICZONE,
                    'PSH_MICREGN' => $MICREGN,
                    'PSH_MICISDT' => $MICISDT,
                    'PSH_MICISUS' => $MICISUS,
                    'PSH_MICVLCD' => $MICVLCD,
                    'PSH_MICCDFL' => $MICCDFL,
                    'PSH_MICCDDT' => $MICCDDT,
                    'PSH_MICCDUS' => $MICCDUS,
                    'PSH_MICCRDT' => $MICCRDT,
                    'PSH_MICCRUS' => $MICCRUS,
                    'PSH_MICMNFL' => $MICMNFL,
                    'PSH_MICRESEPF' => $epf_user,
                    'PSH_MICREFNO' => $MICREFNO,
                    'PSH_MICBYHND' => $MICBYHND,
                    'PSH_MICNAME2' => $MICNAME2,
                    'PSH_MICFLG1' => $MICFLG1,
                    'PSH_MICFLG2' => $MICFLG2,
                    'PSH_MICCOD1' => $MICCOD1,
                    'PSH_MICCOD2' => $MICCOD2,
                    'PSH_MICRMK2' => $MICRMK2,
                    'PSH_MICRML3' => $MICRML3,
                    'PSH_MICNUM1' => $MICNUM1,
                    'PSH_MICNUM2' => $MICNUM2,
                    'PSH_MICPRDU' => $MICPRDU,
                    'PSH_MICREMK' => $MICREMK,
                    'PSH_MICGNDR' => $MICGNDR,
                    'PSH_MICMPNO' => $MICMPNO,
                    'PSH_MICDOBT' => $MICDOBT,
                    'PSH_MICNICN' => $MICNICN,
                    'PSH_TRN_DATE' => $print_date,
                    'PSH_USER_ACT_NAME' => $print_user,
                    'PSH_B_STATE' => $branch_state,
                    'PSH_BRANCH_CODE' => $branch_code,
                    'PSH_REGION_CODE' => $region_code,
                    'PSH_ZONE_CODE' => $zone_code,
                    'PSH_REG_CODE' => $new_region,
                    'PSH_NEW_BR_CODE' => $post_branch_id,
                    'PSH_NEW_REG_CODE' => $new_region,
                    'PSH_NEW_ZONE_CODE' => $new_zone_code
                );


                $result = $this->db->insert('buy_hand_psh_data', $data3);
                $g++;
            }
        }

        $this->db->where('MIC_ID', $MIC_ID);


        $arr = array('MICBYHND' => 'B',
            'MICBRCD' => $post_branch_id,
            'MICREGN' => $new_region,
            'MICZONE' => $new_zone_code);



        $result = $this->db->update('daily_policy', $arr);
        //echo $str = $this->db->last_query();  
        return $result; //->db->affected_rows();
    }

    public function serach_alter_card_count_at_head_office($branch_state, $branch_code, $region_code, $zone_code, $search_txt) {

        $sql = "";



        if ($branch_state == 'B') {

            /* $sql .= "SELECT MICPLNO FROM daily_policy WHERE MICREFNO=0 AND ( MICMNFL<>'M'  OR ";

              //$sql .= " MICBYHND<>'B' ) AND  ";



              //$sql .= " MICPLNO LIKE '$search_txt%'  OR MICVENO LIKE '$search_txt%'   GROUP BY MIC_ID  "; */

            $sql .= "SELECT MICPLNO FROM daily_policy  
			           WHERE ( MICREFNO=0 AND MICCITY='Y' AND MICBYHND='') OR ( MICREFNO=0 AND MICCITY='N' AND MICBYHND='') 
					   AND  MICMNFL<>'M' ";

            if ($search_txt != "") {

                $sql .= " AND MICPLNO LIKE '%$search_txt%'  OR MICVENO LIKE '$search_txt%'   GROUP BY MIC_ID  ";
            }
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {

            return 0;
        }

        $this->load->database()->close();
    }

    public function serach_alter_card_display($branch_state, $branch_code, $region_code, $zone_code, $search_txt, $start, $per_page) {

        $sql = "";

        if ($branch_state == 'B') {

            /* $sql .= "SELECT MICPLNO,MICVENO,MICSPNO,MIC_ID,MICPCOV,MICRNCT,MICENCT,MICTITL,MICNAME
              FROM daily_policy WHERE MICREFNO=0 AND ( MICMNFL<>'M'  OR ";

              $sql .= " MICBYHND<>'B' ) AND  ";



              $sql .= " MICPLNO LIKE '$search_txt%'  OR MICVENO LIKE '$search_txt%'   GROUP BY MIC_ID  ";

             */

            $sql .= " SELECT MICPLNO,MICVENO,MICSPNO,MIC_ID,MICPCOV,MICRNCT,MICENCT,MICTITL,MICNAME
			           FROM daily_policy  
			           WHERE ( MICREFNO=0 AND MICCITY='Y' AND MICBYHND='') OR ( MICREFNO=0 AND MICCITY='N' AND MICBYHND='') 
					   AND  MICMNFL<>'M' ";

            if ($search_txt != "") {

                $sql .= " AND MICPLNO LIKE '%$search_txt%'  OR MICVENO LIKE '$search_txt%'   GROUP BY MIC_ID  ";
            }



            $sql.=" LIMIT $start, $per_page";
        }


        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {

            return 0;
        }

        ////$this->load->database()->close();
    }

}

?>
