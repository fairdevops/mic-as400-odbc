<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{TITLE}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en-us" />
<style type="text/css" media="screen">
	
	body {
		margin: 0px;
		padding: 0px;
	}
	#templatemo_body_wrapper {
	width: 100%;
	background: url(<?php echo base_url(); ?>assets/images/templatmeo_body.png) top center no-repeat
}

	#BodyImposter {
		color: #666;
		background: #fff;
		width: 100%;
		margin: 0px;
		padding: 0px;
	}
	#BodyImposter dd,
	#BodyImposter dt,
	#BodyImposter li,
	#BodyImposter p,
	#BodyImposter td {
		font-size: 12px;
		line-height: 1.5em;
		font-family: Helvetica, Arial, sans-serif;
	}
	#BodyImposter dl,
	#BodyImposter p,
	#BodyImposter ol,
	#BodyImposter ul {
		margin: 0em 0em 1em 0em;
	}
	#BodyImposter img {
		border: 0px;
	}
	#BodyImposter small {
		font-size: 10px;
		line-height: 1.3em;
	}
	#BodyImposter ol {
		padding-left: 1em;
	}
	#Primary ul {
		padding-left: 1em;
		padding-right: 1em;
	}
	#BodyImposter ol li {
		margin: 0px;
		padding: 0px;
	}
	#BodyImposter ul li {
		background: #666666; /*ur l("{SITE-URL}/images/bullet.gif") no-repeat 0em 0.5em;*/
		padding: 0px 0px 0px 8px;
		margin: 0px;
		list-style: none;
	}
	
	/* links
	--------------------------------------------------*/
	
	#BodyImposter a {
		text-decoration: none;
	}
	#Primary a:link,
	#Primary a:visited,
	#Secondary a:link,
	#Secondary a:visited {
		color: #000;
		background: #fff;
	}
	#BodyImposter h2 a:link,
	#BodyImposter h2 a:visited {
		color: #000;
		background: #fff;
		text-decoration: none;
	}
	#Helpful a:link,
	#Helpful a:visited {
		color: #c2c2c2;
		background: #fff;
	}
	#Legal a:link,
	#Legal a:visited {
		color: #999;
		background: #ccc;
	}
	#BodyImposter a:hover {
		text-decoration: none;
	}
	#BodyImposter h2 a:hover {
		text-decoration: underline;
	}
	#BodyImposter a:active {
		color: #000;
		background: #fff;
		text-decoration: none;
	}
	#Legal a:active {
		color: #000;
		background: #ccc;
		text-decoration: none;
	}
	
	/* heads
	--------------------------------------------------*/
	
	#BodyImposter h1,
	#BodyImposter h2 {
		color: #000;
		background: #fff;
		font-family: Helvetica, Arial, sans-serif;
		font-weight: bold;
		line-height: normal;
		margin: 0.75em 0em;
		padding: 0px;
	}
	#Masthead h1 {
		font-size: 28px;
		color: #fff;
		background: transparent;
	}
	#Primary h2 {
		font-size: 18px;
	}
	#Tertiary h2 {
		font-size: 14px;
	}
	
	/* boxes
	--------------------------------------------------*/
	
	#Box {
		width: 100%;
		text-align: center;
	}
	#Content {
		margin: 0px auto;
		text-align: left;
	}
	#Content td#Primary {
		width: 550px;
	}
	
	#Helpful table {
		color: #c2c2c2;
		background: #fff;
		margin: 0px auto;
	}
	#Helpful p {
		width: 550px;
		margin: 0px auto;
		padding: 0px 20px;
		text-align: left;
	}
	#Legal {
		padding: 20px 0px;
	}
	#Legal table {
		color: #999;
		background: #ccc;
		margin: 0px auto;
	}
	#Legal p {
		width: 550px;
		padding: 0px 20px;
		text-align: left;
	}
	#Masthead {
		padding: 0px 0px;
	}
	#Masthead table {
		color: #fff;
		background: #DA7C0C;
		margin: 0px auto;
	}
	#Masthead h1,
	#Masthead p {
		margin: 0px auto;
		padding: 0px 20px;
		text-align: left;
	}
	#Masthead p {
		font-size: 14px;
	}
	#Masthead p #Date {
		float: right;
	}
	#Tertiary {
		padding: 20px 0px;
	}
	#Tertiary table {
		color: #d69898;
		background: #000;
		margin: 0px auto;
	}
	#Tertiary h2 {
		color: #fff;
		background: #000;
		padding: 0em 0em 0em 0em;
	}
	#Tertiary h2,
	#Tertiary p {
		margin: 0px auto;
		padding: 0px 20px;
		text-align: left;
	}
	#Helpful,
	#Legal,
	#Masthead,
	#Tertiary {
		width: 100%;
		text-align: center;
	}

	
.cleaner {clear: both }
#templatemo_footer {	clear: both;
	width: 940px;
	padding: 10px 0; 
	color: #000;
	text-align: center;
	margin: 0 auto;
}
#templatemo_footer_wrapper {	clear: both;
	width: 100%;
	background: #a3a3a3;
}
</style>
</head>

<body>
<div id="templatemo_body_wrapper">
<div id="BodyImposter">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="45%">&nbsp;</td>
      <td width="35%"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" style="font-family:Helvetica, Arial, sans-serif; color: #000; font-size:20px; ">&nbsp;</td>
        </tr>
      </table></td>
        <td width="20%">&nbsp;</td>
      </tr>
    </table>
	<table  style="color: #fff; background-color:#DA7C0C;" cellspacing="0" cellpadding="15" width="102%">
		<tr>
		  <td height="83" id="Masthead" style="color: #fff; background-color:#DA7C0C;"><table width="692"  border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="right"><div align="left">TEST SAMPLE FORMAT</div></td>
            </tr>
          </table>
	      </td>
	  </tr>
	</table>
	<div id="Box">
		<p>&nbsp;</p>
		<p>&nbsp;</p> <div id="dd" style="padding-left:206px">
	  <p>&nbsp;</p>
	  <table id="Content" cellspacing="0" cellpadding="15" width="931"  align="left"  >
<tr>
				<td width="899" id="Primary" style="font-family: Helvetica, Arial, sans-serif; font-size:12px">
				  <h2>Dear <?php echo $Name;?></h2>

<?php echo $CONTENT;?></td>
	    </tr>
		</table></div>
  </div>
  <p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p><br>
                </p>
</div>
<div id="templatemo_footer_wrapper">
  <div id="templatemo_footer"><strong>All rights reserved by Union Assurance PLC © 2013 </strong>
      <div class="cleaner"></div>
  </div>
</div>
</div>
</body>
</html>