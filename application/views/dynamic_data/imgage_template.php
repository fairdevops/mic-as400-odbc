<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
Author: Reality Software
Website: http://www.realitysoftware.ca
Note: This is a free template released under the Creative Commons Attribution 3.0 license, 
which means you can use it in any way you want provided you keep the link to the author intact.
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style>
body { margin:0; padding:0; font-size:11px; line-height:16px; font-family: Arial, Tahoma;}
a { color:#fe5800;}
a:hover { text-decoration:none;}
.orange { color:#fe5800;}

#header { background: url(<?php echo base_url(); ?>assets/images/tpl_images/header_bckg.gif) repeat-x ; height:120px;  }
#logo { width:780px; margin:0 auto; padding:50px 0 0 20px; }
#logo a{ color:#FFFFFF; text-decoration:none; font-weight:bold; height:12px; font-size:20px; }
#logo a:hover { }
#menu {color:#fff; text-align:center; margin-top:22px;}
#menu ul{ width:780px; margin:0 auto;list-style:none; padding:0; text-align:left;}
#menu ul li{display:inline}
#menu ul a { display:block; float:left; font-weight:bold; font-size:13px; text-decoration:none; color:#fff; background:url(<?php echo base_url(); ?>assets/images/tpl_images/menu_enactive.gif) no-repeat bottom center; padding:8px 10px; width:118px; text-align:center; }
#menu ul a:hover { background:url(<?php echo base_url(); ?>assets/images/tpl_images/menu_over.gif) no-repeat bottom center; color:#232323;}
#menu ul a.active { background:url(<?php echo base_url(); ?>assets/images/tpl_images/menu_active.gif) no-repeat bottom center; color:#232323;}
#menu ul a.active:hover { background:url(images/menu_active.gif) no-repeat bottom center; color:#232323;}

#main { width:100%; margin:20px 0 20px 0; float:left;}
#head_image { height:150px; background:url(<?php echo base_url(); ?>assets/images/tpl_images/head_bckg.jpg) no-repeat #fe5d00; padding:30px 0 30px 50px; line-height:24px;}
#slogan { color:#fff; font-family: "Arial Narrow", Arial, Tahoma; font-size:21px;  }
#under_slogan_text { color:#ffff00; font-size:10px; line-height:12px; padding-top:15px;}
#content { margin:0 auto; padding:0; width:780px;}
#content h1 { margin:20px 0 0 0;  font-size:20px; color:#000; font-weight:normal;}
#text {float:left; width:760px; padding-left:20px;}
#sidebar { background:url(<?php echo base_url(); ?>assets/images/tpl_images/sidebar_bckg.gif) repeat-x; margin:20px 0 0 560px; color:#6f6e6e; padding:20px; }
#sidebar h2 {color:#fe5800; margin:0 0 15px 0; font-size:14px; font-weight:bold; }

#footer {background:url(<?php echo base_url(); ?>assets/images/tpl_images/footer_bckg.gif) repeat-x #3f3e3e 0 2px; height:94px; margin-top:20px; clear:both;}
#left_footer { float:left; padding:40px 0 0 30px;  color:#FFFFFF; font-weight:bold; font-size:11px;}
#left_footer a { color:#FFFFFF;}
#left_footer a:hover { text-decoration:none;}
#right_footer { float:right;  padding:40px 30px 0 0;  color:#FFFFFF; font-weight:bold; font-size:11px; text-align:right;}
#right_footer a { color:#FFFFFF;}
#right_footer a:hover { text-decoration:none;}
</style>
</head>
<body>
<div id="container">
	<!-- header -->
    <div id="header">
    	<div id="logo"><a href="http://www.unionassurace.lk/"><img src="<?php echo base_url(); ?>assets/images/logo.gif" alt="" /></a><a href="#">ame</a></div>
        <div id="menu">
        	<ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            </ul>
        </div>
    </div>
    <!--end header -->
    <!-- main -->
    <div id="main">
        <div id="content">
        <div id="head_image">
        	<div id="slogan"><strong>TEST TEMPLATE ONLY </strong></div>
          <div id="under_slogan_text"></div>
        </div>
        <div id="text">
          <h1>Dear  <?php echo $Name;?> </h1> 
                <p><b><?php echo $CONTENT;?></p>
          		<h1>&nbsp;</h1>
                <p>This is only a test teamplete created by this application developer .Application must run on PHP5/MYSQL 5 and built on codeigniter 2.02 frame work .This template can be used for testing purposes only., </p>
          </div>
        </div>
    </div>
    <!-- end main -->
    <!-- footer -->
    <div id="footer">
    <div id="left_footer"><strong>All rights reserved by Union Assurance PLC © 2013 </strong></div>
    </div>
    <!-- end footer -->
</div>
</body>
</html>
