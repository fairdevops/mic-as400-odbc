<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style>
* {
	margin:0;
	padding:0;
}
body  {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	background:#FFFFFF;
	color:#515151;
}
img {
	border:0;
}
h1 {
	color:#dd8551;
	font-size:24px;
	font-family:tahoma, arial;
	font-weight:bold;
	padding:0 0 25px 0;
}
h2 {
	color:#dd8551;
	font-size:24px;
	font-family:tahoma, arial;
	padding:0 0 25px 0;
}
h3 {
	color:#dd8551;
	font-size:24px;
	font-family:tahoma, arial;
	padding:0 0 5px 0;
}
.aboutUs {
	background:#ececec;	
	padding:40px;
}
.boxes {
	padding:5px 0;
}
.box {
	float:left;
	width:273px;
	padding:0 25px 20px 25px;
}
.boxButton {
	float:right;
}
.box p {
	padding:10px 0;
}
.buttonStyle {
	border:0;
	cursor:pointer;
	font-size:14px;
	color:#ffffff;
	background:#dd8551;
	padding:2px 7px 2px 7px;
}
.icons {
	float:right;
	background:#FFFFFF;
	margin:4px 3px 0px 3px;
	padding:40px 0 0 0;
}
.icons li {
	list-style:none;
	display:inline;
}
a {
	color:#dd8551;
	text-decoration:underline;
}
a:hover {
	color:#dd8551;
	text-decoration:none;
}
#container { 
	width: 980px;  
	margin: 0 auto;
} 
#header { 
}
.menu {
	background:#0b0b08;
	padding:15px 0;
}
.menu li {
	list-style:none;
	display:inline;
	padding:5px 25px 5px 25px;
	border-right:1px solid #787877;
}
.menu a {
	color:#bdbdbd;
	font-size:12px;
	text-decoration:none;
}
.menu a:hover {
	color:#ffffff;
	font-size:12px;
	text-decoration:none;
}
.menu #active a {
	color:#ffffff;
}
.logoContainer {
	float:left;
	padding:30px 0;
}
.logo {
}
.slogan {
	color:#747474;
	font-size:12px;
}
.headerPicContainer {
	padding:25px 0;
	background: #FFFFFF;
	height:313px;
	width:980px;
}
.headerText1 {
	font-size:48px;
	color:#dd8551;
	padding:50px 0 0 50px;
}
.headerText2 {
	font-size:18px;
	color:#0b0b08;
	padding:0 0 0 50px;
}
.headerPic {
	float:left;
	padding:0 20px 0 0;
}
.headerPicBody {
} 
.welcome {
	padding:20px 0;
}
.picRight {
	float:right;
	padding:0 0 15px 20px;
}
#footer {
	color:#515151;
	padding:30px 0;
	font-size:11px; 
}
#footer p {
	text-align:center;
} 
#footer a {
	color:#dd8551;
	text-decoration:underline;
	font-size:11px;
}
#footer a:hover {
	color:#dd8551;
	text-decoration:none;
	font-size:11px;
}
.clearfloat { /* this class should be placed on a div or break element and should be the final element before the close of a container that should fully contain a float */
	clear:both;
    height:0;
    font-size: 1px;
    line-height: 0px;
}
</style>
</head>
<body>
<!-- begin #container -->
<div id="container">
	<!-- begin #header -->
    <div id="header">
    	<p>&nbsp;</p>
    	<div class="logoContainer">
        	<div><a href="http://www.unionassurace.lk/"><img src="<?php echo base_url(); ?>assets/images/logo.gif" alt="" /></a></div>
      </div>
   	  <div class="clearfloat"></div>
        </div>
    <!-- end #header -->
  <div class="clearfloat"></div>
    <div class="clearfloat"></div>
    <div class="welcome">
    	<h1>Dear <?php echo $Name;?></h1>
  <p><span style="font-family: Helvetica, Arial, sans-serif; font-size:12px"><?php echo $CONTENT;?></span></p>
  </div>
<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />
    <!-- begin #footer -->
    <div id="footer" align="left">
		<p>
        	<!---Terms of Use | <a title="This page validates as XHTML 1.0 Strict" href="http://validator.w3.org/check/referer" class="footerLink"><abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a> | 
        <a title="This page validates as CSS" href="http://jigsaw.w3.org/css-validator/check/referer" class="footerLink"><abbr title="Cascading Style Sheets">CSS</abbr></a>  |  Copyright &copy; Security Company . Design by <a href="http://www.flash-templates-today.com">Free Flash Templates</a><br />Desing Downloaded From <a href="http://www.template4all.com">Free Website Templates</a> | <a href="http://www.freethemes4all.com">Free CSS Templates</a> | <a href="http://www.seodesign.us">Free Backgrouds</a>.
        </p>-->
            <strong>All rights reserved by Union Assurance PLC © 2013 </strong></div>
  <!-- end #footer -->
</div>
<!-- end #container -->
</body>
</html>
