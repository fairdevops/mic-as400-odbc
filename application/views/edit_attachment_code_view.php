<?php $this->load->view('main_page/header_view'); 

   if ($this->input->post('BtnSave')) {

		if ($this->input->post('txt_attach_code')) {
			$txt_attach_code = strtoupper($this->input->post('txt_attach_code'));
		}
	
		if ($this->input->post('txt_description')) {
			$txt_description = $this->input->post('txt_description');
		}
	
    }
	
 else 
    {
				if ($this->input->post('txt_attach_code')) {
				
				 $txt_attach_code = strtoupper($this->input->post('txt_attach_code'));
				
				}
				else
					{ 
					$txt_attach_code = $rs_attchment_infor['ATTACH_CODE'];
					
					}
				
			
				if ($this->input->post('txt_description')) {
				
				    $txt_description = strtoupper($this->input->post('txt_description'));
				
				}
				else
					{ 
				    
					$txt_description = $rs_attchment_infor['ATT_DESC'];
						
					}
				
				
				
				
    }




?>



<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Edit Attachment Information</h3>
      <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:418px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:345px;"> <span class="rounded_notopgap"></span><br class="clear" />

                        <?php echo form_open_multipart('email_shedule_configure/edit_attachment/'.$this->uri->segment(3)); ?><br /><br />

                        <table width="66%" border="0" align="center" cellpadding="5" cellspacing="0" style="padding-top:14px;">

                            <tr>
                                <td width="26%">Attachment Code <span class="warning">*</span></td>
                                <td width="74%"><?php echo form_input(array('id' => 'txt_attach_code', 'name' => 'txt_attach_code', 'maxlength' => '3', 'class' => 'text-input', 'style' => 'width:50%', 'value' => $txt_attach_code)); ?>
                                <?php echo form_error('txt_attach_code'); ?></tr>
                            <tr>

                                <td>Short Description <span class="warning">*</span></td>
                                <td><?php echo form_input(array('id' => 'txt_description', 'name' => 'txt_description', 'maxlength' => '30', 'class' => 'text-input', 'style' => 'width:50%', 'value' => $txt_description)); ?> <?php echo form_error('txt_description'); ?></td>
                            </tr>

                            <tr>
                                <td>Upload Attachment <span class="warning">*</span></td>
                                <td>
                                    <div align="left">
                                        <input type="file" name="userfile" id="userfile" />                                   
                                    </div>                                </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td><span class="warning">*</span> Denotes required Fields</td>
                              <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td><input type="button" onclick="location.replace('<?php echo base_url(); ?>index.php/email_shedule_configure/manage_attachments');" class="button button-orange" value="Back" id="btnCancel" name="btnCancel" />
                                    <input name="btnCancel4" type="button" id="btnCancel5"  value="Reset" class="button button-orange" />

                                    <input  type="submit" id="BtnSave" name="BtnSave"  value="Submit" class="button button-orange" />                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td><div align="left"><?php echo $edit_upload_msg; ?></div></td>
                            </tr>
                        </table>

              <?php echo form_close(); ?>

                        <!-- Pagination -->    
                        <div class="paging"><?php //echo $pagination;  ?></div>

                        <span class="rounded_nobottomgap">&nbsp;</span> </div>   
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('main_page/footer_view'); ?>