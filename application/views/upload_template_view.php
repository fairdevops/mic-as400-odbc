<?php

  $this->load->view('main_page/header_view');
  $t=0;
  
  

?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/javascripts/css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/javascripts/css/style_multi_select" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/javascripts/css/prettify.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/prettify.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery.multiselect.js"></script>

<script language="javascript">


            $(document).ready(function(){
			
			      $("#parameters").multiselect();	
				  
			      var P=$("#notification_type").val(); 
				  var Y =P.substr(0,3);
				  
				  $('#notification_code').val(Y);
	
 
             
		
                $("#notification_type").change( function() {
		
                  
				  
				  
				  var notification_type=$("#notification_type").val(); 
				  var T =notification_type.substr(0,3);
				  
				  $('#notification_code').val(T);
				  
		

			
                });
				
				    	      //$("#btnBack").click(function(){ notification_code
		  
		                           //$(location).attr('href','<?php //echo base_url();?>index.php/admin/manage_assign_jobs');
		  
		                        //});

            

    
            });



</script>
<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Upload Template</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:488px;">
                <div id="columnSingle" style="padding-top:3px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:455px;"> <span class="rounded_notopgap"></span><br class="clear" />
                    <?php //$attr=array('name' => 'my_form', 'id' => 'my_form');
						 echo form_open_multipart('email_shedule_configure/upload_template'); ?>
                        
                            <p>&nbsp;</p>
                            <table width="76%" border="0" align="center" cellpadding="5" cellspacing="0">

                                <tr>
                                    <td width="29%"> Selecct Notification Type  <span class="warning">*</span> </td>
                                          <td colspan="5">
                                                    <select name="notification_type" id="notification_type">
                                                        <option value="">-------------SELECT----------</option>
                                                                <?php foreach($rs_notifiction_type as $row ){ $code=$row['NOTYPE'].'-'.$row['ID'];?>
                                                          <option value="<?php echo $code;?>"<?php if ($notification_type==$code){ ?>selected="selected"<?php }?>><?php echo $row['NODESC'];?></option>
                                                                <?php }?>
                                                    </select> 
                                                    <?php echo form_error('notification_type'); ?> 
                                                    <span class="warning">*</span> Denotes required Fields</td>
                              </tr>
                              
                                <tr>
                                    <td>Notification Code</td>
                                    <td colspan="5">
									                <?php echo form_input(array('id' => 'notification_code', 'name' => 'notification_code','readonly'=>TRUE, 'maxlength' => '3', 'class' => 'text-input', 'style' => 'width:15%')); ?>                                    </td>
                               </tr>
                                
                                <tr>
                                    <td>Attachment Allow  <span class="warning">*</span> </td>
                                    <td colspan="5">
													<select name="attachment_state" id="attachment_state">
                                                      
                                                              <option value="">-------------SELECT----------</option>
                                                              
                                                               <option value="N"<?php if ($attachment_state=='N'){ ?>selected="selected"<?php }?>>Not Allow</option>
                                                             
                                                                <?php if ($count_attachment_types > 0 ) {?>
                                                                  
                                                                  <?php foreach ( $rs_attachment_codes as $row ) { ?>
                                                                
                                                                     <option value="<?php echo $row['ID'];?>"<?php if ($attachment_state==$row['ID']){ ?>selected="selected"<?php }?>><?php echo $row['ATTACH_CODE'];?></option>
                                                                     
                                                                  <?php }?>   
                                                                
                                                                
                                                                <?php } ?>
                                                    </select>
                                                   <?php echo form_error('attachment_state'); ?>                                  </td>
                              </tr>
                                
                                <tr>
                                    <td>Upload Template  <span class="warning">*</span> </td>
                                    <td colspan="5"><div align="left"><div align="left"><input type="file" name="userfile" id="userfile" /></div></div></td>
                                </tr>
                                
                                <tr>
                                  <td>Parameters assign in Template <span class="warning">*</span></td>
                                  <td width="17%">
                                                  <input type="checkbox" name="PPLNO" id="PPLNO"   value="A"<?php if ($PPLNO == "A") echo "checked=\"checked\""; ?>  />PPLNO                                   </td>
                                  
                                  <td width="16%">
                                                 <input type="checkbox" name="CTITLE" id="CTITLE"   value="B"<?php if ($CTITLE == "B") echo "checked=\"checked\""; ?> >CTITLE                                  </td>
                                  
                                 <td width="14%">
                                                <input type="checkbox" name="CNAME" id="CNAME"   value="C"<?php if ($CNAME == "C") echo "checked=\"checked\""; ?> />
                                   CNAME                    </td>
                                
                                 <td width="13%"><input type="checkbox" name="MOBILE" id="MOBILE"   value="D"<?php if ($MOBILE == "D") echo "checked=\"checked\""; ?> />
                                  MOBILE</td>
                                <td width="11%"><input type="checkbox" name="EMAIL" id="EMAIL"   value="E"<?php if ($EMAIL == "E") echo "checked=\"checked\""; ?> />
                                  EMAIL </td>
                              </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td><input type="checkbox" name="CADD1" id="CADD1"  value="F"<?php if ($CADD1 == "F") echo "checked=\"checked\""; ?>  />
CADD1</td>
                                  <td><input type="checkbox" name="CADD2" id="CADD2"  value="G"<?php if ($CADD2 == "G") echo "checked=\"checked\""; ?>  />
CADD2</td>
                                  <td><input type="checkbox" name="CADD_3" id="CADD_3"  value="W"<?php if ($CADD_3 == "W") echo "checked=\"checked\""; ?>  />
CADD3</td>
                                  <td colspan="2"><input type="checkbox" name="CADD4" id="CADD4" value="I"<?php if ($CADD4 == "I") echo "checked=\"checked\""; ?> />
CADD4</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td><input type="checkbox" name="COMDATE" id="COMDATE" value="J"<?php if ($COMDATE == "J") echo "checked=\"checked\""; ?> />
                                  COMDATE</td>
                                  <td><input type="checkbox" name="MATDATE" id="MATDATE" value="K"<?php if ($MATDATE == "K") echo "checked=\"checked\""; ?>/>
                                  MATDATE</td>
                                  <td><input type="checkbox" name="RPDATE" id="RPDATE"  value="L"<?php if ($RPDATE == "L") echo "checked=\"checked\""; ?>/>
                                  RPDATE</td>
                                  <td colspan="2"><input type="checkbox" name="NTDATE" value="M"<?php if ($NTDATE == "M") echo "checked=\"checked\""; ?> id="NTDATE" />
                                  NTDATE</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td><input type="checkbox" name="PYAMT" id="PYAMT" value="NE"<?php if ($PYAMT == "NE") echo "checked=\"checked\""; ?> />
                                  PYAMT</td>
                                  <td><input type="checkbox" name="OUTCAP" id="OUTCAP" value="O"<?php if ($OUTCAP == "O") echo "checked=\"checked\""; ?> />
                                  OUTCAP</td>
                                  <td><input type="checkbox" name="OUTINT" id="OUTINT" value="P"<?php if ($OUTINT == "P") echo "checked=\"checked\""; ?> />
                                  OUTINT</td>
                                  <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="5">&nbsp;</td>
                              </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="5"><input type="button" onclick="location.replace('<?php echo base_url(); ?>index.php/email_shedule_configure/list_templates');" class="button button-orange" value="Back" id="btnCancel" name="btnCancel" />
                                    <input name="btnCancel4" type="button" id="btnCancel5"  value="Reset" class="button button-orange" />
                                    <input  type="submit" id="BtnSave" name="BtnSave"  value="Submit" class="button button-orange" /></td>
                                </tr>
                                
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5"><?php echo $upload_msg_state;?></td>
                                </tr>
                            </table>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        <?php echo form_close(); ?>
                        <!-- Pagination -->
                        <span class="rounded_nobottomgap">&nbsp;</span> </div>
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

 $this->load->view('main_page/footer_view');

?>