<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PCS</title>
        <!-- CSS for Tab Menu #3 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tabmenu/solidblocksmenu.css" />

        <!--	Manage Entire Layout	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/layout.css"/>
        <!--	Manage Entire container columns	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/container.css"/>
        <!--	Manage Entire navigation	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/navi.css"/>
        <!--	Manage Entire rounded corner box	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/round-corners.css"/>
        <!--	Manage Entire Form elemets roundecd corners	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/invalid.css"/>
        <!--	Manage Entire Form elemets	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/forms.css"/>
        <!--	Manage Entire Tables	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tables.css"/>
        <!--	Manage Entire GD	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/datagrids.css"/>
        <!--	Manage Entire Pegination	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/pegination.css"/>
        <!--	Manage Entire Messages	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/messages.css"/>
        <!--	General styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/general.css"/>
        <!--	ToolTip styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/toolTip.css"/>
        <!--	Rounded Holder styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/mainBodyWrapper.css"/>
        <!--	TAB styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tabs.css"/>
        <!--	ACCORDIAN styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/accordian.css"/>
        <!--	Auto Complet	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/jquery-ui.css"/>
        <!--	jquery temp 	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/jquery-ui-1.8.20.custom.css"/>
        <!--	jquery Tooltip 	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/colortip-1.0-jquery.css"/>

        <!--	Latest features are enhanced	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/html5.js"></script>
        <!--	jQuery Libraray	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery.tools.min.js"></script>
        <!--script type="text/javascript" src="< ?php echo base_url(); ?>assets/javascripts/global.js"></script-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/hoverIntent.js"></script>
        <!--	Tool tip library	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/toolTip.js"></script>
        <!--	Collapse and Expand	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/animatedcollapse.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/ddtabmenu.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/scw.js"></script>
        <!-- Auto Complet -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-ui.min.js"></script>
        <!-- jquery temp -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.20.custom.min.js"></script>
        <!-- jquery Tooltip -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/colortip-1.0-jquery.js"></script>
        
        <script type="text/javascript">
		

            animatedcollapse.addDiv('jason', 'fade=1,height=110px')
            animatedcollapse.addDiv('kelly', 'fade=1,height=110px')

            animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
                //$: Access to jQuery
                //divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
                //state: "block" or "none", depending on state
            }

            animatedcollapse.init()

        </script>
        <!--	Manage Entire navigation	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/superfish.js"></script>
        <script type="text/javascript">
            // initialise plugins
            //jQuery(function(){
                //jQuery('ul.sf-menu').superfish();
            //});
        </script>

    </head>
    <body id="top">
        <div id="pageWrapper">
            <div class="wrapper">
                <div style="padding:5px;"><img src="<?php echo base_url(); ?>assets/images/logo.gif" alt="" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+3">Petty Cash System</font></div>
                <br class="clear" />
            </div>
            <div id="topmenu"><!-- start top menu-->
                <?php include("topmenu.php"); ?>
            </div><!-- end top menu-->