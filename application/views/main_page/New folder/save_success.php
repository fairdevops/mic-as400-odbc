<div class="clear"></div>
<div class="wrapper">
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Search User</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header">
                <div id="columnSingle">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"> <span class="rounded_notopgap"></span><br class="clear" />

<?php if($this->session->userdata('user_level') == "admin" && $this->session->userdata('super_admin') == "super_admin_GM" && $this->session->userdata('go_back_user_pc_list') != "go_back_user_pc_list") {?>
	<meta http-equiv="refresh" content="1;url=<?php echo base_url(); ?>/index.php/Pc_controller/my_action_gm_load_pc_list">
<?php } else if($this->session->userdata('user_level') == "admin" && $this->session->userdata('go_back_user_pc_list') == "go_back_user_pc_list") { ?>
	<meta http-equiv="refresh" content="1;url=<?php echo base_url(); ?>/index.php/Pc_controller/user_load_pc_list">
<?php } else if($this->session->userdata('user_level') == "admin") { ?>
	<meta http-equiv="refresh" content="1;url=<?php echo base_url(); ?>/index.php/Pc_controller/my_action_admin_load_pc_list">
<?php } else if($this->session->userdata('user_level') == "user"){ ?>
	<meta http-equiv="refresh" content="1;url=<?php echo base_url(); ?>/index.php/Pc_controller/user_load_pc_list">
<?php } ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/policy_box.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/template.css" type="text/css">

<div id="stylesheetTest"></div>
    <div id="policy_box_main" class="policy_box_full">
        <div class="policy_box_header" id="policy_input_box" >
            <h3><span style="padding: 0.6em 0 0 10px;"></span></h3>
            <div  id="policy_input_box_content" >
                <div style="padding-left: 5px; padding-right: 5px;">
                    <div id="msg_display" class="message success">
                    	<h3 id="msg_header">Success</h3>
                        
                        <?php if($this->session->userdata('go_back_user_pc_list') == "go_back_user_pc_list"){ ?>
                    		<p id="msg_body"> Record Successfuly Delete. </p>
                        <?php } else { ?>
                        	<p id="msg_body"> Record Successfuly Saved. </p>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>    
            <h2><span>&nbsp;</span></h2> 
        </div>
</div>

<span class="rounded_nobottomgap">&nbsp;</span> </div>   
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>
