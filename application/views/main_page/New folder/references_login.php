<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PCS</title>
        <!-- CSS for Tab Menu #3 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tabmenu/solidblocksmenu.css" />

        <!--	Manage Entire Layout	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/layout.css"/>
        <!--	Manage Entire container columns	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/container.css"/>
        <!--	Manage Entire navigation	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/navi.css"/>
        <!--	Manage Entire rounded corner box	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/round-corners.css"/>
        <!--	Manage Entire Form elemets roundecd corners	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/invalid.css"/>
        <!--	Manage Entire Form elemets	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/forms.css"/>
        <!--	Manage Entire Tables	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tables.css"/>
        <!--	Manage Entire GD	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/datagrids.css"/>
        <!--	Manage Entire Pegination	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/pegination.css"/>
        <!--	Manage Entire Messages	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/messages.css"/>
        <!--	General styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/general.css"/>
        <!--	ToolTip styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/toolTip.css"/>
        <!--	Rounded Holder styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/mainBodyWrapper.css"/>
        <!--	TAB styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tabs.css"/>
        <!--	ACCORDIAN styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/accordian.css"/>

        <!--	Latest features are enhanced	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/html5.js"></script>
        <!--	jQuery Libraray	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery.tools.min.js"></script>
        <!--script type="text/javascript" src="< ?php echo base_url(); ?>assets/javascripts/global.js"></script-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/hoverIntent.js"></script>
        <!--	Tool tip library	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/toolTip.js"></script>
        <!--	Collapse and Expand	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/animatedcollapse.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/ddtabmenu.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/scw.js"></script>
        <script type="text/javascript">
		

            animatedcollapse.addDiv('jason', 'fade=1,height=110px')
            animatedcollapse.addDiv('kelly', 'fade=1,height=110px')

            animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
                //$: Access to jQuery
                //divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
                //state: "block" or "none", depending on state
            }

            animatedcollapse.init()

        </script>
        <!--	Manage Entire navigation	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/superfish.js"></script>
        <script type="text/javascript">
            // initialise plugins
            //jQuery(function(){
                //jQuery('ul.sf-menu').superfish();
            //});
        </script>

    </head>
    <body id="top">
        <div id="pageWrapper">
            <div class="wrapper">
                <div id="head"> <a href="#"><img src="<?php echo base_url(); ?>assets/images/logo.gif" alt="" /></a> </div>
                <br class="clear" />
            </div>
            