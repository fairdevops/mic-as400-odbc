<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Motoro Insurnace Card Printing System</title>
        <!-- CSS for Tab Menu #3 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tabmenu/solidblocksmenu.css" />

        <!--	Manage Entire Layout	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/layout.css"/>
        <!--	Manage Entire container columns	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/container.css"/>
        <!--	Manage Entire navigation	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/navi.css"/>
        <!--	Manage Entire rounded corner box	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/round-corners.css"/>
        <!--	Manage Entire Form elemets roundecd corners	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/invalid.css"/>
        <!--	Manage Entire Form elemets	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/forms.css"/>
        <!--	Manage Entire Tables	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tables.css"/>
        <!--	Manage Entire GD	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/datagrids.css"/>
        <!--	Manage Entire Pegination	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/pegination.css"/>
        <!--	Manage Entire Messages	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/messages.css"/>
        <!--	General styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/general.css"/>
        <!--	ToolTip styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/toolTip.css"/>
        <!--	Rounded Holder styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/mainBodyWrapper.css"/>
        <!--	TAB styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/tabs.css"/>
        <!--	ACCORDIAN styles	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/accordian.css"/>
        <!--	Auto Complet	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/jquery-ui.css"/>
        <!--	jquery temp 	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/jquery-ui-1.8.20.custom.css"/>
        <!--	jquery Tooltip 	-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/styles/colortip-1.0-jquery.css"/>

        <!--	Latest features are enhanced	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/html5.js"></script>
        <!--	jQuery Libraray	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery.tools.min.js"></script>
        <!--script type="text/javascript" src="< ?php echo base_url(); ?>assets/javascripts/global.js"></script-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/hoverIntent.js"></script>
        <!--	Tool tip library	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/toolTip.js"></script>
        <!--	Collapse and Expand	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/animatedcollapse.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/ddtabmenu.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/scw.js"></script>
        <!-- Auto Complet -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-ui.min.js"></script>
        <!-- jquery temp -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.20.custom.min.js"></script>
        <!-- jquery Tooltip -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/colortip-1.0-jquery.js"></script>
        
        <script type="text/javascript">
		

            animatedcollapse.addDiv('jason', 'fade=1,height=110px')
            animatedcollapse.addDiv('kelly', 'fade=1,height=110px')

            animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
                //$: Access to jQuery
                //divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
                //state: "block" or "none", depending on state
            }

            animatedcollapse.init()

        </script>
        <!--	Manage Entire navigation	-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/superfish.js"></script>
        <script type="text/javascript">
            // initialise plugins
            //jQuery(function(){
                //jQuery('ul.sf-menu').superfish();
            //});
        </script>

    </head>
    <body id="top">
        <div id="pageWrapper">
            <div class="wrapper">
                <div style="padding:5px;"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" width="100px" height="100px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="+2">Insurance Card Printing System</font></div>
              <br class="clear" />
            </div>
            <div id="topmenu"><!-- start top menu-->
                <?php if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'A')) {?>
                
				<?php include("topmenu_admin.php"); ?>
                
                <?php } else { ?>
                
                <?php if (($this->session->userdata('loguser_username') ) && ($this->session->userdata('loguser_level') == 'U')) {?>
                
                <?php include("topmenu_user.php"); ?>
                 
                <?php } else { 
				
				      include("topmenu_manager.php");
				
				} }?>
                
            </div><!-- end top menu-->


<!-- popup screen -->

<style type="text/css">
    #overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    filter:alpha(opacity=70);
    -moz-opacity:0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
    z-index: 100;
    display: none;
    }
    .cnt223 a{
    text-decoration: none;
    }
    .popup{
    width: 79%;
    margin: 0 auto;
    display: none;
    position: fixed;
    z-index: 101;
    }
    .cnt223{
    min-width: 600px;
    width: 600px;
    min-height: 150px;
    margin: 100px auto;
    background: #f3f3f3;
    position: relative;
    z-index: 103;
    padding: 15px 35px;
    border-radius: 5px;
    box-shadow: 0 2px 5px #000;
    }
    .cnt223 p{
    clear: both;
        color: #555555;
        /* text-align: justify; */
        font-size: 20px;
        font-family: sans-serif;
    }
    .cnt223 p a{
    color: #d91900;
    font-weight: bold;
    }
    .cnt223 .x{
    float: right;
    height: 35px;
    left: 22px;
    position: relative;
    top: -25px;
    width: 34px;
    }
    .cnt223 .x:hover{
    cursor: pointer;
    }
</style>
<script type='text/javascript'>
    $(function(){
    var overlay = $('<div id="overlay"></div>');
    overlay.show();
    overlay.appendTo(document.body);
    $('.popup').show();
    $('.close').click(function(){
    $('.popup').hide();
    overlay.appendTo(document.body).remove();
    return false;
    });
    
    
     
    
    $('.x').click(function(){
    $('.popup').hide();
    overlay.appendTo(document.body).remove();
    return false;
    });
    });
</script>
<div class='popup'>
    <div class='cnt223'>
        <h2 style="color:red;">Action Required!!! - Card Printer Desktop Icon Change</h2>
        <p>Please <b><u>delete old printer shortcut icon </u></b> and use new shortcut in the desktop to start your card print jobs.
            <br/>
            <br/>
            If you have already switched please ignore this message.
            <br/>
            <br/>
            <a href='' class='close'>I Understand</a>
        </p>
    </div>
</div>