
<script>
    $(document).ready(function() {
	
        var default_message_for_dialog = 'You are sure ? You want to Delete ?';
	
        $("#dialog").dialog({
            modal: true,
            bgiframe: true,
            width: 300,
            height: 200,
            autoOpen: false,
            title: 'Confirm'
        });

        // LINK
        $("a.confirm").click(function(link) {
            link.preventDefault();
            var theHREF = $(this).attr("href");
            var theREL = $(this).attr("rel");
            var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
            // set windows content
            $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');

            $("#dialog").dialog('option', 'buttons', {
                "Confirm" : function() {
                    window.location.href = theHREF;
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            });

            $("#dialog").dialog("open");

        });

	
        /*// FORMS
        $('input.confirm').click(function(theINPUT){
                theINPUT.preventDefault();
                var theFORM = $(theINPUT.target).closest("form");
                var theREL = $(this).attr("rel");
                var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
                var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
                $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');
                $("#dialog").dialog('option', 'buttons', {
                "Confirm" : function() {
                                        theFORM.submit();
                    },
                "Cancel" : function() {
                    $(this).dialog("close");
                    }
                });
                $("#dialog").dialog("open");
                });
         */	

    });
</script>

<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">View Debtor Codes</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:418px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:345px;"> <span class="rounded_notopgap"></span><br class="clear" />
                        
                         <div class="search" style="float:left; padding-bottom:2px; padding-top:15px; padding-left:212px;">

        <form name='search' action=<?php echo site_url('admin/debtor_name_search'); ?> method='post'>
            <a href="#"><img src="<?php echo base_url() ?>assets/images/search.png" name="ExSearch" width="28" height="28" border="0" align="texttop" id="ExSearch" /></a>
            &nbsp;&nbsp;
            Search Users &nbsp;&nbsp;
            <select name="search_type" id="search_type">

                <option value="0"<?php if ($search_type == 0) echo "selected=\"selected\""; ?>>---SELECT---</option>
                <option value="1"<?php if ($search_type == 1) echo "selected=\"selected\""; ?>>Debtor Code</option>

            </select>


            &nbsp;
            Search Text   &nbsp;&nbsp;
            <input name="seek_data" id="seek_data"  class="text-input" type='text' value='<?php  echo $seek_data; ?>' /> &nbsp;
            <input type='submit' name='search' value='Search' class="button button-orange" />
        </form>

    </div>

<?php echo form_open(''); ?>
                        <div align="center" style="padding-top:37px;">
                            <div id="dialog"></div>
                            <table border="0" cellspacing="1" cellpadding="1" width="96%">
                                <tr>
                                    <td >
                                        <div align="center">

<?php if ($rec_count > 0) { ?>

                                                <table border="0" cellspacing="1" cellpadding="1" width="58%">
                                                    <tr>
                                                        <td valign="top">
                                                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrapTable">
                                                                <tr align="center">
                                                                    <th width="21" >&nbsp;</th>
                                                                    <th nowrap="nowrap" width="214" class="cellLeftLine"><div align="left" style="padding-left:8px; padding-right:4px;">Debtor Code</div></th>
                                                                <th width="172" class="cellLeftLine">Delete</th>
                                                    </tr>
                                                    <?php
                                                    $no = $this->uri->segment(3) + 1;

                                                    foreach ($rs_email_type as $row) {

                                                        $color1 = "even";
                                                        $color2 = "add";
                                                        $now = $no + 1;
                                                        $row_color = ($now % 2) ? $color2 : $color1;

                                                        
                                                        $debtor_name = $row['debtor_name'];
                                                        $debtor_id = $row['debtor_id'];
                                                       
                                                        ?>

                                                        <tr onMouseOver="className='mouseOver';" onMouseOut="className='<?php echo $row_color; ?>';" class="<?php echo $row_color; ?>">

                                                            <td>
														<?php if ((strlen($no) < 2)) {
                                                            echo "0" . $no;
                                                        } else {
                                                            echo $no;
                                                        } ?>                                                           </td>

                                                            <td class="cellLeftLine"><div align="left" style="padding-left:2px;"><a href="<?php echo base_url(); ?>index.php/admin/edit_user/<?php echo $debtor_id; ?>" class="CLIKER_ID" ><?php echo trim($debtor_name); ?></a></div></td>
                                                            <td class="cellLeftLine"><div align="center"><a href="<?php echo base_url(); ?>index.php/admin/delete_debtor_code/<?php echo $debtor_id; ?>" class="confirm myButton" ><img src="<?php echo base_url() ?>assets/images/delete_r.png" height="24px" width="24px" border="0" /></a></div></td>
                                                        </tr>
														<?php $no++;
														
                                                                   } 
																   
                                                         ?>
                                                </table>

                                        </td>
                                    </tr>

                                </table>
                                <br />
                                <div align="left"><div id="dd3" style="padding-left:191px; padding-right:2px;" ><?php echo $this->pagination->create_links(); ?></div></div>  


<?php } else { ?><div class="my_close_note">No Records Found ..!</div><?php } ?>
                        </div>
                        </td>
                        </tr>
                        </table>                               
                    </div>
                    
                    <div align="center">
                        <input type="button" onclick="location.replace('assign_debtor_name')" class="button button-orange" value="Add New" id="btnCancel" name="btnCancel" />
                    </div>
                    <p><?php echo form_close(); ?></p>

                    <!-- Pagination -->    
                    <div class="paging"><?php //echo $pagination;  ?></div>

                    <span class="rounded_nobottomgap">&nbsp;</span> </div>   
                <div class="rounded_bottom-left"></div>
                <div class="rounded_bottom-right"></div>
                <!-- Button Set End -->
            </div>
            <br class="clear" />
        </div>
        <div class="box-level3b">
            <div class="box-level2b">
                <div class="box-level1b"></div>
            </div>
        </div>
    </div>
</div>
</div>