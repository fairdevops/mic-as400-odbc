<?php $this->load->view('main_page/header_view'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/error_handler.js"></script>
<script  type="text/javascript">

    function radioValue(rObj) {

        for (var i = 0; i < rObj.length; i++)
            if (rObj[i].checked)
                return rObj[i].value;

    }


    $(document).ready(function() {


        $("tr.display_region").hide();
        $("tr.display_zone").hide();
		


        $("input[name$='group_name']").click(function() {




            var radio_value = $(this).val();

            if (radio_value == 'B')
            {
                
                $("tr.display_branch").show();
                $("tr.display_region").hide();
                $("tr.display_zone").hide();

                //$("#list_location").val('B');
                //$("tr.display_gl_branch").show();
                //$("tr.display_region").hide();
                //$("tr.display_zone").show();
                // $("tr.display_department").hide();
                //$("tr.display_gl_branch").css({'background-color' : '#E9F8F8', 'font-weight' : 'bolder'});
                //$("tr.display_region").css({'background-color' : '#E9F8F8', 'font-weight' : 'bolder'});
                ////$("tr.display_zone").css({'background-color' : '#E9F8F8', 'font-weight' : 'bolder'});
                /// $("tr.display_issue_branch").css({'background-color' : '#E9F8F8', 'font-weight' : 'bolder'});
                ///noOfSubmits = 0;
            }
            else
            if (radio_value == 'R')
            {
                
                $("tr.display_branch").hide();
                $("tr.display_region").show();
                $("tr.display_zone").hide();

                //$("#list_location").val('D');
                //$("tr.display_gl_branch").hide();
                // $("tr.display_region").hide();
                // $("tr.display_zone").hide();
                // $("tr.display_department").show();
                //$("tr.display_gl_branch").css({'background-color' : '#E7FAD5', 'font-weight' : 'bolder'});
                // $("tr.display_department").css({'background-color'   : '#E7FAD5', 'font-weight' : 'bolder'});
                // $("tr.display_issue_branch").css({'background-color' : 'white', 'font-weight' : 'bolder'});
                //noOfSubmits = 0;
            }
            else
            {
               
                $("tr.display_branch").hide();
                $("tr.display_region").hide();
                $("tr.display_zone").show();



            }

        });

    });


    function add_user_Data() {

        jQuery("#flash").show();
        jQuery("#flash").html('<img src="<?php echo base_url(); ?>assets/images/ajax.gif" align="absmiddle">&nbsp;&nbsp;');
		
		
				if(document.myform.vonly.checked == true)
					{
					document.myform.vonly.value=1;
					} 
				else{
					document.myform.vonly.value=0;
					}
					
					
				
				if(document.myform.all_reports.checked == true)
					{
					document.myform.all_reports.value = 1;
					} 
				else{
					document.myform.all_reports.value = 0;
					}	
				
				if(document.myform.up_fleet_policy.checked == true)
					{
					document.myform.up_fleet_policy.value = 1;
					} 
				else{
					document.myform.up_fleet_policy.value = 0;
					}		
					
				
				if(document.myform.ponly.checked == true)
					{
					document.myform.ponly.value = 1;
					} 
				else{
					document.myform.ponly.value = 0;
					}	
					
				if(document.myform.search_allow.checked == true)
					{
					document.myform.search_allow.value  = 1;
					} 
				else{
					document.myform.search_allow.value  = 0;
					}	
					
				if(document.myform.all_nexus.checked == true)
					{
					document.myform.all_nexus.value  = 1;
					} 
				else{
					document.myform.all_nexus.value  = 0;
					}	
					
					
		
		var vo=document.getElementById("vonly").value;

		


        jQuery.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>index.php/admin/form_p1_q1_send_user_registration_data",
            data: jQuery("#myform").serialize(),
            beforeSend: function() {

                jQuery("#flash").show();
                jQuery("#flash").html('<img src="<?php echo base_url(); ?>assets/images/ajax.gif" align="absmiddle">&nbsp;&nbsp;');
            },
            dataType: "json",
            success: function(data) {
                if (data.my_msg) {
                    jQuery.each(data.my_msg, function(key, value) {
                        var jquery_id = '#' + key;
                        if (value == "")
                        {
                            jQuery(jquery_id).hide();

                        }
                        else
                        {

                            var value = value.substring(3, value.length - 4);
                            jQuery(jquery_id).show();
                            jQuery(jquery_id).html(value);
                            jQuery("#flash").hide();
                        }
                    });
                } else {
                    jQuery('.validation').hide();


                    if (data.result > 0) {

                        // jQuery('#BtnSave').removeClass("btn btn-primary"); 
                        //jQuery('#BtnSave').addClass("button button-gray"); 
                        //jQuery('#BtnSave').addClass("btn"); 

                        //jQuery('#BtnSave').attr("disabled", "disabled"); 


                        jQuery("alert_user_creation").show();
                        jQuery('#alert_user_creation').empty();


                        jQuery('#alert_user_creation').append('<div style="width:309px" class="validation sucess">User Successfully created..!</div>');



                        jQuery("#flash").hide();


                    }
                    else
                    {
                        error: Error_Handler;
                        jQuery('#alert_user_creation').append('<div style="width:309px;" class="validation fielderror">Error in user creation ..! </div>');
                    }


                }

            }, error: Error_Handler

        });




    }




</script>		   


<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Add New  User
            </h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:518px;">
                <div id="columnSingle" style="padding-top:12px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:466px;"> <span class="rounded_notopgap"></span><br class="clear" />
                        <!---
                        <div class="search" style="float:right; padding-bottom:2px;">
                        <?php //if ($if_user == "2"){ ?>
                            <form name='search' action=<?php //echo site_url('Pc_controller/admin_load_pc_list/');   ?> method='post'>
                        <?php //} //else if ($if_user == "3") { ?>
                                <form name='search' action=<?php //echo site_url('Pc_controller/gm_load_pc_list/');   ?> method='post'>
                        <?php //} else if ($if_user == "2_1") { ?>
                                <form name='search' action=<?php //echo site_url('Pc_controller/my_action_admin_load_pc_list/');   ?> method='post'>
                        <?php //} else if ($if_user == "3_1") { ?>
                                <form name='search' action=<?php //echo site_url('Pc_controller/my_action_gm_load_pc_list/');   ?> method='post'>
                        <?php //} ?>
                            
                                    Status <?php
                        //switch($if_user){
                        //case "2" :
                        //$data = array(
                        ///'1'	=> 'Submit for Approval',
                        //'2'	=> 'Recommend',
                        //'3'	=> 'Submit for Correction',
                        //'4'	=> 'Rejected');
                        //break;
                        //case "3" :
                        //$data = array(
                        //	'2'	=> 'Recommend',
                        //'1'	=> 'Submit for Approval',
                        //'4'	=> 'Rejected',
                        //'5'	=> 'Approved');
                        //break;
                        //case "2_1" :
                        //$data = array(
                        //'1'	=> 'Submit for Approval');
                        //break;
                        //case "3_1" :
                        //$data = array(
                        //'2'	=> 'Recommend',
                        //'1'	=> 'Submit for Approval');
                        //break;
                        //}
                        //echo form_dropdown('STATUS', $data); 
                        ?>	&nbsp;
                                     Expense Type <?php
                        //$data = array(
                        // ''	=> 'All...',
                        //  '1'	=> 'Meals',
                        // '2'	=> 'Travelling',
                        // '3'	=> 'Meals & Travelling',
                        //'4'	=> 'Others');
                        //echo form_dropdown('EXPENSE_TYPE', $data); 
                        ?> &nbsp;
                                    Payee Name <input name="PAY_NAME" id="PAY_NAME" type='text' value='<?php //echo trim($PAY_NAME);    ?>' /> &nbsp;
                                    <input type='submit' name='search' value='Search' class="button button-orange" />
                            </form>
                    </div>---->
                        <div id="flash" align="center"></div>
                        <?php $attributes = array('name' => 'myform', 'id' => 'myform');
                        echo form_open('admin/add_new_user', $attributes);
                        ?>      

                        <div align="center" style="padding-top:38px;">

                            <table border="0" cellspacing="5" cellpadding="1" width="96%">
                            
                                <tr>
                                    <td >
                                        <div align="center">


                                            <table width="75%" border="0" align="center" cellpadding="5" cellspacing="2">
                                                <tr>
                                                    <td width="32%"><div align="left">EPF Number <span class="warning">*</span> </div></td>
                                                    <td colspan="3">
                                                                    <div align="left">
																				      <?php echo form_input(array('id' => 'epf_no', 'name' => 'epf_no', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '6', 'value' => set_value('epf_no'))); ?>
                                                                    <div class="validation fielderror" id="error_epf_no" style="display:none; width:309px;"></div>
                                                                    </div>                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><div align="left">Actual Name <span class="warning">*</span> </div></td>
                                                    <td colspan="3">
                                                                    <div align="left">
                                                                                      <?php echo form_input(array('id' => 'user_name', 'name' => 'user_name', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '120', 'value' => set_value('user_name'))); ?>
                                                                    <div class="validation fielderror" id="error_user_name" style="display:none; width:309px;"></div>
                                                                     </div>                                                                     </td>
                                                </tr>
                                                <tr>
                                                    <td>

                                                        <div align="left">
                                                            Card Printing  Level <span class="warning">*</span>                                                        </div>                                                    </td>

                                                    <td colspan="3">

                                                        <div align="left">
                                                            <input type="radio" id="group_name_0"   value="B" name="group_name">Branch
                                                            <input type="radio" id="group_name_1"   value="R" name="group_name">Region 
                                                            <input type="radio" id="group_name_"   value="Z" name="group_name"/>Zone                                                        </div>
                                                        <div class="validation fielderror" id="error_group_name" style="display:none; width:309px;"></div>                                                     </td>
                                                </tr>
                                                <tr class="display_branch">
                                                    <td><div align="left"> Branch Code <span class="warning">*</span> </div></td>

                                                    <td colspan="3">
                                                        <div align="left">                                       

                                                            <select name="branch" id="branch" class="text-input">                                                       

                                                                <option value="">--------------SELECT----------------</option>
																<option value="A">ALL</option>
                                                                <?php foreach ($rs_branch as $row) {
                                                                    $cur_branch = $row['BRCODE'];
                                                                    ?>

                                                                <option value="<?php echo $cur_branch; ?>"<?php if ($branch == $cur_branch) { ?>selected="selected"<?php } ?>><?php echo strtoupper($row['BRDESCRP']); ?></option>

																<?php } ?>
                                                                
                                                            </select>
                                                        </div>
                                                        <div class="validation fielderror" id="error_branch" style="display:none; width:309px;"></div>                                                        </td>
                                                </tr>
                                                
                                                <tr class="display_region">
                                                    <td><div align="left"> Region Code <span class="warning">*</span> </div></td>
                                                    <td colspan="3"><div align="left">
                                                            <select name="region" id="region" class="text-input">
                                                                <option value="">--------------SELECT----------------</option>
                                                                 <option value="A">ALL</option>
                                                                  <?php foreach ($rs_region as $row) {
                                                                    $cur_region = $row['RGCODE'];
                                                                    ?>
                                                                    <option value="<?php echo $cur_region; ?>"<?php if ($region == $cur_region) { ?>selected="selected"<?php } ?>><?php echo strtoupper($row['RGDESCRP']); ?></option>
																 <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="validation fielderror" id="error_region" style="display:none; width:309px;"></div>                                                        </td>
                                                </tr>
                                                <tr class="display_zone">
                                                    <td><div align="left"> Zone Code <span class="warning">*</span> </div></td>
                                                    <td colspan="3"><div align="left">
                                                            <select name="zone" id="zone" class="text-input">
                                                                <option value="">--------------SELECT----------------</option>
                                                                <option value="A">ALL</option>
																	<?php foreach ($rs_zone as $row) {
                                                                        $cur_zone = $row['ZNCODE'];
                                                                        ?>
                                                                        <option value="<?php echo $cur_zone; ?>"<?php if ($zone == $cur_zone) { ?>selected="selected"<?php } ?>><?php echo strtoupper($row['ZNDESCRP']); ?></option>
                                                                    <?php } ?>
                                                            </select>
                                                        </div> <div class="validation fielderror" id="error_zone" style="display:none; width:309px;"></div></td>
                                                </tr>
                                                <tr>
                                                  <td><div align="left"> User Level <span class="warning">*</span> </div></td>
                                                  <td colspan="3">
                                                                 <select name="user_level" id="user_level">
                                                                     <option value="U">User</option>
                                                                     <option value="M">Manager</option>
                                                                 </select>
                                                  </td>
                                                </tr>

                                                <tr>
                                                    <td><div align="left"> Define Roles <span class="warning">*</span> </div></td>
                                                    <td width="20%"><input type="checkbox" name="vonly" id="vonly"   value="1"  />
                                                        View Only</td>
                                                    <td width="18%"><input type="checkbox" name="all_reports" id="all_reports"   value="1" /> 
                                                        Allow Reports</td>
                                                    <td width="30%"><input type="checkbox" name="up_fleet_policy" id="up_fleet_policy"   value="1"  /> 
                                                        Allow  Upload for Fleet Policy</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><input type="checkbox" name="ponly" id="ponly"   value="1"  />
                                                        Print Only</td>
                                                    <td><input type="checkbox" name="search_allow" id="search_allow"   value="1"  /> 
                                                        Search Allow</td>
                                                    <td><input type="checkbox" name="all_nexus" id="all_nexus"   value="1"  />
                                                        Allow  Manual issue Policy</td>
                                                </tr>
                                                <tr>
                                                    <td><span class="warning">*</span> Denotes required Fields</td>
                                                    <td colspan="3"><div class="validation fielderror" id="error_vonly" style="display:none; width:309px;" ></div></td>
                                                </tr>
                                            </table>

                                      </div>
                                    </td>
                                </tr>
                            </table>    <div id="alert_user_creation" align="center" style="padding-left:40px;" >            </div>                           
                        </div><br /><div align="center">
                            <input type="button" onclick="location.replace('<?php echo base_url(); ?>index.php/admin/user_creation');" class="button button-orange" value="Back" id="btnCancel2" name="btnCancel2" />
                            <input name="btnCancel2" type="reset" id="btnCancel3"  value="Rest" class="button button-orange" />
                            <input name="BtnSubmit" type="button" id="BtnSubmit"   value="Submit" class="button button-orange"  onclick="add_user_Data();"/>
                        </div> 




                        <p><?php echo form_close(); ?></p>

                        <!-- Pagination -->    


                        <span class="rounded_nobottomgap">&nbsp;</span> </div>   
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('main_page/footer_view');
?>