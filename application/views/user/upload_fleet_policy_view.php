<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Upload Fleet Policies</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:2918px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:2845px;"> <span class="rounded_notopgap"></span><br class="clear" />
                       
                          <?php echo form_open_multipart('user/do_upload_fleet_policy'); ?>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <table width="66%" border="0" align="center" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td width="37%">Upload Fleet Policies as Excel</td>
                                  <td width="63%"><div align="left"><div align="left"><input type="file" name="userfile" id="userfile" /></div></div></td>
                              </tr>
                                <tr>
                                    <td>Ex- fleet.xls</td>
                                    <td><input  type="submit" id="BtnSave" name="BtnSave"  value="Submit" class="button button-orange" /></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo base_url(); ?>/assets/fleet_format.xls">Download Sample Format </a></td>
                                    <td><div align="left"><?php echo $upload_msg; ?></div></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        <?php echo form_close(); ?>
                        <!-- Pagination -->
                        <span class="rounded_nobottomgap">&nbsp;</span> </div>
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>