<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Upload Single Fleet Policy</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:468px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:411px;"> <span class="rounded_notopgap"></span><br class="clear" />
                          
                    <p>&nbsp;</p>
							<?php $attributes = array('name' => 'myform', 'id' => 'myform');
                        echo form_open('user/upload_fleet_policy_single', $attributes);
                        ?>      
              <table width="66%" border="0" align="center" cellpadding="5" cellspacing="0" style="font-size:15px;">
                                <tr>
                                    <td width="34%">Vehicle Number </td>
                                    <td width="66%"><?php echo form_input(array('id' => 'vehicle_no', 'name' => 'vehicle_no', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '10', 'value' => set_value('vehicle_no'))); ?>
									<?php echo form_error('vehicle_no'); ?>									</td>
                </tr>
                                <tr>
                                    <td>Policy Number </td>
                                    <td><?php echo form_input(array('id' => 'policy_no', 'name' => 'policy_no', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '16', 'value' => set_value('policy_no'))); ?>
									<?php echo form_error('policy_no'); ?>									</td>
                                </tr>
                                <tr>
                                  <td>Policy Commencement Date</td>
                                  <td><?php echo form_input(array('id' => 'periods', 'name' => 'periods', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '24', 'value' => set_value('periods','2014-11-20'))); ?>
								  <?php echo form_error('periods'); ?>								  </td>
                                </tr>
                                <tr>
                                  <td>Policy Expiry Date</td>
                                  <td><?php echo form_input(array('id' => 'periode', 'name' => 'periode', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '24', 'value' => set_value('periode','2015-11-19'))); ?> <?php echo form_error('periode'); ?> </td>
                                </tr>
                                <tr>
                                  <td>Name</td>
                                  <td><?php echo form_input(array('id' => 'name', 'name' => 'name', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '190', 'value' => set_value('name'))); ?>
								  <?php echo form_error('name'); ?>								  </td>
                                </tr>
                                <tr>
                                  <td>Address Line 1 </td>
                                  <td><?php echo form_input(array('id' => 'address', 'name' => 'address', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '200', 'value' => set_value('address'))); ?>
								   <?php echo form_error('address'); ?>								  </td>
                                </tr>
                                <tr>
                                  <td>Address Line 2 </td>
                                  <td><?php echo form_input(array('id' => 'address2', 'name' => 'address2', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '200', 'value' => set_value('address2'))); ?> </td>
                                </tr>
                                <tr>
                                  <td>Reason </td>
                                  <td><?php echo form_input(array('id' => 'reason', 'name' => 'reason', 'class' => 'text-input', 'style' => 'width:70%', 'maxlength' => '200', 'value' => set_value('reason'))); ?></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td><input type="button" onclick="location.replace('<?php echo base_url(); ?>index.php/user/fleet_policy_infor');" class="button button-orange" value="Back" id="btnCancel2" name="btnCancel2" />
                                    <input name="BtnSubmit" type="submit" id="BtnSubmit"   value="Submit" class="button button-orange"  onclick="add_user_Data();"/>
                                    <input name="btnCancel22" type="reset" id="btnCancel3"  value="Rest" class="button button-orange" /></td>
                                </tr>
                            </table>
							<?php echo form_close(); ?>
                <p>
                              <!-- Pagination -->
                              <span class="rounded_nobottomgap">&nbsp;</span></p>
                  </div>
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>