<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/ajax_pagination.css" type="text/css" />
 <style>
  #loader{
        position: absolute;
        text-align: center;
        top: 75px;
        width: 100%;
        display:none;
    }
 
 
 </style>
 
 <script>
    $(document).ready(function() {
	
        var default_message_for_dialog = 'You are sure ? You want to Send it to Printer ?';
	
        $("#dialog").dialog({
            modal: true,
            bgiframe: true,
            width: 300,
            height: 200,
            autoOpen: false,
            title: 'Confirm'
        });

        // LINK
		<?php for ($j=1 ; $j<=$search_count ;$j++ ){?>
        
		$("#btn_search<?php echo $j;?>").click(function(link) {
		
		
		
            link.preventDefault();
            var theHREF = $(this).attr("href");
            var theREL = $(this).attr("rel");
            var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
			
			var Y = $("#hdd_pol_id<?php echo $j;?>").val();
			
	
            // set windows content
            $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');

            $("#dialog").dialog('option', 'buttons', {
			
                "Confirm" : function() {
				                  				
				 $(location).attr('href','<?php echo base_url(); ?>index.php/user/search_print_nexus_card/'+ Y);
				 
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            });

            $("#dialog").dialog("open");

        });

	   <?php }?>

    });

</script>

 

<table border="0" cellspacing="1" cellpadding="1" width="96%">
                                <tr>
                                    <td >
                                        <div align="center">

<?php if ($search_count > 0) { ?>
<div id="dialog"></div>

                                                <table border="0" cellspacing="1" cellpadding="1" width="82%">
                                              <tr>
                                                        <td valign="top">
                                                            <table width="97%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrapTable">
  <tr align="center">
                                                                    <th width="28" >&nbsp;</th>
                                                                    <th nowrap="nowrap" width="135" class="cellLeftLine"><div align="left" style="padding-left:8px; padding-right:4px;">Policy Number</div></th>
                                                                <th width="114" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Vehicle  No </strong></div></th>
                                                                <th width="386" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Customer  Name</strong></div></th>
                                                                <th width="98" class="cellLeftLine"><div align="left" style="padding-left:8px;"><strong>Debtor Code </strong></div></th>
                                                                <th width="112" class="cellLeftLine"><div align="left" style="padding-left:8px;"><strong>City </strong></div></th>
                                                                <?php if ($print_facility !=0 ) {?>
                                                                <th width="57" class="cellLeftLine">View</th>
                                                                <?php } ?>
                                                    </tr>
                                                    <?php
                                                    $no = $this->uri->segment(3) + 1; $h=0;

                                                    foreach ($search_listing as $row) {

                                                        $color1 = "even";
                                                        $color2 = "add";
                                                        $now = $no + 1;
                                                        $row_color = ($now % 2) ? $color2 : $color1;

                                                        //$usr_id = $row['usr_id'];
                                                       
                                                        
                                                        $MICPLNO = $row['MICPLNO'];
														$MICVENO = $row['MICVENO'];
														$MICSPNO = $row['MICSPNO'];
														$id      = $row['MIC_ID_NEXUS'];
														$h++;
                                                        ?>

                                                        <tr onMouseOver="className='mouseOver';" onMouseOut="className='<?php echo $row_color; ?>';" class="<?php echo $row_color; ?>">

                                                            <td>
														<?php 
														        if ($offset_count == 0 ) {
														if ((strlen($no) < 2)) {
                                                           echo "0" . $no; $list_count ="0" . $no;
                                                           } else {
                                                            echo $no;  $list_count =$no;
                                                        } }else {echo $list_count=$offset_count+$h; } ?>                                                        </td>

                                                            <td nowrap="nowrap" class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo trim($MICPLNO); ?></div></td>
                                                            <td class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo $MICVENO; ?></div></td>
                                                            <td class="cellLeftLine" nowrap="nowrap"><div align="left"><?php echo $row['MICTITL'];?>. <?php echo $row['MICNAME'];?></div></td>
                                                            <td class="cellLeftLine"><div align="left"><?php echo $MICSPNO;?></div></td>
                                                            <td class="cellLeftLine" nowrap="nowrap"><div align="left"><?php echo $row['MICTOWN'];?></div></td>
                                                            <?php if ($print_facility !=0 ) {?>
                                                            <td class="cellLeftLine"><div align="center">
                                                            <input type="hidden" name="hdd_pol_id<?php echo $h;?>" id="hdd_pol_id<?php echo $h;?>" value="<?php echo $id;?>" />
                                                            
                                                            <input type='button' name='btn_search<?php echo $h;?>'  id="btn_search<?php echo $h;?>" value='Print' class="button button-orange" />
                                                            <?php }?>  
                                                          </div></td>
                                                        </tr>
														<?php $no++;
														
                                                                   } 
																   
                                                         ?>
                                                </table>

                                        </td>
                                    </tr>

                                </table>
                                <br />
                                <!--<div align="left"><div id="dd3" style="padding-left:191px; padding-right:2px;" ><?php //echo $this->pagination->create_links(); ?></div></div> -->
                                <div class="newpagiongsec" style=" width:auto"><?php echo $paginater;?></div> 
                                <div align="left"  class="listing" style="padding-left:76px;"><?php echo $list_count;?> of <?php if ((strlen($rec_count) < 2)) { echo "0".$rec_count;}else { echo $rec_count;}?></div>


<?php } else { ?><br><br><br><br><br><div align="center"><div class="my_close_note" style="width:321px;" > No Records Found  for search in the seaarch ..! </div></div><?php } ?>
                        </div>
                        </td>
                        </tr>
                        </table>