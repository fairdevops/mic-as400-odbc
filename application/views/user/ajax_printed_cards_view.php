<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/ajax_pagination.css" type="text/css" />
 <style>
  #loader{
        position: absolute;
        text-align: center;
        top: 75px;
        width: 100%;
        display:none;
    }
 
 
 </style>
 
  <script>
  
  
  
    $(document).ready(function() {
	
        var default_message_for_dialog = 'You are sure ? You want to re-print this card ?';
		
		var default_message_for_dialog_2 = 'You are sure ? You want to confirm re-print this card ?';
	
        $("#dialog").dialog({
            modal: true,
            bgiframe: true,
            width: 300,
            height: 200,
            autoOpen: false,
            title: 'Confirm'
        });

        // LINK
		<?php for ($j=1 ; $j<=$print_count ;$j++ ){?>
        
		$("#btn_re_print<?php echo $j;?>").click(function(link) {
		
		
		
            link.preventDefault();
            var theHREF = $(this).attr("href");
            var theREL = $(this).attr("rel");
            var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
			
			var Y = $("#hdd_pol_id<?php echo $j;?>").val();
			
	
            // set windows content
            $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');

            $("#dialog").dialog('option', 'buttons', {
			
                "Confirm" : function() {
				                  				
				 $(location).attr('href','<?php echo base_url(); ?>index.php/user/search_re_print_card/'+ Y);
				 
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            });

            $("#dialog").dialog("open");

        });

	   <?php }?>
	   
	   
	   ////btn_re_print_allow
	   
	   
	   		<?php for ($j=1 ; $j<=$print_count ;$j++ ){?>
        
		$("#btn_re_print_allow<?php echo $j;?>").click(function(link) { alert("ccc");
		
		            link.preventDefault();
            var theHREF = $(this).attr("href");
            var theREL = $(this).attr("rel");
            var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog_2 : theREL;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
			
			var Y = $("#hdd_pol_id<?php echo $j;?>").val();
			
	
            // set windows content
            $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');

            $("#dialog").dialog('option', 'buttons', {
			
                "Confirm" : function() {
				                  				
				 $(location).attr('href','<?php echo base_url(); ?>index.php/user/search_confirm_card/'+ Y);
				 
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            });

            $("#dialog").dialog("open");

        });

	   <?php }?>



    });

</script>


<table border="0" cellspacing="1" cellpadding="1" width="96%">
                                <tr>
                                    <td >
                                        <div align="center">

<?php if ($print_count > 0) { ?>

                                                <table border="0" cellspacing="1" cellpadding="1" width="83%">
                                      <tr>
                                                        <td valign="top">
                                                            <table width="113%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrapTable">
                                                                <tr align="center">
                                                                    <th width="31" >&nbsp;</th>
                                                                  <th nowrap="nowrap" width="151" class="cellLeftLine"><div align="left" style="padding-left:8px; padding-right:4px;">Policy Number</div></th>
                                                                <th width="127" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Vehicle  No </strong></div></th>
                                                                <th width="432" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Customer  Name</strong></div></th>
                                                                <th width="109" class="cellLeftLine"><div align="left" style="padding-left:8px;"><strong>Debtor Code </strong></div></th>
                                                                <th width="110" class="cellLeftLine"><div align="left" style="padding-left:8px;"><strong>City </strong></div></th>
                                                                <th width="82" class="cellLeftLine">Re-Print </th>
                                                    </tr>
                                                    <?php
													
                                                    $no = $this->uri->segment(3) + 1; $h=0;

                                                    foreach ($rs_listing as $row) {

                                                        $color1 = "even";
                                                        $color2 = "add";
														$color3 = "print";
                                                        $now = $no + 1;
                                                        $row_color = ($now % 2) ? $color2 : $color1;

                                                        //$usr_id = $row['usr_id'];
                                                       
                                                        
                                                        $MICPLNO     = $row['PR_MICPLNO'];
														$MICVENO     = $row['PR_MICVENO'];
														$MICSPNO     = $row['PR_MICSPNO'];
														$print_state = $row['PR_CARD_PRINT'];
														$id          = $row['PR_MIC_ID'];
														$h++;
                                                        ?>

                                                        <tr onMouseOver="className='mouseOver';" onMouseOut="className='<?php if ($print_state=='N') {echo $row_color; }else { echo $color3;} ?>';" class="<?php if ($print_state=='N') {echo $row_color; }else { echo $color3;} ?>">

                                                            <td>
														<?php //echo $offset_count; 
														           if ($offset_count == 0 ) {
														if ((strlen($no) < 2)) {
                                                            echo "0" . $no; $list_count ="0" . $no;
                                                        } else {
                                                            echo $no;  $list_count =$no;
                                                        } }else {echo $list_count=$offset_count+$h; } ?>                                                           </td>

                                                            <td nowrap="nowrap" class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo trim($MICPLNO); ?></div></td>
                                                            <td class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo $MICVENO; ?></div></td>
                                                            <td class="cellLeftLine" nowrap="nowrap"><div align="left"><?php echo $row['PR_MICTITL'];?>. <?php echo $row['PR_MICNAME'];?></div></td>
                                                            <td class="cellLeftLine"><div align="left"><?php echo $MICSPNO;?> </div></td>
                                                            <td class="cellLeftLine" nowrap="nowrap"><div align="left"><?php echo $row['PR_MICTOWN'];?></div></td>
                                                            <td class="cellLeftLine"><div align="center">
                                                             <?php $req_count= $this->re_print_model->check_request_made($id);?>
                                                            
                                                              <input type="hidden" name="hdd_pol_id<?php echo $h;?>" id="hdd_pol_id<?php echo $h;?>" value="<?php echo $id;?>" />
                                                              <?php if ($req_count==0){?>
                                                              <input type='button' name='btn_re_print<?php echo $h;?>'  id="btn_re_print<?php echo $h;?>" value='Request Re-Print' class="button button-orange" />
                                                              <?php }else {  $req_state= $this->re_print_model->check_request_made_state($id); if ($req_state=='P'){ ?><div id="" class="request_pending"> Request Pending</div><?php }else {?>
															  <?php if ($req_state=='A'){?>
                                                              <input type='button' name='btn_re_print_allow<?php echo $h;?>'  id="btn_re_print_allow<?php echo $h;?>" value='Confirm Re-Print' class="button button-orange" style=" color:#000000; font-weight:400;" />
															  <?php }else{ ?>  <input type='button' name='btn_re_print<?php echo $h;?>'  id="btn_re_print<?php echo $h;?>" value='Request Re-Print' class="button button-orange" /><?php } } } ?>
                                                            </div></td>                                                        </tr>
														<?php $no++;
														
                                                                   } 
																   
                                                         ?>
                                                </table>

                                        </td>
                                    </tr>

                                </table>
                                <br />
                                <!--<div align="left"><div id="dd3" style="padding-left:191px; padding-right:2px;" ><?php //echo $this->pagination->create_links(); ?></div></div> -->
                                <div class="newpagiongsec" style=" width:auto"><?php echo $paginater;?></div> 
                                <div align="left"  class="listing" style="padding-left:76px;"><?php echo $list_count;?> of <?php if ((strlen($no) < 2)) {echo "0".$rec_count;}else{echo $rec_count;}?></div>


<?php } else { ?><div align="right"><div class="my_close_note" > No Records Found in the Printer Spool</div></div><?php } ?>
                        </div>
                        </td>
                        </tr>
                        </table>