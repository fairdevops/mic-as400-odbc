<style>
.outer_div{
    margin:auto;
    width: auto;
}

.sortsec{float:left; width:780px; height:auto; margin-top:6px; margin-bottom:16px;padding:1px;}
.sortby{ background:url("<?php echo base_url(); ?>assets/images/arrowred.png") no-repeat;  font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; font-weight:700;  line-height:42px; float:left; width:66px; color:#FFFFFF; text-transform:uppercase; padding-left:10px;}
.sortsecin{float:right; background:url("<?php echo base_url(); ?>assets/images/sortbg.png") repeat-x;width:580px; height:auto; width:546px;  border-top:1px solid #7db5de;  border-bottom:1px solid #7db5de;  border-right:1px solid #7db5de;border-left:1px solid #7db5de;} 
.sortselect{ font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:700; color:#000000; float:center; width:585px; line-height:39px; padding-left:4px; height:27px; padding-bottom:12px;		}
.showingsec{ float:center; width:720px; height:auto; margin-top:16px;}
</style>
<script language="javascript">	
	var noOfSubmits = 0;
	
	function radioValue(rObj) {
        
    for (var i=0; i<rObj.length; i++) 
        if (rObj[i].checked) return rObj[i].value;    
    } 
		
    function load(page){	

    //alert('load'+page);		

       jQuery("#loader").css('display','block').fadeIn('slow');

       	var txn_type  = $('#txn_type').val();
       	var searchVal = $("#seek_data").val();

        $.ajax({
            type: "POST",
            data:jQuery("#form1").serialize()+ "&page="+page+"&txn_type="+txn_type+"&txt_search="+searchVal,
            url: "<?php echo base_url(); ?>index.php/user/serach_bulklist_by_debtor",
            success:function(data){
                jQuery(".outer_div").html(data).fadeIn('slow');
                jQuery("#loader").fadeOut('slow');
            }
        });							
    }
	
	
	function load_page(page){
	//	alert('load_page');
		var txn_type  = $('#txn_type').val();
       	var searchVal = $("#seek_data").val();

         $.ajax({
            type: "POST",
            data:jQuery("#form1").serialize()+ "&page="+page+"&txn_type="+txn_type+"&txt_search="+searchVal,
            url: "<?php echo base_url(); ?>index.php/user/serach_bulklist_by_debtor",
            success:function(data){
                jQuery(".outer_div").html(data).fadeIn('slow');
                jQuery("#loader").fadeOut('slow');
            }
        });						
    }

    /*
	* Ajax call to fetch debtor bulk card list
	*/
	function fnSearchDebtorBulkList(page){

		alert('fnSearchDebtorBulkList');
		var txn_type  = $('#txn_type').val();
       	var searchVal = $("#seek_data").val();

		jQuery("#loader").css('display','block').fadeIn('slow');
         $.ajax({
            type: "POST",
            data:jQuery("#form1").serialize()+ "&page="+page+"&txn_type="+txn_type+"&txt_search="+searchVal,
            url: "<?php echo base_url(); ?>index.php/user/serach_bulklist_by_debtor",
            success:function(data){
            	alert('here');
                jQuery(".outer_div").html(data).fadeIn('slow');
                jQuery("#loader").fadeOut('slow');
            }
        });	
	}	


</script>



<script type="text/javascript">
	 


	//load(1);

    $(document).ready(function() {
	
	    load(1);
		
        var default_message_for_dialog = 'You are sure ? You want to proceed ..?';
	
        $("#dialog").dialog({
            modal: true,
            bgiframe: true,
            height: 332,
            width: 588,
            autoOpen: false,
            title: 'Confirm'
        });
		

		
		jQuery('#BtnSubmit_bulk').click(function(){
		 
						  jQuery('#loader').html("<img src='<?php echo base_url(); ?>assets/images/ajax.gif'/>").fadeIn('fast');
	   
	   					  var txn_type = $('#txn_type').val();
	   					  var seek_data = $('#seek_data').val();
						  noOfSubmits++;
						  
						  var r = confirm("Confirm Printing");
							if (r == true) {
			    						
							   $('*.validation').hide();
						 		if(noOfSubmits>1)
								{
							 		  alert("Submission already done. Please wait....");
									  $('#BtnSubmit_by_click').removeClass("button button-orange"); 
						              $('#BtnSubmit_by_click').addClass("button button-gray"); 
						    		  $('#BtnSubmit_by_click').attr("disabled", "disabled"); 
									  return false;

					    	 	}
					   			else{			 

								        $.ajax({

								                type: "POST",
								                url: "<?php echo base_url(); ?>index.php/user/send_debtor_bulk_information",
								                data:  {  rad_val : txn_type, dc : seek_data},
								                dataType: "json",

									            success: function(data) {

									                if(data.my_msg) { 
									                    $.each(data.my_msg, function(key, value) {               
									                        var jquery_id = '#' + key;                               
									                        if(value == "")
									                        {
									                             $(jquery_id).hide();
									                        } 
															else 
															{
									                           noOfSubmits = 0;
															   var value = value.substring(3, value.length-4);
									                           $(jquery_id).show();
									                           $(jquery_id).html(value);                            
									                        }
									                    });
									                } else { 
															$('.validation').hide();
															$("#loader").fadeOut('slow');
														    $('#BtnSubmit_by_click').attr("disabled", "disabled"); 
															$('#BtnSubmit_by_click').hide();
															$('#BtnSubmit_by_click').removeClass("button button-orange"); 
															$('#BtnSubmit_by_click').addClass("button button-gray"); 

															if (data.result!=0){
														      $(location).attr('href','<?php echo base_url(); ?>index.php/user/bulk_print');
														      }
															 else
															{
															   error: Error_Handler
															}
															   
									                }
									            }///,error: Error_Handler
								           
								        });
										
								}

							} else {
			   						
			   						//false
							}
		});
				
		/*
		* Button search
		*/

		$("#btn_search").click(function() {

				load_page(1);			
				
				$('#BtnSubmit_by_click').hide();
				$('#BtnSubmit_by_colombo').hide();
				$('#BtnSubmit_by_outstation').hide();
				$('#group_name_0').attr("disabled", "disabled"); 
				$('#group_name_1').attr("disabled", "disabled"); 
				$('#group_name_2').attr("disabled", "disabled"); 
				$('#group_name_3').attr("disabled", "disabled");

		});
				
    });
</script>

<div class="clear"></div>
<div class="wrapper">
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black  first ">
            <h3 class="header">Bulk Card Printing Facility</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:822px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:772px;"> <span class="rounded_notopgap"></span><br class="clear" />

	 				<div id="loader" align="center" style="display: none;"><img src="<?php echo base_url(); ?>assets/images/ajax.gif"></div>

                    <form name="form1" action="user/daily_mic_list_generate" method="post">
                    
                    <div align="center"></div>
                    
                     <div class="showingsec">
						<div class="sortsecin">
							<div class="sortselect">
                                <span>Debtor code : &nbsp;</span>

                                <span><input name="seek_data" id="seek_data"  class="text-input" type='text' /></span>

                                <span><select name="txn_type" id="txn_type">
                                	<option value="0">--Select Transaction Type--</option>
                                	<!-- <option value="0">New Business</option> -->
                                	<option value="1">Renewal</option>
                                </select></span>
                                <span><input type='button' name='btn_search'  id="btn_search" value='Search' class="button button-orange"  />
                                </span>
                            </div>
                        </div>
                    </div>
                    <br /><br />

                        <div align="center" style="padding-top:12px;">
                            <div id="dialog"></div>
                             <div class="outer_div" style="padding-left:15px;"></div>                               
                    </div><br />
                    
                                      
                      <?php 
                      $print_facility = true;
                      if ($print_facility ) {?>
                      <div align="center">
				
						
                        <?php //if ($by_hand_count > 0) { ?>
                          <!--  <input name="BtnSubmit_by_click"  type="button"  id="BtnSubmit_by_click"    value="Send this list to print"  class="button button-orange" /> -->

                            <input name="BtnSubmit_bulk"  type="button"  id="BtnSubmit_bulk"    value="Send this list to print"  class="button button-orange" />

                        <?php //} ?>
                        
                      </div>
                      <?php } ?>
                      
                    <p>
                    </form></p>
                    <span class="rounded_nobottomgap">&nbsp;</span> </div>   
                <div class="rounded_bottom-left"></div>
                <div class="rounded_bottom-right"></div>
                <!-- Button Set End -->
            </div>
            <br class="clear" />
        </div>
        <div class="box-level3b">
            <div class="box-level2b">
                <div class="box-level1b"></div></div>
            </div>
        </div>
    </div>

  </div>