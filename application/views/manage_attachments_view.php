<?php 
 $this->load->view('main_page/header_view');
 
?>
<script>
    $(document).ready(function() {
	
        var default_message_for_dialog = 'You are sure ? You want to Delete ?';
	
        $("#dialog").dialog({
            modal: true,
            bgiframe: true,
            width: 300,
            height: 200,
            autoOpen: false,
            title: 'Confirm'
        });

        // LINK
        $("a.confirm").click(function(link) {
            link.preventDefault();
            var theHREF = $(this).attr("href");
            var theREL = $(this).attr("rel");
            var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
            var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
            // set windows content
            $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');

            $("#dialog").dialog('option', 'buttons', {
                "Confirm" : function() {
                    window.location.href = theHREF;
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            });

            $("#dialog").dialog("open");

        });

	
        /*// FORMS
        $('input.confirm').click(function(theINPUT){
                theINPUT.preventDefault();
                var theFORM = $(theINPUT.target).closest("form");
                var theREL = $(this).attr("rel");
                var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
                var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
                $('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');
                $("#dialog").dialog('option', 'buttons', {
                "Confirm" : function() {
                                        theFORM.submit();
                    },
                "Cancel" : function() {
                    $(this).dialog("close");
                    }
                });
                $("#dialog").dialog("open");
                });
         */	

    });

</script>

<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Manage Attachments
            </h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:418px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height:345px;"> <span class="rounded_notopgap"></span><br class="clear" />
                        <!---
                        <div class="search" style="float:right; padding-bottom:2px;">
                        <?php //if ($if_user == "2"){ ?>
                            <form name='search' action=<?php //echo site_url('Pc_controller/admin_load_pc_list/'); ?> method='post'>
                        <?php //} //else if ($if_user == "3") { ?>
                                <form name='search' action=<?php //echo site_url('Pc_controller/gm_load_pc_list/'); ?> method='post'>
                        <?php //} else if ($if_user == "2_1") { ?>
                                <form name='search' action=<?php //echo site_url('Pc_controller/my_action_admin_load_pc_list/'); ?> method='post'>
                        <?php //} else if ($if_user == "3_1") { ?>
                                <form name='search' action=<?php //echo site_url('Pc_controller/my_action_gm_load_pc_list/'); ?> method='post'>
                        <?php //} ?>
                            
                                    Status <?php
                        //switch($if_user){
                        //case "2" :
                        //$data = array(
                        ///'1'	=> 'Submit for Approval',
                        //'2'	=> 'Recommend',
                        //'3'	=> 'Submit for Correction',
                        //'4'	=> 'Rejected');
                        //break;
                        //case "3" :
                        //$data = array(
                        //	'2'	=> 'Recommend',
                        //'1'	=> 'Submit for Approval',
                        //'4'	=> 'Rejected',
                        //'5'	=> 'Approved');
                        //break;
                        //case "2_1" :
                        //$data = array(
                        //'1'	=> 'Submit for Approval');
                        //break;
                        //case "3_1" :
                        //$data = array(
                        //'2'	=> 'Recommend',
                        //'1'	=> 'Submit for Approval');
                        //break;
                        //}
                        //echo form_dropdown('STATUS', $data); 
                        ?>	&nbsp;
                                     Expense Type <?php
                        //$data = array(
                        // ''	=> 'All...',
                        //  '1'	=> 'Meals',
                        // '2'	=> 'Travelling',
                        // '3'	=> 'Meals & Travelling',
                        //'4'	=> 'Others');
                        //echo form_dropdown('EXPENSE_TYPE', $data); 
                        ?> &nbsp;
                                    Payee Name <input name="PAY_NAME" id="PAY_NAME" type='text' value='<?php //echo trim($PAY_NAME);  ?>' /> &nbsp;
                                    <input type='submit' name='search' value='Search' class="button button-orange" />
                            </form>
                    </div>---->

                                        <?php echo form_open('super_admin/view'); ?>
                                         <div id="dialog"></div>
                                        <div align="center" style="padding-top:24px;">
                        <table border="0" cellspacing="1" cellpadding="1" width="96%">
                            <tr>
                                <td ><div align="center">
<?php if ($total_rows> 0 ) { ?>
                                        <table border="0" cellspacing="1" cellpadding="1" width="58%">
                                            <tr>
                                                <td valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrapTable">
                                                        <tr align="center">
                                                            <th width="21" >&nbsp;</th>
                                                            <th nowrap="nowrap" width="165" class="cellLeftLine"><div align="left" style="padding-left:8px; padding-right:4px;">Attachment Code</div></th>
                                                        <th width="360" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Description </strong></div></th>
                                                        <th width="172" class="cellLeftLine">Delete</th>
                                            </tr>
                                            <?php
                                            $no = $this->uri->segment(3) + 1;
                                             foreach ($rs_attachments as $row)
                                            {
                                               $color1="even";  $color2="add";
                                               $now=$no+1;
                                               $row_color = ($now % 2) ? $color2 : $color1;
                                               $id=$row['ID'];
                                               $attach_code=$row['ATTACH_CODE'];
                                               $description=$row['ATT_DESC'];
                                            // $ATT_FILE=$row['ATT_FILE'];
											   $ATT_PATH=$row['ATT_PATH'];
                                            ?>
                                            <tr onmouseover="className='mouseOver';" onmouseout="className='<?php echo $row_color; ?>';" class="<?php  echo $row_color; ?>">
                                                <td><?php if ( (strlen($no)<2 )){echo "0".$no;} else{echo $no;}  ?>
                                                </td>
                                                <td class="cellLeftLine"><div align="left" style="padding-left:2px;"><a href="<?php echo base_url(); ?>index.php/email_shedule_configure/edit_attachment/<?php echo $id;?>" class="CLIKER_ID"><?php echo $attach_code;   ?></a></div></td>
                                                <td class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo $description;   ?></div></td>
                                                <td class="cellLeftLine"><div align="center"><a href="<?php echo base_url(); ?>index.php/email_shedule_configure/delete_attachments/<?php echo $id;  ?>" class="confirm myButton" ><img src="<?php echo base_url() ?>assets/images/delete_r.png" height="24px" width="24px" border="0" /></a></div></td>
                                            </tr>
                                            <?php $no++; }  ?>
                                        </table></td>
                            </tr>
                        </table></div>
                        <br />
                        <div align="left">
                            <div id="dd" style="padding-left:191px; padding-right:2px;" ><?php echo $this->pagination->create_links();  ?></div>
                        </div>
                        <?php  } else {   ?>
                        No Records Found
                        <?php  } ?>
                    </td>
                    </tr>
                    </table></div>
                    <br />
                    <div align="left">
                        <div id="dd3" style="padding-left:18px; padding-right:2px;" >
                            <?php //echo $this->pagination->create_links();  ?>
                        </div>
                    </div>
                    <div align="center">
                        <input type="button" onclick="location.replace('add_new_attachment_code')" class="button button-orange" value="Add New" id="btnCancel2" name="btnCancel2" />
                        <input type="button" onclick="location.replace('upload_template')" class="button button-orange" value="Upload Template" id="btnCancel3" name="btnCancel2" />
                    </div>                              


                    <p><?php echo form_close(); ?></p>

                    <!-- Pagination -->    
                    <div class="paging"><?php //echo $pagination;  ?></div>

                    <span class="rounded_nobottomgap">&nbsp;</span> </div>   
                <div class="rounded_bottom-left"></div>
                <div class="rounded_bottom-right"></div>
                <!-- Button Set End -->
            </div>
            <br class="clear" />
        </div>
        <div class="box-level3b">
            <div class="box-level2b">
                <div class="box-level1b"></div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('main_page/footer_view');?>