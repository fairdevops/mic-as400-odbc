<?php

  $this->load->view('main_page/header_view');
  $t=0;
  
  

?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/error_handler.js"></script>

<script language="javascript">

            function test_email_job(){ 
				
                $("#loader").html('');
                $("#loader").html('Wait...');
                $('#alert_msg_projects').empty();
								  
                $('*.validation').hide();


                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/email_shedule_configure/form_p1_q1",
                    data:$("#form_p1_q1").serialize(),
                    beforeSend: function() {
		   
                        $("#flash").show();
                        $("#flash").html('<img src="<?php echo base_url(); ?>images/ajax.gif" align="absmiddle">&nbsp;&nbsp;');
                    },
                    dataType: "json",

                    success: function(data) {
                        if(data.my_msg) { 
                            $.each(data.my_msg, function(key, value) {               
                                var jquery_id = '#' + key;                               
                                if(value == "")
                                {
                                    $(jquery_id).hide();							
							
                                }
                                else
                                {
                          
                                    var value = value.substring(3, value.length-4);
                                    $(jquery_id).show();
                                    $(jquery_id).html(value);   
                                    $("#flash").hide();                         
                                }
                            });
                        } else { 
                            $('.validation').hide();
                   	  
                   				 
                            if(data.result >0 ) {
					
                                $("alert_msg_projects").show();
                                $('#alert_msg_projects').empty();
                                $('#alert_msg_projects').append('<div class="message success">Pls check Your Mail Box in a short while...! </div>');
                                $("#flash").hide();
                                
                            }
                            else
                            { 
                                error: Error_Handler;
                            }							
				   
			
                        }
            
                    },error: Error_Handler
           
                });
		
		
            }						   
		



</script>
<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Testing E-mail Configuration</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height: auto;">
                <div id="columnSingle" style="padding-top:3px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                    <div class="rounded_inside"  style="height: auto;"> <span class="rounded_notopgap"></span><br class="clear" />
                    
                   	<?php   $prop_form_cov_pref_att = array('name' => 'form_p1_q1', 'id' => 'form_p1_q1');
			                echo form_open('admin/test_mode_email_configure', $prop_form_cov_pref_att);						  
			         ?>                       
                            <p>&nbsp;</p>
                            
                            <Div align="center">
									   <div id="flash"></div>
                            </Div>
                            
                            <table width="76%" border="0" align="center" cellpadding="5" cellspacing="0">

                                <tr>
                                    <td width="28%"> Selecct Title  <span class="warning">*</span> </td>
                                          <td colspan="5">
                                          
                                                    <select name="CTITLE" id="CTITLE" class="text-input">                                                       
                                                        <option value="">--SELECT--</option>
                                                        <option value="MR">MR</option>
                                                        <option value="MRS">MRS</option>
                                                        <option value="MISS">MISS</option>
                                                        <option value="DR">DR</option>
                                                        <option value="REV">REV</option>              
                                                     </select>
                                                     
                                                     <?php //echo form_error('CTITLE'); ?>                                           </td>
                              </tr>
                              
                                <tr>
                                    <td>Name <span class="warning">*</span></td>
                                    <td colspan="5">
									                <?php echo form_input(array('id' => 'CNAME', 'name' => 'CNAME', 'maxlength' => '50', 'class' => 'text-input', 'style' => 'width:45%')); ?>                                    </td>
                               </tr>
                               
                               <tr>
                                  <td>E-mail  <span class="warning">*</span></td>
                                  <td colspan="5">
								                     <?php echo form_input(array('id' => 'EMAIL', 'name' => 'EMAIL', 'maxlength' => '50', 'class' => 'text-input', 'style' => 'width:45%')); ?>                                  </td>
                               </tr>
                                
                                <tr>
                                    <td>Attachment Allow  <span class="warning">*</span> </td>
                                    <td colspan="5">
													<select name="attachment_state" id="attachment_state">
                                                      
                                                      <?php if ($ATTACH_STATE=='N'){?>                                                           
                                                           
                                                         <option value="N"<?php if ($ATTACH_STATE=='N'){ ?>selected="selected"<?php }?>>Not Allow</option>
                                                              
													   <?php } else {?>
                                                       
                                                      
                                                           <option value="Y"><?php  echo $file_attachment_code;?></option>
                                                           
                                                           <?php 
                                                       }
													   
													   ?>
                                                       
                                                       
                                                    </select>   
                                       </td>
                              
                              </tr>
                               
                                <?php foreach  ($rs_fields_required as $row ) {?>
                                <tr>
                                    <td><?php echo $field= $row['TPL_PASS_FIELD'];?> <span class="warning">*</span></td>
                                    <td colspan="5"><?php echo form_input(array('id' => $field, 'name' =>$field, 'maxlength' => '50', 'class' => 'text-input', 'style' => 'width:45%')); ?></td>
                                </tr>
                                
								<?php }?>
								
								<?php   $x=0;
								      foreach ($rs_fields_pass as $row ){  $x++;
									  ?>
                                <!---<tr>
                                  <td><?php //if ($x==1){?>Parameters assign in Template<?php } ?></td>
                                  <td width="21%">
                                                 <img src="<?php //echo base_url(); ?>assets/images/yes.png" width="15" height="15" align="absmiddle"  /> <b><?php //echo $row['TPL_PASS_FIELD'];?></b> 
                                                 <input type="hidden" name="hdd_id" id="hdd_id" value="<?php //echo $id;?>" /></td>
                                  <td width="16%">&nbsp;</td>
                                  <td width="13%">&nbsp;</td>
                                  <td width="14%">&nbsp;</td>
                                  <td width="8%">&nbsp;</td>
                                </tr><?php //}?>--->
                                
                                <tr>
                                  <td><span class="warning">*</span> Denotes required Fields</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                
                                <tr>
                                  <td colspan="5"></td>
                                </tr>
                                
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="5"><input type="button" onclick="location.replace('<?php echo base_url(); ?>index.php/email_shedule_configure/list_templates');" class="button button-orange" value="Back" id="btnCancel" name="btnCancel" />
                                    <input name="btnCancel4" type="button" id="btnCancel5"  value="Reset" class="button button-orange" />
                                    <input  type="button" id="BtnSave" name="BtnSave"  onclick="test_email_job();" value="Test Email" class="button button-orange" />
                                    <input type="hidden" name="hdd_id" id="hdd_id" value="<?php echo $id;?>" /></td>
                                </tr>
                                
                                
                                <tr>
                                    <td colspan="6"><div align="center">
                                      <div class="validation fielderror" id="error_CTITLE" style="display:none; width:auto;" ></div>
                                      <div class="validation fielderror" id="error_CNAME" style="display:none; width:auto;" ></div>
                                      <div class="validation fielderror" id="error_EMAIL" style="display:none; width:auto;" ></div>
                                       <?php foreach  ($rs_fields_required as $row ) {?>
                                       <div class="validation fielderror" id="error_<?php echo $row['TPL_PASS_FIELD']; ?>" style="display:none; width:auto;" ></div>
                                       <?php } ?>
                                      <div id="alert_msg_projects" style="padding:0 20px;"> </div>
                                    </div></td>
                                </tr>
                            </table>
                   
                        <?php echo form_close(); ?>
                        <!-- Pagination -->
                        <span class="rounded_nobottomgap">&nbsp;</span> </div>
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

 $this->load->view('main_page/footer_view');

?>