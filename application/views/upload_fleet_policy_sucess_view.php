
<div class="clear"></div>
<div class="wrapper" >
    <div id="roundedContainer">
        <div class="module mod-black mod-menu mod-menu-black   first ">
            <h3 class="header">Upload Fleet Policies List</h3>
            <div class="box-level1">
                <div class="box-level2">
                    <div class="box-level3"></div>
                </div>
            </div>
            <div class="box-content deepest with-header" style="height:<?php  echo (1289+$myrows);?>px;">
                <div id="columnSingle" style="padding-top:19px;">
                    <div class="rounded_top-left"></div>
                    <div class="rounded_top-right"></div>
                  <div class="rounded_inside"  style="height:<?php  echo (1245+$myrows);?>px;"> <span class="rounded_notopgap"></span><br class="clear" />
                   <?php echo form_open_multipart('user/upload_fleet_policy_submit'); ?>
                    <p>&nbsp;</p>
                            <div align="center">
                            <table border="0" cellspacing="1" cellpadding="1" width="86%">
                              <tr>
                              <td valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrapTable" style="font-size:9px;">
                                    <tr align="center">
                                      <th width="32" >&nbsp;</th>
                                      <th nowrap="nowrap" width="117" class="cellLeftLine"><div align="left" style="padding-left:8px; padding-right:4px;">Policy Number</div></th>
                                      <th width="113" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Vehicle  No </strong></div></th>
                                      <th width="138" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Period</strong></div></th>
                                      <th width="318" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Name</strong></div></th>
                                      <th width="274" class="cellLeftLine"><div align="left" style="padding-left:10px;"><strong>Address 1</strong></div></th>
                                      <th width="137" class="cellLeftLine"><div align="left" style="padding-left:8px;">
                                        <div align="left" style="padding-left:10px;"><strong>Address 2</strong></div>
                                      </div></th>
                                    </tr>
                                    <?php ///$this->load->model('fleet_model');	
									
									
									
										   $tot_exixts=0; $exists_count_print=0;
        //ini_set('log_errors', 1);
        error_reporting(E_ALL);

                                                    $no = $this->uri->segment(3) + 1; $h=0;

                                                  for ($a = 1; $a < $myrows; $a++){

                                                        $color1 = "even";
                                                        $color2 = "add";
														$color3 = "danger";
														$color4 = "green_light";
                                                        $now = $no + 1;
                                                        $row_color = ($now % 2) ? $color2 : $color1;
														
														$col_1=trim($this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][2]);
														$col_2=trim($this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][1]);
														$col_3=trim($this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][3]);
														$col_4=trim($this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][4]);
														$col_5=trim($this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][5]);
														$col_6=trim($this->spreadsheet_excel_reader->sheets[0]['cells'][$a+1][6]);
														
														
														
														$itemExt      = explode("TO", $col_3);
														$count        = count($itemExt);
														$string_need = "";  
														$data_item    =array();
										
												for ($m = 0; $m < $count; $m++) {
										
													  $data_item[$m] = trim(str_replace("/","",$itemExt[$m])); 
													 
													} 
													 
													 $sh_miccmdt = substr($data_item[0],4,4).substr($data_item[0],2,2).substr($data_item[0],0,2);
													 $sh_micexdt = substr($data_item[1],-4).(substr($data_item[1],2,2)).(substr($data_item[1],0,2));
													 
													 $exists_count   = $this->fleet_model->get_fleet_count($col_2,$sh_miccmdt,$sh_micexdt);
													 
													 $exists_count_print = $this->fleet_model->get_fleet_count_in_printer_file($col_2,$sh_miccmdt,$sh_micexdt);
													 
													 if ( ($exists_count >0) || ( $exists_count_print >0) )  { $tot_exixts=$tot_exixts +1 ;}

														

														$h++;
                                                        ?>
                                    <tr onMouseOver="className='mouseOver';" onMouseOut="className='<?php if (($exists_count >0) || ( $exists_count_print >0) ){ echo $color3; } else{echo $color4; }?>';" class="<?php if (($exists_count >0) || ( $exists_count_print >0) > 0 ){ echo $color3; } else{echo $color4; }?>">
                                      <td><?php //echo $offset_count; 
														          
														if ((strlen($no) < 2)) {
                                                            echo "0" . $no;
                                                        } else {
                                                            echo $no; 
                                                        }  ?>                                      </td>
                                      <td nowrap="nowrap" class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo $col_1; ?>
                                        <input type="hidden" name="hdd_pol_no_<?php echo $h;?>" id="hdd_pol_no_<?php echo $h;?>"  value="<?php echo $col_1; ?>"/>
                                      </div></td>
                                      <td class="cellLeftLine"><div align="left" style="padding-left:2px;"><?php echo $col_2; ?>
                                        <input type="hidden" name="hdd_veh_no_<?php echo $h;?>" id="hdd_veh_no_<?php echo $h;?>"  value="<?php echo $col_2; ?>"/>
                                      </div></td>
                                      <td nowrap="nowrap" class="cellLeftLine"><div align="left"><span style="padding-left:2px;"><?php echo $col_3; ?></span><span style="padding-left:2px;">
                                      <input type="hidden" name="hdd_period_<?php echo $h;?>" id="hdd_period_<?php echo $h;?>"  value="<?php echo $col_3; ?>"/>
                                      </span></div></td>
                                      <td nowrap="nowrap" class="cellLeftLine"><span style="padding-left:2px;"><?php echo $col_4; ?>
                                      <input type="hidden" name="hdd_name_<?php echo $h;?>" id="hdd_name_<?php echo $h;?>"  value="<?php echo $col_4; ?>"/>
                                      </span></td>
                                      <td nowrap="nowrap" class="cellLeftLine"><span style="padding-left:2px;"><?php echo $col_5; ?>
                                        <input type="hidden" name="hdd_add_1<?php echo $h;?>" id="hdd_add_1<?php echo $h;?>"  value="<?php echo $col_5; ?>"/>
                                      </span></td>
                                      <td nowrap class="cellLeftLine"><div align="left"><span style="padding-left:2px;"><?php echo $col_6; ?></span><span style="padding-left:2px;">
                                        <input type="hidden" name="hdd_add_2<?php echo $h;?>" id="hdd_add_2<?php echo $h;?>"  value="<?php echo $col_6; ?>"/>
                                      </span><?php //echo $exists_count;?></div></td>
                                </tr>
                                    <?php $no++;
														
                                                                   } 
																   
                                                         ?>
                                </table>
                              </tr>
                            </table>
                    </div>
                            <p>&nbsp;</p><input type="hidden" name="total" id="total"  value="<?php echo $h; ?>"/>
                          <p><div align="center"><?php if ($tot_exixts ==0 ) {?>
                              <input type="submit" name="button" id="button" class="button button-orange" value="Submit">
                              <?php }else {?>
                              <div class="validation fielderror"  align="left" style="width:374px;">Please remove the red mark policies from the excel file and re-pupload</div>
                              <?php }?>
                              </div>
                          </p>
                        <?php echo form_close(); ?>
                        <!-- Pagination -->
                  <span class="rounded_nobottomgap">&nbsp;</span> </div>
                    <div class="rounded_bottom-left"></div>
                    <div class="rounded_bottom-right"></div>
                    <!-- Button Set End -->
                </div>
                <br class="clear" />
            </div>
            <div class="box-level3b">
                <div class="box-level2b">
                    <div class="box-level1b"></div>
                </div>
            </div>
        </div>
    </div>
</div>