// HTML5 placeholder plugin version 0.3
// Enables cross-browser* html5 placeholder for inputs, by first testing
// for a native implementation before building one.
//
// USAGE: 
//$('input[placeholder]').placeholder();



// perform JavaScript after the document is scriptable.
$(document).ready(function() {
    $("ul.tabs").tabs("div.panes > div");
    $(".accordion").tabs(".accordion div.pane", {tabs: 'h2', effect: 'slide', initialIndex: null});
});