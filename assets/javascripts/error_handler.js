// JavaScript Document

function Error_Handler(xhr,error)
{
    $('*.validation').hide();
             
    if(xhr.status==0)
    {
        $("#display_error").append("System is in the offline mode.");
		$('#display_error').addClass("my_close_note"); 
    }
    else
    if(xhr.status==404)
    {
        $("#display_error").append("URL Dection Fails ..!");		
		$('#display_error').addClass("my_close_note"); 
		
    }
    else
    if(xhr.status==500)
    {
        $("#display_error").append("Unkwon Internal Server Error.");
		$('#display_error').addClass("my_close_note"); 
    }
    else
    if(error=='parsererror')
    {
        $("#display_error").append("Unable to Perform JASON Request .\n"+xhr.responseText);
		$('#display_error').addClass("my_close_note"); 
    }
    else 
    if(error=='timeout')
    {
        $("#display_error").append("System Time out.");
		$('#display_error').addClass("my_close_note"); 
    }
    else
    {
        $("#display_error").append('Error.\n'+xhr.responseText);
		$('#display_error').addClass("my_close_note"); 
    }
                    
}		